<?php

// DB.php
// contains the DB object upon which other modules can extend
// uses the mysqli class

// Usage: extended by Web Object

abstract class MVC_DB extends MVC_Object
{
	protected $db; // if this is a PHP / MySQL app, this is a mysqli object
	
	// we'd like to use prepared statements, these might be useful placeholders?
	public $stmt;
	private $result;
	
	// this can be hardcoded or come from a config file.
	// if it comes from a config, do you need some kind of encryption?
	private $db_host;
	private $db_user;
	private $db_pass;
	private $db_name;

	public function __construct()
	{
		parent::__construct();
		
		// get database login info from DB.config.php
		$this->db_host = DB_HOST;
		$this->db_user = DB_USER;
		$this->db_pass = DB_PASS;
		$this->db_name  = DB_NAME;
		
		$this->connect();
	}

	public function __destruct()
	{
		$this->disconnect();
		parent::__destruct();
	}
	
	// what are my specialized behaviors
	
	// connect()
	// connect to the database
	// TODO - switch to return false/ throw Exception (want better error handling)
	protected function connect (){
		try{
		$this->db = @new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
	//	$this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		// note: use procedural style connection error warning
		// http://us2.php.net/manual/en/mysqli.connect-error.php
		// if ($this->db->connect_error or $this->db->connect_errno or $this->db->error or mysqli_connect_error())
		} catch (mysqli_sql_exception $e) { 
			var_dump($e);
			exit;
		
		} 
		if (mysqli_connect_error())
		{
			die("Database connection failed for ".get_class($this));
		}
		// log the connection?
		$event = "DB Connection open: ".get_class($this);
		MVC_Log::write($event);
		return true;
	}
	
	// query()
	public function query($query) {
		$ret = $this->db->query($query);
		return $ret;
	}
	
	// prepare()
	// set the sql prepared statement
	protected function prepare($query) {
		$this->stmt = $this->db->prepare($query);
		if (!$this->stmt)
			throw new Exception("error preparing statement: {$query} -- {$this->db->error}");
		return true;
	}
	
	// bind_param()
	// bind parameters/data to a mysqli prepared statement
	// i think we're gonna have some problems figuring this out...
	protected function bind_param($params, $data)
	{
		// let's set up the bind_param args
		$args = array();
		// $arg[0] = the 'parameter type' string
		// ex: 'issi' should represent 4 following args for type int, type string, type string, and type int
		$args[] = implode('', array_values($params)); 
		// $arg[1] through $arg[...]
		// data to pass in
		foreach ($data as $val)
			$args[] = $val;
		// now you can use call_user_func_array to pass an unspecified number of args to bind_param as an array
		// resulting behavior:
		// $this->stmt->bind_param($args[0], $args[1], ....);
		return call_user_func_array(array($this->stmt,'bind_param'),$args);
	}
	
	// execute()
	// execute the prepared statement
	protected function execute() {
		return $this->stmt->execute();
	}
	
	// result_metadata()
	// result metadata
	// a way to get the fields of a result (useful for bind_result)
	protected function result_metadata() {
		return $this->stmt->result_metadata();
	}
	
	// bind_result()
	// bind results of the prepared statement
	// i think we're gonna have some problems figuring this out...
	protected function bind_result($vars=array()) {
		// presumably, if we have no params, we don't need to do any binding
		if (empty($vars))
			return;

		for ($i = 0; $i < count($vars); $i++) {
			$bind_name = 'bind'.$i;
			$$bind_name = $vars[$i];
			$bind_names[] = &$$bind_name;
		}	
		$return = call_user_func_array(array($this->stmt,'bind_result'),$bind_names);
	}
	
	// fetch()
	// fetch next result row
	public function fetch() {
		return $this->stmt->fetch();
	}
	
	// affected_rows()
	// show the affected rows after a statement is executed
	// affected rows - provided for update, delete, insert
	protected function affected_rows() {
		return $this->stmt->affected_rows;
	}
	
	// num_rows()
	// provide the number of rows pulled up in the query
	// num rows - provided for select
	// relies on mysqli_stmt_store_result, if you intend to use right after an execute
	protected function num_rows() {
		return $this->stmt->num_rows;
	}
	
	// store_result()
	// store result - buffers the entire result set from an executed prepared stmt
	protected function store_result() {
		$this->stmt->store_result();
	}
	
	// close_stmt()
	// close the prepared statement
	protected function close_stmt() {
		if ($this->stmt)
			$this->stmt->close();
	}
	
	// disconnect()
	// disconnect from the DB
	protected function disconnect() {
		if ($this->db) 
		{
			if (!@$this->db->close())
				trigger_error("Failed to close db connection for: ".get_class($this).'<br />');
			else
			{
				$event = "DB Connection close: ".get_class($this);
				MVC_Log::write($event);
			}
		}
	}
}
