<?php

/**
 * TODO 7/21/2011
 * Most of the UEE application should be using DataModel3, not DataModel2
 * Unless we can find a reason otherwise, we should consider removing
 * DataModel2.php
 */

// DataModel.php
// contains the DataModel object upon which other modules can extend

// Usage: base for actual Models

class MVC_DataModel2 extends MVC_DB2
{
	protected $tableName; 
	protected $recordId;
	
	public $fields; // fieldnames - is this necessary now that i have types?
	public $types; // field types (associative array : $fieldname => $type
	public $data; // data, if i have a specific row(s?) from this table

	// search
	public $search; // search parameters, if any
	public $search_results; // any search results (rows of $data arrays)
	public $search_results_ordering; // what fields to order the results on, if any;

	// default values?
	public $defaults = array();
	
	// some property validators - if the field not a string, this helps us do extra validation testing later
	public $is_numeric = array(); 
	public $is_date = array();
	public $is_boolean = array();
	public $varchar_lengths = array();
	
	// the primary key fields
	public $primary_key = array(); // can potentially be a multifield array
	public $foreign_keys = array(); // is this useful? maybe...	
	
	public function __construct($tableName, $id=NULL)
	{
		parent::__construct();
		
		$this->tableName = $tableName;
		
		// let's populate the fields
		// purge the fields
		$this->fields = array();
		$this->types = array();
		$this->data = array();
		
		$this->prepare("DESCRIBE ". $this->tableName);
		$this->execute();
		// temporary solution
		// this is not ideal, beacuse we don't want to directly refer to a statement variable
		$this->stmt->bind_result($field,$type,$null,$key,$default,$extra);
		while ($this->fetch())
		{
			// store the field
			$this->fields[] = $field;
			$this->types[$field] = $type;
			// can i figure out the type?
			switch (strtolower(strtok($type,"("))) {
				case 'tinyint':
					$this->is_boolean[] = $field;
					break;
				case 'int':
				case 'float':
				case 'double':
					$this->is_numeric[] = $field;
					break;
				case 'date':
				case 'datetime':
				case 'timestamp':
					$this->is_date[] = $field;
					break;
				case 'varchar':
					$this->varchar_lengths[$field] = strtok(")");
					break;
				default:
					break;
			}
			// is this field part of the primary key
			if (strtolower($key) == 'pri') // presumes MySQL
			{
				$this->primary_key[] = $field;
			}
			// does this field have a default value?
			$this->defaults[$field] = $default;
		}
		$this->stmt->close();
		if ($id and is_numeric($id))
		{
			$this->recordId = $id;	
			$this->prepare("SELECT * FROM {$this->tablename} WHERE {$this->tablename}_id=?");
			// MVC_Log::write("SELECT * FROM {$this->tablename} WHERE {$this->tablename}_id={$this->recordId}");
			$this->stmt->bind_param("i",$this->recordId);
			$this->execute();
			$meta = $this->result_metadata();
			while ($field = $meta->fetch_field())
				$params[] = &$row[$field->name];
			call_user_func_array(array($this->stmt,'bind_result'),$params);
			while ($this->fetch()) {
				foreach ($row as $key => $val)
					$this->data[$key] = $val;
			}		
		}
	}
	
	public function __destruct()
	{
		parent::__destruct();
	}

	// let's provide some specific behaviors for dataModels
	
	// let's have a cleared data array
	public function clear() {
		foreach ($this->fields as $field)
			$this->data[$field] = NULL;
	}
	
	// how about getting all the records from a table
	public function getAllRows($objName = '', $orders = array()) {
		// TODO
		// utilize $this->dbSearchTable (no search parameters)
		if (empty($orders))
			$orders = implode(", ",$this->primary_key);
		else 
			$orders = implode(", ",$orders);			
		$where = "";
		if ($this->tableName != 'open_id' and $this->tableName != 'test') {
			$where = "WHERE active_flag=1";
		}
		$this->prepare("SELECT * FROM {$this->tableName} {$where} ORDER BY {$orders}");
		// MVC_Log::write("SELECT * FROM {$this->tableName} WHERE active_flag = 1 ORDER BY {$orders}");
		$this->execute();
		$meta = $this->result_metadata();
		while ($field = $meta->fetch_field())
			$params[] = &$row[$field->name];
		call_user_func_array(array($this->stmt,'bind_result'),$params);
		while ($this->fetch()) {
			foreach ($row as $key => $val)
				$c[$key] = $val;
			$result[] = $c;
		}
		
		if ($result)
		{
			return $result;
		}		
		else
		{
			return array();
		}
	}

	// http://www.php.net/manual/en/mysqli-stmt.bind-param.php#100879
	public function exec($sql, $params, $return_result = false) {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$stmt = $mysqli->prepare($sql) or die("Failed to prepare statement");
		call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params));
		$execute_status = $stmt->execute() or die('error executing statement: '. $stmt->error);

		if ($return_result) {
			$meta = $stmt->result_metadata();
			while ($field = $meta->fetch_field() ) {
				$parameters[] = &$row[$field->name];
			}
			call_user_func_array(array($stmt,'bind_result'), $this->refValues($parameters) );
			$results = [];
			while ($stmt->fetch() ) {
				$x = array();
				foreach ($row as $key => $val) {
					$x[$key] = $val;
				}
				$results[] = $x;
			}
			$result = $results;
		}
		else {
			$result = $mysqli->affected_rows;
		}
		$stmt->close();
		$mysqli->close();

		return $result;
	}
	
	// inserting a row, using bindParameters
	public function dbInsertRow() {
		if (!$this->validateData())
			die("There was an error entering your record: invalid data type");
		// let's at least try to generate a query
		$query = "INSERT INTO {$this->tableName} (";
		// let's expand the parameters
		$query .= implode(", ", array_keys($this->types));
		$query .= ") VALUES ( ";
		$log_query = $query;

		$temp_arr = array_fill(0,count($this->data),"?");
		$query .= implode(", ", $temp_arr);
		// $query.= implode(", ",$this->data);
		$log_query.= implode(", ",$this->data);
		
		$query .= ")";
		$log_query .= ")";		
		MVC_Log::write($log_query);		
		// echo $query . '<br />';
		// echo $log_query.'<br />';
		
		// assign the parameters
		$params = array();
		foreach ($this->types as $fieldname=>$type)
		{
			switch (strtok($type,"(")) {
				case 'int':
				case 'tinyint':
					$params[$fieldname] = 'i';
					break;
				case 'float':
				case 'double':
					$params[$fieldname] = 'd';
					break;
				default:
					$params[$fieldname] = 's';
					break;
			}
		}
		$paramStr = implode("",$params);
		array_unshift($this->data,$paramStr);
		$this->exec($query,$this->data);
		// pop the paramStr off the data
		array_shift($this->data);
	}

	// TODO - do we still use this function at all? is it necessary to update?	
	public function deleteRow() {
		$query = "UPDATE {$this->tableName} SET active_flag = 0 WHERE ";
		$first_field = true;
		foreach ($this->primary_key as $pk_field)
		{
			if (!$first_field)
				$query .= " AND ";
			// $query .= "{$pk_field} = {$this->data[$pk_field]}"; 
			$query .= "{$pk_field} = ?"; 
			$first_field = false;
			// set the param type
			switch (strtok($this->types[$pk_field],"(")) {
				case 'int':
				case 'tinyint':
					$params[$pk_field] = 'i';
					break;
				case 'float':
				case 'double':
					$params[$pk_field] = 'd';
					break;
				default:
					$params[$pk_field] = 's';
					break;
			}
			// hold the arg
			$args[] = $this->data[$pk_field];			
		}
		$paramStr = implode("", $params);
		$this->prepare($query);
		$this->bindParameters($this->stmt, $params, $args, $this->primary_key);	
		$this->execute();		
	}
	
	// http://us3.php.net/manual/en/mysqli-stmt.bind-param.php#100879
	private function refValues($arr) {
		if (strnatcmp(phpversion(), '5.3') >= 0) { 
			// reference is required for php 5.3+
			$refs = array();
			foreach ($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			return $refs;
		}
		return $arr;
	}
	
	// updating a row in the database
	public function dbUpdateRow() {
		if (!$this->validateData())
			die("There was an error updating your record: invalid data type");
		$query = "UPDATE {$this->tableName} SET ";
		$log_query = $query;
		$first_field = true;
		$params = array();
		$args = array();
		foreach ($this->data as $field=>$data)
		{
			if (in_array($field,$this->primary_key))
				continue;
			if ($field == 'auth_method')
				continue;
			if (!$first_field)
			{
				$query .= ", ";
				$log_query .= ", ";
			}
			$log_query .= $field ."=".$data;
			$query .= $field ."=?";
			$first_field = false;			
			// set the param type
			switch (strtok($this->types[$field],"(")) {
				case 'int':
				case 'tinyint':
					$params[$field] = 'i';
					break;
				case 'float':
				case 'double':
					$params[$field] = 'd';
					break;
				default:
					$params[$field] = 's';
					break;
			}
			// hold the arg
			$args[] = $data;
		}
		$query .= " WHERE ";
		$log_query .= " WHERE ";
		$first_field = true;
		foreach ($this->primary_key as $pk_field)
		{
			if (!$first_field)
			{
				$query .= " AND ";
				$log_query .= " AND ";
			}
			$log_query .= "{$pk_field} = {$this->data[$pk_field]}"; 
			$query .= "{$pk_field} = ?"; 
			$first_field = false;
			// set the param type
			switch (strtok($this->types[$pk_field],"(")) {
				case 'int':
				case 'tinyint':
					$params[$pk_field] = 'i';
					break;
				case 'float':
				case 'double':
					$params[$pk_field] = 'd';
					break;
				default:
					$params[$pk_field] = 's';
					break;
			}
			// hold the arg
			$args[] = $this->data[$pk_field];			
		}
		$paramStr = implode("", $params);
		array_unshift($args, $paramStr);
		$this->exec($query, $args);
		MVC_Log::write($log_query);
		// MVC_Log::write($query);
	}
	
	public function resetSearch() {
		$this->search = array();
		$this->search_results = array();
	}
	
	// search for records
	public function dbSearchTable() {
		// data holds search parameters
		if (!$this->validateSearch())
			die("There was a problem with your search parameters");
		$query = "SELECT * FROM {$this->tableName} WHERE 1 ";
		$log_query = $query;
		$params = array();
		$args = array();
		if (!empty($this->search)) {
			foreach ($this->search as $fieldname=>$val)
			{
				$query .= " AND ";
				$log_query .= " AND ";
				$query .= "{$fieldname}=?";
				$log_query .= "{$fieldname}={$val}";
				$first_field = false;
				// set the param type
				switch (strtok($this->types[$fieldname],"(")) {
					case 'int':
					case 'tinyint':
						$params[$fieldname] = 'i';
						break;
					case 'float':
					case 'double':
						$params[$fieldname] = 'd';
						break;
					default:
						$params[$fieldname] = 's';
						break;
				}
				// hold the arg
				$args[] = $val;			
			}
		}
		$orders = implode(", ",$this->primary_key);
		MVC_Log::write($log_query);
		$query .= " ORDER BY {$orders}";
		$paramStr = implode("", $params);
		array_unshift($args,$paramStr);
		$this->search_results = $this->exec($query,$args, true);
	}
	
	// how do i get all the existing values for a certain field?
	public function getAllFieldValues($fieldname) {
		if (!in_array($fieldname,array_keys($this->types)) )
			die("you're trying to populate a field that doesn't exist: $fieldname");
		$this->prepare("SELECT DISTINCT {$fieldname} FROM {$this->tablename} ORDER BY {$fieldname}");
		$this->execute();
		$meta = $this->result_metadata();
		while ($field = $meta->fetch_field())
			$params[] = &$row[$field->name];
		call_user_func_array(array($this->stmt,'bind_result'),$params);
		$values = array();
		while ($this->fetch()) {
			foreach ($row as $key => $val)
				$values[] = $val;
		}
		return $values;
	}
	
	// let's provide a behavior to get the parents / children info
	public function getChildren($local_fieldname, $fk_table, $fk_fieldname) {
		$query = "SELECT * FROM ";
		// what tables do i need?
		if ($fk_table == $this->tablename)
			$query .= "{$this->tablename} ";
		else
			$query .= "{$this->tablename}, {$fk_table} ";
		$query .= "WHERE {$this->tablename}.{$local_fieldname}={$fk_table}.{$fk_fieldname} ";
		// what's my local id?
		foreach ($this->primary_key as $fieldname)
		{
			$query .= "AND {$fieldname}={} ";
		}
	}
	
	public function validateData() {
		if (empty($this->data))
			return true; // no data = i guess it's valid!
		foreach ($this->data as $fieldname=>$val)
		{
			if (empty($val))
				continue;
			// invalid boolean type
			if (in_array($fieldname,$this->is_boolean) and $fieldname != 0 and $fieldname != 1)
			{
				echo "invalid data for {$fieldname} <br />";
				return false;
			}
			// invalid numeric type
			if (in_array($fieldname,$this->is_numeric) and !is_numeric($val) and !is_null($val))
			{
				echo "invalid data for {$fieldname} <br />";
				return false;
			}
			// invalid date type
			// date's will typically be encapsulated so remove the quotes
			if (in_array($fieldname,$this->is_date) and $val!= 'NOW()' and !strtotime(str_replace("'","",$val)) )
			{
				echo "invalid data for {$fieldname} <br />";
				return false;
			}
			// string data overflow
			if (in_array($fieldname,array_keys($this->varchar_lengths)) and strlen($val) > $this->varchar_lengths[$fieldname])
			{
				echo "data overflow for {$fieldname} <br />";
				return false;
			}
		}
		return true;
	}
	
	// validate the search parameters
	public function validateSearch() {
		// TODO - validate them search parameters
		return true;
	}
	
}
