<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * DataModel3.php
 *
 * The most recent version of the DataModel object. It should support PHP 5.3
 * and most of the actual data Model objects should extend from this code.
 * There was a signficant pass-by-reference issue in transititioning from
 * PHP 5.2 to 5.3 and DataModel3 addresses those issues.
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

// DataModel.php
// DataModel object

require_once dirname(__FILE__).'/../config.php';
require_once dirname(__FILE__).'/Object.php';
require_once dirname(__FILE__).'/DB2.php';

class MVC_DataModel3 extends MVC_DB2 {

	protected $tablename;

	public $fields; // fieldnames from the db

    /**
     * constructor($tablename : string, $id = null : int)
     *
     * The constructor identifies compares the fields of a matching mysql
     * database table $tablename and stores some type information
     * about the table fields
     */
	public function __construct($tablename, $id=null) {
		parent::__construct();

		$this->tablename = $tablename;

		$this->fields = array();
		// populate the field and type information
		$this->prepare("DESCRIBE {$this->tablename}");
		$this->execute();
		$this->stmt->bind_result($field, $type, $null, $key, $default, $extra);
		while ($this->fetch()) {
			$this->fields[$field] = $type;
		}
		$this->stmt->close();

		// print_r($this->fields);
	}

    /**
     * getAllRows( $orderBy : array( $fieldname+ : string ) , $limit : int )
     * return all the rows from the database table
     * optionally ordered by certain $fieldname(s) and limited to $limit
     * number of records
     * returns: an array. each item in a row from the database as an associative
     *          array keyed by $fieldname.
     */
	public function getAllRows($orderBy = array(), $limit=-1) {
		$query = "SELECT * FROM {$this->tablename}";
		if (!empty($orderBy))
			$query .= " ORDER BY ". implode(", ", $orderBy);
		if ($limit > 0) {
			$query .= " LIMIT {$limit}";
		}
		$this->prepare($query);
		$this->execute();
		$meta = $this->result_metadata();
		$params = array();
		$result = array();
		while ($field = $meta->fetch_field())
			$params[] = &$row[$field->name];
		call_user_func_array(array($this->stmt, 'bind_result'), $params);
		while ($this->fetch()) {
			foreach ($row as $key => $val) {
				$c[$key] = $val;
			}
			$result[] = $c;
		}
		return $result;
	}

    /**
     * refValues($arr : array() )
     * Internal helper function to deal with PHP 5.3 pass by reference issues
     * See http://us3.php.net/manual/en/mysqli-stmt.bind-param.php#100879
     */
	private function refValues($arr) {
		if (strnatcmp(phpversion(), '5.3') >= 0) {
			// reference is required for php 5.3+
			$refs = array();
			foreach ($arr as $key => $value) {
				$refs[$key] = &$arr[$key];
			}
			return $refs;
		}
		return $arr;
	}

    /**
     * exec($sql, $params, $return_result)
     * TODO 7/21/2011 - document this function - essentially mysqli_stmt->exec()
     * TODO 7/21/2011 - why is this function public?
     */
	public function exec($sql, $params, $return_result = false) {

		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$stmt = $mysqli->prepare($sql) or die("Failed to prepare statement: {$sql}");
		call_user_func_array( array($stmt, 'bind_param'), $this->refValues($params) );
		$execute_status = $stmt->execute() or die("error executing statemnt: {$stmt->error}");
		
		if ($return_result) {
			$results = array();
			$meta = $stmt->result_metadata();
			while ($field = $meta->fetch_field() ) {
				$parameters[] = &$row[$field->name];
			}
			call_user_func_array(array($stmt, 'bind_result'), $this->refValues($parameters) );
			while ($stmt->fetch() ) {
				$x = array();
				foreach ($row as $key => $val) {
					$x[$key] = $val;
				}
				$results[] = $x;
			}
			$result = $results;
		}
		else {
			$result = $mysqli->affected_rows;
		}
		$stmt->close();
		$mysqli->close();
		return $result;
	}

    /**
     * dbInsertRow( $data : array( $fieldname => $value ) )
     * Insert a row into the database table $tablename
     * 
     */
	public function dbInsertRow($data) {
		if (!$this->validateData($data))
			die("There was an error entering your record: invalid data type");
		$query = "INSERT INTO {$this->tablename}";
		// what columns do we want to assign values to?
		// is this based on some kind of input data?
		$query .= "(".implode(',',array_keys($data)).")";
		$query .= " VALUES ";
		$query .= "(".implode(',',array_fill(0,count($data),"?")).")";
		// generate the params
		$args = array();
		$param_types = array();
		foreach ($data as $k=>$v) {
			$args[] = $v;
			$type = $this->fields[$k];
			switch ($type) {
				case 'int':
					$param_types[] = 'i';
					break;
				case 'double':
					$param_types[] = 'd';
					break;
				default:
					$param_types[] = 's';
					break;
			}
		}
		$paramStr = implode("",$param_types);
		array_unshift($args,$paramStr);
		$this->exec($query,$args);
	}

    /**
     * dbUpdateRow( $data : array( $fieldname => $value ),
                    $conditions : array( $fieldname => $value) )
     * Update record(s) for the database table $tablename with the data in $data
     * provided that the rows match $conditions
     */
	public function dbUpdateRow($data, $conditions) {
		if (!$this->validateData($data))
			die("Error updating record: invalid data type");
		if (!$this->validateData($conditions)) {
			die("Error updating record: invalid data type");
		}
		// start constructing a query
		$query = "UPDATE {$this->tablename} SET ";
		$update1 = array();
		$param_types = array();
		$args = array();
		foreach ($data as $k=>$v) {
			$update1[] = "{$k}=?";
			$type = $this->fields[$k];
			switch ($type) {
				case 'int':
					$param_types[] = 'i';
					break;
				case 'double':
					$param_types[] = 'd';
					break;
				default:
					$param_types[] = 's';
					break;
			}
			$args[] = $v;
		}
		$query .= implode(", ",$update1);
		$query .= " WHERE ";
		$update2 = array();
		foreach ($conditions as $k=>$v) {
			$update2[] = "{$k}=?";
			$type = $this->fields[$k];
			switch ($type) {
				case 'int':
					$param_types[] = 'i';
					break;
				case 'double':
					$param_types[] = 'd';
					break;
				default:
					$param_types[] = 's';
					break;
			}
			$args[] = $v;
		}
		$query .= implode(", ", $update2);
		$paramStr = implode("",$param_types);
		array_unshift($args,$paramStr);
		$this->exec($query,$args);
	}

    /**
     * dbDeleteRow( $conditions : array( $fieldname => $value) )
     * Remove record(s) from the database table $tablename
     * provided that the rows match $conditions
     */
	public function deleteRow($conditions) {
		if (!$this->validateData($conditions)) {
			die("error removing record: invalid data types");
		}
		// start constructing a query
		$query = "DELETE FROM {$this->tablename} WHERE ";
		$delete1 = array();
		$param_types = array();
		$args = array();
		foreach ($conditions as $k=>$v) {
			$delete1[] = "{$k}=?";
			$type = $this->fields[$k];
			switch ($type) {
				case 'int':
					$param_types[] = 'i';
					break;
				case 'double':
					$param_types[] = 'd';
					break;
				default:
					$param_types[] = 's';
					break;
			}
			$args[] = $v;
		}
		$query .= implode(", ", $delete1);
		$paramStr = implode("",$param_types);
		array_unshift($args,$paramStr);
		$this->exec($query,$args);

	}

	// TODO - we might want to have or conditions
	// TODO - limits, order of results
	public function dbSearchTable($conditions, $order_by = array()) {
		if (!$this->validateSearch($conditions))
			die("error: bad search parameters");
		$query = "SELECT * FROM {$this->tablename} WHERE ";
		$where1 = array();
		$param_types = array();
		$args = array();
		foreach ($conditions as $k=>$v) {
			$where1[] = "{$k}=?";
			$type = $this->fields[$k];
			switch ($type) {
				case 'int':
					$param_types[] = 'i';
					break;
				case 'double':
					$param_types[] = 's';
					break;
				default:
					$param_types[] = 's';
					break;
			}
			$args[] = $v;
		}
		$query .= implode(" AND ",$where1);
		if (!empty($order_by) ) {
			$query .= " ORDER BY " . implode(",",$order_by);
		}
		$paramStr = implode("",$param_types);
		array_unshift($args,$paramStr);
		$result = $this->exec($query,$args,$results = true);
		return $result;
	}

	// TODO - fix validation code
	private function validateData($data) {
		return true;
	}
	
	// TODO - fix validation code
	private function validateSearch($conditions) {
		return true;
	}
	
}

/**
 * 7/18/2011
 * This code below is left over from initial testing
 * Should things break you may decide you want to use it again for reference
 * but otherwise it shouldnt be touched
 */

/*  
// test code

// test constructor
$dm = new MVC_DataModel3('open_id'); // OK
$rows = $dm->getAllRows(); // OK
$rows = $dm->getAllRows(array('uee_user_id'), 5); // OK
$testexec1 = $dm->exec("SELECT uee_user_id, identity, pending, approved, can_edit FROM open_id WHERE uee_user_id=?",array('i',25),true); // OK

$testdata1 = array('identity'=> '1234', "firstname" => "joe", "lastname" => "bruin"); // OK (or insertion error duplicate key)
// $testinsert1 = $dm->dbInsertRow($testdata1); // OK or insertion duplicate key error
$testexec2 = $dm->exec("SELECT * FROM open_id WHERE firstname=?",array('s','joe'),true);

$testcondition1 = array('identity'=>'1234');
$testdata2 = array('firstname' => 'josephine');

// $testupdate1 = $dm->dbUpdateRow($testdata2, $testcondition1); // OK
$testexec3 = $dm->exec("SELECT * FROM open_id WHERE identity=?", array('s','1234'),true);

// $testdelete1 = $dm->deleteRow($testcondition1);
$testexec4 = $dm->exec("SELECT * FROM open_id WHERE identity=?", array('s','1234'),true);

$res = $dm->dbSearchTable($testcondition1);
print_r($res);


// end test code
*/
