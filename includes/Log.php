<?php

// Log.php
// contains the Log object to record DB queries and other events
// uses the mysqli class

// Usage: extended by Web Object

class MVC_Log extends MVC_Object
{
	public $event;
	public $timestamp;

	public function __construct($event = '') {
		parent::__construct();
		$this->event = $event;
		$this->timestamp = date("M d, Y  H:i:s");
		$this->write_log();
	}

	public function __destruct() {
		parent::__destruct();
	}

	public static function write($event) {
		$log_event = new MVC_Log($event);
	}

	private function write_log() {
		$lines = array();
		$lines[] = $this->timestamp;
		$lines[] = $_SERVER['REQUEST_URI'];
		$lines[] = $_SERVER['HTTP_REFERER'];
		$lines[] = $_SERVER['REMOTE_ADDR'];
		$lines[] = $this->event;
		$lines[] = '';
		$filepath = $_SERVER['DOCUMENT_ROOT']."/logs/log_".date("Y-m-d").".txt";
		$fileh = fopen($filepath,"a");
		foreach ($lines as $line)
			fwrite($fileh,$line."\n");
		fclose($fileh);
	}
}
