<?php

// Session.php
// contains a Session object representing the contents of the HTTP session

// Usage: can be referenced by other objects

// TODO - this should be a singleton object
// reference: http://en.wikipedia.org/wiki/Singleton_pattern
// there should only be one instance of the Session object for any page request
class MVC_Session
{
	private static $instance;

	private $data;

	// construct()
	// constructor function
	// we actually don't want this called externally, or we can't control the singleness
	private function __construct()
	{
		//session_start();

		$this->data = array();

		if ($_SESSION)
		{
			foreach ($_SESSION as $key => $val)
			{
				$this->data[$key] = $val;
			}
		}
	}

	// let's deny clone and wake up methods - want to avoid duplcate objects
	public function __clone() {
		throw new Exception("You cannot clone the Session object");
	}

	public function __wakeup() {
		throw new Exception("You cannot wakeup the Session object");
	}

	// this method must be status and must return an instance of the object if the object does not already exist
	public static function getInstance() {
		if (!self::$instance instanceof self) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function __get($name)
	{
		return $this->data[$name];
	}

	public function __set($name,$value)
	{
		$this->data[$name] = $value;
	}

	// generic behavior functions for any object
	// see php documentation for built-ins
	// http://www.php.net/manual/en/language.oop5.php
	// http://www.php.net/manual/en/ref.classobj.php

	// destruct()
	// destructor function
	// doesn't need to call a parent destructor (why? no parent)
	public function __destruct()
	{
		foreach ($this->data as $key => $val)
		{
			$_SESSION[$key] = $val;
		};
	}

}
