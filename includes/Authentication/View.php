<?php

// LoggedIn.php
// contains a generic authentication object upon which other modules can extend
// if users need basic login authentication of some kind to access a module, Modules can extend
//     this authentication

// Usage: extended by modules which do need login authentication

abstract class MVC_Authentication_View extends MVC_Authentication_LoggedIn
{
	private $logged_in;

	// construct()
	// constructor function
	public function __construct() {
		parent::__construct();
		$this->logged_in = false;
	}
	
	
	// specific Authentication behavior functions
	
	// authenticate()
	// in order to have the view permission
	// users must have a user_id and have been approved for access
	public function authenticate()
	{
		$sess = MVC_Session::getInstance();
		$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$stmt = $db->prepare("SELECT keyphrase, pending, approved, can_edit, manage_users, admin, completed_profile FROM open_id WHERE uee_user_id=?") or die("Error preparing statement");
		$stmt->bind_param("i",$sess->user_id);
		$stmt->execute() or die("Error executing select statement");
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		if ($num_rows > 0)
		{		
			; // echo "OpenID record already exists <br />";
			$stmt->bind_result($keyphrase, $pending, $approved, $can_edit, $manage_users, $admin, $completed_profile);
			while ($stmt->fetch()) {
				; // echo "found attached id {$uee_id} <br />";
			}
			$stmt->close();
		}
		else 
		{
			$stmt->close();
			return false;
		}
	
		$salted_str = UEE_SALT.$keyphrase.$uee_id.UEE_SALT;
		$hash_str = md5($salted_str);
		$matching_keyphrase = false;
		if ($hash_str == $sess->uee_session_id);
			$matching_keyphrase = true;
		// set some permission appropriate
		// this is really an authorization piece - we should try to separate authentication and authorization
		if ($pending == 1)
			$this->setPermission("pending");
		if ($approved == 1)
			$this->setPermission("view");
		if ($pending == 0 and $approved == 0)
			$this->setPermission("not approved");
		if ($approved == 1 and $can_edit == 1)
			$this->setPermission("edit");
		if ($approved == 1 and $manage_users == 1)
			$this->setPermission("manage users");
		if ($admin == 1)
			$this->setPermission("admin");
		if ($completed_profile)
			$this->setPermission("completed profile");
		if($matching_keyphrase and $this->hasPermission("view") )
			return true;
		else
			return false;
	}
	
	public function login() {
		// determine if I'm logged in
		$sess = MVC_Session::getInstance();
		if( isSet($sess->user_id) )
			$logged_in = true;
		else
			$logged_in = false;
	}

	// destruct()
	// destructor function
	public function __destruct()
	{
		parent::__destruct();
	}
	
}
