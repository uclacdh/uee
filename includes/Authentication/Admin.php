<?php

// Admin.php
// contains an authentication object upon which other modules can extend
// if users need admin level authentication to access a module, Modules can extend
//     this authentication

// Usage: extended by modules which do need admin level authentication (ie, certain DB editing modules)

class MVC_Authentication_Admin extends MVC_Authentication
{

	// construct()
	// constructor function
	public function __construct()
	{
		parent::__construct();
	}
	
	// specific Authentication behavior functions
	
	// authenticate()
	// the logic which is used to determine if i have proper authentication
	// for now, no one has admin authentication
	// TODO - proper admin authentication logic
	public function authenticate()
	{
		$sess = MVC_Session::getInstance();
		if ($sess->user_id === 1)
			return true;
		return false;
	}

	// destruct()
	// destructor function
	// doesn't need to call a parent destructor (why? no parent)
	public function __destruct()
	{
		parent::__destruct();
	}
	
}
