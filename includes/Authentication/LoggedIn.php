<?php

// LoggedIn.php
// contains a generic authentication object upon which other modules can extend
// if users need basic login authentication of some kind to access a module, Modules can extend
//     this authentication

// Usage: extended by modules which do need login authentication

abstract class MVC_Authentication_LoggedIn extends MVC_Authentication
{
	private $logged_in;

	// construct()
	// constructor function
	public function __construct() {
		parent::__construct();
		$this->logged_in = false;
	}
	
	
	// specific Authentication behavior functions
	
	// authenticate()
	// the logic which is used to determine if i have proper authentication
	public function authenticate()
	{
		$sess = MVC_Session::getInstance();
		//echo 'inside auth user_id: '. $sess->user_id . '<br/>';
		if( is_numeric($sess->user_id) and $sess->user_id > 0)
			return true;
		else
			return false;

		//return $this->logged_in; // if no authentication required => we are always authenticated!
	}
	
	public function login() {
		// determine if I'm logged in
		$sess = MVC_Session::getInstance();
		if( isSet($sess->user_id) )
			$logged_in = true;
		else
			$logged_in = false;
	}

	// destruct()
	// destructor function
	public function __destruct()
	{
		parent::__destruct();
	}
	
}
