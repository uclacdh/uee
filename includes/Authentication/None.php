<?php

// None.php
// contains a generic no authentication object upon which other modules can extend
// if users do not need authentication of some kind to access a module, Modules can extend
//     this authentication

// Usage: extended by modules which do not need authentication

abstract class MVC_Authentication_None extends MVC_Authentication
{
	// construct()
	// constructor function
	public function __construct() {
		parent::__construct();
	}
	
	// specific Authentication behavior functions
	
	// authenticate()
	// the logic which is used to determine if i have proper authentication
	public function authenticate()
	{
		return true; // if no authentication required => we are always authenticated!
	}

	// destruct()
	// destructor function
	public function __destruct()
	{
		parent::__destruct();
	}
	
}
