<?php

// Web.php
// contains the Web object upon which other modules can extend
// extends from DB object
// (why? because we want our webapp to be able to talk to a database)

// Usage: extended by Module Object

abstract class MVC_Web extends MVC_DB
{
	// all web objects should be able to reference the session
	// TODO - does this need to be public?
	protected $session;
	
	public function __construct()
	{
		parent::__construct();
		$this->session = MVC_Session::getInstance();
	}
	
	public function __destruct()
	{
		parent::__destruct();
	}
}
