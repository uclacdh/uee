<?php

// Module.php
// contains the Module object upon which other modules can extend
// extends from Web object
// (why? because we want our webapp to be able to talk to a database and have Session info)

// Usage: extended by specific Modules
// Note: before you get to specific modules, you need to include authentication somewhere in chain
// example: see includes/object/module/public

abstract class MVC_Module extends MVC_Web
{
	// all modules have a templateFile to control display
	public $templateFile;
	// what module was called?
	public $moduleName;
	// what class was called?
	public $className;
	// what event was called? (so we don't keep pointing to $_GET)
	public $eventName;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function __destruct()
	{
		parent::__destruct();
	}
	
	// event listeners
	// provide a default that does nothing (let specific modules override with their default behaviors
	public function defaultEvent() {
		return;
	}

	// specific behaviors
	// all modules need to be able to display based on their corresponding templateFile
	public function display() {
		if (file_exists($this->templateFile))
			include($this->templateFile);
		else
			die("You tried to use a template that does not exist: ". $this->templateFile);
	}
	

}
