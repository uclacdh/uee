<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Authentication.php
 *
 * contains a generic authentication object upon which other modules can extend
 * if users need authentication of some kind to access a module, Modules can i
 * extend the appropriate authentication subclass
 * think of the Authentication subclasses as the 'role' level of the user on the website
 * some examples:
 * - general public (no authentication)
 * - site administrator (very specific admin authentication)
 * - general user (some authentication)
 * - power users (more specific authentication)

 * Usage: parent authentication object of the MVC application
 *             extended by specific authentication objects
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

abstract class MVC_Authentication extends MVC_Module
{

    /**
     * We keep a private array of permissions that each level of
     * authentication is privileged to have 
     */
	private $permissions;

    /**
     * Constructor
     * No specialized behaviors other than initializing an empty set of
     * permissions
     */
	public function __construct()
	{
		parent::__construct();
		$this->permissions = array();		
	}
	
    /**
     * authenticate()
     * Determine if the user has appropriate authentication for this level
     * more general than checking for a certain permission
     * actual logic to be filled in by specific child classes
     */
	public abstract function authenticate();
	
    /**
     * hasPermission($p : string)
     * Determine if, at this authentication level, the user has a permission $p.
     * Return true if the user has the permission, false otherwise
     */
	public function hasPermission($p) {
		return in_array($p,$this->permissions);
	}
	
    /**
     * setPermission($p : string)
     * Add a permission $p to the set of permissions this level of
     * authentication has.
     */
	protected function setPermission($p) {
		$this->permissions[] = $p;
	}


    /**
     * displayPermissions()
     * display the set of permissions available at this authentication level
     * TODO 7/21/2011 - Why is this a public function? Does it need to be?
     */
	public function displayPermissions() {
		print_r($this->permissions);
		echo "<br />";
	}

    /**
     * destructor function
     * No special behaviors, just call the parent destructor
     */
	public function __destruct()
	{
		parent::__destruct();
	}
	
}
