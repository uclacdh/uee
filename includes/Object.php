<?php

// Object.php
// contains a generic base object upon which other modules can extend
// let's provide some common behavior functions
// get, set, constructor, destructor, etc...

// Usage: parent Object of the MVC application

abstract class MVC_Object 
{

	// construct()
	// constructor function
	// doesn't need to call a parent constructor (why? no parent)
	public function __construct()
	{
		;
	}
	
	// generic behavior functions for any object
	// see php documentation for built-ins
	// http://www.php.net/manual/en/language.oop5.php
	// http://www.php.net/manual/en/ref.classobj.php

	// destruct()
	// destructor function
	// doesn't need to call a parent destructor (why? no parent)
	public function __destruct()
	{
		;
	}
	
}
