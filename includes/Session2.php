<?php

// Session.php
// contains a Session object representing the contents of the HTTP session

// Usage: can be referenced by other objects
class MVC_Session2
{

	private $session;
	// construct()
	// constructor function
	// we actually don't want this called externally, or we can't control the singleness
	public function __construct()
	{
		$this->session = $_SESSION;
	}
	
	// let's deny clone and wake up methods - want to avoid duplcate objects
	public function __clone() {
		throw new Exception("You cannot clone the Session object");
	}
	
	public function __wakeup() {
		throw new Exception("You cannot wakeup the Session object");
	}
	
	public function commit() {
		session_write_close();
	}
	
	public function clear() {
		foreach ($this->session as $key => $val)
			$this->session[$key] = null;
		session_unset();
	}
	
	public function __get($name)
	{
		// echo "getting $name: {$this->session[$name]}; <br />";
		return $this->session[$name];		
	}
	
	public function __set($name,$value)
	{
		// echo "setting $name to $value <br />";
		$this->session[$name] = $value;
		// print_r($this->session);
		// echo '<br />';
	}
	
	// destruct()
	// doesn't need to call a parent destructor (why? no parent)
	public function __destruct()
	{
	
	}
	
}
