<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

// config.php
	
// let's put some application specific configuration here
// i'm going to define some constants that are application wide
	
// DB connection info
define('DB_HOST', 'ENTER-VALUE');
define('DB_USER', 'ENTER-VALUE');
define('DB_PASS', 'ENTER-VALUE');
define('DB_NAME', 'ENTER-VALUE');

// hashing constant
define('UEE_SALT', 'ENTER-VALUE');
	
// library soap connection WSDL file
if ($_SERVER["SERVER_NAME"] == "shadowuee.idre.ucla.edu" )
    define('WSDL',"http://digidev.library.ucla.edu/axis2/services/UEEServiceProvider?wsdl"); 
else
    define('WSDL',"http://dlws.library.ucla.edu/axis2/services/UEEServiceProvider?wsdl"); 
// this is a testing service
// define('WSDL',"http://164.67.152.30/axis2/services/UEEServiceProvider?wsdl"); 

// encryption info
// must be less than/equal to 16 chars
define('AES_KEY','ENTER-VALUE');

// Use Google Maps API Key -- not required for using Google maps in DEV
define('USE_GMAP_API_KEY', false);
// Google Maps API Key
define('GMAP_API_KEY','');

// Use Google Analytics
define('GOOGLE_ANALYTICS', false);
// Google Analytics ID
define('GA_ACCOUNT_ID','ENTER-VALUE');

// Google OPENID CLIENT_ID and CLIENT_SECRET
define('GOOGLE_OPENID_CLIENT_ID','722328557173-4ivr3dq8bfidedplho5utoj16dupuial.apps.googleusercontent.com');
define('GOOGLE_OPENID_CLIENT_SECRET','e35Vuw_lt41vlmQHPXIs51ol');
	
// development flag
// are we making changes to the site?
define('WORK_IN_PROGRESS', true);
define('WORK_NOTES', 'Note: This site is currently being worked on. Functionality may change or break temporarily. <br />');
	
// site path
// DO NOT include the trailing slash

// TODO - may have to change to handle https
define('SITE_PATH',($_SERVER['HTTPS']?'https://':'http://').$_SERVER['SERVER_NAME']);

// server sites
define('PRODUCTION','https://uee.ats.ucla.edu');
define('SHADOW','https://shadowuee.idre.ucla.edu');
define('STAGE','https://ueedev.ats.ucla.edu');

// scaffolding configuration info (prefix with SCAFFOLD_ )
// things the scaffolding script reads when doing it's work
define('SCAFFOLD_DATAMODEL_OVERWRITE',true); // overwrite existing DataModels objects
define('SCAFFOLD_MODULE_OVERWRITE',true); // overwrite existing modules
	
// NOTE: constants cannot be array type
// when overwriting DataModels, which should we ignore (provide DB tablename)
$SCAFFOLD_DATAMODEL_IGNORE = array("category", "gloss", "gloss_name", "time_period", "time_period_name", 'feature_type'); 
// when overwriting DataModels, which should we ignore (entries as array("tablename"=>'module')
$SCAFFOLD_MODULE_IGNORE =  array( 
	array("category"=>"input"),
	array("category"=>"search"),
	array("gloss"=>"input"),
	array("gloss"=>"search"),
	array("gloss_name"=>"input"),
	array("gloss_name"=>"search"),
	array("time_period"=>"input"),
	array("time_period"=>"search"),
	array("time_period_name"=>"input"),
	array("time_period_name"=>"search"),
	array("place"=>"input")
	);
	
?>
