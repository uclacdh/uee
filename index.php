<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 * 
 * PHP 5.3
 * 
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

/**
 * This is a "Docblock Comment," also known as a "docblock." The class'
 * docblock, below, contains a complete description of how to write these.
 */

// TODO 6/27/11
// Apply the Pear PHP coding standards
// http://pear.php.net/manual/

// load any global config variables
require_once('config.php');

// openid is an external library used to assist the openID authentication
// workflow
require_once('openid.php');

// helper objects to make use of the SOAP method calls. 
require_once(dirname(__FILE__).'/modules/soap/libraryObjects.php');

// temporary load time resolution
set_time_limit(180);

// temporary
 error_reporting(E_ALL ^ E_NOTICE);
// var_dump($_SERVER);

/**
 * Check if we can connect to the SOAP service at the UCLA Library
 * Invoke a simple echo call and see if we get a response
 * An error should invoke a catchable exception
 * Possible errors - no return set (likely the service is down)
 *                 - empty return set (service is up but broken)
 */
 
try {
    $tempSoap = @new SoapClient(WSDL, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
    $repeatRes = $tempSoap->repeat(new repeat('12345'));
    if (!property_exists($repeatRes,'return') ) {
        throw new Exception("No return set");
    }
    if (empty($repeatRes->return)) // empty return set
        throw new Exception("Empty return set");
    ; // reaching here == no obvious errors
} catch (Exception $e) {
    // TODO 6/27/11 - possibly log the error
    echo $e->getMessage() .'<br />';
    die();
}

/**
 * The PHP __autoload allows us to implement the MVC framework easily
 * Relies on 'good' file naming and structure conventions
 * All class files in this application go in {webroot}/includes/
 * Exceptions to autload
 * - external libraries (ie php extensions)
 *   - DOMDocument
 *   - SoapClient
 */
function __autoload($className) {
    if ($className == 'DOMDocument' or $className == 'SoapClient')
        return false;
    /**
     * Our class files will start with 'MVC_' in the class name
     * Truncate the 'MVC_' from the object name
     * Then determine where to find the matching file
     */
    $objectName = substr($className,4);
    $objectName = str_replace("_","/",$objectName);
    // where's the object file located?
    $objectDir = substr((__FILE__),0,-10)."/includes/";
    $objectPath = $objectDir.$objectName.'.php';
    if (!file_exists($objectPath))
        die("You tried to load a file that doesn't exist: $objectPath");
    require_once($objectPath);
}

session_start();	//start session variable

/**
 * Load the appropriate module
 * In the MVC framework, the module combines models and views into a
 * working chunk of application for the user.
 * Originally we had one model associated with one module,
 * but for the public frontend this is not so true
 * Example - the Category module has 'view', and 'input' views of the
             Category data model
 */
// load a module (essentially, the model view, but not entirely the same)
if (!empty($_GET['module'])) {
    $moduleName = strtolower($_GET['module']);
}

/** 
 * Validate the $moduleName variable 
 * If this fails, die or redirect to an appropriate error page
 */
if (empty($moduleName))
{
    // redirect to welcome page?
    header('Location: '.SITE_PATH.'/welcome/');
    exit();
}

/**
 * Resolve the class object - closest to the 'Model' part of MVC
 * If no className is provided, we assume a 'default' class
 * usually, the same name as the moduleName
 */
 
$className = strtolower($_GET['class']);
if (empty($className) and $moduleName != 'home') {
    $className = 'input';
}
if ($moduleName == 'home' or $moduleName == 'login' or 
    $moduleName == 'notfound' or $moduleName == 'pending' or 
    $moduleName == 'permission' or $moduleName == 'users' or
    $moduleName == 'about'
    ) {
    $className = $moduleName;
}
if ($moduleName == 'front' or 
    ($moduleName == 'login' and !empty($_GET['class']) ) ) {
    $className = strtolower($_GET['class']);
}
$objName = $className;

/**
 * load the module
 * index.php is 9 chars long, so cut off the last 9 chars
 */
$classDir = substr((__FILE__),0,-9)."/modules/$moduleName/";
$classPath = $classDir.$className.".php";

/**
 * If we can't resolve the class
 * redirect to a notfound page
 */
if (!file_exists($classPath))
{
    header('Location: '.SITE_PATH.'/notfound');
    exit();
}

/**
 * Load the class file, then load an instance of the class
 */
include($classPath); 
$instance = new $className;
	
/**
 * Check the authentication based on the instance
 * If not authenicated, redirect to a login page
 */
if (!$instance->authenticate())
{
	// if we're not approved, send to the permissions error page
    if ($instance->hasPermission("not approved")) {
        header('Location: '.SITE_PATH.'/?module=permission');
        exit();
    }
    // if we're pending, but not approved, send to the pending page
    if ($instance->hasPermission("pending")) {
        header('Location: '.SITE_PATH.'/pending');
        exit();
    }
    $_SESSION['target'] = $_SERVER["REQUEST_URI"];
    session_write_close(); // make sure to write the session!
    header('Location: '.SITE_PATH.'/login/openid');
    exit();
}

$instance->moduleName = $moduleName;
$instance->className = $className;
	
// did we specify a particular event to be triggered for this view?
/**
 * Events - some modules will have associated 'events' associated
 * The events are basically method calls which can be invoked by the user
 * through assorted interactions - they should be invoked after the class is
 * loaded but before the page is rendered
 */
if ($_GET['event'])
{
    // TODO - validate the event
    $event = $_GET['event'];
}
else
    $event = 'defaultEvent';

if ($event)
{
    // todo: validate the event exists
    if (!method_exists($instance,$event))
    {
        header('Location: '.SITE_PATH.'/notfound');
        exit();
    }
    $instance->eventName = $event;
    $instance->$event();
}

/**
 * Load the display view
 * We'll need to resolve the display template that is supposed to be used
 * Each module can have multiple display templates, or a single display
 * template with multiple events.
 */

// we need our actual presentation template now
$templateFile = $classDir."templates/".str_replace("_","",$className).".tpl";
if (!file_exists($templateFile))
    die("Could not load specified template file: $templateFile");
$instance->templateFile = $templateFile;

/**
 * Display the page
 * Some pages will not require the general header/footer
 * - sitereport
 */

// global header file 
if ($className != 'sitereport' )
    require_once('header.php');

// main display	
$instance->display();

// global footer file
if ($className != 'sitereport' )	
    require_once('footer.php');

?>

