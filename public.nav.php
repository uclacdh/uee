<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * public.nav.php 
 *
 * A block of HTML that represents the navigation menu for
 * the public interface to the UEE application. Conditionally
 * included by index.php
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

/**
 * This is a "Docblock Comment," also known as a "docblock." The class'
 * docblock, below, contains a complete description of how to write these.
 */

// TODO 6/27/11
// Apply the Pear PHP coding standards
// http://pear.php.net/manual/

?>
<div id='topnav'>
    <div id='topnav-wrapper'>
        <form id='topnav-search' action='/search/' method='post' >
            <p>
                <label for='topnav-search-input' >Search UEE</label>
                <input type="text" id='topnav-search-input' name='search' value='Search UEE' />
                <input type="image" id='topnav-search-submit' src='/graphics/Go.png' alt='Go' />
            </p>
        </form>
        <ul id='topnav-list' >
            <li style='width:12%;'><a href="/welcome/">Home</a></li>
            <li style='width:12%;' ><a href="/myuee/">my Uee</a></li>
            <li style='width:14%;'><a href="/titleindex/">title index</a></li>
            <!--
            <li style='width:20%;'><a href="/search/advanced/">advanced search</a></li>
            -->
            <li style='width:18%;'><a href="/subjectbrowse/">subject browse</a></li>
            <li style='width:14%;'><a href="/timemap/">time map</a></li>
            <?php if ($_SESSION['user_id']): ?>
            <!--
	            <li style='width:10%;'><a href="/logout/">logout </a></li>
	    -->
            <?php else: ?>    
	            <li style='width:10%;'><a href="/login/openid">login </a></li>
	    <?php endif; ?>            		
        </ul>
        <div class='clearfix'></div>
    </div>
</div>


