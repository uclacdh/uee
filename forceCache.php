<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/** checkSoap.php
 * a script, intended to be run by a cron job or some other service that
 *  notifies various people if the soap service is unavailable
 * usage: scheduled task/cron job; can be pulled up by a webbrowser or wget
	
 * Andrew Tai
 * Sept 10, 2009
 * adtai@ats.ucla.edu

 * Known issues:
 * TODO 7/18/2011 - This page shares similar code to checkCache, checkSoap
 * Should push the shared code to a library file
 */
	
// add some restrictions on IP or some other lockdown
	
// load any global config variables
require_once('config.php');
require_once(dirname(__FILE__).'/modules/soap/libraryObjects.php');	
	
if (!empty($_REQUEST['specificArticle']))
    $specificArticle = $_REQUEST['specificArticle']; // assumes underscore notation
else
    $specificArticle = null;
	
// are we gonna be able to get to the soap service?
try {
    // throw new Exception("testing an automated email; please ignore");
    $tempSoap = @new SoapClient(WSDL, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
    $articlesRes = $tempSoap->listAllArticles();
    if (!property_exists($articlesRes,'return')) // no return set
        throw new Exception("No return set; it's likely the soap service is down"."\r\n");
    if (count($articlesRes->return)<= 0) // empty return set)
        throw new Exception("Empty return set; is the soap service returning data?"."\r\n");
    // here, we know the article exists and there should be a copy on DLCS we could download.
    foreach ($articlesRes->return as $article) {
        // do we have a specificArticle?
        $article_filename = str_replace(array(" ","/"),array("_","-"),$article->title).'.xml';
        if ($specificArticle and $article_filename != $article->title)
            continue;
        // print_r($article);
        // $article_filename = str_replace(" ","_",$article->title).'.xml';
        $article_path = $_SERVER['DOCUMENT_ROOT'] .'/cache/articles/'.$article_filename;
        // echo '<br />';
        echo $article_filename. '<br />';
        if (file_exists($article_path)) {
            echo 'cached: <br />';
            echo date("F d Y H:i:s", filemtime($article_path)) .'<br />';
            // echo filemtime($article_path) .'<br />';
            if ($article->xmlCreateDate) {
                echo 'created on DLCS: <br />';
                echo $article->xmlCreateDate .'<br />';
                // echo strtotime($article->createDate) .'<br />';
                if (true) {
                    echo "<strong>Updating...</strong><br />";
                    // todo: update!!!
                    if (copy($article->xmlURL, $article_path) )
                    {
                        echo "<strong><em>Updated</em></strong><br />";
                    }
                    else
                        throw new Exception("error updating article ".$article->title. " from DLCS");
                }
                else 
                    echo "<em>does not need update</em> <br />";
            }
        }
        else {
            echo "<strong>Does not exist in cache; caching...</strong><br />";
            // todo: update!!!
            if (copy($article->xmlURL, $article_path) )
            {
                echo "<strong><em>Created</em></strong><br />";
            }			
            else
                throw new Exception("error caching article ".$article->title. " from DLCS");
            }
            echo '<br />';
        }
        return true; // success
    } catch (Exception $e) {
    // email the appropriate peoples
    ini_set("SMTP","em2.ad.ucla.edu"); // set the outgoing mail server
						
    $to = 'uee@humnet.ucla.edu'; 
    $subject = '[ueeweb] Soap Service seems offline';
    $message = 'Hi Henry,'."\r\n".
        'It appears the soap services is down. Could you please check for us? Thanks!'."\r\n".
        'Error output: '.$e->getMessage()." \r\n".
        'ATS Staff'."\r\n".
        '[Automatically generated via PHP script]'."\r\n";
    $headers = 'From: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'Cc: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'Reply-To: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();
    // send_success = mail($to,$subject,$message, $headers);
    return $send_success;
}

?>

