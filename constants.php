<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * constants.php
 * various global constants used by the UEE application

 * Andrew Tai (adtai@ats.ucla.edu)
 * UCLA Academic Technology Services
 * 7/18/2011
	
 * this might be more useful to have a corresponding javascript set of
 *  constants for some of the jquery toggles?
 * some constants for the Place data entry forms

 */
	
// place types
define('PLACE_TYPE_NOME', 1);
define('PLACE_TYPE_REGION', 2);
define('PLACE_TYPE_GOVERNATE', 3);
define('PLACE_TYPE_SITE', 4);
define('PLACE_TYPE_FEATURE', 5);
	
// place name types
define('PLACE_NAME_TYPE_MAIN',1);
define('PLACE_NAME_TYPE_CLASSICAL',2);
define('PLACE_NAME_TYPE_MODERN',3);
define('PLACE_NAME_TYPE_EGYPTIAN',4);
define('PLACE_NAME_TYPE_COPTIC',5);
define('PLACE_NAME_TYPE_OTHER',6);
	
// geodata types
define('GEODATA_POINT',1);
define('GEODATA_LINE',2);
define('GEODATA_POLYGON',3);
		
?>
