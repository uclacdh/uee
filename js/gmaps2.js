$(document).ready(function() {
	 mapInit()
});


// archived on Nov 24, 2008
// switching text inputs to a textarea box

// google maps behaviors (for data entry: place records)
// holders for the marker data
var gmarker = null; // this is for point_mode;
var gpolyline = new Array(); // this is for line_mode;
var gpolygon = new Array(); // this is for polygon_mode;

// poly options - something needed for drawing polygons
var polyOptions = {geodesic:true};		

// holders for the 'mode' of the marker placement
// var geomap_mode = "point_mode";
var geomap_mode = "";
var coord_mode = 'latlong_mode';
var map;

var addMarkerToggle = null; // just want a global so i can remove this behavior...

// google maps v3
function mapInit()
{
	// location: gmaps2.js - 05/22/2013
	var zoom_level = 6;
	var centerSpot = new google.maps.LatLng(29.8,30.5); // this is pretty close to where Faiyum is...
	
	var myOptions = {
		zoom: zoom_level,
		center: centerSpot,
		
	// pan control
	panControl: true,
		
		// street  view
		streetViewControl: false,
		
		// scale control
		scaleControl: false,
		
		// zoom controls
		zoomControl: true,
			zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
    		},
		
		// map type control	
		mapTypeControl: true,
			mapTypeControl: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
			},
		
		mapTypeId: google.maps.MapTypeId.TERRAIN
	};

	// making the map
	// map = new google.maps.Map(document.getElementById("mini-map"), myOptions);
	
	// admin side? can we have two of these?
	map = new google.maps.Map(document.getElementById("map"), myOptions);
	
	// by default, no mode is selected so there are no drawable items yet
	// console.log('geodata...');
	// console.log(geodata);
	if (document.getElementById('geodata').value && document.getElementById('geodata').value.length > 0)
	{
		// umm, let's set the 'geomap mode icon'
		document.getElementById(geomap_mode).className = 'selected';
		//05-24
		//need to fix funct for v3 - mapUpdateFromText();
		mapUpdateFromText();
	} 
	else if (document.getElementById('geodata').innerHTML && document.getElementById('geodata').innerHTML.length > 0) {
		//05-24
		//need to fix funct for v3 - mapUpdateViewonly();
		mapUpdateViewonly();
	}
	// done!
}
// end of mapInit()

// v3
// clears the map of any overlays that may exist
function mapRemoveMarkers() {
	// remove the map overlay
	if (gmarker != null)
	{
		// clear the map of any existing markers
		if (map) {
	    	// Clearing gmarker...
        	while(gmarker[0]) {
				//console.log(markersArray.length);
            	gmarker.pop().setMap(null);
        	}
        
       		gmarker.length = 0;
	   		gmarker = [];
    	}
	}
	
	if (gpolyline != null)
	{
		if (map) {
	    	// Clearing overlays...
        	while(gpolyline[0]) {
				//console.log(markersArray.length);
            	gpolyline.pop().setMap(null);
        	}
        
       		gpolyline.length = 0;
	   		gpolyline = [];
    	}
	}
	
	if (gpolygon != null)
	{
		if (map) {
	    	// Clearing overlays...
        	while(gpolygon[0]) {
				//console.log(markersArray.length);
            	gpolygon.pop().setMap(null);
        	}
        
       		gpolygon.length = 0;
	   		gpolygon = [];
    	}
	}
		
	if (addMarkerToggle != null)
	{
		if (map) {
	    	// Clearing overlays...
        	while(addMarkerToggle[0]) {
				//console.log(markersArray.length);
            	addMarkerToggle.pop().setMap(null);
        	}
        
       		addMarkerToggle.length = 0;
	   		addMarkerToggle = [];
    	}
	}

	return true;
}

// v3 and v2 okay
// reset the text geodata
function mapResetText() {
	document.getElementById('geodata').value = '';
	// since there's no geodata, we should reset the map overlays
	mapUpdateFromText();
	return true;
}

// ====================================================
// v2
// mapUpdateFromText()
// given the textbox of coordinates
// add the 'appropriate' map overlay
function mapUpdateFromText() {
	// start by removing the overlays
	mapRemoveMarkers();
	
	if (addMarkerToggle != null)
	{
		google.maps.event.removeListener(addMarkerToggle);
		addMarkerToggle = null;
	}
	
	var geodata = document.getElementById('geodata').value;
	if (geodata == null || geodata.length <= 0)
	{
		// so, set up something the user can start drawing with
		if (geomap_mode == 'point_mode')
		{
			// restore the map marker onclick
			addMarkerToggle = GEvent.addListener(map,"click", function(overlay, latlong) {
				// reset the marker?
				if (gmarker)
					map.removeOverlay(gmarker);
				gmarker = new GMarker(latlong, {draggable: true});
				vertices = new Array();
				vertices.push(gmarker.getLatLng());
				mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
				// some draggable behaviors
				GEvent.addListener(gmarker, "drag", function() { 
					vertices = new Array();
					vertices.push(gmarker.getLatLng());
					mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
				});
				GEvent.addListener(gmarker, "dragend", function() { 
					vertices = new Array();
					vertices.push(gmarker.getLatLng());
					mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
				});				
				map.addOverlay(gmarker);
				map.panTo(gmarker.getLatLng());		
			});			
		}
		else if (geomap_mode == 'line_mode')
		{
			gpolyline = new GPolyline([], "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolyline);
			gpolyline.enableDrawing();
				
			// let's add an event for when the line is updated?
			GEvent.addListener(gpolyline, "lineupdated", function() { 
				mapUpdateTheText(gpolyline);
				// if ( map.getBoundsZoomLevel(gpolyline.getBounds()) < map.getZoom() ) 
				//	map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );
			} );
		
			// once done, you can start removing points?
			GEvent.addListener(gpolyline, "endline", function() {
				map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );				gpolyline.enableEditing({onEvent: "mouseover"});
				gpolyline.disableEditing({onEvent: "mouseout"});				
/* 				select("hand_b");
				var cells = addFeatureEntry(name, color);
				GEvent.bind(gpolyline, "lineupdated", cells.desc, onUpdate); */
				GEvent.addListener(gpolyline, "click", function(latlng, index) {
					if (typeof index == "number") {
						gpolyline.deleteVertex(index);
						mapUpdateTheText(gpolyline);		
					} 
				});
				GEvent.addListener(gpolyline, "lineupdated", function() { 
					map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );
				} );
			});			
		}
		else if (geomap_mode == 'polygon_mode')
		{
			gpolygon = new GPolygon([], "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolygon);
			gpolygon.enableDrawing();
				
			// let's add an event for when the line is updated?
			GEvent.addListener(gpolygon, "lineupdated", function() { 
				mapUpdateTheText(gpolygon);
				// if ( map.getBoundsZoomLevel(gpolygon.getBounds()) < map.getZoom() ) 
				//	map.setCenter( (gpolygon.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolygon.getBounds()) );
			} );
		
			// once done, you can start removing points?
			GEvent.addListener(gpolygon, "endline", function() {
				map.setCenter( (gpolygon.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolygon.getBounds()) );
				gpolygon.enableEditing({onEvent: "mouseover"});
				gpolygon.disableEditing({onEvent: "mouseout"});				
/* 				select("hand_b");
				var cells = addFeatureEntry(name, color);
				GEvent.bind(gpolygon, "lineupdated", cells.desc, onUpdate); */
				GEvent.addListener(gpolygon, "click", function(latlng, index) {
					if (typeof index == "number") {
						gpolygon.deleteVertex(index);
						mapUpdateTheText(gpolygon);		
					} 
				});
				GEvent.addListener(gpolygon, "lineupdated", function() { 
					map.setCenter( (gpolygon.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolygon.getBounds()) );
				} );				
			});			
		}
		return true; // can stop here if there's no data
	}
	// here, we have geodata
	// parse it
	var geocoords = geodata.split("\n");
	// alert(geocoords.length);
	var vertices = [];
	for (var i = 0; i < geocoords.length; i++)
	{
		if (geocoords[i].length > 0)
		{
			var geopoint = (geocoords[i].replace(" ","")).split(","); // this just strips any spaces, and separates items based on the comma delimiter
			// interpreting will depend on latlong or longlat mode
			if (coord_mode == 'latlong_mode')
				// vertices.push(new GLatLng(geopoint[0],geopoint[1]));
				vertices.push(new google.maps.LatLng(geopoint[0],geopoint[1]));
			else if (coord_mode == 'longlat_mode')
				vertices.push(new GLatLng(geopoint[1],geopoint[0]));
		}
	}	
	// so, are there any items?
	// start w/ more than one coordinate stuff
	if (vertices.length > 1)
	{
		// am i drawing a line or a polygon?
		if (geomap_mode == 'line_mode')
		{
			gpolyline = new GPolyline(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolyline);
			gpolyline.enableEditing({onEvent: "mouseover"});
			gpolyline.disableEditing({onEvent: "mouseout"});				
				
			// let's add an event for when the line is updated?
			GEvent.addListener(gpolyline, "lineupdated", function() { 
				mapUpdateTheText(gpolyline);
				// if ( map.getBoundsZoomLevel(gpolyline.getBounds()) < map.getZoom() ) 
					map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );
			} );
		
			// once done, you can start removing points?
			GEvent.addListener(gpolyline, "endline", function() {
/* 				select("hand_b");
				var cells = addFeatureEntry(name, color);
				GEvent.bind(gpolyline, "lineupdated", cells.desc, onUpdate); */
				GEvent.addListener(gpolyline, "click", function(latlng, index) {
					if (typeof index == "number") {
						gpolyline.deleteVertex(index);
						mapUpdateTheText(gpolyline);		
					} 
				});
			});	
		}
		else 
		// assume it's polygon mode, (even if you come from point_mode)
		{
			// alert(vertices.length);
			// alert(coord_mode);
			// for (var i = 0; i < vertices.length; i++)
			// {
				// alert(vertices[i].lat() + ", " + vertices[i].lng());
			// }
			document.getElementById("polygon_mode").className = 'selected';
			document.getElementById("line_mode").className = '';
			document.getElementById("point_mode").className = '';
			gpolygon = new GPolygon(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolygon);
			gpolygon.enableEditing({onEvent: "mouseover"});
			gpolygon.disableEditing({onEvent: "mouseout"});				
				
			// let's add an event for when the line is updated?
			GEvent.addListener(gpolygon, "lineupdated", function() { 
				mapUpdateTheText(gpolygon);
				// if ( map.getBoundsZoomLevel(gpolygon.getBounds()) < map.getZoom() ) 
					map.setCenter( (gpolygon.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolygon.getBounds()) );
			} );
		
			// once done, you can start removing points?
			GEvent.addListener(gpolygon, "endline", function() {
/* 				select("hand_b");
				var cells = addFeatureEntry(name, color);
				GEvent.bind(gpolygon, "lineupdated", cells.desc, onUpdate); */
				GEvent.addListener(gpolygon, "click", function(latlng, index) {
					if (typeof index == "number") {
						gpolygon.deleteVertex(index);
						mapUpdateTheText(gpolygon);		
					} 
				});
			});	
		}
	}
	else if (vertices.length == 1) // could be any of the 3 modes?
	{
		// do we just assume point mode for now?
		if (geomap_mode == 'point_mode')
		{
			// single marker?
			gmarker = new google.maps.Marker({
				position: vertices[0], 
				map: map
				});
			map.panTo(gmarker.getPosition());
			
			//gmarker = new GMarker(vertices[0], {draggable: true});
			//map.addOverlay(gmarker);
			//map.panTo(gmarker.getLatLng());
			
			
			mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
			// some draggable behaviors
			GEvent.addListener(gmarker, "drag", function() { 
				vertices = new Array();
				vertices.push(gmarker.getLatLng());
				mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
			});
			GEvent.addListener(gmarker, "dragend", function() { 
				vertices = new Array();
				vertices.push(gmarker.getLatLng());
				mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
			});					
			// add my map listener for clicks?
			addMarkerToggle = GEvent.addListener(map,"click", function(overlay, latlong) {
				// reset the marker?
				// marker
				if (gmarker)
					map.removeOverlay(gmarker);
				gmarker = new GMarker(latlong, {draggable: true});
				vertices = new Array();
				vertices.push(gmarker.getLatLng());
				mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
				// some draggable behaviors
				GEvent.addListener(gmarker, "drag", function() { 
					vertices = new Array();
					vertices.push(gmarker.getLatLng());
					mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
				});
				GEvent.addListener(gmarker, "dragend", function() { 
					vertices = new Array();
					vertices.push(gmarker.getLatLng());
					mapUpdateTheText(new GPolyline(vertices, "#ff0000", 10, 1, polyOptions));
				});				
				map.addOverlay(gmarker);
				map.panTo(gmarker.getLatLng());		
			});
		}
		// line mode
		else if (geomap_mode == 'line_mode')
		{
			gpolyline = new GPolyline(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolyline);
			gpolyline.enableDrawing();
				
			// let's add an event for when the line is updated?
			GEvent.addListener(gpolyline, "lineupdated", function() { 
				mapUpdateTheText(gpolyline);
				if ( map.getBoundsZoomLevel(gpolyline.getBounds()) < map.getZoom() ) 
					map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );
			} );
		
			// once done, you can start removing points?
			GEvent.addListener(gpolyline, "endline", function() {
				gpolyline.enableEditing({onEvent: "mouseover"});
				gpolyline.disableEditing({onEvent: "mouseout"});				
				map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );
				GEvent.addListener(gpolyline, "lineupdated", function() { 
					map.setCenter( (gpolyline.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolyline.getBounds()) );
				} );				
				GEvent.addListener(gpolyline, "click", function(latlng, index) {
					if (typeof index == "number") {
						gpolyline.deleteVertex(index);
						mapUpdateTheText(gpolyline);		
					} 
				});
			});			
		}
		else if (geomap_mode == 'polygon_mode')
		{
			gpolygon = new GPolygon(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolygon);
			gpolygon.enableEditing({onEvent: "mouseover"});
			gpolygon.disableEditing({onEvent: "mouseout"});				
				
			// let's add an event for when the line is updated?
			GEvent.addListener(gpolygon, "lineupdated", function() { 
				mapUpdateTheText(gpolygon);
			} );
		
			// once done, you can start removing points?
			GEvent.addListener(gpolygon, "endline", function() {
				map.setCenter( (gpolygon.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolygon.getBounds()) );
				GEvent.addListener(gpolygon, "lineupdated", function() { 
					map.setCenter( (gpolygon.getBounds()).getCenter(), map.getBoundsZoomLevel(gpolygon.getBounds()) );
				} );				
				GEvent.addListener(gpolygon, "click", function(latlng, index) {
					if (typeof index == "number") {
						gpolygon.deleteVertex(index);
						mapUpdateTheText(gpolygon);		
					} 
				});
			});			
		}
	}
	return true;
}
// ====================================================


// v2
// ====================================================
function mapUpdateViewonly() {
	// start by removing the overlays
	mapRemoveMarkers();
	
	var geodata = document.getElementById('geodata').innerHTML;
	// here, we have geodata
	// parse it
	var geocoords = geodata.split("\n");
	// alert(geocoords.length);
	var vertices = [];
	for (var i = 0; i < geocoords.length; i++)
	{
		if (geocoords[i].length > 0)
		{
			var geopoint = (geocoords[i].replace(" ","")).split(","); // this just strips any spaces, and separates items based on the comma delimiter
			// interpreting will depend on latlong or longlat mode
			if (coord_mode == 'latlong_mode')
				//vertices.push(new GLatLng(geopoint[0],geopoint[1]));
				vertices.push(new google.maps.LatLng(geopoint[0],geopoint[1]));
			else if (coord_mode == 'longlat_mode')
				//vertices.push(new GLatLng(geopoint[1],geopoint[0]));
				vertices.push(new google.maps.LatLng(geopoint[1],geopoint[0]));
		}
	}	
	// so, are there any items?
	// start w/ more than one coordinate stuff
	if (vertices.length > 1)
	{
		// am i drawing a line or a polygon?
		if (geomap_mode == 'line_mode')
		{
			gpolyline = new GPolyline(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolyline);
		}
		else 
		// assume it's polygon mode, (even if you come from point_mode)
		{
			document.getElementById("polygon_mode").className = 'selected';
			document.getElementById("line_mode").className = '';
			document.getElementById("point_mode").className = '';
			gpolygon = new GPolygon(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolygon);
		}
	}
	else if (vertices.length == 1) // could be any of the 3 modes?
	{
		// do we just assume point mode for now?
		if (geomap_mode == 'point_mode')
		{
			console.log("line 509");
			// single marker?
			gmarker = new GMarker(vertices[0]);
			map.addOverlay(gmarker);
			map.panTo(gmarker.getLatLng());
		}
		else if (geomap_mode == 'line_mode')
		{
			gpolyline = new GPolyline(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolyline);
		}
		else if (geomap_mode == 'polygon_mode')
		{
			gpolygon = new GPolygon(vertices, "#ff0000", 10, 1, polyOptions);
			map.addOverlay(gpolygon);
		}
	}
	return true;
}
// ====================================================

// mapUpdateTheText()
// given the overlay mode, update the textbox accordingly?
// may not be necessary since polylines, polygons, and markers should all have appropriate listeners
// maybe used by listeners
// ====================================================
function mapUpdateTheText(polyobj) {
	// let's populate the text inputs
	var geodata = "";
	for (var i = 0; i < polyobj.getVertexCount(); i++)
	{
		switch (coord_mode) {
			case 'latlong_mode':
				geodata	= geodata + polyobj.getVertex(i).lat().toString() + ", " + polyobj.getVertex(i).lng().toString() + "\n"; 
				break;
			case 'longlat_mode':
				geodata	= geodata + polyobj.getVertex(i).lng().toString() + ", " + polyobj.getVertex(i).lat().toString() + "\n"; 
				break;
			default:
				break;
		}
	}
	document.getElementById('geodata').value = geodata;
	return true;
}
// ====================================================

// mapSwitchOverlay()
// switch the overlay to the appropriate mode
// ====================================================
function mapSwitchOverlay(marker_mode) {
	mapRemoveMarkers();
	geomap_mode = marker_mode;
	// update the form field value

	if (marker_mode == 'point_mode')
		document.getElementById("geodata_type_id").value = 1;
	if (marker_mode == 'line_mode')
		document.getElementById("geodata_type_id").value = 2;
	if (marker_mode == 'polygon_mode')
		document.getElementById("geodata_type_id").value = 3;

	// if we switch to point mode, we need to set the text to one line only (or it ends up drawing a polygon)
	if (marker_mode == 'point_mode') {
		var geodata = document.getElementById('geodata').value;
		if (geodata != null && geodata.length > 0)
		{
			// here, we have geodata
			// parse it
			var geocoords = geodata.split("\n");	
			document.getElementById('geodata').value = geocoords[0];
		}
	}
	// when we switch modes, we wipe any existing text data
	mapResetText();
	
	return true;
}
// ====================================================

// mapSwitchCoordMode()
// switch the overlay based on coordinate mode
function mapSwitchCoordMode(new_mode) {
	coord_mode = new_mode;
	mapRemoveMarkers();	
	mapUpdateFromText();
	return true;
}