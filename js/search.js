/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * search.js
 * some jquery behaviors related to the search display page
 */

$(document).ready( function () {

    /**
     * When an article title is clicked, load and display the article
     * abstract.
     */
	$("ul#titles li a").click( function() {
		$("div#brief").hide("slow");
		$.get("/modules/front/ajax/get_article2.php", { title:$(this).html() }, function(data) {	
			// alert(data);
			// process them data
			if (!data || data.length <= 0)
			{
				$('div#brief').append(
                    "<strong>Coming soon</strong><br />This article is under development and will be available soon"
				);
				alert("There was an error loading the article: are you sure the article exists?");
				return;
			} 

			var brief = eval("(" + data + ")");
			// update the brief
			// titles
			$("div#brief").empty();
            $("div#brief").append(
                '<h3 class="english-title">'
                + '<span xml:lang="en-US">'
                + brief.titles[0]
                + '</span>'
                + '</h3>'
            );
            // English is handled above
			for (var i = 1; i < brief.titles.length; i++)
			{
				var span = "<span lang=\"" + brief.titlelanguages[i] + "\" xml:lang=\""+ brief.titlelanguages[i] +"\" >" + brief.titles[i] + "</span><br />";
				$("div#brief").append('<h3>' + span + '</h3>');
			}
			// author(s)
			$("div#brief").append('<p id="author"></p>');
			for (var i = 0; i < brief.authors.length; i++)
				$("div#brief p#author").append(brief.authors[i] + "<br />");
			// abstracts
            $("div#brief").append('<p id="abstract"></p>');
			$("div#brief p#abstract").append(brief.english_abstract);
            $("div#brief").append('<p id="arabic_abstract"></p>');
			$("div#brief p#arabic_abstract").append(brief.arabic_abstract);
			// permalink
            var articleLink = $("<a/>").attr({"href": "/articles/" + brief.permalink});
            $(articleLink).append($("<img />").attr({"src": "/graphics/Read_Article.png"}));
            $("div#brief").append(articleLink);
			return;
		});
		$("div#brief").show("slow");
	});
	
} )
