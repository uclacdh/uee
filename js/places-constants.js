	// constants.js
	
	// this might useful to have a set of constants for some of the jquery toggles?
	// some constants for the Place data entry forms
	
	// place types
	var PLACE_TYPE_NOME = 1;
	var PLACE_TYPE_REGION =  2;
	var PLACE_TYPE_GOVERNATE =  3;
	var PLACE_TYPE_SITE =  4;
	var PLACE_TYPE_FEATURE =  5;
	
	// place name types
	var PLACE_NAME_TYPE_MAIN = 1;
	var PLACE_NAME_TYPE_CLASSICAL = 2;
	var PLACE_NAME_TYPE_MODERN = 3;
	var PLACE_NAME_TYPE_EGYPTIAN = 4;
	var PLACE_NAME_TYPE_COPTIC = 5;
	var PLACE_NAME_TYPE_OTHER = 6;
	
	// geodata types
	var GEODATA_POINT = 1;
	var GEODATA_LINE = 2;
	var GEODATA_POLYGON = 3;
