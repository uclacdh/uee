// set up our jquery behaviors
// contains case insensitve
jQuery.extend(jQuery.expr[':'], {
    "contains-ic": "jQuery.fn.text.apply([a]).toLowerCase().indexOf(m[3].toLowerCase())>=0"
});


// TODO 6/2/11 - we need to better document what's going on in here
$(document).ready(function() {

	// add an extra glossname field
	// TODO 6/2/11 - there's probably a better way to do this
	$("p.glossname_row a").click(function () {
		var thisNode = this.parentNode.id;
		var nodeNum = thisNode.substr("glossname_".length);
		// determine some properties for the new Node
		// var newNodeNum = parseInt(nodeNum) + 1;
		var newNodeNum = 0;
		var newNodeStr = "glossname_" + newNodeNum.toString();
		while (document.getElementById(newNodeStr) )
		{
			newNodeNum++;
			newNodeStr = "glossname_" + newNodeNum.toString();
		}
		var newNodeId = "glossname_" + newNodeNum.toString();
		var newFieldId = "name_field_" + newNodeNum.toString();
		// make the new Node
		var newNode = document.createElement("p");
		newNode.setAttribute("id",newNodeId);
		newNode.className="glossname_row";
		// make the label
		var newLabel = document.createElement("label");
		newLabel.setAttribute("for",newFieldId);
		newLabel.appendChild(document.createTextNode("Name"));
		newNode.appendChild(newLabel);
		// make the field
		var newInput = document.createElement("input");
		newInput.setAttribute("type","text");
		newInput.setAttribute("id",newFieldId);
		newInput.setAttribute("name","gloss_name[]");
		newInput.setAttribute("maxlength","100");
		newNode.appendChild(newInput);
		newNode.style.display="none";
		document.getElementById('glossform').insertBefore(newNode,document.getElementById('definition_p'));
		$("#glossform p").show("fast");
		// having some issues with assinging the new onclick functionality
		// what happens if i remove the thisNode's clicker
		// alert(document.getElementById(thisNode));
		// while ( document.getElementById(thisNode).childNodes.length > 4)
		// {
			// document.getElementById(thisNode).removeChild(document.getElementById(thisNode).childNodes[4]);
		// }
	});

	// these behaviors are for the categories table
	// some of the admin input pages will be able to toggle between
	// a tree view and a table view
	// turns on table view
	$("a#toTable").click(function () {
		$("#treeView").hide("slow");
		$("a#toTable").hide("slow");
		$("form#searchbox").hide("slow");
		$("#tableView").show("slow");
		$("a#toTree").show("slow");
	});

	// turns on tree view	
	$("a#toTree").click(function () {
		$("#tableView").hide("slow");
		$("a#toTree").hide("slow");
		// $("form#searchbox").show("slow");
		$("#treeView").show("slow");
		$("a#toTable").show("slow");
	});
	
	// add an extra timeperiod name field
	// TODO 6/2/11 - there is probably a better way to do this
	$("p.timeperiodname_row a").click(function () {
		var thisNode = this.parentNode.id;
		var nodeNum = thisNode.substr("timeperiodname_".length);
		// determine some properties for the new Node
		// var newNodeNum = parseInt(nodeNum) + 1;
		var newNodeNum = 0;
		var newNodeStr = "timeperiodname_" + newNodeNum.toString();
		while (document.getElementById(newNodeStr) )
		{
			newNodeNum++;
			newNodeStr = "timeperiodname_" + newNodeNum.toString();
		}
		var newNodeId = "timeperiodname_" + newNodeNum.toString();
		var newFieldId = "name_field_" + newNodeNum.toString();
		// make the new Node
		var newNode = document.createElement("p");
		newNode.setAttribute("id",newNodeId);
		newNode.className="timeperiodname_row";
		// make the label
		var newLabel = document.createElement("label");
		newLabel.setAttribute("for",newFieldId);
		newLabel.appendChild(document.createTextNode("Name"));
		newNode.appendChild(newLabel);
		// make the field
		var newInput = document.createElement("input");
		newInput.setAttribute("type","text");
		newInput.setAttribute("id",newFieldId);
		newInput.setAttribute("name","timeperiod_name[]");
		newInput.setAttribute("maxlength","100");
		newNode.appendChild(newInput);

		newNode.style.display="none";
		document.getElementById('timeperiodform').insertBefore(newNode,document.getElementById('timeperiodform_submit'));
		$("#timeperiodform p").show("fast");
		// having some issues with assinging the new onclick functionality
		// what happens if i remove the thisNode's clicker
		// alert(document.getElementById(thisNode));
		// while ( document.getElementById(thisNode).childNodes.length > 4)
		// {
			// document.getElementById(thisNode).removeChild(document.getElementById(thisNode).childNodes[4]);
		// }
	});
	
	// place behaviorsa
	// 7/8/11 - added by chris to get the feature type dropdown to show when editing an existing feature place record
	//if (document.getElementById('place_type_id').value == PLACE_TYPE_FEATURE) {
	//	$("#feature_type").show();
	//});
	// if we change the place_id value, toggle various displays
	// TODO 6/2/11 - document what displays are expected to toggle
	$("select#place_type_id").change( function() {
		// id 4 and 5
		if (document.getElementById('place_type_id').value == PLACE_TYPE_SITE || document.getElementById('place_type_id').value == PLACE_TYPE_FEATURE) {
			$("fieldset#geotime-data").show("slow", function() {
				if (document.getElementById('place_type_id').value == PLACE_TYPE_SITE) {
					$("fieldset#feature-timeperiod-data").hide("slow", function() {
						$("fieldset#site-timeperiod-data").show("slow");				
					});
				}
				if (document.getElementById('place_type_id').value == PLACE_TYPE_FEATURE) {
					$("#feature_type").show();
					$("fieldset#site-timeperiod-data").hide("slow", function() {
						$("fieldset#feature-timeperiod-data").show("slow");
					} );
				}			
			});
		}
		else {
			$("fieldset#site-timeperiod-data").hide("slow");
			$("fieldset#feature-timeperiod-data").hide("slow");
			$("fieldset#geotime-data").hide("slow");
		}
		// site 4, feature 5, region 2
		if (document.getElementById('place_type_id').value == PLACE_TYPE_SITE || document.getElementById('place_type_id').value == PLACE_TYPE_FEATURE || document.getElementById('place_type_id').value == PLACE_TYPE_REGION)
		{
			$("p#place_movable").show("slow");
			// adjust the google map (jquery's show messes it up, checkResize allows gmap api to adjust)
			map.checkResize();
			map.panTo(new GLatLng(30.05, 30.366667));
		}
		else
			$("p#place_movable").hide("slow");
		
		// region 2, governate 3, nome 1
		/*
		if (document.getElementById('place_type_id').value == 2 || document.getElementById('place_type_id').value == 3 || document.getElementById('place_type_id').value == 1)
		{
			$("div#child-sites").show("slow");
		}
		else
		{
			$("div#child-sites").hide("slow");			
		}*/
	});
	
	// set up more alternate names?
	// add an extra timeperiod name field
	// TODO 6/2/11 - there's probably a better way to do this
	$("div.timerow a").click(function () {
		var thisNode = this.parentNode.parentNode.id;
		var nodeNum = thisNode.substr("timerow_".length);
		// determine some properties for the new Node
		// var newNodeNum = parseInt(nodeNum) + 1;
		var newNodeNum = 0;
		var newNodeStr = "timerow_" + newNodeNum.toString();
		while (document.getElementById(newNodeStr) )
		{
			newNodeNum++;
			newNodeStr = "timerow_" + newNodeNum.toString();
		}
		var newNodeId = "timerow_" + newNodeNum.toString();
		var newTimeTypeId = "time_type_id" + newNodeNum.toString();
		var newStartId = "time_start_period_id_" + newNodeNum.toString();
		var newEndId = "time_end_period_id_" + newNodeNum.toString();		
		// make the new Node
		var newNode = document.createElement("div");
		newNode.setAttribute("id",newNodeId);
		newNode.className="timerow";
		// make the first col (time_type)
		var firstCol = document.getElementById('time_type_col').cloneNode(true);
		var firstColLabel = firstCol.childNodes[1]; 
		firstColLabel.setAttribute("for",newTimeTypeId);
		var firstColSelect = firstCol.childNodes[4];
		// firstColSelect.setAttribute("name",newTimeTypeId);
		firstColSelect.setAttribute("id",newTimeTypeId);
		newNode.appendChild(firstCol);
		// make the second col?
		// ehh... it would be easier to copy the existing timerow and change the ids / fields
		var startCol = document.getElementById('start_time_period_col').cloneNode(true);
		var secondColLabel = startCol.childNodes[1]; 
		secondColLabel.setAttribute("for",newStartId);
		var secondColSelect = startCol.childNodes[4];
		// secondColSelect.setAttribute("name",newStartId);
		secondColSelect.setAttribute("id",newStartId);
		newNode.appendChild(startCol);		
		// make the third col?
		// ehh... it would be easier to copy the existing timerow and change the ids / fields
		var endCol = document.getElementById('end_time_period_col').cloneNode(true);
		var thirdColLabel = endCol.childNodes[1]; 
		thirdColLabel.setAttribute("for",newEndId);
		var thirdColSelect = endCol.childNodes[4];
		// thirdColSelect.setAttribute("name",newEndId);
		thirdColSelect.setAttribute("id",newEndId);
		newNode.appendChild(endCol);
		
		// attach and display the node
		newNode.style.display='none';
		document.getElementById('time').appendChild(newNode);
		$("fieldset#time div.timerow").show("fast");
		
		
	});
	
	// add an extra timeperiod name field
	// TODO 6/2/11 - there's probably a better way to do this
	$("p.placename_row a").click(function () {
		var thisNode = this.parentNode.id;
		var nodeNum = thisNode.substr("placename_".length);
		// determine some properties for the new Node
		// var newNodeNum = parseInt(nodeNum) + 1;
		var newNodeNum = 0;
		var newNodeStr = "placename_" + newNodeNum.toString();
		while (document.getElementById(newNodeStr) )
		{
			newNodeNum++;
			newNodeStr = "placename_" + newNodeNum.toString();
		}
		var newNodeId = "placename_" + newNodeNum.toString();
		var newFieldId = "name_field_" + newNodeNum.toString();
		var newTypeFieldId = "nametype_field_" + newNodeNum.toString();
		// make the new Node
		var newNode = document.createElement("p");
		newNode.setAttribute("id",newNodeId);
		newNode.className="placename_row";
		// make the label
		var newLabel = document.createElement("label");
		newLabel.setAttribute("for",newFieldId);
		newLabel.appendChild(document.createTextNode(String.fromCharCode(160) + String.fromCharCode(160) + "Alternate Name"));
		newNode.appendChild(newLabel);
		// make the field
		var newInput = document.createElement("input");
		newInput.setAttribute("type","text");
		newInput.setAttribute("id",newFieldId);
		newInput.setAttribute("name","place_name[]");
		newInput.setAttribute("maxlength","100");
		newNode.appendChild(newInput);
		newNode.appendChild(document.createTextNode(" "));
		// make the label
		var newLabel2 = document.createElement("label");
		newLabel2.setAttribute("for",newTypeFieldId);
		newLabel2.appendChild(document.createTextNode("Type"));
		newLabel2.style.display='none';
		newNode.appendChild(newLabel2);
		var newSelect = document.createElement("select");
		newSelect.setAttribute("id",newTypeFieldId);
		newSelect.setAttribute("name","place_name_type_id[]");
		var newOption = document.createElement("option");
		newOption.setAttribute("value","");
		newOption.appendChild(document.createTextNode("Type"));
		var newOption2 = document.createElement("option");
		newOption2.setAttribute("value","1");
		newOption2.appendChild(document.createTextNode("Main"));
		var newOption3 = document.createElement("option");
		newOption3.setAttribute("value","2");
		newOption3.appendChild(document.createTextNode("Classical"));
		var newOption4 = document.createElement("option");
		newOption4.setAttribute("value","3");
		newOption4.appendChild(document.createTextNode("Modern"));
		var newOption5 = document.createElement("option");
		newOption5.setAttribute("value","4");
		newOption5.appendChild(document.createTextNode("Egyptian"));
		var newOption6 = document.createElement("option");
		newOption6.setAttribute("value","5");
		newOption6.appendChild(document.createTextNode("Coptic"));
		var newOption7 = document.createElement("option");
		newOption7.setAttribute("value","6");
		newOption7.appendChild(document.createTextNode("Other"));
		newSelect.appendChild(newOption);
		newSelect.appendChild(newOption2);
		newSelect.appendChild(newOption3);
		newSelect.appendChild(newOption4);
		newSelect.appendChild(newOption5);
		newSelect.appendChild(newOption6);
		newSelect.appendChild(newOption7);
		newNode.appendChild(newSelect);
		newNode.style.display="none";
		document.getElementById('placeform').insertBefore(newNode,document.getElementById('place_type_buffer'));
		$("#placeform p").show("fast");
		// having some issues with assinging the new onclick functionality
		// what happens if i remove the thisNode's clicker
		// alert(document.getElementById(thisNode));
		// while ( document.getElementById(thisNode).childNodes.length > 4)
		// {
			// document.getElementById(thisNode).removeChild(document.getElementById(thisNode).childNodes[4]);
		// }
	});

	// in the geomap, set behaviors based on what kind of markers
	// we are putting on the map
	$("a.geomarker_mode").click(function () {	
		$("div#geomarker-data").show("fast");
		$("textarea#geodata").show("fast");
		geomap_mode = this.id;
		this.className = 'selected';
		var textInput = document.getElementById('geodata').value;
		var geocoords = textInput.split("\n");
		// alert(geocoords.length);
		var vertices = [];
		for (var i = 0; i < geocoords.length; i++)
		{
			if (geocoords[i].length > 0)
			{
				var geopoint = (geocoords[i].replace(" ","")).split(",");
				vertices.push(new GLatLng(geopoint[0],geopoint[1]));
				// alert(geopoint[0]);
				// alert(geopoint[1]);	
			}
		}		
		switch(geomap_mode) {
			case 'point_mode':
			{
				document.getElementById('line_mode').className = '';
				document.getElementById('polygon_mode').className = '';
				$("p#polygon-data").hide("fast");
				$("div.polygondata").hide("fast");
				$("div#polygon-data").hide("fast");
				$("div.linedata").hide("fast");
				$("div#line-data").hide("fast", function() {
					$("div#point-data").show("fast");
					$("div.pointdata").show("fast");
				});
				// TODO - update the map
				mapSwitchOverlay('point_mode');
				break;
			}
			case 'line_mode':
			{
				document.getElementById('point_mode').className = '';
				document.getElementById('polygon_mode').className = '';
				$("p#polygon-data").hide("fast");
				$("div.polygondata").hide("fast");
				$("div#polygon-data").hide("fast");
				$("div.pointdata").hide("fast");				
				$("div#point-data").hide("fast", function() {
					$("div#line-data").show("fast");
					$("div.linedata").show("fast");
				});
				// TODO - update the map
				mapSwitchOverlay('line_mode');
				break;
			}
			case 'polygon_mode':
			{
				document.getElementById('point_mode').className = '';
				document.getElementById('line_mode').className = '';
				$("p#point-data").hide("fast");
				$("div.pointdata").hide("fast");
				$("div.linedata").hide("fast");
				$("div#line-data").hide("fast", function() {
					$("div#polygon-data").show("fast");
					$("div.polygondata").show("fast");
				});
				// TODO - update the map
				mapSwitchOverlay('polygon_mode');		
				break;
			}
			default:
				break;
		}
	});

	// set that the geo coordinates are in latlong mode	
	$("a#latlong_mode").click(function() {
		document.getElementById('latlong_mode').className = 'selected';
		document.getElementById('longlat_mode').className = '';
		document.getElementById('geocoord_mode').value = 'latlong_mode';

	} );
	
	// set that the geo coordinates are in longlat mode
	$("a#longlat_mode").click(function() {
		document.getElementById('latlong_mode').className = '';
		document.getElementById('longlat_mode').className = 'selected';
		document.getElementById('geocoord_mode') = 'longlat_mode';

	} );
	
	// update the helper text describing the other places piece
	$("select.other_places-select").change(function() {
		updateOtherText();
	} );

	// update the helper text describing how places change	
	$("select.pastpresent-toggle").change(function() {
		updateOtherText();
	} );

	// update the helper text describing how places change	
	$("select.transition_type_id").change(function() {
		updateOtherText();
	} );

	// add an extra row for place changes
	$("a#newotherbox").click(function() {
		var boxnode = document.getElementById("other-box_0").cloneNode(true);
		// what number do i need?
		var boxnode_num = 0;
		while (document.getElementById('other-box_'+boxnode_num.toString()))
			boxnode_num++;
		// gotta update the children values
		// other place label
		boxnode.childNodes[1].childNodes[1].setAttribute("for",'other_places-select_'+boxnode_num.toString());
		boxnode.childNodes[1].childNodes[3].childNodes[1].setAttribute("id",'other_places-select_'+boxnode_num.toString());
		boxnode.childNodes[5].childNodes[1].childNodes[2].setAttribute("id",'other-places-pastpresent-toggle_'+boxnode_num.toString());
		boxnode.childNodes[7].childNodes[1].childNodes[3].setAttribute("id",'transition_type_id_'+boxnode_num.toString());
		boxnode.removeChild(boxnode.childNodes[13]);
		boxnode.removeChild(boxnode.childNodes[11]);
		boxnode.removeChild(boxnode.childNodes[9]);
		boxnode.style.display='none';
		var bn_id = "other-box_"+boxnode_num.toString();
		boxnode.setAttribute('id',bn_id);
		document.getElementById('other-places').appendChild(boxnode);
		$(".other-box").show("fast");
		return;
	} );
	
	// i want a search box for some of the parent/children stuff.
	$("input#parents-search").keyup(function(e) {
		var search= $(e.target).val();
		if (!search || search.length == 0)
		{
			return;
		}
		// everything is grey first!
		$("#parent_places div label").css("background-color","rgb(220,220,220)");
		// TODO 6/2/11 - case insensitive
	    $("#parent_places div div label:contains('"+search+"')").css("background-color","yellow");

	} );

	// show/hide the various parent place tabs	
	$("ul#parents_tabs li a").click( function() {
		$("ul#parents_tabs li").removeClass("selected");
		$(this).parent().addClass("selected");
		var linkname = $(this).text().toLowerCase();
		var section = "div#parent_" + linkname;
		if ($("#place_type_id").val() == PLACE_TYPE_FEATURE && linkname != 'sites')
			alert("Are you sure you want to enter parents that are not features? This should only be done for special cases, such as large rivers, wadis, etc... If you enter a parent site, it's parents will be inherited by this feature...");
		$("fieldset#parent_places div div").hide("fast", function() {
			$(section).show("fast").children().show("fast");
		});
	} );

	// by default, hide some of the parent tabs	
	$("fieldset#parent_places div div").hide("fast", function() {
		$("div#parent_regions").show("fast").children().show("fast");
		$("ul#parents_tabs li:last").hide();
		// for edit page
		if ($("#place_type_id").val() == PLACE_TYPE_FEATURE)
		{
			$("ul#parents_tabs li").removeClass("selected");
			$("ul#parents_tabs li:last").addClass("selected").show();	
			$("div#child-sites").hide().children().hide();
			$("fieldset#parent_places div div").hide("fast", function() {
				$("div#parent_sites").show("fast").children().show("fast");
			});				
		}
		else
		{
			$("ul#parents_tabs li:last").hide();
			$("div#child-sites").show().children().show();
		}

	});	

	// if the place type changes to a FEATURE
	// toggle some displays	
	$("#place_type_id").change(function() {
		if ($(this).val() == PLACE_TYPE_FEATURE)
		{
			$("ul#parents_tabs li").removeClass("selected");
			$("ul#parents_tabs li:last").addClass("selected").show();	
			$("div#child-sites").hide().children().hide();
			$("fieldset#parent_places div div").hide("fast", function() {
				$("div#parent_sites").show("fast").children().show("fast");
			});				
		}
		else
		{
			$("ul#parents_tabs li:last").hide();
			$("div#child-sites").show().children().show();
		}
	} );

	// TODO 6/2/11 - if the place type is already set and is a site or feature, show the map
	if ($("#place_type_id").val() == PLACE_TYPE_FEATURE || $("#place_type_id").val() == PLACE_TYPE_SITE) {
		$("#geotime-data").show();
		// TODO 6/2/11 - select the right marker type given the existing geodata?
		// mapUpdateViewonly();
	}
	
});

function updateOtherText()
{
	var otherplace;
	var boxnode_num = 0;
	while (document.getElementById('other-box_'+boxnode_num.toString()))
	{
		for (var i = 0; i < document.getElementById('other-descriptions_'+boxnode_num.toString()).childNodes.length; i++)
			document.getElementById('other-descriptions_'+boxnode_num.toString()).removeChild(document.getElementById('other-descriptions_'+boxnode_num.toString()).childNodes[i]);
		var msg = '';
		if (document.getElementById('other_places-select_'+boxnode_num.toString() ) )
			var otherplace = document.getElementById('other_places-select_'+boxnode_num.toString()).options[document.getElementById('other_places-select_'+boxnode_num.toString()).selectedIndex].text;
		else
			var otherplace = '';
		var thisplace = document.getElementById('main_name').value;
		var relation = document.getElementById('other-places-pastpresent-toggle_'+boxnode_num.toString()).value;
		var transition = document.getElementById('transition_type_id_'+boxnode_num.toString()).options[document.getElementById('transition_type_id_'+boxnode_num.toString()).selectedIndex].text.substr(3);
		if (relation == 'future')
			msg = thisplace + " " + transition + " and became " + otherplace;
		else if (relation == 'past')
			msg = otherplace + " " + transition + " and became " + thisplace;
		else
			return false;
		document.getElementById('other-descriptions_'+boxnode_num.toString()).appendChild(document.createTextNode(msg));
		boxnode_num++;
	}
}

function showFeatureType()
{
	if (document.getElementById('place_type_id').value == 5) // 5 -> FEATURE
	{
		document.getElementById('feature_type').style.display="block";
		document.getElementById('feature_type').style.float="left";
	}
}

function showMap()
{
	if (document.getElementById('place_type_id').value == 5 || document.getElementById('place_type_id').value == 4 || document.getElementById('place_type_id').value == 2) // 5 -> FEATURE, 4-> SITE, 2 -> REGION
	{
		document.getElementById('geotime-data').style.display="block";
		document.getElementById('geotime-data').style.float="left";
	}
}

function loadOtherDescription() {
	if (document.getElementById('other_places-select_0') && document.getElementById('other_places-select_0').value != '') 
	{
		updateOtherText();
	}
}
