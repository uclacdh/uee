// archived on Nov 24, 2008
// switching text inputs to a textarea box


// google maps behaviors (for data entry: place records)
var gmarker = null; // this is for point_mode;
var gpolyline = new Array(); // this is for line_mode;
var gpolygon = new Array(); // this is for polygon_mode;
// var polyline; // we want these here for being able to remove the overlays
// var polygon; 
var geomap_mode = "point_mode";
var map;
var zoom_level;
//<![CDATA[
function load(lat, lng, zoom) {
	if (GBrowserIsCompatible()) {
		if (lat == null)
			lat = 30.05;
		if (lng == null)
			lng = 31.366667;			
		var markerSpot = new GLatLng(lat, lng);

		// retrieve the zoom level
		if (zoom < 0 || zoom == null)
			zoom_level = 5;
		else
			zoom_level = zoom;

		// make the map
		map = new GMap2(document.getElementById("map"));
		map.clearOverlays();
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		map.setMapType(G_NORMAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		
		map.setCenter(markerSpot, zoom_level);
			
		// initialize our marker
		gmarker = new GMarker(markerSpot, {draggable: true});
		// initialize the text inputs
		document.getElementById('latitude').value = gmarker.getLatLng().lat();
		document.getElementById('longitude').value = gmarker.getLatLng().lng();		
		// initialize the draggable behavior
		GEvent.addListener(gmarker, "drag", function() { 
			document.getElementById('latitude').value = gmarker.getLatLng().lat();
			document.getElementById('longitude').value = gmarker.getLatLng().lng();			
		});
		GEvent.addListener(gmarker, "dragend", function() { 
			document.getElementById('latitude').value = gmarker.getLatLng().lat();
			document.getElementById('longitude').value = gmarker.getLatLng().lng();		
			map.panTo(gmarker.getLatLng());			
		});		
		// display the marker
		// map.addOverlay(gmarker);
		
		// add my map listener for clicks?
		GEvent.addListener(map,"click", function(overlay, latlong) {
			// reset the marker?
			map.removeOverlay(gmarker);
			gmarker = new GMarker(latlong, {draggable: true});
			// some draggable behaviors
			GEvent.addListener(gmarker, "drag", function() { 
			document.getElementById('latitude').value = gmarker.getLatLng().lat();
			document.getElementById('longitude').value = gmarker.getLatLng().lng();			
			});
			GEvent.addListener(gmarker, "dragend", function() { 
			document.getElementById('latitude').value = gmarker.getLatLng().lat();
			document.getElementById('longitude').value = gmarker.getLatLng().lng();			
			});				
			map.addOverlay(gmarker);
			map.panTo(gmarker.getLatLng());
			document.getElementById('latitude').value = gmarker.getLatLng().lat();
			document.getElementById('longitude').value = gmarker.getLatLng().lng();		
		});

	}
}

function updateMarker() {
	var lat = document.getElementById('latitude').value;
	var lng = document.getElementById('longitude').value;
	// do nothing if we have an empty text input value
	if (!lat || lat == "" || lat == null || !lng || lng == "" || lng == null)
		return false;
	
	switch (geomap_mode) {
		case 'point_mode':
		default:
			load(lat,lng,map.getZoom());
			break;
	}
}

// let's set up the line loader?
function lineLoad(lat1, lng1, lat2, lng2, zoom, vertices) {
	if (GBrowserIsCompatible()) {
		// alert(vertices);
		// let's declare some variables
		var lineCenter;
		if (lat1 == null)
			lat1 = 30.05;
		if (lng1 == null)
			lng1 = 31.366667;			
		var markerSpot = new GLatLng(lat1, lng1);		
		// do we already have vertices?
		/*
		if (vertices == null || vertices.length == 0)
		{
			lat1 = 30.05;
			lng1 = 31.366667;			
			
			var markerSpot1 = new GLatLng(lat1, lng1);

			lat2 = 31.05;
			lng2 = 32.366667;			
			var markerSpot2 = new GLatLng(lat2, lng2);
			var lineCenterLat = (lat1 + lat2) / 2.0;
			var lineCenterLng = (lng1 + lng2) / 2.0;
			lineCenter = new GLatLng(lineCenterLat,lineCenterLng);
			vertices = new Array();
			// vertices.push(markerSpot1);
			// vertices.push(markerSpot2);
		}
		else
		{
			// alert(vertices.length);
			// determine the 'centering' point for this line
			var latSum = 0;
			var lngSum = 0;
			for (var i = 0; i < vertices.length; i++)
			{
				latSum += parseFloat(vertices[i].lat());
				lngSum += parseFloat(vertices[i].lng());
			}
			var lineCenterLat = (latSum) / vertices.length;
			var lineCenterLng = (lngSum) / vertices.length;
			lineCenter = new GLatLng(lineCenterLat,lineCenterLng);
		}
		*/ 
		
		// gpolyline = vertices;
		// retrieve the zoom level
		if (zoom < 0 || zoom == null)
			zoom_level = 6;
		else
			zoom_level = zoom;

		// make the map
		map = new GMap2(document.getElementById("map"));
		// let's remove any overlays
		// map.removeOverlay(polyline);
		// map.removeOverlay(polygon);
		
		map.clearOverlays();
		
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		map.setMapType(G_NORMAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		
		map.setCenter(markerSpot, zoom_level);

		// add my map listener for clicks?
        var polyOptions = {geodesic:true};		
		var polyline = new GPolyline([], "#ff0000", 10, 1, polyOptions);
		map.addOverlay(polyline);	
		polyline.enableDrawing();
		polyline.enableEditing({onEvent: "mouseover"});
		polyline.disableEditing({onEvent: "mouseout"});				
		
		/*
        var polyOptions = {geodesic:true};
        var polyline = new GPolyline(gpolyline, "#ff0000", 10, 1, polyOptions);
        map.addOverlay(polyline);
		
		// polyline.enableDrawing();
		polyline.enableEditing({onEvent: "mouseover"});
		polyline.disableEditing({onEvent: "mouseout"});
		
		*/
		
		// let's add an event for when the line is updated?
		GEvent.addListener(polyline, "lineupdated", function() { 
			// this is where we could modify the coordinate pieces
			var inputdivCount = getLineInputDivCount();
			// let's populate the text inputs
			for (var i = 0; i < polyline.getVertexCount(); i++)
			{
				var vertex = polyline.getVertex(i);
				var pid = "line-geodata_" + i.toString();
				if (document.getElementById(pid))
				{
					// update the values;
					var lat_id = "latitude_" + i.toString();
					document.getElementById(lat_id).value = vertex.lat();
					var lng_id = "longitude_" + i.toString();
					document.getElementById(lng_id).value = vertex.lng();
				}
				else
				{
					// do something!
					// probably clone the 0 p, change the id's
					var zero_obj = document.getElementById('line-geodata_0').cloneNode(true);
					zero_obj.setAttribute("id",'line-geodata_'+i.toString());
					// update the values;
					var label_text = "Point "+ (i+1).toString();
					zero_obj.childNodes[1].replaceChild(document.createTextNode(label_text), zero_obj.childNodes[1].childNodes[0]);
					var lat_id = "latitude_" + i.toString();
					// this seems to be causing issues for IE7
					// alert(zero_obj.childNodes[3]);
					zero_obj.childNodes[3].childNodes[4].setAttribute('id',"latitude_" + i.toString());
					zero_obj.childNodes[3].childNodes[4].value = vertex.lat();
					var lng_id = "longitude_" + i.toString();
					zero_obj.childNodes[5].childNodes[4].setAttribute('id',"longitude_" + i.toString());
					zero_obj.childNodes[5].childNodes[4].value = vertex.lng();
					// where do I append this?
					document.getElementById("line-data").appendChild(zero_obj);
				}
			}
			// let's remove any coordinates that no longer exist?
			for (var i = polyline.getVertexCount(); i < inputdivCount; i++)
			{
				var obj = document.getElementById('line-geodata_'+i.toString());
				if (obj)
				{
					// if (i < 2)
					// {
						// document.getElementById('latitude_'+i.toString()).value='';
						// document.getElementById('longitude_'+i.toString()).value='';
					// }
					// else
						// remove it?
						document.getElementById("line-data").removeChild(obj);
				}
			}
		} );
		
		// once done, you can start removing points?
		// GEvent.addListener(polyline, "endline", function() {
			// select("hand_b");
			// var cells = addFeatureEntry(name, color);
			// GEvent.bind(polyline, "lineupdated", cells.desc, onUpdate);
			GEvent.addListener(polyline, "click", function(latlng, index) {
				if (typeof index == "number") {
					polyline.deleteVertex(index);
				} 
			});
		// });
		
	}
}

function updateLineMarker() {
	var count = 0;
	var vertices = new Array();
	while (document.getElementById('latitude_'+count.toString()) )
	{
		var lat = document.getElementById('latitude_'+count.toString()).value;
		var lng = document.getElementById('longitude_'+count.toString()).value;
		if (!lat || lat == "" || lat == null || !lng || lng == "" || lng == null)
			return false; // do nothing if we have an empty text input value
		var markerSpot = new GLatLng(lat,lng);
		vertices.push(markerSpot);
		count++;
	} 
	switch (geomap_mode) {
		case 'polygon_mode':
			break;
		case 'point_mode':
			break;
		case 'line_mode':
		default:
			lineLoad(null,null,null,null,null, vertices);
			break;
	}
}

// let's set up the line loader?
function polygonLoad(lat1, lng1, lat2, lng2, lat3, lng3, zoom, vertices) {
	if (GBrowserIsCompatible()) {

		// let's declare some variables
		var lineCenter;
		if (lat1 == null)
			lat1 = 30.05;
		if (lng1 == null)
			lng1 = 31.366667;			
		var markerSpot = new GLatLng(lat1, lng1);		
		/*
		// do we already have vertices?
		if (vertices == null || vertices.length == 0)
		{
			lat1 = 30.05;
			lng1 = 31.366667;			
			lat2 = 31.05;
			lng2 = 32.366667;			
			lat3 = 31.05;
			lng3 = 31.366667;			
			lat4 = 30.05;
			lng4 = 31.366667;			
			
			var markerSpot1 = new GLatLng(lat1, lng1);
			var markerSpot2 = new GLatLng(lat2, lng2);
			var markerSpot3 = new GLatLng(lat3, lng3);
			var markerSpot4 = new GLatLng(lat4, lng4);
			var lineCenterLat = (lat1 + lat2 + lat3 + lat4) / 4.0;
			var lineCenterLng = (lng1 + lng2 + lat3 + lat4) / 4.0;
			lineCenter = new GLatLng(lineCenterLat,lineCenterLng);
			vertices = new Array();
			vertices.push(markerSpot1);
			vertices.push(markerSpot2);
			vertices.push(markerSpot3);
			vertices.push(markerSpot4);
		}
		else
		{
			// alert(vertices.length);
			// determine the 'centering' point for this line
			var latSum = 0;
			var lngSum = 0;
			for (var i = 0; i < vertices.length; i++)
			{
				latSum += parseFloat(vertices[i].lat());
				lngSum += parseFloat(vertices[i].lng());
			}
			var lineCenterLat = (latSum) / vertices.length;
			var lineCenterLng = (lngSum) / vertices.length;
			lineCenter = new GLatLng(lineCenterLat,lineCenterLng);
		}
		gpolygon = vertices;
		*/
		
		// retrieve the zoom level
		if (zoom < 0 || zoom == null)
			zoom_level = 6;
		else
			zoom_level = zoom;

		// make the map
		map = new GMap2(document.getElementById("map"));
		// map.removeOverlay(polyline);
		// map.removeOverlay(polygon);
		map.clearOverlays();
		
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		map.setMapType(G_NORMAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		
		map.setCenter(markerSpot, zoom_level);

        var polyOptions = {geodesic:true};
		// gpolygon.push(markerSpot1);
		// gpolygon.push(markerSpot2);
		// gpolygon.push(markerSpot3);
        var polygon = new GPolygon([], "#ff0000", 10, 1, polyOptions);
        map.addOverlay(polygon);
		
		polygon.enableDrawing();
		polygon.enableEditing({onEvent: "mouseover"});
		polygon.disableEditing({onEvent: "mouseout"});
		
		
		// let's add an event for when the line is updated?
		GEvent.addListener(polygon, "lineupdated", function() { 
			// this is where we could modify the coordinate pieces
			var inputdivCount = getPolygonInputDivCount();
			// let's populate the text inputs
			for (var i = 0; i < (polygon.getVertexCount() -1); i++)
			{
				var vertex = polygon.getVertex(i);
				var pid = "polygon-geodata_" + i.toString();
				if (document.getElementById(pid))
				{
					// update the values;
					var lat_id = "pg_latitude_" + i.toString();
					document.getElementById(lat_id).value = vertex.lat();
					var lng_id = "pg_longitude_" + i.toString();
					document.getElementById(lng_id).value = vertex.lng();
				}
				else
				{
					// do something!
					// probably clone the 0 p, change the id's
					var zero_obj = document.getElementById('polygon-geodata_0').cloneNode(true);
					zero_obj.setAttribute("id",'polygon-geodata_'+i.toString());
					// update the values;
					var label_text = "Point "+ (i+1).toString();
					zero_obj.childNodes[1].replaceChild(document.createTextNode(label_text), zero_obj.childNodes[1].childNodes[0]);
					var lat_id = "pg_latitude_" + i.toString();
					// this seems to be causing issues for IE7
					// alert(zero_obj.childNodes[3]);
					zero_obj.childNodes[3].childNodes[4].setAttribute('id',"pg_latitude_" + i.toString());
					zero_obj.childNodes[3].childNodes[4].value = vertex.lat();
					var lng_id = "pg_longitude_" + i.toString();
					zero_obj.childNodes[5].childNodes[4].setAttribute('id',"pg_longitude_" + i.toString());
					zero_obj.childNodes[5].childNodes[4].value = vertex.lng();
					// where do I append this?
					document.getElementById("polygon-data").appendChild(zero_obj);
				}
			}
			// let's remove any coordinates that no longer exist?
			for (var i = (polygon.getVertexCount()-1); i < inputdivCount; i++)
			{
				var obj = document.getElementById('polygon-geodata_'+i.toString());
				if (obj && i > 0)
				{
					// remove it?
					document.getElementById("polygon-data").removeChild(obj);
				}
			}
		} );
		
		// once done, you can start removing points?
		// GEvent.addListener(polygon, "endline", function() {
			// select("hand_b");
			// var cells = addFeatureEntry(name, color);
			// GEvent.bind(polyline, "lineupdated", cells.desc, onUpdate);
			GEvent.addListener(polygon, "click", function(latlng, index) {
				if (typeof index == "number") {
					polygon.deleteVertex(index);
				} 
			});
		// });
	}
}

function nextLineGmarkerNum() {
	var newNodeNum = 0;
	var newNodeStr = "latitude_" + newNodeNum.toString();
	while (document.getElementById(newNodeStr) )
	{
		newNodeNum++;
		newNodeStr = "timerow_" + newNodeNum.toString();
	}
	// we just want the index number to return
	return newNodeNum;
}

function getLineInputDivCount() {
	var newNodeNum = 0;
	var newNodeStr = "line-geodata_" + newNodeNum.toString();
	while (document.getElementById(newNodeStr) )
	{
		newNodeNum++;
		newNodeStr = "line-geodata_" + newNodeNum.toString()		
	}
	// we just want the index number to return
	return newNodeNum;
}

function getPolygonInputDivCount() {
	var newNodeNum = 0;
	var newNodeStr = "polygon-geodata_" + newNodeNum.toString();
	while (document.getElementById(newNodeStr) )
	{
		newNodeNum++;
		newNodeStr = "polygon-geodata_" + newNodeNum.toString()		
	}
	// we just want the index number to return
	return newNodeNum;
}

// polyline is a global?
function displayLineCoordinates() {
	var inputdivCount = getLineInputDivCount();
	// alert(inputdivCount);
	// let's populate the text inputs
	for (var i = 0; i < polyline.getVertexCount(); i++)
	{
		var vertex = polyline.getVertex(i);
		var pid = "line-geodata_" + i.toString();
		if (document.getElementById(pid))
		{
			// update the values;
			var lat_id = "latitude_" + i.toString();
			document.getElementById(lat_id).value = vertex.lat();
			var lng_id = "longitude_" + i.toString();
			document.getElementById(lng_id).value = vertex.lng();
		}
		else
		{
			// do something!
			// probably clone the 0 p, change the id's
			// note - this still doesn't handle if i've deleted a point (end loop condition as max of existing input divs / vertex count?)
		}
	}	
}

// i need a way to add a new line coordinate via textboxes
function newLineInput() {
	var inputdivCount = getLineInputDivCount();
	var zero_obj = document.getElementById('line-geodata_0').cloneNode(true);
	zero_obj.setAttribute("id",'line-geodata_'+inputdivCount.toString());
	// update the values;
	var label_text = "Point "+ (inputdivCount+1).toString();
	// this is suposed to replace the label?
	zero_obj.childNodes[1].replaceChild(document.createTextNode(label_text), zero_obj.childNodes[1].childNodes[0]);
	var lat_id = "pg_latitude_" + inputdivCount.toString();
	// this seems to be causing issues for IE7
	alert(zero_obj.childNodes[3].childNodes[1]);
	// TODO - fill for the rest
	zero_obj.childNodes[3].childNodes[1].setAttribute('for',"latitude_" + inputdivCount.toString());
	zero_obj.childNodes[3].childNodes[4].setAttribute('id',"latitude_" + inputdivCount.toString());
	zero_obj.childNodes[3].childNodes[4].value = '';
	var lng_id = "pg_longitude_" + inputdivCount.toString();
	zero_obj.childNodes[5].childNodes[4].setAttribute('id',"longitude_" + inputdivCount.toString());
	zero_obj.childNodes[5].childNodes[4].value = '';
	// where do I append this?
	document.getElementById("line-data").appendChild(zero_obj);	
}