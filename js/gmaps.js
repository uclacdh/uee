// archived on Nov 24, 2008
// switching text inputs to a textarea box

// google maps behaviors (for data entry: place records)
var gmarker = null; // this is for point_mode;
var gpolyline = new Array(); // this is for line_mode;
var gpolygon = new Array(); // this is for polygon_mode;
// var polyline; // we want these here for being able to remove the overlays
// var polygon; 
var geomap_mode = "point_mode";
var coord_mode = 'latlong_mode';
var map;
var zoom_level;
//<![CDATA[
function load(lat, lng, zoom) {
	// just run this on load - an initializer
	document.getElementById('point_mode').className = 'selected';
	document.getElementById('line_mode').className = '';
	document.getElementById('polygon_mode').className = '';
	if (coord_mode == 'latlong_mode')
	{
		document.getElementById('latlong_mode').className = 'selected';
		document.getElementById('longlat_mode').className = '';
	}
	if (coord_mode == 'longlat_mode')
	{
		document.getElementById('latlong_mode').className = '';
		document.getElementById('longlat_mode').className = 'selected';
	}
	geomap_mode = 'point_mode';
	if (GBrowserIsCompatible()) {
		// if (lat == null)
			// lat = 30.05;
		// if (lng == null)
			// lng = 31.366667;	
		var centerSpot = new GLatLng(30.05, 30.366667);
		var markerSpot = null;
		if (lat != null && lng != null)
			markerSpot = new GLatLng(lat, lng);

		// retrieve the zoom level
		if (zoom < 0 || zoom == null)
			zoom_level = 5;
		else
			zoom_level = zoom;

		// make the map
		map = new GMap2(document.getElementById("map"));
		new GKeyboardHandler(map);
		map.clearOverlays();
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		map.setMapType(G_NORMAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		
		map.setCenter(centerSpot, zoom_level);		
		// display the marker
		// if marker exists
		if (markerSpot != null)
		{
			gmarker = new GMarker(markerSpot, {draggable: true});
			map.addOverlay(gmarker);
			// initialize the text inputs
			var geodata = gmarker.getLatLng().lat().toString() + ", " + gmarker.getLatLng().lng().toString() + "\n"; 
			document.getElementById('geodata').value = geodata;
		}
		// add my map listener for clicks?
		GEvent.addListener(map,"click", function(overlay, latlong) {
			// reset the marker?
			if (gmarker)
				map.removeOverlay(gmarker);
			gmarker = new GMarker(latlong, {draggable: true});
			// some draggable behaviors
			GEvent.addListener(gmarker, "drag", function() { 
				var geodata;
				switch (coord_mode) {
					case 'latlong_mode':
						geodata	= gmarker.getLatLng().lat().toString() + ", " + gmarker.getLatLng().lng().toString() + "\n"; 
						break;
					case 'longlat_mode':
						geodata	= gmarker.getLatLng().lng().toString() + ", " + gmarker.getLatLng().lat().toString() + "\n"; 
						break;
					default:
						break;
				}
				document.getElementById('geodata').value = geodata;		
			});
			GEvent.addListener(gmarker, "dragend", function() { 
				var geodata = gmarker.getLatLng().lat().toString() + ", " + gmarker.getLatLng().lng().toString() + "\n"; 
				document.getElementById('geodata').value = geodata;	
			});				
			map.addOverlay(gmarker);
			map.panTo(gmarker.getLatLng());
			var geodata = gmarker.getLatLng().lat().toString() + ", " + gmarker.getLatLng().lng().toString() + "\n"; 
			document.getElementById('geodata').value = geodata;
		});

	}
}

function updateMarker() {
	var lat = document.getElementById('latitude').value;
	var lng = document.getElementById('longitude').value;
	// do nothing if we have an empty text input value
	if (!lat || lat == "" || lat == null || !lng || lng == "" || lng == null)
		return false;
	
	switch (geomap_mode) {
		case 'point_mode':
		default:
			load(lat,lng,map.getZoom());
			break;
	}
}

// let's set up the line loader?
function lineLoad(maptype, zoom, vertices, mapcenter) {
	if (GBrowserIsCompatible()) {
		// let's declare some variables
		var lineCenter;
		lat1 = 30.05;
		lng1 = 31.366667;			
		var markerSpot = new GLatLng(lat1, lng1);	

		if (vertices == null || vertices.length == 0)
			vertices = [];
		
		if (maptype == null)
			maptype = G_NORMAL_MAP;
		
		// gpolyline = vertices;
		// retrieve the zoom level
		if (zoom < 0 || zoom == null)
			zoom_level = 6;
		else
			zoom_level = zoom;

		// make the map
		map = new GMap2(document.getElementById("map"));
		new GKeyboardHandler(map);
		// let's remove any overlays
		// map.removeOverlay(polyline);
		// map.removeOverlay(polygon);
		
		map.clearOverlays();
		
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		map.setMapType(maptype); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		
		map.setCenter(mapcenter, zoom_level);

		// add my map listener for clicks?
        var polyOptions = {geodesic:true};		
		var polyline = new GPolyline(vertices, "#ff0000", 10, 1, polyOptions);
		map.addOverlay(polyline);	
		if (vertices.length == 0)
			polyline.enableDrawing();
		polyline.enableEditing({onEvent: "mouseover"});
		polyline.disableEditing({onEvent: "mouseout"});				
				
		// let's add an event for when the line is updated?
		GEvent.addListener(polyline, "lineupdated", function() { 
			// this is where we could modify the coordinate pieces
			var inputdivCount = getLineInputDivCount();
			// let's populate the text inputs
			var geodata = "";
			for (var i = 0; i < polyline.getVertexCount(); i++)
			{
				switch (coord_mode) {
					case 'latlong_mode':
						geodata	= geodata + polyline.getVertex(i).lat().toString() + ", " + polyline.getVertex(i).lng().toString() + "\n"; 
						break;
					case 'longlat_mode':
						geodata	= geodata + polyline.getVertex(i).lng().toString() + ", " + polyline.getVertex(i).lat().toString() + "\n"; 
						break;
					default:
						break;
				}
			}
			document.getElementById('geodata').value = geodata;

			if ( map.getBoundsZoomLevel(polyline.getBounds()) < map.getZoom() ) 
				map.setCenter( (polyline.getBounds()).getCenter(), map.getBoundsZoomLevel(polyline.getBounds()) );
		} );
		
		// once done, you can start removing points?
		// GEvent.addListener(polyline, "endline", function() {
			// select("hand_b");
			// var cells = addFeatureEntry(name, color);
			// GEvent.bind(polyline, "lineupdated", cells.desc, onUpdate);
			GEvent.addListener(polyline, "click", function(latlng, index) {
				if (typeof index == "number") {
					polyline.deleteVertex(index);
					// let's populate the text inputs
					var geodata = "";
					for (var i = 0; i < polyline.getVertexCount(); i++)
					{
						switch (coord_mode) {
							case 'latlong_mode':
								geodata	= geodata + polyline.getVertex(i).lat().toString() + ", " + polyline.getVertex(i).lng().toString() + "\n"; 
								break;
							case 'longlat_mode':
								geodata	= geodata + polyline.getVertex(i).lng().toString() + ", " + polyline.getVertex(i).lat().toString() + "\n"; 
								break;
							default:
								break;
						}
						// geodata = geodata + gmarker.getLatLng().lat().toString() + ", " + gmarker.getLatLng().lng().toString() + "\n"; 
					}
					document.getElementById('geodata').value = geodata;					
				} 
			});
		// });
		
	}
}

function updateLineMarker() {
	var count = 0;
	var vertices = new Array();
	while (document.getElementById('latitude_'+count.toString()) )
	{
		var lat = document.getElementById('latitude_'+count.toString()).value;
		var lng = document.getElementById('longitude_'+count.toString()).value;
		if (!lat || lat == "" || lat == null || !lng || lng == "" || lng == null)
			return false; // do nothing if we have an empty text input value
		var markerSpot = new GLatLng(lat,lng);
		vertices.push(markerSpot);
		count++;
	} 
	switch (geomap_mode) {
		case 'polygon_mode':
			break;
		case 'point_mode':
			break;
		case 'line_mode':
		default:
			lineLoad(map.getCurrentMapType(),map.getZoom(), vertices);
			break;
	}
}

// let's set up the line loader?
function polygonLoad(maptype, zoom, vertices, mapCenter) {
	document.getElementById('point_mode').className = '';
	document.getElementById('line_mode').className = '';
	document.getElementById('polygon_mode').className = 'selected';
	geomap_mode = 'polygon_mode';
	if (GBrowserIsCompatible()) {
		
		if (maptype == null)
			maptype = G_NORMAL_MAP;		
			
		// retrieve the zoom level
		if (zoom < 0 || zoom == null)
			zoom_level = 6;
		else
			zoom_level = zoom;

		if (vertices == null || vertices.length == 0)
			vertices = [];
			
		// make the map
		map = new GMap2(document.getElementById("map"));
		new GKeyboardHandler(map);
		// map.removeOverlay(polyline);
		// map.removeOverlay(polygon);
		map.clearOverlays();
		
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		map.setMapType(maptype); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		
		map.setCenter(mapCenter, zoom_level);

        var polyOptions = {geodesic:true};
        var polygon = new GPolygon(vertices, "#ff0000", 10, 1, polyOptions);
        map.addOverlay(polygon);
		
		if (vertices.length == 0)
			polygon.enableDrawing();
		polygon.enableEditing({onEvent: "mouseover"});
		polygon.disableEditing({onEvent: "mouseout"});
			
		// let's add an event for when the line is updated?
		GEvent.addListener(polygon, "lineupdated", function() { 

			// let's populate the text inputs
			var geodata = "";
			for (var i = 0; i < polygon.getVertexCount() ; i++)
			{
				switch (coord_mode) {
					case 'latlong_mode':
						geodata	= geodata + polygon.getVertex(i).lat().toString() + ", " + polygon.getVertex(i).lng().toString() + "\n"; 
						break;
					case 'longlat_mode':
						geodata	= geodata + polygon.getVertex(i).lng().toString() + ", " + polygon.getVertex(i).lat().toString() + "\n"; 
						break;
					default:
						break;
				}
			}
			document.getElementById('geodata').value = geodata;
			// we want to readjust to the center of the map and change the zoom level if necessary
			// alert(map.getBoundsZoomLevel(polygon.getBounds()).toString() + ", " + map.getZoom().toString() );
			if ( map.getBoundsZoomLevel(polygon.getBounds()) < map.getZoom() ) 
				map.setCenter( (polygon.getBounds()).getCenter(), map.getBoundsZoomLevel(polygon.getBounds()) );
		} );
		
		// once done, you can start removing points?
		// GEvent.addListener(polygon, "endline", function() {
			// select("hand_b");
			// var cells = addFeatureEntry(name, color);
			// GEvent.bind(polyline, "lineupdated", cells.desc, onUpdate);
			GEvent.addListener(polygon, "click", function(latlng, index) {
				if (typeof index == "number") {
					polygon.deleteVertex(index);
					var geodata = "";
					for (var i = 0; i < polygon.getVertexCount() ; i++)
					{
						switch (coord_mode) {
							case 'latlong_mode':
								geodata	= geodata + polygon.getVertex(i).lat().toString() + ", " + polygon.getVertex(i).lng().toString() + "\n"; 
								break;
							case 'longlat_mode':
								geodata	= geodata + polygon.getVertex(i).lng().toString() + ", " + polygon.getVertex(i).lat().toString() + "\n"; 
								break;
							default:
								break;
						}
					}
					document.getElementById('geodata').value = geodata;					
				} 
			});
		// });
		
		// return something
		return polygon;
	}
}

function nextLineGmarkerNum() {
	var newNodeNum = 0;
	var newNodeStr = "latitude_" + newNodeNum.toString();
	while (document.getElementById(newNodeStr) )
	{
		newNodeNum++;
		newNodeStr = "timerow_" + newNodeNum.toString();
	}
	// we just want the index number to return
	return newNodeNum;
}

function getLineInputDivCount() {
	var newNodeNum = 0;
	var newNodeStr = "line-geodata_" + newNodeNum.toString();
	while (document.getElementById(newNodeStr) )
	{
		newNodeNum++;
		newNodeStr = "line-geodata_" + newNodeNum.toString()		
	}
	// we just want the index number to return
	return newNodeNum;
}

function getPolygonInputDivCount() {
	var newNodeNum = 0;
	var newNodeStr = "polygon-geodata_" + newNodeNum.toString();
	while (document.getElementById(newNodeStr) )
	{
		newNodeNum++;
		newNodeStr = "polygon-geodata_" + newNodeNum.toString()		
	}
	// we just want the index number to return
	return newNodeNum;
}

// polyline is a global?
function displayLineCoordinates() {
	var inputdivCount = getLineInputDivCount();
	// alert(inputdivCount);
	// let's populate the text inputs
	for (var i = 0; i < polyline.getVertexCount(); i++)
	{
		var vertex = polyline.getVertex(i);
		var pid = "line-geodata_" + i.toString();
		if (document.getElementById(pid))
		{
			// update the values;
			var lat_id = "latitude_" + i.toString();
			document.getElementById(lat_id).value = vertex.lat();
			var lng_id = "longitude_" + i.toString();
			document.getElementById(lng_id).value = vertex.lng();
		}
		else
		{
			// do something!
			// probably clone the 0 p, change the id's
			// note - this still doesn't handle if i've deleted a point (end loop condition as max of existing input divs / vertex count?)
		}
	}	
}

// i need a way to add a new line coordinate via textboxes
function newLineInput() {
	var inputdivCount = getLineInputDivCount();
	var zero_obj = document.getElementById('line-geodata_0').cloneNode(true);
	zero_obj.setAttribute("id",'line-geodata_'+inputdivCount.toString());
	// update the values;
	var label_text = "Point "+ (inputdivCount+1).toString();
	// this is suposed to replace the label?
	zero_obj.childNodes[1].replaceChild(document.createTextNode(label_text), zero_obj.childNodes[1].childNodes[0]);
	var lat_id = "pg_latitude_" + inputdivCount.toString();
	// this seems to be causing issues for IE7
	alert(zero_obj.childNodes[3].childNodes[1]);
	// TODO - fill for the rest
	zero_obj.childNodes[3].childNodes[1].setAttribute('for',"latitude_" + inputdivCount.toString());
	zero_obj.childNodes[3].childNodes[4].setAttribute('id',"latitude_" + inputdivCount.toString());
	zero_obj.childNodes[3].childNodes[4].value = '';
	var lng_id = "pg_longitude_" + inputdivCount.toString();
	zero_obj.childNodes[5].childNodes[4].setAttribute('id',"longitude_" + inputdivCount.toString());
	zero_obj.childNodes[5].childNodes[4].value = '';
	// where do I append this?
	document.getElementById("line-data").appendChild(zero_obj);	
}

function updateMarkersFromTextbox() {
	var textInput = document.getElementById('geodata').value;
	var geocoords = textInput.split("\n");
	// alert(geocoords.length);
	var vertices = [];
	for (var i = 0; i < geocoords.length; i++)
	{
		if (geocoords[i].length > 0)
		{
			var geopoint = (geocoords[i].replace(" ","")).split(",");
			vertices.push(new GLatLng(geopoint[0],geopoint[1]));
			// alert(geopoint[0]);
			// alert(geopoint[1]);	
		}
	}
	// alert(vertices.length.toString() + ", " + geomap_mode.toString());
	switch (geomap_mode) {
		case 'point_mode':
			if (vertices.length > 1)
			{
				var shape = polygonLoad(map.getCurrentMapType(), map.getZoom(), vertices, map.getCenter() );
				// set the zoom as necessary
				map.setCenter( (shape.getBounds()).getCenter(), map.getBoundsZoomLevel(shape.getBounds()) );
			}
			else if (vertices.length > 0)
				load(vertices[0].lat(), vertices[0].lng(), map.getZoom());
			else
				load(null,null,map.getZoom());
			break;
		case 'line_mode':
			lineLoad(map.getCurrentMapType(), map.getZoom(), vertices, map.getCenter() );
			break;
		case 'polygon_mode':
			var shape = polygonLoad(map.getCurrentMapType(), map.getZoom(), vertices, map.getCenter() );
			// set the zoom as necessary
			map.setCenter( (shape.getBounds()).getCenter(), map.getBoundsZoomLevel(shape.getBounds()) );
			break;
		default:
			break;
	}
}

function swapCoordinates() {
	var textInput = document.getElementById('geodata').value;
	var geocoords = textInput.split("\n");
	// alert(geocoords.length);
	var vertices = [];
	for (var i = 0; i < geocoords.length; i++)
	{
		if (geocoords[i].length > 0)
		{
			var geopoint = (geocoords[i].replace(" ","")).split(",");
			vertices.push(new GLatLng(geopoint[0],geopoint[1]));
			// alert(geopoint[0]);
			// alert(geopoint[1]);	
		}
	}
	var newtextInput = '';
	for (var i = 0; i < vertices.length; i++)
	{
		newtextInput = newtextInput + vertices[i].lng().toString() + ", " + vertices[i].lat().toString() + "\n";
	}
	document.getElementById('geodata').value = newtextInput;
	updateMarkersFromTextbox();
}