$(document).ready(function() {
	 mapInit()
});

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

// Copyright 2011 UCLA Academic Technology Services

/**
 * @fileoveriew TODO - Description of file, its uses and information
 * author adtai@ats.ucla.edu (Andrew Tai)
 */

/**
 * article5.js
 * 
 * Set up a series of dynamic behaviors to be configured on runtime
 * These should be behaviors specific to the display of articles on UEE
 *
 * Dependencies:
 * - Requires Jquery (what version?)
 * - Requires 'DataTables' jquery plugin
 * - Requires Google Maps API (what version)
 */

var startDate = new Date();
$(document).ready( function () {

	// capture page scrolling, adjust the position of the sidebar
	$(window).scroll(function() {
		var p = $("#toc-wrapper").offset();
		// alert("toc-wrapper top:" + p.top);
		var win = $(window).scrollTop();
		// alert("window top:" + win);
		var top = Math.floor(p.top);
		var newtop = 0;
		if (win > 250)
			newtop = 0;
		else
			newtop = 250 - win;
		if (newtop < 0)
			newtop = 0;
		$("#toc-wrapper").css("top",newtop+"px");
		$("#popup-wrapper").css("top",newtop+"px");
	});
	
	// toggle the TOC column
	$("a#tocColumn-toggle").click(function() {
		if ($("#leftcol").css("display") != "none") {
			$("#leftcol").hide("fast", function() {
				$("#maincol").css("width","78%");
			});
			$("#tocColumn-toggle img").attr("src","/graphics/TOC_Show.png").attr("alt","Show Table of Contents");
		}
		else {
			$("#maincol").css("width","58%");
			$("#leftcol").show("fast");
			$("#tocColumn-toggle img").attr("src","/graphics/TOC_Hide.png").attr("alt","Hide Table of Contents");
		}
	});
		
	// toggle the myUEE window 
	$("a#myuee-toggle").click( function() {
		$("div#myuee").slideToggle("slow");
	} );
	
	$("div#myuee a#close-myuee").click( function() {
		$("div#myuee").slideUp("slow");
	} );
	
	// onload gloss links
	if (!$("input#glosses-toggle").attr("checked") )
	{
		// let's strip some of the CSS (the underline only i think?)
		$("a.glossref").css("borderBottom","none").css("cursor","text").css("color","rgb(70,70,70)");
		// remove the popup events
		$("a.glossref").unbind("click"); // this is sufficient; because then there's not a way to trigger mouseout / blur
	}	

	// onload bibls
	if (!$("input#biblio-toggle").attr("checked") )
	{
		// let's strip some of the CSS (the underline only i think?)
		$("a.biblioref").css("borderBottom","none").css("cursor","text").css("color","rgb(70,70,70)").children("img").hide();
		// remove the popup events
		$("a.biblioref").unbind("click"); // this is sufficient; because then there's not a way to trigger mouseout / blur
	}	
	
	// onload links toggle
	if (!$("input#links-toggle").attr("checked") )
	{
		// let's strip some of the CSS (the underline only i think?)
		$("a.articleref").css("textDecoration","none").css("fontWeight","normal").css("color","#5D737C;").css("cursor","text");
	}
		
	// onload places toggle
	if (!$("input#places-toggle").attr("checked") )
	{
		// let's strip some of the CSS (the underline only i think?)
		$("img.place-icon").css("display","none");	
	}
	
	// toggle place icons
	$("input#places-toggle").click( function() {
		if ($(this).attr("checked") ) {
			// restore the css
			$("img.place-icon").css("display","inline");	
		}
		else {
			// let's strip some of the CSS (the underline only i think?)
			$("img.place-icon").css("display","none");			
		}
	});
	
	// toggle gloss links
	$("input#glosses-toggle").click( function() {
		if ($(this).attr("checked") )
		{
			// alert("trying to toggle glosses"); 
			// restore the css
			$("a.glossref").css("borderBottom","1px dotted gray").css("cursor","pointer").css("color","rgb(105,128,140)");
			// restore the popup events
			$("a.glossword").bind("click", function glossover(e) { // over
				// if you click one, remove any other existing popups:
				$(".gloss-popup").remove();
				$(".biblio-popup").remove();
				// $("#rightcol p:first").remove();
				$("#popup-content").hide();
				// hooray for jQuery trim!
				var glossName = jQuery.trim($(this).text());
				var glossId = jQuery.trim($(this).attr("name"));
				// this function represents the ajax call
				// load Glossary uses the ajax call and processes the results
				loadGlossary2(glossId, glossName, $(this));
			});
		}
		else
		{
			// remove any existing popups
			$("#popup-content").hide();
			// let's strip some of the CSS (the underline only i think?)
			$("a.glossref").css("borderBottom","none").css("cursor","text").css("color","rgb(70,70,70)");
			// remove the popup events
			$("a.glossref").unbind("click"); // this is sufficient; because then there's not a way to trigger mouseout / blur
		}
	} );

	// toggle biblio reference links
	$("input#biblio-toggle").click( function() {
		if ($(this).attr("checked") )
		{
			// restore the css
			$("a.biblioref").css("borderBottom","1px dashed gray").css("cursor","pointer").css("color","rgb(105,128,140)").children("img").show();
			// restore the popup events
			$("a.biblioref").bind("click", function (e) { // over
				// if you click one, remove any other existing popups:
				$(".gloss-popup").remove();
				$(".biblio-popup").remove();
				// $("#rightcol p:first").remove();
				$("#popup-content").hide();
				// hooray for jQuery trim!
				var biblioName = jQuery.trim($(this).text());
				var biblioTarget = $(this).attr("name");
				// this function represents the ajax call
				loadReference(biblioName, biblioTarget, $(this));	
			});
		}
		else
		{
			// hide any existing popups
			$("#popup-content").hide();
			// let's strip some of the CSS (the underline only i think?)
			$("a.biblioref").css("borderBottom","none").css("cursor","text").css("color","rgb(70,70,70)").children("img").hide();
			// remove the popup events
			$("a.biblioref").unbind("click"); // this is sufficient; because then there's not a way to trigger mouseout / blur
		}
	} );
	
	// toggle article links 
	$("input#links-toggle").click( function() {
		if ($(this).attr("checked") )
		{
			$("a.articleref").css("textDecoration","underline").css("cursor","pointer").css("color","#69808C");		
		}
		else {
			$("a.articleref").css("textDecoration","none").css("cursor","text").css("color","rgb(70,70,70)");		
		}
	});


	// edit annotations?
	$("a.annotation_edit").click( function() {
		var text = $(this).parent("p").text().substr(0, ($(this).parent("p").text().length - 5) );
		$("#myuee-comments").val(text);
		$("#annotation_id").val($(this).attr("name") );
	} );
	
	// save user settings?
	$("input#save-comments-button").click( function() {
		$.get("/modules/front/ajax/save_annotations.php", { article_id: $("input#article_id").val(), annotation:$("#myuee-comments").val(), annotation_id:$("#annotation_id").val() }, function(data) {
			// alert(data);
			$("label#toggles-save-status").hide().html(data).show().fadeOut(3000);
			$("#myuee-comments").val("");
			$("#annotation_id").val("");
			// refresh the annotations list?
			$.get("/modules/front/ajax/get_annotations.php", { article_id: $("input#article_id").val() }, function(moredata) {
				if (!moredata || moredata.length <= 0) {
					alert("no data");
					return;
				}
				var arr = eval("("+moredata+")");
				if (arr.length <= 0)
					return;
				$("div#myannotations").empty();
				for (var i = 0; i < arr.length; i++)
				{
					// alert(arr[i].id + ": " + arr[i].annotation);
					var newp = "<p>"+arr[i].annotation+" <a class='annotation_edit' href='javascript:;' name='"+arr[i].id+"'>Edit</a></p>";
					$("div#myannotations").append(newp);
					// edit annotations?
					$("a.annotation_edit").click( function() {
						var text = $(this).parent("p").text().substr(0, ($(this).parent("p").text().length - 5) );
						$("#myuee-comments").val(text);
						$("#annotation_id").val($(this).attr("name") );
					} );
				}
			} );
			return;
		} );
	});
		
	$("input#save-toggles-button").click( function() {
		var glosses;
		var links;
		var biblios;
		var places;
		if ($("input#glosses-toggle").attr("checked") )
			glosses = 1;
		else
			glosses = 0;
		if ($("input#links-toggle").attr("checked") )
			links = 1;
		else
			links = 0;
		if ($("input#biblio-toggle").attr("checked") )
			biblios = 1;
		else
			biblios = 0;
		if ($("input#places-toggle").attr("checked") )
			places = 1;
		else
			places = 0;
		$.get("/modules/front/ajax/save_user_settings.php", { glosses:glosses, links:links, biblios:biblios, places:places }, function(data) {
			$("label#toggles-save-status").hide().html(data).show().fadeOut(3000);
			return;
		} );
	} );
	
	// section toggles
	$("a.section-toggle").click( function() {
		if (this.id != 'minimap-toggle')
			$(this).parent("div.section-head").next("div").toggle("slow");
		else
			$("div#minimap-section").children().toggle("slow");
		if ($(this).children("img").attr("src") == "/graphics/Contract.png")
			$(this).children("img").attr("src","/graphics/Expand.png");
		else {
			$(this).children("img").attr("src","/graphics/Contract.png");
		}
			
	} );
	
	// adjust the google maps css
	$("div#mini-map div div div div.gmnoprint div.gmnoprint").height(100).width(200);

	var lightboxSrc, lightboxScale;
	// set the lightbox popup
	$("a.lightbox-link").click( function() {
		// what do we put in the lightbox img tag
		lightboxSrc = $(this).attr("href");
		lightboxScale = 0.8;
		$("div.lightbox-layer p img").attr("src",$(this).attr("href")+"&scaleFactor=0.8");
		$("div.lightbox-layer p").css("textAlign","center");
		// alert($("div.lightbox-layer p img").width());
		// var marginLeft = (960 - $("div.lightbox-layer p img").width())/(2.0);
		// alert(marginLeft);
		$("div.lightbox-layer").slideDown("fast",function() {
			$("div.lightbox-layer p img").width($("div.lightbox-layer p img").width());
			var marginLeft = (950 - $("div.lightbox-layer p img").width())/(2.0);
			$("div.lightbox-layer p.lightbox-wrapper").css("marginLeft",marginLeft+"px");
			
			; // setTimeout('$("div.lightbox-layer").slideUp("fast");',5000);
		});
		// suppress the click through
		return false;
	});
	
	$("div.lightbox-layer a.lightbox-close").click( function() {
		$("div.lightbox-layer").slideUp("fast");
	});
	
	$("div.lightbox-layer a.lightbox-zoomin").click( function() {
		var oldScale = lightboxScale;
		lightboxScale = lightboxScale + 0.1;
		var newWidth = Math.round( (lightboxScale/oldScale)*$("div.lightbox-layer p img").width());
		$("div.lightbox-layer p img").attr("src",lightboxSrc+"&scaleFactor="+lightboxScale);
		$("div.lightbox-layer p").css("textAlign","center");
		$("div.lightbox-layer").show("fast",function() {
			$("div.lightbox-layer p img").width(newWidth);
			var marginLeft = (950 - $("div.lightbox-layer p img").width())/(2.0);
			$("div.lightbox-layer p.lightbox-wrapper").css("marginLeft",marginLeft+"px");			
		});
	});
	
	$("div.lightbox-layer a.lightbox-zoomout").click( function() {
		var oldScale = lightboxScale;
		lightboxScale = lightboxScale - 0.1;
		var newWidth = Math.round( (lightboxScale/oldScale)*$("div.lightbox-layer p img").width());
		$("div.lightbox-layer p img").attr("src",lightboxSrc+"&scaleFactor="+lightboxScale);
		$("div.lightbox-layer p").css("textAlign","center");
		$("div.lightbox-layer").show("fast",function() {
			$("div.lightbox-layer p img").width(newWidth);
			var marginLeft = (950 - $("div.lightbox-layer p img").width())/(2.0);
			$("div.lightbox-layer p.lightbox-wrapper").css("marginLeft",marginLeft+"px");			
		});
	});
	
	// table of contents
	// clear the loading
	$("#toc ul").empty();
	// add the sections
	$("a.section-toggle").each( function(i) {
		var jqid;
		if (this.id != null && this.id.length > 0)
		{
			jqid = "a#"+this.id +" + .article-section-title";
			// alert(jqid);
			// alert($(jqid).text());
			var litag = "<li><a href=\"#"+this.id+"\">"+$(jqid).text()+"</a></li>";
			$("#toc ul").append(litag);
		}
	});
	
	var endDate = new Date();
	var scriptTime = endDate - startDate;
	var scriptTimeSeconds = scriptTime/1000.0;
	$("p.loadtime").append("scripts loaded in " + scriptTimeSeconds + " seconds");
});

// google maps behaviors (for data entry: place records)
// holders for the marker data
var gmarker = null; // this is for point_mode;
var gpolyline = new Array(); // this is for line_mode;
var gpolygon = new Array(); // this is for polygon_mode;

// poly options - something needed for drawing polygons
var polyOptions = {geodesic:true};		

// holders for the 'mode' of the marker placement
// var geomap_mode = "point_mode";
var geomap_mode = "";
var coord_mode = 'latlong_mode';
var map;
var cluster;
var marker;
var markersArray=[];

var addMarkerToggle = null; // just want a global so i can remove this behavior...

// mapInit()
// mapInit is responsible for loading the map - nothing more, nothing less
// it does not add overlays

// v3 maps
function mapInit()
{
	// in file: article5.js
	var zoom_level = 6;
	var centerSpot = new google.maps.LatLng(29.8,30.5); // this is pretty close to where Faiyum is...
	
	var myOptions = {
		zoom: zoom_level,
		center: centerSpot,
		
		// pan control
		panControl: true,
		
		// street  view
		streetViewControl: false,
		
		// scale control
		scaleControl: false,
		
		// zoom controls
		zoomControl: true,
			zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
    		},
		
		// map type control	
		mapTypeControl: true,
			mapTypeControl: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
			},
		
		mapTypeId: google.maps.MapTypeId.TERRAIN
	};

	// Rendering map
	map = new google.maps.Map(document.getElementById("mini-map"), myOptions);

	// for the minimap you
	// alert($("div#place_ids").text());
	$.get("/modules/front/ajax/get_place_geodata.php", { place_ids:$("div#place_ids").text() }, function(data) {	
		// alert(data);
		makeMarkers(data);
	});	
}
// end of mapInit()

/* v2 mapInit()
function mapInit() {
	// make the map
	if (GBrowserIsCompatible()) {	
		map = new GMap2(document.getElementById("mini-map"));
		if (!map)
		{
			alert('error creating map');
			return;
		}
		new GKeyboardHandler(map);
		map.clearOverlays();
		map.checkResize();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		// set the map type
		// feb 24, 2009- yoh wants the default to be the 'terrain' type.
		map.addMapType(G_PHYSICAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		map.setMapType(G_PHYSICAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		// feb 25, 2009
		map.addMapType(G_SATELLITE_3D_MAP);				
		var zoom_level = 6;
		var centerSpot = new GLatLng(29.8, 30.5); // this is pretty close to where Faiyum is...

		map.setCenter(centerSpot, zoom_level);		
	
		// for the minimap you
		// alert($("div#place_ids").text());
		$.get("/modules/front/ajax/get_place_geodata.php", { place_ids:$("div#place_ids").text() }, function(data) {	
			// alert(data);
			makeMarkers(data);
		});		
		
	}
	else
	{
		alert("Warning: Your browser is not compatible with the GoogleMaps API we are using.");
		return;
	}
}
*/


// v2 version
/*
// makeMarkers(data)
// clear all the existing markers
// take the json data, parse it and display the new markers....
function makeMarkers(data) {
	// clear the map of any existing markers
	if (data.length <= 0 || !data)
		return;
	var placeArr = eval("(" + data + ")");
	if (placeArr.length == 0)
		return;
	if (!map)
	{
		alert('error: cannot place markers without a map!');
		return;
	}
	map.clearOverlays();
	markersArray = [];
	// little bug with json data, needs to be string encapsulated
	// http://willcode4beer.com/tips.jsp?set=jsonInvalidLabel	
	var placeArr = eval("(" + data + ")");
	
	for (var i = 0; i < placeArr.length; i++)
	{
		var place = placeArr[i];
		// alert(place);
		// if (i == 0)
			// alert(place.place_type_id);
		marker = newMarker(new GLatLng(place.latitude, place.longitude), place);
		markersArray.push(marker);
		// addMarker(place);
	}
	// let's set up the cluster icons
	var clusterIcon = new GIcon(G_DEFAULT_ICON);
	clusterIcon.shadow = "";
	clusterIcon.image = "/graphics/Site_Multiple.png";
	clusterIcon.iconSize = new GSize(34,34);
	clusterIcon.imageMap = [2,3,29,3,29,30,2,30];
	
	var cluster = new ClusterMarker(map, { markers:markersArray, clusterMarkerIcon:clusterIcon } );
	cluster.fitMapToMarkers();
}
*/

// v3
// makeMarkers(data)
// take the json data, parse it and display the new markers....
function makeMarkers(data) {
	
	// clear the map of any existing markers
	if (map) {
	    // Clearing overlays...
        while(markersArray[0]) {
			//console.log(markersArray.length);
            markersArray.pop().setMap(null);
        }
        
        markersArray.length = 0;
	    markersArray = [];

    }
    
	// clear marker 
	if(marker) 
	{
		marker = '';
	}
	
	// from loop below    
    if(cluster)
    {
        cluster.clearMarkers();
    }
	
    // little bug with json data, needs to be string encapsulated
	// http://willcode4beer.com/tips.jsp?set=jsonInvalidLabel	
	if (data.length <= 0 || !data) 
	{
		return;
    }
	
    // alert(data);
	var placeArr = eval("(" + data + ")");
	
	// alert(placeArr);
	for (var i = 0; i < placeArr.length; i++)
	{
		var place = placeArr[i];
		var markerLatLng = new google.maps.LatLng(place.latitude, place.longitude);	
		
		// markersArray is being opulated here.
		// original
		// marker = newMarker(new google.maps.LatLng(place.latitude, place.longitude), place);
		marker = newMarker(new google.maps.LatLng(place.latitude, place.longitude), place);	
	}

    // marker cluster jenny's design
    
	var mcOptions = {styles: [{
		height: 36,
		width: 35,
		url: "/graphics/Site_Multiple.png",
		textColor: "FFFFFF",
		anchor: [1, 30]
		}],
		gridSize: 35, 
		maxZoom: 15
		};
	
	// cluster marker  default
	/*
	var mcOptions = {
		gridSize: 30, 
		maxZoom: 15
		};	
    */
	
	// rendering cluster
	cluster = new MarkerClusterer(map, markersArray, mcOptions);
	
	// something happens after clusterer ends.
	// oc: google.maps.event.addListener(markerClusterer, 'clusteringend', myFunction);
	// google.maps.event.addListener(cluster, 'clusteringend', alert('its the end!'));
}
// end makeMarkers()

// v3
function newMarker(markerLocation, place) {
    
    var myLatlng = markerLocation;
	
	// Switching on place.place_type_it...
    switch (place.place_type_id) {
        case 4: // site
            
			var markerIcon = new google.maps.MarkerImage(
    			'/graphics/Site_InUse.png',
        		new google.maps.Size(31, 31),
				null,
				new google.maps.Point(10,25),
				new google.maps.Size(30, 30)
 			);
			break;
			
		case 5: // feature
            
			var markerIcon = new google.maps.MarkerImage(
    			'/graphics/Feature_InUse.png',
        		new google.maps.Size(31, 31),
				null,
				new google.maps.Point(10,25),
				new google.maps.Size(30, 30)
 			);			
			break;
			
		default:
            break;
    }
	
	// console.log(marker);
    // return marker;
    
	// Rendering markers on map.
	var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: place.place_name,
        icon: markerIcon
    });


	// Render infoWindow
	var infoWindow = new google.maps.InfoWindow({
    	maxWidth: 200
  	});

	// internal function
	//==================================
	function load_content(marker, place)
	{
    	var myHtml = "<div class='geomarker-popup' style='height:100px;'><p><strong>" + place.place_name + "</strong><br />";
		var myHtml2 = '';
    	var counts;
    	// myHtml += "<em>From " + place.year_start + " to " + place.year_end + "</em></p><div id='sal-"+place.place_id+"'>loading...</div>";
    	myHtml += "<em>From " + place.year_start + " to " + place.year_end + "</em></p><div id='sal-"+place.place_id+"'></div>";
		//-- ajax stuff
    	$.get("/modules/front/ajax/get_search_counts.php", { place_name:place.place_name, old_id:place.old_id, place_id:place.place_id }, function(data) {
        
        	if (!data || data == null || data.length <= 0)
        	{
            	myHtml2 += '<p>There was an error getting the related information</p>';
            	return;
        	}
        
        	counts = eval("(" + data + ")");
        	place.articles = counts.articles;
        	place.images = counts.images;       
    
        	myHtml2 += "<ul>";
        	if (place.articles > 0) {
            	myHtml2 += "<li><a href='/search/placeId=" + place.place_id + "'>articles (" + place.articles + ")</a></li>";
        	}
        	if (place.images > 0) {
            	myHtml2 += "<li><a href='/imageresults/placeId="+ place.place_id + "'>images (" + place.images + ")</a></li>";
        	}
        	myHtml2 += "</ul></div>";
        
        	//console.log(myHtml2);
		
			//add link data to infowindow
			var salwithid = '#sal-'+place.place_id;   
			$(salwithid).html(myHtml2);

     	});
				    
    	infoWindow.setContent(myHtml);
    	infoWindow.open(map, marker);
	}
	//==================================
	// end: load_content()
	
    	google.maps.event.addListener(marker, 'click', function() {
 			infoWindow.close();
			load_content(marker, place);
    	});
    	markersArray.push(marker);
}
// end: newMarker()

// v2
/*
function newMarker(markerLocation, place) {
	// what kinda icon are we using?
	// let's set up some initial properties
	var myIcon = new GIcon(G_DEFAULT_ICON);
	myIcon.shadow = "";
	myIcon.iconAnchor = new GPoint(15,15); // do i need to adjust the positioning at all?
	myIcon.infoWindowAnchor = new GPoint(15,7); // do i need to adjust the positioning at all?
	switch (place.place_type_id) {
		case 4: // site
			myIcon.image = "/graphics/Site_InUse.png";
			// myIcon.iconSize = new GSize(87,87);
			myIcon.iconSize = new GSize(30,30);
			myIcon.imageMap = [3,2,25,2,25,24,3,24];
			break;
		case 5: // feature
			myIcon.image = "/graphics/Feature_InUse.png";
			// myIcon.iconSize = new GSize(88,87);
			myIcon.iconSize = new GSize(30,30);
			myIcon.imageMap = [3,2,25,2,25,24,3,24];
			break;
		default:
			break;
	}
	// create them marker!
	
	var marker = new GMarker (markerLocation, {title:place.place_name, icon:myIcon });	
	var loadingHtml = "<div class='geomarker-popup'><p><strong>Loading...</strong></p></div>";
	GEvent.addListener(marker,'click',function() {
		map.closeInfoWindow();
		map.openInfoWindowHtml(marker.getLatLng(),loadingHtml, {maxContent:loadingHtml, pixelOffset: new GSize(0,-5), maxWidth:200, onOpenFn: function() {
			// seems to cap min-width at 217px, 60 seems to be reasonable.
			// how do i control the info window tail size
			map.getInfoWindow().reset(map.getInfoWindow().getPoint(),map.getInfoWindow().getTabs(),new GSize(175,75),null,null);
		} } );
		var myHtml = "<div class='geomarker-popup'><p><strong>" + place.place_name + "</strong><br />";
		var counts;
		myHtml += "<em>From " + place.year_start + " to " + place.year_end + "</em></p>";
		$.get("/modules/front/ajax/get_search_counts.php", { place_name:place.place_name, old_id:place.old_id }, function(data) {
			if (!data || data == null || data.length <= 2)
			{
				myHtml += '<p>Error retreiving data</p>';
				map.closeInfoWindow();
				map.openInfoWindowHtml(marker.getLatLng(),myHtml, {maxContent:myHtml, pixelOffset: new GSize(0,-5), maxWidth:200, onOpenFn: function() {
					map.getInfoWindow().reset(map.getInfoWindow().getPoint(),map.getInfoWindow().getTabs(),new GSize(175,75),null,null);
				} } );
				return;
			}
			counts = eval("(" + data + ")");
			place.articles = counts.articles;
			place.images = counts.images;
			myHtml += "<ul>";
			if (place.articles > 0)
				myHtml += "<li><a href=\"/search/"+place.place_name.replace(" ","_") + "\">articles (" + place.articles + ")</a></li>";
			if (place.images > 0)
				myHtml += "<li><a href=\"/imageresults/"+place.place_name.replace(" ","_") + "\">images (" + place.images + ")</a></li>";
			myHtml += "</ul></div>";
			map.closeInfoWindow();
			map.openInfoWindowHtml(marker.getLatLng(),myHtml, {maxContent:myHtml, pixelOffset: new GSize(0,-5), maxWidth:200, onOpenFn: function() {
				map.getInfoWindow().reset(map.getInfoWindow().getPoint(),map.getInfoWindow().getTabs(),new GSize(175,75),null,null);
			} } );
		} );
	});
	return marker;
}
*/
