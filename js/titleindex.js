/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

// Copyright 2011 UCLA Academic Technology Services

/**
 * @fileoverview TODO - Description of file, its uses and information
 * author adtai@ats.ucla.edu (Andrew Tai)
 */

/**
 * titleindex.js
 *
 * Set up a series of dynamic behaviors to be configured for runtime
 * Should be functionality specifc to the 'title index' page.
 * - TODO 6/28/11 - list features
 *
 * Dependencies:
 * - Requires >= jQuery 1.3.1
 *
 */

$(document).ready(function () {

    /**
     * When an article title in the left hand column is clicked on,
     * we want to load the article brief on the right hand side of the page
     * Content to be updated:
     * - article title(s)
     * - article author(s)
     * - article abstract (english)
     * - article abstract (arabic)
     * The updated information for the clicked article comes from an
     * php script which is accessed via an ajax call.
     */
    $("ul.titles li a").click(function () {
        var title = $(this).html();
        $("div#brief").hide("slow");

        // move the article display screen closer to the link that user click.
        var rect = document.body.getBoundingClientRect();

        $.get("/modules/front/ajax/get_article2.php", {title: $(this).attr("class")}, function (data) {
            /**
             * If we didnot receive any data, display a place holder
             * - title
             * - generic info in the abstract
             */


            $("div#brief").empty();  // empty everything
            var output = eval("(" + data + ")");

            if (!output.titles) {
                // abstracts
                $("div#brief").append("<p><span id='abstract'><strong>Coming soon</strong><br />" +
                    "This article is under development and will be available soon</span></p>");
                // permalink
                $("div#brief form").hide()
                $("#status").empty();
                $('#brief').css('margin-top', Math.abs(rect.top));
                $("div#brief").show("slow");
            }
            else {
                // titles
                $("div#brief").append(
                    '<h3 class="english-title">'
                        + '<span xml:lang="en-US">'
                        + output.titles[0]
                        + '</span>'
                    + '</h3>'
                );
                for (var i = 1; i < output.titles.length; i++) {
                    var span = "<span lang=\"" + output.titlelanguages[i] + "\" xml:lang=\"" + output.titlelanguages[i] + "\" >" + output.titles[i] + "</span><br />";
                    $("div#brief").append('<h3>' + span + '</h3>');
                }
                // author(s)
                var authoroutput = "<p id='author'>";
                for (var i = 0; i < output.authors.length; i++) {
                    authoroutput += output.authors[i] + "<br />";
                }
                authoroutput += "</p>";
                $("div#brief").append(authoroutput);
                // abstracts
                // $("div#brief p#abstract").empty().append(brief.english_abstract);
                $("div#brief").append("<p><span id = 'abstract'>" + output.english_abstract + "</span></p>");
                $("div#brief").append("<p><span id = 'arabic_abstract'>" + output.arabic_abstract + "</span></p>");
                $("#status").empty().append(output.status);

                // permalink
                var articleLink = $("<a/>").attr({"href": "/articles/" + output.permalink});
                $(articleLink).append($("<img />").attr({"src": "/graphics/Read_Article.png"}));
                $("div#brief").append(articleLink);
                $("div#brief").show();
                $('#brief').css('margin-top', Math.abs(rect.top));

                $("div#brief").show("slow");
            }
        });
    });

    /**
     * The article titles exist in 4 different possible languages
     * - English
     * - French
     * - German
     * - Arabic
     * Clicking on various links toggles which listing of titles is available
     */
    $("p#sort-wrapper a").click(function () {
        $("ul.titles").hide();
        var lang = $(this).html().toLowerCase();
        switch (lang) {
            case 'english':
                $("ul.titles#titles-english").slideDown("fast");
                break;
            case 'french':
                $("ul.titles#titles-french").slideDown("fast");
                break;
            case 'german':
                $("ul.titles#titles-german").slideDown("fast");
                break;
            case 'arabic':
                $("ul.titles#titles-arabic").slideDown("fast");
                break;
            default:
                break;
        }
    });

    // by default, hide all the title menus except the english menu
    $("ul.titles").hide();
    $("ul.titles#titles-english").slideDown("fast");

    /*
     * A set of toggles available for filtering the article menu by
     * specific letter. Currently only set up to work for the English menu
     */
    $("#letter-filters a.letter-filter").click(function () {
        var letter = $(this).text();
        if ($("ul.titles#titles-english").css("display") == "block") {
            if (letter == "All") {
                $("ul.titles li").css("display", "list-item");
                $("ul.titles#titles-english li").each(function (index) {
                    $(this).show("fast");
                });
            }
            else {
                $("ul.titles#titles-english li").each(function (index) {
                    $("ul.titles li").css("display", "list-item");
                    if ($(this).text().substr(0, 1) != letter) {
                        $(this).hide("fast");
                    }
                    else {
                        $(this).show("fast");
                    }
                });
            }
        }
        else {
            alert("Sorry! This filter only works for titles in English");
        }
    });

});

