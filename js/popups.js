// popups

$(document).ready(function() {
	// glossary popups
	// clicking or hovering over a glossary will allow you to pullup the glossary definition
	$("a.glossref").click(function() {
		// if you click one, remove any other existing popups:
		$(".gloss-popup").remove();
		$(".biblio-popup").remove();
		// $("#rightcol p:first").remove();
		$("#popup-content").hide();
		// hooray for jQuery trim!
		var glossName = jQuery.trim($(this).text());
		var glossId = jQuery.trim($(this).attr("name"));
		// this function represents the ajax call
		// load Glossary uses the ajax call and processes the results
		loadGlossary2(glossId, glossName, $(this));
	});
	
	$("a.biblioref").click(function() {
		// if you click one, remove any other existing popups:
		$(".gloss-popup").remove();
		$(".biblio-popup").remove();
		// $("#rightcol p:first").remove();
		$("#popup-content").hide();
		// hooray for jQuery trim!
		var biblioName = jQuery.trim($(this).text());
		var biblioTarget = $(this).attr("name");
		// this function represents the ajax call
		loadReference(biblioName, biblioTarget, $(this));	
	});
});

// use an ajax function to load the Glossary term and make a popup out of it
function loadGlossary(glossName, obj) {
	var urlStr = "/modules/front/ajax/popups.php?method=getGlossary&input="+encodeURI(glossName);
	// alert(urlStr);
	$.ajax({ url: urlStr, context: obj, success: function(data){
		processGlossary(obj, data);
	}});
}

function loadGlossary2(glossId, glossName, obj) {
	var urlStr = "/modules/front/ajax/popups.php?method=getGlossary2&input="+glossId+"&input2="+encodeURI(glossName);
	// alert(urlStr);
	$.ajax({ url: urlStr, context: obj, success: function(data){
		processGlossary(obj, data);
	}});
}

function processGlossary(obj, data) {
	// some validation
	if (data.length <= 0)
	{
		var popupStr = "<span class='gloss-popup'>"+"Sorry, we could not find this glossary term."+"</span>";
		$("#popup-content").html(popupStr).show("fast");
		return;
	}
	// make sure to use parentheses wrapper
	// http://willcode4beer.com/tips.jsp?set=jsonInvalidLabel
	var gloss = eval("("+data+")");
	var popupStr = "<span class='gloss-popup'>" + "<span class='glossary-term'>" + gloss['glossName'] + "</span>" + "<br />" + gloss['glossDefinition'] + "</span>";
	$("#popup-content").html(popupStr).show("fast");
}

// use an ajax function to load the Reference and make a popup out of it
// we will try to make use of the inline reference
function loadReference(biblioName, biblioTarget, obj) {
	var referenceJqId = "div#"+biblioTarget+".reference";
	var referenceHtml = $(referenceJqId).html();
	var popupStr = "<div class='biblio-popup'>"+referenceHtml+"</div>";
	$("#popup-content").html(popupStr).show("fast");
}

