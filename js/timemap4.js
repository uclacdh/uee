// TODO - we're only using timemap3 right now.

// array of color-related classes
var timesections = ["timesection_red","timesection_green","timesection_blue","timesection_yellow"];

// set up our jquery behaviors
$(document).ready(function() {

	// time slider for the timemap on the frontend
	sliderInit();
	
	sliderCursorPositionSet();
	
	// i'd like a way to text type the filters....
	$("#min-time-period-val").blur( function(event) {
		// alert($("#min-time-period-val").val());
		// update the slider?
		var minval = $("#min-time-period-val").val();
		var maxval = $("#max-time-period-val").val();
		if (minval < -5000)
			minval = -5000;
		if (maxval > 500)
			maxval = 500;
		var left = Math.floor(900*(minval+4925)/5500);
		$("#min-time-period-val").css("left",left+"px"); 
		left = Math.floor(900*(maxval-340)/(5500));
		$("#max-time-period-val").css("left",left+"px"); 		
		$("#slider").slider('destroy');		
		$("#slider").slider(
			{
				'range':true,
				'min':-5000,
				'max':500,
				'values':[minval,maxval],
				'slide':function(e, ui) { 
					$("#min-time-period-val").val(ui.values[0]); 
					$("#max-time-period-val").val(ui.values[1]); 
					var left = Math.floor(900*(ui.values[0]+4925)/5500);
					$("#min-time-period-val").css("left",left+"px"); 
					left = Math.floor(900*(ui.values[1]-275)/(5500));
					$("#max-time-period-val").css("left",left+"px"); 
				},
				'stop':function(e, ui) {
					$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
						if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
						{
							if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
								alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
							return;
						}
						makeMarkers(data);
						$("form#search-map-form label#search-status").html("Done").hide("slow");
					} );	
				}
			}
		);
		$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
	} );
	
	$("#max-time-period-val").blur( function(event) {
		// alert($("#min-time-period-val").val());
		// update the slider?
		var minval = $("#min-time-period-val").val();
		alert(minval);
		var maxval = $("#max-time-period-val").val();
		if (minval < -5000)
			minval = -5000;
		if (maxval > 500)
			maxval = 500;
		$("#slider").slider('values',0,minval);
		$("#slider").slider('values',1,maxval);
		sliderCursorAnimate();
		$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
	} );
	
	// if the user clicks one of the phase boxes, toggle the slider to that particular range?
	$("a.phase-link").hover( 
		function() {
			$(this).addClass("phase-hover");
		},
		function() {
			$(this).removeClass("phase-hover");
		}		
	);

	$("a.phase-link").click( function() {
		// desired behavior
		// 'select' on this link
		// show the dynasties for this link (if rulers listed, remove rulers)
		// shrink the phases row to smaller height
		$("a.phase-link").removeClass("phase-selected"); // untoggle other selected phases
		$("div#rulers > *").remove();		
		$(this).addClass("phase-selected");
		// $("a.phase-link").css("height","5px");
		var pId = -1;
		var phaseName = $(this).attr("id");
		$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_phases.php", function(data) {
			var phaseArr = eval("(" + data + ")");			
			for (var i = 0; i < phaseArr.length; i++)
			{
				var phase = phaseArr[i];
				if ("phase-"+phase.time_period_name.replace(/ /g,"") == phaseName)
				{
					if (phase.year_start < -5000)
						phase.year_start = -5000;
					$("#slider").slider("values",0,phase.year_start);
					$("#slider").slider("values",1,phase.year_end);
					// todo: can I animate the text inputs?
					$("#min-time-period-val").val($("#slider").slider("values", 0)); 
					$("#max-time-period-val").val($("#slider").slider("values", 1)); 
					var left = Math.floor(900*($("#slider").slider("values", 0)+4925)/5500);
					$("#min-time-period-val").animate( {"left":left+"px"}, 1000); 					
					left = Math.floor(900*($("#slider").slider("values", 1)-275)/(5500));
					$("#max-time-period-val").animate( {"left":left+"px"}, 1000); 					
					pId = phase.time_period_id;
					// update the markers
					$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
						if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
						{
							if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
								alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
							return;
						}
						makeMarkers(data);
						$("form#search-map-form label#search-status").html("Done").hide("slow");
					} );	
					// set up the dynasties div?
					$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_dynasties.php", { parentId:pId }, function(data) {
						$("div#dynasties > *").remove();
						makeDynasties(data);
						// add the behaviors...
						$("a.dynasty-link").click( function() {
							$("a.dynasty-link").removeClass("dynasty-selected"); // untoggle other selected phases
							$(this).addClass("dynasty-selected");
							// update the slider
							var dynastyName = $(this).attr("id");
							// alert(data);
							var dId = 0;
							var dynastyArr = eval("(" + data + ")");
							for (var j = 0; j < dynastyArr.length; j++)
							{
								var dynasty = dynastyArr[j];
								// alert(dynasty.time_period_name.replace(/ /g,""));
								if ("dynasties-"+dynasty.time_period_name.replace(/ /g,"") == dynastyName)
								{
									if (dynasty.year_start < -5000)
										dynasty.year_start = -5000;								
									$("#slider").slider("values",0,dynasty.year_start);
									$("#slider").slider("values",1,dynasty.year_end);
									$("#min-time-period-val").val($("#slider").slider("values", 0)); 
									$("#max-time-period-val").val($("#slider").slider("values", 1));				
									var left = Math.floor(900*($("#slider").slider("values", 0)+4925)/5500);
									$("#min-time-period-val").css("position","relative"); 
									$("#min-time-period-val").animate( {"left":left+"px"}, 750); 	
									left = Math.floor(900*($("#slider").slider("values", 1)-275)/(5500));
									$("#max-time-period-val").css("position","relative"); 
									$("#max-time-period-val").animate( {"left":left+"px"}, 750); 	
					 					
									dId = dynasty.time_period_id;
									// update the markers
									$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(markerdata) {
										if (!markerdata || markerdata == null || markerdata.length <= 2) // an empty set returns '[  ]'
										{
											if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
												alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
											return;
										}
										makeMarkers(markerdata);
										$("form#search-map-form label#search-status").html("Done").hide("slow");
									} );	
									// set up the rulers div
									$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_dynasties.php", { parentId:dId }, function(rulerdata) {
										$("div#rulers > *").remove();
										makeRulers(rulerdata);
										// todo: Add the behaviors;
										$("a.ruler-link").hover( 
											function() {
												$(this).addClass("ruler-hover");
											},
											function() {
												$(this).removeClass("ruler-hover");
											}		
										);	
										$("a.ruler-link").click( function() {
											$("a.ruler-link").removeClass("ruler-selected"); // untoggle other selected phases
											$(this).addClass("ruler-selected");
											// update the slider
											var rulerName = $(this).attr("id");
											// alert(rulerName);
											// alert(data);
											var rId = 0;
											var rulerArr = eval("(" + rulerdata + ")");
											for (var k = 0; k < rulerArr.length; k++)
											{
												var ruler = rulerArr[k];
												if ("rulers-"+ruler.time_period_name.replace(/ /g,"") == rulerName)
												{
													if (ruler.year_start < -5000)
														ruler.year_start = -5000;								
													// alert(ruler.time_period_name);
													$("#slider").slider("values",0,ruler.year_start);
													$("#slider").slider("values",1,ruler.year_end);
													$("#min-time-period-val").val($("#slider").slider("values", 0)); 
													$("#max-time-period-val").val($("#slider").slider("values", 1));
													var left = Math.floor(900*($("#slider").slider("values", 0)+4750)/5500);
													$("#min-time-period-val").animate( {"left":left+"px"}, 500); 	
													left = Math.floor(900*($("#slider").slider("values", 1)-150)/(5500));
													$("#max-time-period-val").animate( {"left":left+"px"}, 500); 	
													$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(markerdata) {
														if (!markerdata || markerdata == null || markerdata.length <= 2) // an empty set returns '[  ]'
														{
															if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
																alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
															return;
														}
														makeMarkers(markerdata);
														$("form#search-map-form label#search-status").html("Done").hide("slow");
													} );												
													break;
												}												
											}
										} );
									} )
									break;
								}
							}
							
							// update the marker
						} );
												
					} );
					break;
				}
			}
		} );
	} );
	
	// load the timemap with the page load
	$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1) }, function(data) {
		makeMarkers(data);
	} );	
	
	// how to reset the timeline slider?
	$("a#reset-timeline").click( function() {
		$("div#rulers > *").remove();	
		$("div#dynasties > *").remove();
		$("a.phase-link").removeClass('phase-selected');
		$("#slider").slider('values',0,-5000);	
		$("#slider").slider('values',1,500);	
		$("#min-time-period-val").val($("#slider").slider('values',0)); 
		$("#max-time-period-val").val($("#slider").slider('values',1)); 
		var left = Math.floor(900*($("#slider").slider('values',0)+4750)/5500);
		$("#min-time-period-val").animate( {"left":left+"px"}, 1000); 			
		left = Math.floor(900*($("#slider").slider('values',1)-150)/(5500));
		$("#max-time-period-val").animate( {"left":left+"px"}, 1000); 		
		$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
	} );
	
	// search filter
	$("form#search-map-form input#search-map-submit").click( function() {
		$("form#search-map-form label#search-status").html("Searching").show();
		$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
		return false;
	} );
	
	// alert($("div#phases").width()); // 941 px
		
});

// google maps behaviors (for data entry: place records)
// holders for the marker data
var gmarker = null; // this is for point_mode;
var gpolyline = new Array(); // this is for line_mode;
var gpolygon = new Array(); // this is for polygon_mode;

// poly options - something needed for drawing polygons
var polyOptions = {geodesic:true};		

// holders for the 'mode' of the marker placement
// var geomap_mode = "point_mode";
var geomap_mode = "";
var coord_mode = 'latlong_mode';
var map;
var cluster;
var marker, markersArray=[];

var addMarkerToggle = null; // just want a global so i can remove this behavior...

// mapInit()
// mapInit is responsible for loading the map - nothing more, nothing less
// it does not add overlays
function mapInit() {
	// make the map
	if (GBrowserIsCompatible()) {	
		map = new GMap2(document.getElementById("time-map"));
		new GKeyboardHandler(map);
		map.clearOverlays();
		map.checkResize();
		map.enableScrollWheelZoom();
		map.addControl(new GLargeMapControl3D());
		map.addControl(new GMenuMapTypeControl());
		// set the map type
		// feb 24, 2009- yoh wants the default to be the 'terrain' type.
		map.addMapType(G_PHYSICAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		map.setMapType(G_PHYSICAL_MAP); // possible options: G_NORMAL_MAP,  G_SATELLITE_MAP, G_HYBRID_MAP, G_DEFAULT_MAP_TYPES
		// feb 25, 2009
		map.addMapType(G_SATELLITE_3D_MAP);		
		
		var customUI = map.getDefaultUI();
		
		
		var zoom_level = 6;
		var centerSpot = new GLatLng(29.8, 30.5); // this is pretty close to where Faiyum is...

		map.setCenter(centerSpot, zoom_level);		

	}
	else
	{
		alert("Warning: Your browser is not compatible with the GoogleMaps API we are using.");
		return;
	}
}

// makeMarkers(data)
// clear all the existing markers
// take the json data, parse it and display the new markers....
function makeMarkers(data) {
	// alert(data);
	// clear the map of any existing markers
	if (map)
		map.clearOverlays();
	markersArray.length = 0;
	markersArray = [];
	// little bug with json data, needs to be string encapsulated
	// http://willcode4beer.com/tips.jsp?set=jsonInvalidLabel	
	if (data.length <= 0 || !data)
		return;
	// alert(data);
	var placeArr = eval("(" + data + ")");
	
	// alert(placeArr);
	for (var i = 0; i < placeArr.length; i++)
	{
		var place = placeArr[i];
		// alert(place);
		// if (i == 0)
			// alert(place.place_type_id);
		marker = newMarker(new GLatLng(place.latitude, place.longitude), place);
		
		markersArray.push(marker);
		// addMarker(place);
	}
	// let's set up the cluster icons
	var clusterIcon = new GIcon(G_DEFAULT_ICON);
	clusterIcon.shadow = "";
	clusterIcon.image = "http://schnauzer.ats.ucla.edu/uee/graphics/Site_Multiple.png";
	clusterIcon.iconSize = new GSize(34,34);
	clusterIcon.imageMap = [2,3,29,3,29,30,2,30];
	
	// if the cluster's been set; remove any markers
	if (cluster)
		cluster.removeMarkers();
	cluster = new ClusterMarker(map, { markers:markersArray, clusterMarkerIcon:clusterIcon } );
	cluster.fitMapToMarkers();
}

function newMarker(markerLocation, place) {
	// what kinda icon are we using?
	// let's set up some initial properties
	var myIcon = new GIcon(G_DEFAULT_ICON);
	// remove the default shadows
	myIcon.shadow = "";
	
	switch (place.place_type_id) {
		case 4: // site
			myIcon.image = "http://schnauzer.ats.ucla.edu/uee/graphics/Site_InUse.png";
			// myIcon.iconSize = new GSize(87,87);
			myIcon.iconSize = new GSize(30,30);
			myIcon.imageMap = [3,2,25,2,25,24,3,24];
			break;
		case 5: // feature
			myIcon.image = "http://schnauzer.ats.ucla.edu/uee/graphics/Feature_InUse.png";
			// myIcon.iconSize = new GSize(88,87);
			myIcon.iconSize = new GSize(30,30);
			myIcon.imageMap = [3,2,25,2,25,24,3,24];
			break;
		default:
			break;
	}
	// this should center the icon over the geopoint being referenced
	myIcon.iconAnchor = new GPoint(16,16); // do i need to adjust the positioning at all?
	// this should center the info window
	myIcon.infoWindowAnchor = new GPoint(15,7); // do i need to adjust the positioning at all?
	// create them marker!
	
	var marker = new GMarker (markerLocation, {title: place.place_name, icon:myIcon });	
	var loadingHtml = "<div class='geomarker-popup'><p><strong>Loading...</strong></p></div>";
	GEvent.addListener(marker,'click',function() {
		marker.openInfoWindow(loadingHtml);
		var myHtml = "<div class='geomarker-popup'><p><strong>" + place.place_name + "</strong><br />";
		var counts;
		myHtml += "<em>From " + place.year_start + " to " + place.year_end + "</em></p>";
		$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_search_counts.php", { place_name:place.place_name, old_id:place.old_id }, function(data) {
			if (!data || data == null || data.length <= 0)
			{
				myHtml += '<p>There was an error getting the related information</p>';
				marker.openInfoWindowHtml(myHtml);
				return;
			}
			counts = eval("(" + data + ")");
			place.articles = counts.articles;
			place.images = counts.images;
			myHtml += "<ul>";
			if (place.articles > 0)
				myHtml += "<li><a href=\"/uee/search/"+place.place_name.replace(/ /g,"_") + "\">articles (" + place.articles + ")</a></li>";
			if (place.images > 0)
				myHtml += "<li><a href=\"/uee/imageresults/"+place.place_name.replace(/ /g,"_") + "\">images (" + place.images + ")</a></li>";
			myHtml += "</ul></div>";
			marker.openInfoWindowHtml(myHtml);
		} );
	});
	return marker;
}

// makeDynasties
// makem them dynasty blocks
function makeDynasties(data) {
	if (data.length <= 0 || !data)
		return;
	var dynastyArr = eval("(" + data + ")");
	var width = Math.floor(960*0.98) - dynastyArr.length*3;
	var sum = 0; 
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;			
		
		sum += (dynasty.year_end - year_start);
	}
	var dynastiesDiv = document.getElementById('dynasties');
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;
		
		var range = (dynasty.year_end - year_start);
		var pixels = Math.round((width*range/sum)) - 4;
		if (pixels < 5)
			pixels = 5;		
		var anchor = document.createElement("a");
		var idAttr = document.createAttribute("id");
		idAttr.nodeValue='dynasties-'+dynasty.time_period_name.replace(/ /g,"");
		anchor.setAttributeNode(idAttr);
		var classAttr = document.createAttribute("class");
		classAttr.nodeValue='dynasty-link';
		classAttr.nodeValue += " " + timesections[i%4];
		anchor.setAttributeNode(classAttr);
		var hrefAttr = document.createAttribute("href");
		hrefAttr.nodeValue='javascript:;';
		anchor.setAttributeNode(hrefAttr);
		var titleAttr = document.createAttribute("title");
		titleAttr.nodeValue='view places in the ' + dynasty.time_period_name + ' dynasty';
		anchor.setAttributeNode(titleAttr);
		var text = document.createTextNode(dynasty.time_period_name);
		anchor.appendChild(text);
		anchor.style.width = pixels + "px";	
		dynastiesDiv.appendChild(anchor);
		// $("#"+idAttr.nodeValue).hover( function() {
			
		// } );
	}
}

// makeRulers
// makem them dynasty blocks
function makeRulers(data) {
	if (data.length <= 0 || !data)
		return;
	var dynastyArr = eval("(" + data + ")");
	var width = Math.floor(960*0.98)-dynastyArr.length*3;
	var sum = 0; 
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;			
		
		sum += (dynasty.year_end - year_start);
	}
	var dynastiesDiv = document.getElementById('rulers');
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;
		
		var range = (dynasty.year_end - year_start);
		var pixels = Math.floor((width*range/sum)) - 5;
		if (pixels < 5)
			pixels = 4;
		var anchor = document.createElement("a");
		var idAttr = document.createAttribute("id");
		idAttr.nodeValue='rulers-'+dynasty.time_period_name.replace(/ /g,"");
		anchor.setAttributeNode(idAttr);
		var classAttr = document.createAttribute("class");
		classAttr.nodeValue='ruler-link';
		classAttr.nodeValue += " " + timesections[i%4];
		anchor.setAttributeNode(classAttr);
		var hrefAttr = document.createAttribute("href");
		hrefAttr.nodeValue='javascript:;';
		anchor.setAttributeNode(hrefAttr);
		var titleAttr = document.createAttribute("title");
		titleAttr.nodeValue='view places in the ' + dynasty.time_period_name + ' rulership';
		anchor.setAttributeNode(titleAttr);
		var text = document.createTextNode(dynasty.time_period_name);
		anchor.appendChild(text);
		anchor.style.width = pixels + "px";	
		dynastiesDiv.appendChild(anchor);
		// $("#"+idAttr.nodeValue).hover( function() {
			
		// } );
	}
}

// sliderInit()
// initializes the jQuery slider
// will this be able to take in dynamic values? hopefully...
function sliderInit() {
	// time slider for the timemap on the frontend
	$("#slider").slider( 
		{
			'range':true,
			'min':-5000,
			'max':500,
			'values':[-5000,500],
			'slide':function(e, ui) { 
				$("#min-time-period-val").val(ui.values[0]); 
				$("#max-time-period-val").val(ui.values[1]);				
				sliderCursorPositionSet()
			},
			'stop':function(e, ui) {
				$.get("http://schnauzer.ats.ucla.edu/uee/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
					if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
					{
						if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
							alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
						return;
					}
					makeMarkers(data);
					$("form#search-map-form label#search-status").html("Done").hide("slow");
				} );	
			}
		}
	);
}

// sliderCursorValuesSet() 
// set the slider cursor at the right position
function sliderCursorPositionSet() {
	var left = Math.floor(940*($("#slider").slider("values", 0)+4875)/5500);
	$("#min-time-period-val").css("position","relative"); 
	$("#min-time-period-val").css("left",left+"px"); 
	left = Math.floor(940*($("#slider").slider("values", 1)-170)/(5500));
	$("#max-time-period-val").css("position","relative"); 
	$("#max-time-period-val").css("left",left+"px"); 	
}

// sliderCursorAnimate() 
// move the slider cursor to the right position
function sliderCursorAnimate() {
	var left = Math.floor(940*($("#slider").slider("values", 0)+4875)/5500);
	$("#min-time-period-val").css("position","relative"); 
	$("#min-time-period-val").css("left",left+"px"); 
	left = Math.floor(940*($("#slider").slider("values", 1)-170)/(5500));
	$("#max-time-period-val").css("position","relative"); 
	$("#max-time-period-val").css("left",left+"px"); 	
}
