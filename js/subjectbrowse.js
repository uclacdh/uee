/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * subjectbrowse.js
 * jquery behaviors for the subject browser display page

 */

$(document).ready( function () {

    /** 
     * Initially, hide everything but the top level subjects
     * Also hide the abstract display
     */
	$("ul#subjects li a").next().hide();
	$("div#brief").hide();

    /**
     * When the toggle is clicked, expand the subject
     */	
	$("ul#subjects li a").click( function() {
		var subject = $(this).html();
		$("ul#subjects li a").removeClass("selected");
		$(this).addClass("selected").parents().children("ul#subjects li a").addClass("selected");
		$(this).addClass("selected").next().slideToggle("medium");
		if ($(this).css("backgroundImage").indexOf('Contract')  > -1 && $(this).css("backgroundImage").indexOf('Expand')  < 0)
			$(this).css("backgroundImage","url('/graphics/Expand.png')");
		else if ($(this).css("backgroundImage").indexOf('Contract')  < 0 && $(this).css("backgroundImage").indexOf('Expand')  > -1)
			$(this).css("backgroundImage","url('/graphics/Contract.png')");		
		$("ul#subjects li.endnode").css("backgroundImage","none");
		// if this one has a sublist, we don't need to display an article for it
		if (!$(this).parent().hasClass("endnode"))
			return;
		$("div#brief").hide("slow");		
		$.get("/modules/front/ajax/get_article2.php", { title:$(this).html() }, function(data) {	
			// alert(data);
			if (!data || data.length <= 0)
			{
				$("div#brief h3").empty();
				$("div#brief h3").append(subject);
				// author(s)
				$("div#brief p#author").empty();
				$("div#brief p#author").append("Author: ");
				// abstracts
				$("div#brief p#abstract").empty();
				$("div#brief p#abstract").append("<strong>Coming soon</strong><br />This article is under development and will be available soon");
				$("div#brief p#arabic_abstract").empty();
				// permalink
				$("div#brief form").hide()
				$("#status").empty();
				$("div#brief").show("slow");
				$("#buttons").hide();
				return;
			} 
			// process them data
			var brief = eval("(" + data + ")");
			// update the brief
			// titles
			$("div#brief h3").empty();
			for (var i = 0; i < brief.titles.length; i++)
			{
				var span = "<span lang=\"" + brief.titlelanguages[i] + "\" xml:lang=\""+ brief.titlelanguages[i] +"\" >" + brief.titles[i] + "</span><br />";
				$("div#brief h3").append(span);
			}
			// author(s)
			$("div#brief p#author").empty();
			$("div#brief p#author").append("Author:<br />");
			for (var i = 0; i < brief.authors.length; i++)
				$("div#brief p#author").append(brief.authors[i] + "<br />");
			// abstracts
			$("div#brief p#abstract").empty();
			$("div#brief p#abstract").append(brief.english_abstract);
			$("div#brief p#arabic_abstract").empty();
			$("div#brief p#arabic_abstract").append(brief.arabic_abstract);			
			// permalink
			$("div#brief a").attr('href', '/articles/' + brief.permalink + '/');
			$("#status").empty().append(brief.status);
			// only reappear if there's content worth showing!
            $("#buttons").show();
			$("div#brief").show("slow");
			return;
		});
	});
		
    /**
     * If we were supposed to initially display a category, do so
     */
	if ($("div#showinitial").html().length > 0)
	{
		var cat = $("div#showinitial").html()
		$("ul#subjects li a:contains(" + cat +")").addClass("selected").next().slideToggle("medium");
		$("div#brief").hide("slow");		
		$.get("/modules/front/ajax/get_article.php", { title:$(this).html() }, function(data) {	
			if (!data || data.length <= 0)
				return;
			// process them data
			var brief = eval("(" + data + ")");
			// update the brief
			// titles
			$("div#brief h3").empty();
			for (var i = 0; i < brief.titles.length; i++)
			{
				var span = "<span lang=\"" + brief.titlelanguages[i] + "\" xml:lang=\""+ brief.titlelanguages[i] +"\" >" + brief.titles[i] + "</span><br />";
				$("div#brief h3").append(span);
			}
			// author(s)
			$("div#brief p#author").empty();
			$("div#brief p#author").append("Author:<br />");
			for (var i = 0; i < brief.authors.length; i++)
				$("div#brief p#author").append(brief.authors[i] + "<br />");
			// abstracts
			$("div#brief p#abstract").empty();
			$("div#brief p#abstract").append(brief.english_abstract);
			$("div#brief p#arabic_abstract").empty();
			$("div#brief p#arabic_abstract").append(brief.arabic_abstract);			
			// permalink
			$("div#brief form").attr("action", "/articles/"+brief.permalink+"/");	
    
			// only reappear if there's content worth showing!
			$("div#brief").show("slow");
			return;
		});		
	}
} )
