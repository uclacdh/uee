var imgid;

$(document).ready( function () {
	$("a img.related-image").click( function() {
		imgid = this.id;
		var src="http://digital2.library.ucla.edu/imageResize.do?contentFileId="+this.id+"&scaleFactor=0.84";
		var margintop = $(this).css("margin-top").substr(0,$(this).css("margin-top").length-2); // take off the px
		var bigmargin = Math.round(margintop*630/120); // the 400/120 is the ratio from my featured image to my thumb image
		var newmargin = bigmargin +"px";
		$("div#featured span img").attr("src",src).attr("title",$(this).attr("title")).hide('slow', function() {
			$(this).animate({marginTop:newmargin},1000).fadeIn('slow');
		});
		// load the metadata
		$.get("/modules/front/ajax/get_metadata.php", {ark: $(this).parent("a").next("span").html() }, function(data) {
			if (!data || data == null || data.length <= 0)
				return;
			$("div#metadata").html(data).show("fast");
		});
		// $("div#featured span img").attr("src",src).css("margin-top",newmargin);
	});

	// do an initial setup
	var first = $("a img.related-image:first");
	imgid = first.attr("id");
	var src="http://digital2.library.ucla.edu/imageResize.do?contentFileId="+first.attr("id")+"&scaleFactor=0.84";
	var margintop = first.css("margin-top").substr(0,first.css("margin-top").length-2); // take off the px
	var bigmargin = Math.round(margintop*630/120); // the 400/120 is the ratio from my featured image to my thumb image
	var newmargin = bigmargin +"px";
	// load the metadata
	$.get("/modules/front/ajax/get_metadata.php", {ark: $("div#thumbs span:first").html() }, function(data) {
		if (!data || data == null || data.length <= 0)
			return;
		$("div#metadata").html(data).show("fast");
	});
	$("div#featured span img").attr("src",src).attr("title",first.attr("title")).hide('fast', function() {
		$(this).animate({marginTop:newmargin},1000).fadeIn('slow');
	});	
	
	// the enlarge feature
	$("div#featured a").click( function() {		
		$("div#enlarged-image-wrapper").height($(document).height()).fadeIn("fast");
		var src="http://digital2.library.ucla.edu/imageResize.do?contentFileId="+imgid+"&scaleFactor=1";
		$("div#enlarged-image img").attr("src",src);
		$("div#enlarged-image span").html($("div#metadata").html());		
		// alert($("div#content").width());
		// alert($("div#featured img").height());
		// alert($("div#featured img").width());
	});
	
	$("div#enlarged-image-wrapper a").click( function() {
		$("div#enlarged-image-wrapper").fadeOut("fast");
	});
	
	$("a#thumbsonly").click( function() {
		$("div#toprow").hide("medium", function() {
			$("div#imageresults").hide(function() {
				$(this).css("width","98%").fadeIn("slow");
			});
		} );
	});
	
	$("a#theatre").click( function() {
		$("div#imageresults").hide("medium", function() {
			$(this).css("width","28%").fadeIn("slow", function() {
				$("div#toprow").show("medium");
			} );
		} );
	});
	
} );
