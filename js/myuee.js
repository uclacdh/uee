// Copyright 2011 UCLA Academic Technology Services. All Rights Reserved.

/**
 * @fileoverview Description of file, its uses and information
 * about its dependencies
 * @author adtai@ats.ucla.edu (Andrew Tai)
 */


$(document).ready( function () {

	$("#save-toggles-button").click( function() {
		var glosses;
		var links;
		var biblios;
		var places;
		if ($("input#glosses-toggle").attr("checked") )
			glosses = 1;
		else
			glosses = 0;
		if ($("input#links-toggle").attr("checked") )
			links = 1;
		else
			links = 0;
		if ($("input#biblio-toggle").attr("checked") )
			biblios = 1;
		else
			biblios = 0;
		if ($("input#places-toggle").attr("checked") )
			places = 1;
		else
			places = 0;
		$.get("/modules/front/ajax/save_user_settings.php", { glosses:glosses, links:links, biblios:biblios, places:places }, function(data) {
			$("label#toggles-save-status").hide().html(data).show().fadeOut(3000);
			return false;
		} );
	} );
	
});
