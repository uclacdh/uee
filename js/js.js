// Copyright 2011 UCLA Academic Technology Services

/**
 * @fileoverview TODO - Description of file, its uses and information
 * author adtai@ats.ucla.edu (Andrew Tai)
 */

// :set expandtab shiftwidth=4 softtabstop=4 tabstop=4: 

/**
 * TODO 6/27/11 - we may want to split up the code here
 * Set up a series of dynamic behaviors to be configured on runtime
 * Mostly functionality related to UEE Admin
 * - Input form behaviors - 'adding' new rows
 * - Initializing 'DataTables' plugina for various tables
 * TODO 6/27/11 - behaviors for adding / editing places should be in js2.js for now
 * 
 * Dependencies:
 * - Requires Jquery (what version?)
 * - Requires 'DataTables' jquery plugin
 */
$(document).ready(function() {

    /**
     * Add an extra glossname field
     * Usage: When in UEE Admin Gloss, adding a new / editing a gloss record
     * Expected result - generate a new glossname row to the form
     */
    $("p.glossname_row a").click(function () {
        var newNumber = $(this).parent(".glossname_row").parent().children(".glossname_row").size();
        var newNodeId = "name_field_" + newNumber;
        // add a new row
        $(this).parent(".glossname_row").clone().insertBefore("#definition_p");
        // adjust some attributes for the new row
        $(".glossname_row:last").attr("id", "glossname_" + newNumber);
        $(".glossname_row:last label").attr("for", newNodeId);
        $(".glossname_row:last input").attr("id", newNodeId);
        // any rows except the first row don't need a 'More' link
        $(".glossname_row:last a").remove();
       
    });

    /**
     * Some of the input tables will have two views:
     * - tabular
     * - tree
     * Provide a toggle between the two views
     */
    // tree to table
    $("a#toTable").click(function () {
        $("#treeView").hide("slow");
        $("a#toTable").hide("slow");
        $("form#searchbox").hide("slow");
        $("#tableView").show("slow");
        $("a#toTree").show("slow");
    });

    // table to tree
    $("a#toTree").click(function () {
        $("#tableView").hide("slow");
        $("a#toTree").hide("slow");
        $("#treeView").show("slow");
        $("a#toTable").show("slow");
    });

    /**
     * The places tree has a specialized set of toggles
     * related to tree view
     */	
    $("a#regionToggle").click(function () {
        $("div#treeView div").hide("slow");
        $("div#treeView div#regionTree").show("slow");
    });
	
    $("a#governateToggle").click(function () {
        $("div#treeView div").hide("slow");
        $("div#treeView div#governateTree").show("slow");
    });
	
    $("a#nomeToggle").click(function () {
        $("div#treeView div").hide("slow");
        $("div#treeView div#nomeTree").show("slow");
    });	

    /**
     * Toggle tree branches for various tree views
     * Requires 'clean' IDs from the data models (see Category, TimePeriod
     * for filtering functions 
     */	
    $("a.branch_toggle").click(function () {
        var parentname = $(this).attr("id"); // the actual node is the image... the parent is the link, and that's what has the id we want
        var branchname = "ul."+parentname.substr(0,parentname.length - 7); // strip the '_toggle' to get the branchname
        $(branchname).slideToggle('fast');
        // change the image src
        if ($(this).children("img").attr("src").search('plus') >= 0) {
            $(this).children("img").attr("src", '/graphics/minus.gif');
        }
        else {
            $(this).children("img").attr("src", '/graphics/plus.gif');
        }
    });	
	
    /**
     * the tables in the UEE Admin are formatted to
     * be used by the jQuery plugin 'DataTables'
     * This section initializes the tables
     * Tables applied to:
     * - category
     * - gloss
     * - place
     * - timeperiod
     * Typically, Edit/Delete do not need to be sortable
     */
    $("table#category-table").dataTable( {
        "aoData": [
            /* ID */ {"bSortable":true}, 
            /* Name */ {"bSortable":true},
            /* article_id */ {"bSortable":true},
            /* arkId */ {"bSortable":true},
            /* parent name */ {"bSortable":true},
            /* Edit */ {"bSortable":false},
            /* Delete */ {"bSortable":false}
        ]
    } );
	
    $("table#gloss-table").dataTable( {
        "aoData": [
            /* ID */ {"bSortable":true}, 
            /* Name */ {"bSortable":true},
            /* Definition */ {"bSortable":true},
            /* Old Id */ {"bSortable":true},
            /* Edit */ {"bSortable":false},
            /* Delete */ {"bSortable":false}
        ]
    } );

    $("table#timeperiod-table").dataTable( {
        "aoData": [
            /* ID */ {"bSortable":true}, 
            /* Name */ {"bSortable":true},
            /* start */ {"bSortable":true},
            /* end */ {"bSortable":true},
            /* parent time period*/ {"bSortable":false},
            /* Edit */ {"bSortable":false},
            /* Delete */ {"bSortable":false}
        ]
    } );
	
    $("table#places-table").dataTable( {
        "aoData": [
            /* Name */ {"bSortable":true},
            /* AltNames */ {"bSortable":false},
            /* Type */ {"bSortable":true},
            /* ID */ {"bSortable":true},
            /* old ID */ {"bSortable":true},
            /* parents */ {"bSortable":false},
            /* Edit */ {"bSortable":false},
            /* Delete */ {"bSortable":false},
            /* Publish */ {"bSortable":false}
        ]
    } );
	
    $("table#referenceChecks").dataTable( {
        "aoData": [
            /* ID */ {"bSortable":true},
            /* Type */ {"bSortable":true},
            /* Target */ {"bSortable":true},
            /* Reference Text */ {"bSortable":true},
            /* Database/Expected Value */ {"bSortable":false},
        ]
    } );

    // pending-users table
    $("table.users").dataTable( {
        "aoData": [
            /* ID */ {"bSortable":true},
            /* Name */ {"bSortable":true},
            /* Title */ {"bSortable":true},
            /* Status */ {"bSortable":false},
            /* Actions */ {"bSortable":false},
        ]
    } );

    /**
     * The UEE users table will contain users and display their status
     * Two tables exist, one for pending users, and one for those
     * awaiting approval
     */
    $("div#all").hide();

    $("a.showpending").click(function() {
        $("div#all").hide();
        $("div#approval").show();
    });

    $("a.showall").click(function() {
        $("div#approval").hide();
        $("div#all").show();
    });

    /**
     * Add an extra timeperiod name field
     * Usage: TimePeriod add/edit forms
     */	
    $("p.timeperiodname_row a").click(function () {
        var newNumber = $(this).parent(".timeperiodname_row").parent().children(".timeperiodname_row").size();
        var newNodeId = "name_field_" + newNumber;
        // add a new row
        $(this).parent(".timeperiodname_row").clone().insertBefore("#timeperiodform_submit");
        // adjust some attributes for the new row
        $(".timeperiodname_row:last").attr("id", "timeperiodname_" + newNumber);
        $(".timeperiodname_row:last label").attr("for", newNodeId);
        $(".timeperiodname_row:last input").attr("id", newNodeId);
        // any rows except the first row don't need a 'More' link
        $(".timeperiodname_row:last a").remove();
    });

});
