/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * timemap3.js
 * A series of jquery behaviors related to displaying the timemap on the
 * UEE public display
 *
 * Known issues:
 * 
 *
 */

$(document).ready(function() {

    /**
     * Initialize the time slider
     *
     * The time slider makes use of a jQuery UI component known as a 'slider'
     * TODO - discuss the parameters of the slider
     */

    // load map	 
    mapInit()
	 
	 
	$("#slider").slider( 
		{
			'range':true,
			'min':-5000,
			'max':500,
			'values':[-5000,500],
			'slide':function(e, ui) { 
				$("#min-time-period-val").val(ui.values[0]); 
				$("#max-time-period-val").val(ui.values[1]);				
				var left = Math.floor(900*(ui.values[0]+4750)/5500);
				$("#min-time-period-val").css("position","relative"); 
				$("#min-time-period-val").css("left",left+"px"); 
				left = Math.floor(900*(ui.values[1]-150)/(5500));
				$("#max-time-period-val").css("position","relative"); 
				$("#max-time-period-val").css("left",left+"px"); 
			},
			'stop':function(e, ui) {
				if ($("#slider").slider("values",0) == 0) {
					$("#slider").slider("values",0,1);
					$("#min-time-period-val").val($("#slider").slider("values",0)); 
					var left = Math.floor(900*($("#slider").slider("values",0)+4750)/5500);
					$("#min-time-period-val").css("position","relative"); 
					$("#min-time-period-val").css("left",left+"px"); 
				}
				if ($("#slider").slider("values",1) == 0) {
					$("#slider").slider("values",1,1);
					$("#max-time-period-val").val($("#slider").slider("values",1)); 
					var left = Math.floor(900*($("#slider").slider("values",1)-150)/5500);
					$("#max-time-period-val").css("position","relative"); 
					$("#max-time-period-val").css("left",left+"px"); 
				}
				$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
					if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
					{
						if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
							alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
						return;
					}
					console.log("data");
                    console.log(data.length);
                    makeMarkers(data);
					
                    $("form#search-map-form label#search-status").html("Done").hide("slow");
				} );	
			}
		}
	);
	var left = Math.floor(900*($("#slider").slider("values", 0)+4750)/5500);
	$("#min-time-period-val").css("position","relative"); 
	$("#min-time-period-val").css("left",left+"px"); 
	left = Math.floor(900*($("#slider").slider("values", 1)-150)/(5500));
	$("#max-time-period-val").css("position","relative"); 
	$("#max-time-period-val").css("left",left+"px"); 	
	
	// i'd like a way to text type the filters....
	$("#min-time-period-val").blur( function(event) {
		// alert($("#min-time-period-val").val());
		// update the slider?
		var minval = $("#min-time-period-val").val();
		var maxval = $("#max-time-period-val").val();
		if (minval < -5000)
			minval = -5000;
		if (maxval > 500)
			maxval = 500;
		if (minval == 0)
			minval = 1;
		if (maxval == 0)
			maxval = 1;
		$("#min-time-period-val").val(minval);	
		$("#max-time-period-val").val(maxval);	
		var left = Math.floor(900*(parseInt(minval)+4750)/5500);
		$("#min-time-period-val").css("left",left+"px"); 
		left = Math.floor(900*(maxval-150)/(5500));
		$("#max-time-period-val").css("left",left+"px"); 			
		$("#slider").slider('destroy');		
		$("#slider").slider(
			{
				'range':true,
				'min':-5000,
				'max':500,
				'values':[minval,maxval],
				'slide':function(e, ui) { 
					$("#min-time-period-val").val(ui.values[0]); 
					$("#max-time-period-val").val(ui.values[1]); 
					var left = Math.floor(900*(ui.values[0]+4750)/5500);
					$("#min-time-period-val").css("left",left+"px"); 
					left = Math.floor(900*(ui.values[1]-150)/(5500));
					$("#max-time-period-val").css("left",left+"px"); 
				},
				'stop':function(e, ui) {
					if ($("#slider").slider("values",0) == 0) {
						$("#slider").slider("values",0,1);
						$("#min-time-period-val").val($("#slider").slider("values",0)); 
						var left = Math.floor(900*($("#slider").slider("values",0)+4750)/5500);
						$("#min-time-period-val").css("position","relative"); 
						$("#min-time-period-val").css("left",left+"px"); 
					}
					if ($("#slider").slider("values",1) == 0) {
						$("#slider").slider("values",1,1);
						$("#max-time-period-val").val($("#slider").slider("values",1)); 
						var left = Math.floor(900*($("#slider").slider("values",1)-150)/5500);
						$("#max-time-period-val").css("position","relative"); 
						$("#max-time-period-val").css("left",left+"px"); 
					}
					$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
						if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
						{
							if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
								alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
							return;
						}
						makeMarkers(data);
						$("form#search-map-form label#search-status").html("Done").hide("slow");
					} );	
				}
			}
		);
		$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
	} );
	
	$("#max-time-period-val").blur( function(event) {
		// alert($("#min-time-period-val").val());
		// update the slider?
		var minval = $("#min-time-period-val").val();
		var maxval = $("#max-time-period-val").val();
		if (minval < -5000)
			minval = -5000;
		if (maxval > 500)
			maxval = 500;
		if (minval == 0)
			minval = 1;
		if (maxval == 0)
			maxval = 1;
		$("#min-time-period-val").val(minval);	
		$("#max-time-period-val").val(maxval);	
		var left = Math.floor(900*(parseInt(minval)+4750)/5500);
		$("#min-time-period-val").css("left",left+"px"); 
		left = Math.floor(900*(maxval-150)/(5500));
		$("#max-time-period-val").css("left",left+"px"); 					
		$("#slider").slider('destroy');		
		$("#slider").slider(
			{
				'range':true,
				'min':-5000,
				'max':500,
				'values':[minval,maxval],
				'slide':function(e, ui) { 
					$("#min-time-period-val").val(ui.values[0]); 
					$("#max-time-period-val").val(ui.values[1]); 
					var left = Math.floor(900*(ui.values[0]+4750)/5500);
					$("#min-time-period-val").css("left",left+"px"); 
					left = Math.floor(900*(ui.values[1]-150)/(5500));
					$("#max-time-period-val").css("left",left+"px"); 
				},
				'stop':function(e, ui) {
					if ($("#slider").slider("values",0) == 0) {
						$("#slider").slider("values",0,1);
						$("#min-time-period-val").val($("#slider").slider("values",0)); 
						var left = Math.floor(900*($("#slider").slider("values",0)+4750)/5500);
						$("#min-time-period-val").css("position","relative"); 
						$("#min-time-period-val").css("left",left+"px"); 
					}
					if ($("#slider").slider("values",1) == 0) {
						$("#slider").slider("values",1,1);
						$("#max-time-period-val").val($("#slider").slider("values",1)); 
						var left = Math.floor(900*($("#slider").slider("values",1)-150)/5500);
						$("#max-time-period-val").css("position","relative"); 
						$("#max-time-period-val").css("left",left+"px"); 
					}
					$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
						if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
						{
							if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
								alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
							return;
						}
						makeMarkers(data);
						$("form#search-map-form label#search-status").html("Done").hide("slow");
					} );	
				}
			}
		);
		$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
	} );
	
	// if the user clicks one of the phase boxes, toggle the slider to that particular range?
	$("a.phase-link").hover( 
		function() {
			$(this).addClass("phase-hover");
		},
		function() {
			$(this).removeClass("phase-hover");
		}		
	);

	$("a.phase-link").click( function() {
		// desired behavior
		// 'select' on this link
		// show the dynasties for this link (if rulers listed, remove rulers)
		// shrink the phases row to smaller height
		$("a.phase-link").removeClass("phase-selected"); // untoggle other selected phases
		$("div#rulers > *").remove();		
		$(this).addClass("phase-selected");
		$("a.phase-link").css("height","5px");
		var pId = -1;
		var phaseName = $(this).attr("id");
		$.get("/modules/front/ajax/get_phases.php", function(data) {
			var phaseArr = eval("(" + data + ")");			
			for (var i = 0; i < phaseArr.length; i++)
			{
				var phase = phaseArr[i];
				if ("phase-"+phase.time_period_name.replace(/ /g,"") == phaseName)
				{
					var prettyPhaseName = phase.time_period_name + " Phase";
					var prettyPhaseTag = "<a class='phase'>"+prettyPhaseName+"</a>";
					$("#timeline-breadcrumbs").empty().html(prettyPhaseTag);
					if (phase.year_start < -5000)
						phase.year_start = -5000;
					$("#slider").slider("values",0,phase.year_start);
					$("#slider").slider("values",1,phase.year_end);
					// todo: can I animate the text inputs?
					$("#min-time-period-val").val($("#slider").slider("values", 0)); 
					$("#max-time-period-val").val($("#slider").slider("values", 1)); 
					var left = Math.floor(900*($("#slider").slider("values", 0)+4750)/5500);
					$("#min-time-period-val").animate( {"left":left+"px"}, 1000); 					
					left = Math.floor(900*($("#slider").slider("values", 1)-150)/(5500));
					$("#max-time-period-val").animate( {"left":left+"px"}, 1000); 					
					pId = phase.time_period_id;
					// update the markers
					$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
						if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
						{
							if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
								alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
							return;
						}
						makeMarkers(data);
						$("form#search-map-form label#search-status").html("Done").hide("slow");
					} );	
					// set up the dynasties div?
					$.get("/modules/front/ajax/get_dynasties.php", { parentId:pId }, function(data) {
						$("div#dynasties > *").remove();
						makeDynasties(data);
						// add the behaviors...
						$("a.dynasty-link").click( function() {
							$("a.dynasty-link").removeClass("dynasty-selected"); // untoggle other selected phases
							$(this).addClass("dynasty-selected");
							$("a.dynasty-link").css("height","5px");
							// update the slider
							var dynastyName = $(this).attr("id");
							// alert(data);
							var dId = 0;
							var dynastyArr = eval("(" + data + ")");
							for (var j = 0; j < dynastyArr.length; j++)
							{
								var dynasty = dynastyArr[j];
								// alert(dynasty.time_period_name.replace(/ /g,""));
								if ("dynasties-"+dynasty.time_period_name.replace(/ /g,"") == dynastyName)
								{
									var prettyDynastyName = dynasty.time_period_name;
									$("#timeline-breadcrumbs").empty();
									var prettyDynastyTag = "<a class='dynasty'>"+prettyDynastyName+"</a>";
									$("#timeline-breadcrumbs").append(prettyPhaseTag);			
									$("#timeline-breadcrumbs").append(" > ");			
									$("#timeline-breadcrumbs").append(prettyDynastyTag);
									$("#timeline-breadcrumbs a.phase").attr("href","javascript:;").attr("name",phaseName);
									// breadcrumb clicks
									$("a.phase").click(function() {
										// alert($(this).attr("name"));
										$('a.phase-link[id="'+$(this).attr("name")+'"]').click();
									});
									if (dynasty.year_start < -5000)
										dynasty.year_start = -5000;								
									$("#slider").slider("values",0,dynasty.year_start);
									$("#slider").slider("values",1,dynasty.year_end);
									$("#min-time-period-val").val($("#slider").slider("values", 0)); 
									$("#max-time-period-val").val($("#slider").slider("values", 1));				
									var left = Math.floor(900*($("#slider").slider("values", 0)+4750)/5500);
									$("#min-time-period-val").css("position","relative"); 
									$("#min-time-period-val").animate( {"left":left+"px"}, 750); 	
									left = Math.floor(900*($("#slider").slider("values", 1)-150)/(5500));
									$("#max-time-period-val").css("position","relative"); 
									$("#max-time-period-val").animate( {"left":left+"px"}, 750); 	
					 					
									dId = dynasty.time_period_id;
									// update the markers
									$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(markerdata) {
										if (!markerdata || markerdata == null || markerdata.length <= 2) // an empty set returns '[  ]'
										{
											if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
												alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
											return;
										}
										makeMarkers(markerdata);
										$("form#search-map-form label#search-status").html("Done").hide("slow");
									} );	
									// set up the rulers div
									$.get("/modules/front/ajax/get_dynasties.php", { parentId:dId }, function(rulerdata) {
										$("div#rulers > *").remove();
										makeRulers(rulerdata);
										// todo: Add the behaviors;
										$("a.ruler-link").hover( 
											function() {
												$(this).addClass("ruler-hover");
											},
											function() {
												$(this).removeClass("ruler-hover");
											}		
										);	
										$("a.ruler-link").click( function() {
											$("a.ruler-link").removeClass("ruler-selected"); // untoggle other selected phases
											$(this).addClass("ruler-selected");
											// update the slider
											var rulerName = $(this).attr("id");
											// alert(rulerName);
											// alert(data);
											var rId = 0;
											var rulerArr = eval("(" + rulerdata + ")");
											for (var k = 0; k < rulerArr.length; k++)
											{
												var ruler = rulerArr[k];
												if ("rulers-"+ruler.time_period_name.replace(/ /g,"") == rulerName)
												{
													var prettyRulerName = "Reign of " + ruler.time_period_name;
													$("#timeline-breadcrumbs").empty();
													var prettyRulerTag = "<a class='ruler'>"+prettyRulerName+"</a>";
													$("#timeline-breadcrumbs").append(prettyPhaseTag);			
													$("#timeline-breadcrumbs").append(" > ");			
													$("#timeline-breadcrumbs").append(prettyDynastyTag);
													$("#timeline-breadcrumbs").append(" > ");			
													$("#timeline-breadcrumbs").append(prettyRulerTag);			
													$("#timeline-breadcrumbs a.phase").attr("href","javascript:;").attr("name",phaseName);
													$("#timeline-breadcrumbs a.dynasty").attr("href","javascript:;").attr("name",dynastyName);
													// breadcrumb clicks
													$("a.phase").click(function() {
														// alert($(this).attr("name"));
														$('a.phase-link[id="'+$(this).attr("name")+'"]').click();
													});
													$("a.dynasty").click(function() {
														// alert($(this).attr("name"));
														$('a.dynasty-link[id="'+$(this).attr("name")+'"]').click();
													});
													if (ruler.year_start < -5000)
														ruler.year_start = -5000;								
													// alert(ruler.time_period_name);
													$("#slider").slider("values",0,ruler.year_start);
													$("#slider").slider("values",1,ruler.year_end);
													$("#min-time-period-val").val($("#slider").slider("values", 0)); 
													$("#max-time-period-val").val($("#slider").slider("values", 1));
													var left = Math.floor(900*($("#slider").slider("values", 0)+4750)/5500);
													$("#min-time-period-val").animate( {"left":left+"px"}, 500); 	
													left = Math.floor(900*($("#slider").slider("values", 1)-150)/(5500));
													$("#max-time-period-val").animate( {"left":left+"px"}, 500); 	
													$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(markerdata) {
														if (!markerdata || markerdata == null || markerdata.length <= 2) // an empty set returns '[  ]'
														{
															if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
																alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
															return;
														}
														makeMarkers(markerdata);
														$("form#search-map-form label#search-status").html("Done").hide("slow");
													} );												
													break;
												}												
											}
										} );
									} )
									break;
								}
							}
							
							// update the marker
						} );
						
						$("a.dynasty-link").hover( 
							function() {
								$(this).addClass("dynasty-hover");
							},
							function() {
								$(this).removeClass("dynasty-hover");
							}		
						);							
					} );
					break;
				}
			}
		} );
	} );
		
	// load the timemap with the page load
	$.get("/modules/front/ajax/get_markers.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1) }, function(data) {
		makeMarkers(data);
	} );	
	
	// how to reset the timeline slider?
	$("a#reset-timeline").click( function() {
		$("div#rulers > *").remove();	
		$("div#dynasties > *").remove();
		$("a.phase-link").removeClass('phase-selected');
		$("#slider").slider('values',0,-5000);	
		$("#slider").slider('values',1,500);	
		$("#min-time-period-val").val($("#slider").slider('values',0)); 
		$("#max-time-period-val").val($("#slider").slider('values',1)); 
		var left = Math.floor(900*($("#slider").slider('values',0)+4750)/5500);
		$("#min-time-period-val").animate( {"left":left+"px"}, 1000); 			
		left = Math.floor(900*($("#slider").slider('values',1)-150)/(5500));
		$("#max-time-period-val").animate( {"left":left+"px"}, 1000); 		
		$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
		// reset the timeline breadcrumbs 
		$("#timeline-breadcrumbs").empty().html("Full Timeline");
	} );
	
	// search filter
	$("form#search-map-form input#search-map-submit").click( function() {
		$("form#search-map-form label#search-status").html("Searching").show();
		$.get("/modules/front/ajax/get_markers_livesearch.php", { startYear:$("#slider").slider("values", 0), endYear:$("#slider").slider("values", 1), placeName:$("form#search-map-form input#keyword").val() }, function(data) {
			if (!data || data == null || data.length <= 2) // an empty set returns '[  ]'
			{
				if ( $("#slider").slider("values", 0) > -5000 || $("#slider").slider("values", 1) < 500 )
					alert("No results were found. Please note that the time slider parameters are accounted for in the search results; you may have to adjust the slider to get the desired result set.");
				return;
			}
			makeMarkers(data);
			$("form#search-map-form label#search-status").html("Done").hide("slow");
		} );	
		return false;
	} );
	
	// timeline breadcrumbs
		
});

// google maps behaviors (for data entry: place records)
// holders for the marker data
var gmarker = null; // this is for point_mode;
var gpolyline = new Array(); // this is for line_mode;
var gpolygon = new Array(); // this is for polygon_mode;

// poly options - something needed for drawing polygons
var polyOptions = {geodesic:true};		

// holders for the 'mode' of the marker placement
// var geomap_mode = "point_mode";
var geomap_mode = "";
var coord_mode = 'latlong_mode';
var map;
var cluster;
var marker;
var markersArray=[];

var addMarkerToggle = null; // just want a global so i can remove this behavior...

// v3
function mapInit()
{
    // timemap3.js
	var zoom_level = 4;
    var centerSpot = new google.maps.LatLng(29.8,30.5); // this is pretty close to where Faiyum is...

	var unstyledMapOptions = {
		name: "No POI"
	},
		unstyledMapType = new google.maps.StyledMapType([
                {
                    featureType: 'poi',
                    elementType: 'labels',
                    stylers: [
                        { visibility: 'off' }
                    ]
                }
			],
			unstyledMapOptions
		)
	;
    
    var myOptions = {
        zoom: zoom_level,
        center: centerSpot,
        
        // pan control
        panControl: true,
        
        // street  view: is that little person icon on the left sife of the map.
        streetViewControl: false,
        
        // scale control
        scaleControl: false,
        
        // zoom controls
        zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            },

        // map type control 
        mapTypeControl: true,
            mapTypeControl: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            },

		mapTypeControlOptions: {
        	mapTypeIds: [google.maps.MapTypeId.TERRAIN, 'no-poi-labels', google.maps.MapTypeId.SATELLITE]
		},
        
        mapTypeId: google.maps.MapTypeId.TERRAIN
    };
            
	// making the map
	map = new google.maps.Map(document.getElementById("time-map"), myOptions);
	map.mapTypes.set('no-poi-labels', unstyledMapType)

}
// end mapInit(0

// v3
// makeMarkers(data)
// take the json data, parse it and display the new markers....
function makeMarkers(data) {
	
	// clear the map of any existing markers
	if (map) {
	    // Clearing overlays...
        while(markersArray[0]) {
			//console.log(markersArray.length);
            markersArray.pop().setMap(null);
        }
        
        markersArray.length = 0;
	    markersArray = [];
    }
    
	// clear marker 
	if(marker) 
	{
		marker = '';
	}
	
	// from loop below    
    if(cluster)
    {
        cluster.clearMarkers();
    }
	
    // little bug with json data, needs to be string encapsulated
	// http://willcode4beer.com/tips.jsp?set=jsonInvalidLabel	
	if (data.length <= 0 || !data) 
	{
		return;
    }
	
    // alert(data);
	var placeArr = eval("(" + data + ")");
	
	// alert(placeArr);
	for (var i = 0; i < placeArr.length; i++)
	{
		var place = placeArr[i];
		var markerLatLng = new google.maps.LatLng(place.latitude, place.longitude);	
		
		// markersArray is being populated the function newMarker.
		marker = newMarker(new google.maps.LatLng(place.latitude, place.longitude), place);	
	}

    // marker cluster jenny's design
    
	var mcOptions = {styles: [{
		height: 36,
		width: 35,
		url: "/graphics/Site_Multiple.png",
		textColor: "FFFFFF",
		anchor: [1, 30]
		}],
		gridSize: 35, 
		maxZoom: 15
		};
	
	// default cluster markers
	/*
	var mcOptions = {
		gridSize: 35, 
		maxZoom: 15
		};	
    */
	
	// rendering cluster
	cluster = new MarkerClusterer(map, markersArray, mcOptions);
	
	var bounds = new google.maps.LatLngBounds(),
	    markerList = cluster.getMarkers()
	;
	for (var i = 0; i < markerList.length; i++) {
	    bounds.extend(markerList[i].getPosition());
	}
	map.fitBounds(bounds);
	
	// something happens after clusterer ends.
	// oc: google.maps.event.addListener(markerClusterer, 'clusteringend', myFunction);
	// google.maps.event.addListener(cluster, 'clusteringend', alert('its the end!'));
}
// end makeMarkers()


// v3
function newMarker(markerLocation, place) {
    
    var myLatlng = markerLocation;
	
	// Switching on place.place_type_it...
    switch (place.place_type_id) {
        case 4: // site
            
			var markerIcon = new google.maps.MarkerImage(
    			'/graphics/Site_InUse.png',
        		new google.maps.Size(31, 31),
				null,
				new google.maps.Point(10,25),
				new google.maps.Size(30, 30)
 			);
			break;
			
		case 5: // feature
            
			var markerIcon = new google.maps.MarkerImage(
    			'/graphics/Feature_InUse.png',
        		new google.maps.Size(31, 31),
				null,
				new google.maps.Point(10,25),
				new google.maps.Size(30, 30)
 			);			
			break;
			
		default:
            break;
    }
	
	// console.log(marker);
    // return marker;
    
	// Rendering markers on map.
	var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: place.place_name,
        icon: markerIcon
    });


	// Render infoWindow
	var infoWindow = new google.maps.InfoWindow({
    	maxWidth: 200
  	});

	//==
	function load_content(marker, place)
	{
    	var myHtml = "<div class='geomarker-popup' style='height:100px;'><p><strong>" + place.place_name + "</strong><br />";
		var myHtml2 = '';
    	var counts;
    	// myHtml += "<em>From " + place.year_start + " to " + place.year_end + "</em></p><div id='sal-"+place.place_id+"'>loading...</div>";
    	myHtml += "<em>From " + place.year_start + " to " + place.year_end + "</em></p><div id='sal-"+place.place_id+"'></div>";
		//-- ajax stuff
    	$.get("/modules/front/ajax/get_search_counts.php", { place_name:place.place_name, old_id:place.old_id, place_id:place.place_id }, function(data) {
        
        	if (!data || data == null || data.length <= 0)
        	{
            	myHtml2 += '<p>There was an error getting the related information</p>';
            	return;
        	}
        
        	counts = eval("(" + data + ")");
        	place.articles = counts.articles;
        	place.images = counts.images;       
    
        	myHtml2 += "<ul>";
        	if (place.articles > 0) {
            	myHtml2 += "<li><a href='/search/placeId=" + place.place_id + "'>articles (" + place.articles + ")</a></li>";
        	}
        	if (place.images > 0) {
            	myHtml2 += "<li><a href='/imageresults/placeId="+place.place_id + "'>images (" + place.images + ")</a></li>";
        	}
        	myHtml2 += "</ul></div>";
        
        	//console.log(myHtml2);
		
			//add link data to infowindow
			var salwithid = '#sal-'+place.place_id;   
			$(salwithid).html(myHtml2);

     	});
		//--      
    	infoWindow.setContent(myHtml);
    	infoWindow.open(map, marker);
	}
	//==
	// end: load_content()
	
    	google.maps.event.addListener(marker, 'click', function() {
 			infoWindow.close();
			load_content(marker, place);
    	});
    	markersArray.push(marker);
}
// end of newMarker()


// makeDynasties
// makem them dynasty blocks
function makeDynasties(data) {
	if (data.length <= 0 || !data)
		return;
	var dynastyArr = eval("(" + data + ")");
	var width = Math.floor(960*0.98) - dynastyArr.length*3;
	var sum = 0; 
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;			
		
		sum += (dynasty.year_end - year_start);
	}
	var dynastiesDiv = document.getElementById('dynasties');
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;
		
		var range = (dynasty.year_end - year_start);
		var pixels = Math.round((width*range/sum)) - 4;
		if (pixels < 5)
			pixels = 5;		
		var anchor = document.createElement("a");
		var idAttr = document.createAttribute("id");
		idAttr.nodeValue='dynasties-'+dynasty.time_period_name.replace(/ /g,"");
		anchor.setAttributeNode(idAttr);
		var classAttr = document.createAttribute("class");
		classAttr.nodeValue='dynasty-link';
		anchor.setAttributeNode(classAttr);
		var hrefAttr = document.createAttribute("href");
		hrefAttr.nodeValue='javascript:;';
		anchor.setAttributeNode(hrefAttr);
		var titleAttr = document.createAttribute("title");
		titleAttr.nodeValue='view places in the ' + dynasty.time_period_name + ' dynasty';
		anchor.setAttributeNode(titleAttr);
		var text = document.createTextNode(" ");
		anchor.appendChild(text);
		anchor.style.width = pixels + "px";	
		dynastiesDiv.appendChild(anchor);
		// $("#"+idAttr.nodeValue).hover( function() {
			
		// } );
	}
}

// makeRulers
// makem them dynasty blocks
function makeRulers(data) {
	if (data.length <= 0 || !data)
		return;
	var dynastyArr = eval("(" + data + ")");
	var width = Math.floor(960*0.98)-dynastyArr.length*3;
	var sum = 0; 
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;			
		
		sum += (dynasty.year_end - year_start);
	}
	var dynastiesDiv = document.getElementById('rulers');
	for (var i = 0; i < dynastyArr.length; i++)
	{
		var dynasty = dynastyArr[i];
		if (dynasty.year_end < -5000)
			continue;
		var year_start = 0;
		if (dynasty.year_start < -5000)
			year_start = -5000;
		else
			year_start = dynasty.year_start;
		if (dynasty.year_end > 500)
			year_end = 500;
		else
			year_end = dynasty.year_end;
		
		var range = (dynasty.year_end - year_start);
		var pixels = Math.floor((width*range/sum)) - 5;
		if (pixels < 5)
			pixels = 4;
		var anchor = document.createElement("a");
		var idAttr = document.createAttribute("id");
		idAttr.nodeValue='rulers-'+dynasty.time_period_name.replace(/ /g,"");
		anchor.setAttributeNode(idAttr);
		var classAttr = document.createAttribute("class");
		classAttr.nodeValue='ruler-link';
		anchor.setAttributeNode(classAttr);
		var hrefAttr = document.createAttribute("href");
		hrefAttr.nodeValue='javascript:;';
		anchor.setAttributeNode(hrefAttr);
		var titleAttr = document.createAttribute("title");
		titleAttr.nodeValue='view places in the ' + dynasty.time_period_name + ' rulership';
		anchor.setAttributeNode(titleAttr);
		var text = document.createTextNode(" ");
		anchor.appendChild(text);
		anchor.style.width = pixels + "px";	
		dynastiesDiv.appendChild(anchor);
		// $("#"+idAttr.nodeValue).hover( function() {
			
		// } );
	}
}
