<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

// /modules/soap/server.php
// the soap server module - a soap server object
	
// i gotta get some files!
require_once('config.php'); // we need this for database connection
	
function __autoload($className) {
    // let's truncate the 'MVC_' from the object name
    $objectName = substr($className,4);
    $objectName = str_replace("_","/",$objectName);
    // where's the object file located?
    $objectDir = substr((__FILE__),0,-9)."/includes/"; // the substr removes the soap.php
    $objectPath = $objectDir.$objectName.'.php';
    echo $objectPath .'<br />';
    if (!file_exists($objectPath))
        die( "You tried to load a file that doesn't exist: $objectPath");
    require_once($objectPath);
}
	
// here, the actual soap function behaviors
// has to go in the public scope, or it's not as easy to call
function getCategories($cat_id = -1, $name = '') {
    $category = new MVC_Model_Category(null);
    if (!empty($cat_id) and $cat_id != null and is_numeric($cat_id) and $cat_id > 0)
    {
        $category->search['parent_category_id'] = $cat_id;
        $category->dbSearchTable();
        $rows = $category->search_results;
    }
    else if (!empty($name) and $name != null and strlen($name) > 0) // this is not right, we want the children of the category with this now...
    {
        $category->search['category'] = $name;
        $category->dbSearchTable();
        $rows = $category->search_results;
        // what to do...
        if ($rows)
        {
            $cat_id = $rows[0]['category_id'];
            $category->resetSearch();
            $category->search['parent_category_id'] = $cat_id;
            $category->dbSearchTable();
            $rows = $category->search_results;
        }
    }
    else
        $rows = $category->getAllRows();
    $results = array();
    foreach ($rows as $r)
    {
        $results[] = array(
            'categoryId'=>$r['category_id'],
            'categoryName'=>$r['category'],
            'categoryParentId'=>$r['parent_category_id'],
            'categoryEscholarshipId'=>$r['escholar_article_id']
        );
    }
    return $results;
}
	
// $something var doesn't do anything; I just want a placeholder for now.
// not really sure how to handle when we don't have a set of input parameters in the WSDL
function getGlossNames($something='') {
    $gloss = new MVC_Model_GlossName(null);
    $rows = $gloss->getAllRows();	
    $results = array();
    $count = 0;
    foreach ($rows as $r)
    {
        $results[] = array(
            'glossName'=>htmlentities($r['gloss_name']), // problems with special umlaut character?
            'glossNameId'=>$r['gloss_name_id']
        );
    }
    return $results;
}
	
function getTimePeriods($tpid = -1, $name = '')
{
    $timeperiod = new MVC_Model_TimePeriod(null);
    $timeperiodname = new MVC_Model_TimePeriodName(null);
		
    if (!empty($tpid) and $tpid != null and is_numeric($tpid) and $tpid > 0)
    {
        $timeperiod->search['parent_time_period_id'] = $tpid;		
        $timeperiod->dbSearchTable();
        $rows = $timeperiod->search_results;
    }
    else if (!empty($name) and $name != null and strlen($name) > 0)
    {
        $timeperiodname->search['time_period_name'] = $name;
        $timeperiodname->dbSearchTable();
        $rows = $timeperiodname->search_results;
        // what to do...
        if ($rows)
        {
            $tpid = $rows[0]['time_period_id'];
            $timeperiod->resetSearch();
            $timeperiod->search['parent_time_period_id'] = $tpid;
            $timeperiod->dbSearchTable();
            $rows = $timeperiod->search_results;
        }
    }	
    else
        $rows = $timeperiod->getAllRows();
    $results = array();
    foreach ($rows as $r)
    {
        $names = $timeperiod->getTimePeriodNames($r['time_period_id']);
        $results[] = array(
            'timePeriodId'=>$r['time_period_id'],
            'timePeriodName'=>implode(", ",$names),
            'timePeriodParentId'=>$r['parent_time_period_id']
        );
    }
    return $results;
}
	
function getPlaceNames($something='')
{
    $placename = new MVC_Model_PlaceName(null);
    $place = new MVC_Model_Place(null);
    $rows = $placename->getAllRows();
    $results = array();
    foreach ($rows as $r)
    {
        if ($r['place_name']) // sometimes we're getting empty name records???
        {
            $results[] = array(
                'placeId'=>$r['place_id'],
                'placeName'=>$r['place_name'],
                'placeNameType'=>$place->getPlaceNameType($r['place_name_type_id'])
            );
        }
    }
    return $results;
}
	
$opts = Array();
$opts['compression'] = SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP;
$opts['cache_wsdl'] = WSDL_CACHE_NONE;
	
$server = new SoapServer("/modules/soap/uee9i.wsdl", $opts);
$server->addFunction("getCategories");
$server->addFunction("getGlossNames");
$server->addFunction("getPlaceNames");
$server->addFunction("getTimePeriods");
$server->handle();

?>
