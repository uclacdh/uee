<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * checkSoap.php
 *
 * A script, intended to be run by a cron job or some other service
 * that notifies various people if the soap service is unavailable
 * usage: scheduled task/cron job; can be pulled up by a webbrowser or wget
 *
 * Known issues 7/18/2011
 * Sometimes there's some issues with the case-sensitivity of the filenames
 * We try to resolve this using strtolower on the filepaths
 *
 *  Andrew Tai
 * Sept 10, 2009
 * adtai@ats.ucla.edu
 */
	
// TODO 7/18/2011 - add some restrictions on IP or some other lockdown
	
// load any global config variables
require_once('config.php');
require_once(dirname(__FILE__).'/modules/soap/libraryObjects.php');	
	
// are we gonna be able to get to the soap service?
try {
    $tempSoap = @new SoapClient(WSDL, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
    $articlesRes = $tempSoap->listAllArticles();
    if (!property_exists($articlesRes,'return')) // no return set
        throw new Exception("No return set; it's likely the soap service is down"."\r\n");
    if (count($articlesRes->return)<= 0) // empty return set)
        throw new Exception("Empty return set; is the soap service returning data?"."\r\n");
    // here, we know the article exists and there should be a copy on DLCS we could download.
    foreach ($articlesRes->return as $article)
    {
        print_r($article);
        echo "<br />";
        $article_filename = str_replace(array(" ","/"),array("_","-"),strtolower($article->title)).'.xml';
        $article_path = $_SERVER['DOCUMENT_ROOT'] .'/cache/articles/'.$article_filename;
        // echo '<br />';
        echo $article_filename. '<br />';
        echo $article_path. '<br />';
        if (file_exists($article_path)) {
            echo 'cached: <br />';
            echo date("F d Y H:i:s", filemtime($article_path)) .'<br />';
            // echo filemtime($article_path) .'<br />';
            if ($article->xmlCreateDate)
            {
                echo 'created on DLCS: <br />';
                echo $article->xmlCreateDate .'<br />';
                // echo strtotime($article->createDate) .'<br />';
                if (strtotime($article->xmlCreateDate) > filemtime($article_path)) {
                    echo "<strong>Need Update</strong><br />";
                    // todo: update!!!
                    // $testUrl = "http://feeds.gawker.com/kotaku/full";
                    // copy($testUrl,"./testxml.xml");
                    // die();
                    if (!empty($article->xmlURL) and copy($article->xmlURL, $article_path) )
                    {
                        echo "<strong><em>Updated</em></strong><br />";
                    }
                    else
                        throw new Exception("error updating article ".$article->title. " from DLCS");
                }
                else {
                    echo "<em>does not need update</em>";
                    $specificArticle = str_replace(" ","_",$article->title);
                    echo "&nbsp;"."<a href='/forceCache.php?specificArticle={$specificArticle}'>force update</a>";
                    echo "<br />";
                }
            }
        }
        else {
            echo "<strong>Does not exist in cache; caching...</strong><br />";
            // todo: update!!!
            if (!empty($article->xmlURL) and copy($article->xmlURL, $article_path) ) {
                echo "<strong><em>Created</em></strong><br />";
            }
            else if (empty($article->xmlURL)) {
                echo "<strong>No url provided, skipping</strong><br />";
            }
            else {
                ; // throw new Exception("error caching article ".$article->title. " from DLCS");
            }
        }
        echo '<br />';
    }
    return true; // success
} catch (Exception $e) {
    // email the appropriate peoples
    ini_set("SMTP","em2.ad.ucla.edu"); // set the outgoing mail server

    /**
     * Henry Chiong is the primary developer at UCLA Library IT
     * TODO 7/18/2011 - Find out if there's a more generic e-mail address to use
     */						
    $to = 'uee@humnet.ucla.edu'; 
    $subject = '[ueeweb] Soap Service seems offline';
    $message = 'Hi Henry,'."\r\n".
        'It appears the soap services is down. Could you please check for us? Thanks!'."\r\n".
        'Error output: '.$e->getMessage()." \r\n".
        'ATS Staff'."\r\n".
        '[Automatically generated via PHP script]'."\r\n";
    $headers = 'From: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'Cc: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'Reply-To: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();
    // send_success = mail($to,$subject,$message, $headers);
    // return $send_success;
}

?>

