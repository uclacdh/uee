<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * footer.php
 *
 * October 20, 2008
 * adtai@ats.ucla.edu (Andrew Tai) 
 * Generic footer file for the UEE application
 * Conditionally included by index.php as necessary
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

/**
 * This is a "Docblock Comment," also known as a "docblock." The class'
 * docblock, below, contains a complete description of how to write these.
 */

// TODO 6/27/11
// Apply the Pear PHP coding standards
// http://pear.php.net/manual/

/**
 * NOTE: If the footer changes, please check that the changes are applied to
 * the wordpress ueetheme, as desired
 */
	
?>
<!-- end content -->
</div>
<div id='footer'>
    <div id='footer-contact'>
        <div class='column_half' >
            <p>&copy; UC Regents</p>
        </div>
        <div class='column_half' style='text-align:right;'>
            <p><a href="mailto:uee@humnet.ucla.edu">Send Feedback to Webmaster</a></p>
        </div>
        <div class='clearfix'></div>
    </div>
</div>
<!-- end wrapper -->
</div>
</body>
</html>
