<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 *checkSoap.php
 * a script, intended to be run by a cron job or some other service that
 * notifies various people if the soap service is unavailable
 * usage: scheduled task/cron job; can be pulled up by a webbrowser or wget
	
 * Andrew Tai
 * Sept 10, 2009
 * adtai@ats.ucla.edu	

 * Known issues:
 * TODO 7/18/2011 - replace specific e-mails with generic contacts

 */
	
// load any global config variables
require_once('config.php');
require_once(dirname(__FILE__).'/modules/soap/libraryObjects.php');	
	
// are we gonna be able to get to the soap service?
try {
	// throw new Exception("testing an automated email; please ignore");
    $tempSoap = @new SoapClient(WSDL, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
    $articlesRes = $tempSoap->listAllArticles();
    if (!property_exists($articlesRes,'return')) // no return set
        throw new Exception("No return set; it's likely the soap service is down"."\r\n");
    if (count($articlesRes->return)<= 0) // empty return set)
        throw new Exception("Empty return set; is the soap service returning data?"."\r\n");
    return true; // success
} catch (Exception $e) {
    // email the appropriate peoples
    ini_set("SMTP","em2.ad.ucla.edu"); // set the outgoing mail server
	
    /**
     * Henry Chiong is the primary contact person at UCLA Library IT
     * TODO - we should replace w/ a more generic contact
     */				
    $to = 'uee@humnet.ucla.edu'; 
    $subject = '[ueeweb] Soap Service seems offline';
    $message = 'Hi Henry,'."\r\n".
        'It appears the soap services is down. Could you please check for us? Thanks!'."\r\n".
        'Error output: '.$e->getMessage()." \r\n".
        'ATS Staff'."\r\n".
        '[Automatically generated via PHP script]'."\r\n";
    $headers = 'From: uee@humnet.ucla.edu' . "\r\n";
    /**
     * Primary contacts at UCLA ATS / UCLA Library
     * TODO - replace w/ more specific contacts
     */
    $headers .= 'Cc: , uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'Reply-To: uee@humnet.ucla.edu' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();
    $send_success = mail($to,$subject,$message, $headers);
    return $send_success;
}

?>

