<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * client-lib2.php
 * A series of tests for the UCLA Library Soap service
 * Should be fairly self documenting

 * Andrew Tai (adtai@ats.ucla.edu)
 * UCLA Academic Technology Services
 * 7/18/2011

 * Known issues:
 * TODO 7/18/2011 - What is the difference between this and client-lib3.php?
 */

require_once('config.php'); // need this for the WSDL address
$wsdl_url = WSDL;
	
// let's define us the corresponding data objects
// pushed to separate file
$objectsPath = dirname(__FILE__).'/modules/soap/libraryObjects.php';
if (!file_exists($objectsPath))
    die("Error opening corresponding Objects data: $objectsPath");
require_once($objectsPath);
	
echo '<p><strong>WSDL file path</strong><br />';
echo $wsdl_url .'</p>';
try {
    @$client = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
} catch (SoapFault $fault) {
    trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
}		
$funcList = array_unique($client->__getFunctions());
sort($funcList);
echo '<h2>Available Functions</h2>';
echo '<ul>';
foreach ($funcList as $func)
    echo '<li>'.$func.'</li>';
echo '</ul>';

echo '<h2>Data Types</h2>';
echo '<ul>';
$types = array_unique($client->__getTypes());
sort($types);
foreach ($types as $t)
    echo '<li>'.$t.'</li>';
echo '</ul>';

echo '<h2>Test Cases</h2>';
echo '<p><em>Code examples based on PHP SOAP extension</em></p>';
try {
    /*
    TEST CASES
    // expected behavior
    // $client->functionCall($params); // actual result
    */
		
    echo '<p>
        <em>$res = $client->findBibliography(new findBibliography("bib282"));</em><br />
        takes a bibliography reference (based on tei:xml target values) and returns what looks like the article using that citation <br />
        does it return more?
        </p>';
		
    // $res = $client->findBibliography(new findBibliography("bib282")); // OK
    // print_r($res);
    // echo '<br /><br />';

    echo '<p>
        <em>$res = $client->findGloss(new findGloss("g000004")); // temenos</em><br />
        takes a (new) glossaryId from ueeAdmin returns what looks like the article using that citation <br />
        does it return more?
        </p>';

    // $res = $client->findGloss(new findGloss("g000004")); // temenos
    // print_r($res);
    // echo '<br /><br />';

    echo '<p>
        <em>$res = $client->findInText( {keywords:"horus"} );</em> <br />
        takes a string of keywords and returns articles that match			
        </p>';
		
    // $res = $client->findInText(new findInText("horus") );
    // print_r($res);
    // echo '<br /><br /.>';

                echo '<p>
                        <em>$res = $client->findInText( {keywords:"fertility figurines"} );</em> <br />
                        takes a string of keywords and returns articles that match
                </p>';

                $res = $client->findInText(new findInText("fertility figurines") );
                print_r($res);
                echo '<br /><br /.>';

                echo '<p>
                        <em>$res = $client->findInText( {keywords:"fertility"} );</em> <br />
                        takes a string of keywords and returns articles that match
                </p>';

                $res = $client->findInText(new findInText("fertility") );
                print_r($res);
                echo '<br /><br /.>';

                echo '<p>
                        <em>$res = $client->findInText( {keywords:"fertility AND figurines"} );</em> <br />
                        takes a string of keywords and returns articles that match
                </p>';

                $res = $client->findInText(new findInText("fertility AND figurines") );
                print_r($res);
                echo '<br /><br /.>';

                echo '<p>
                        <em>$res = $client->findInText( {keywords:"fertility OR figurines"} );</em> <br />
                        takes a string of keywords and returns articles that match
                </p>';

                $res = $client->findInText(new findInText("fertility OR figurines") );
                print_r($res);
                echo '<br /><br /.>';





		
		echo '<p>
			<em>$res = $client->listAllImages();</em><br />
			Returns an array of all images <br />
			ImageObject(contentFileId, id, thumbnailURL, title)			
		</p>';


		
		echo '<p>
			<em>$res = $client->searchImage(new searchImage("temple"));</em><br />
			Returns a set of images that appear to match the query <br />
			ImageObject(contentFileId, id, thumbnailURL, title)			
		</p>';

		
		// echo '<p>
			// <em>$res = $client->findInGloss( {glossId:, keyword:"tower"} );</em> - OK; use case?
		// </p>';
		
		// echo '<p>
			// <em>$res = $client->findInGloss( {glossId:"g000001", keyword:"tower"} );</em> - OK
		// </p>';
		
		echo '<p>
			<em>$res = $client->repeat( {text:"12345"} );</em> - seems to be giving me late period temples article
		</p>';

		// $res = $client->repeat(new repeat('12345')); // OK
		// print_r($res);
		// echo '<br /><br />';

		echo '<p>
			<em>$res = $client->findPlace( {pid:"p600066"} );</em> - seems to be giving me late period temples article
		</p>';

		// $res = $client->findPlace(new findPlace('p600066')); // OK
		// print_r($res);
		// echo '<br /><br />';
		
		echo '<p>
			<em>$res = $client->findPlace( {pid:"p000297"} );</em> - seems to be giving me late period temples article
		</p>';

		// $res = $client->findPlace(new findPlace('p000297')); // OK
		// print_r($res);
		// echo '<br /><br />';
		
		echo '<p>
			<em>$res = $client->findPlace( {pid:"p000190"} );</em> - seems to be giving me late period temples article
		</p>';

		// $res = $client->findPlace(new findPlace('p000190')); // OK
		// print_r($res);
		// echo '<br /><br />';
		
		// echo '<p>
			// <em>$res = $client->findInGloss( {glossId:"g600008"; keyword:"late"} );</em> - OK; no return set
		// </p>';

		// $res = $client->findInGloss(new findInGloss("g600008","late")); // OK
		// print_r($res);
		// echo '<br /><br />';

		echo '<p>
			<em>$res = $client->findBibliography( {bid:"bib282"} );</em> - OK
		</p>';

		// $res = $client->findBibliography(new findBibliography("bib282")); // OK
		// print_r($res);
		// echo '<br /><br />';
		
		echo '<p>
			<em>$res = $client->findGloss( {glossId:"g000044"} );</em> - temenos
		</p>';

		// $res = $client->findGloss(new findGloss("g000044")); // OK
		// print_r($res);
		// echo '<br /><br />';
		
		// echo '<p>
			// <em>$res = $client->searchImage( {keyword:"tower"} );</em> - OK; use case?
		// </p>';
		
		// echo '<p>
			// <em>$res = $client->getArticle();</em> - disappeared during testing
		// </p>';
		
		echo '<p>
			<em>$res = $client->getRandomArticle();</em> - OK
		</p>';
		
		echo '<p>
			<em>$res = $client->searchArticle( {keyword:"Karnak"} );</em> - OK; no return set
		</p>';
		
		// $res = $client->searchArticle(new searchArticle("Karnak")); // OK
		// print_r($res);
		// echo '<br /><br />';

		// echo '<p>
			// <em>$res = $client->searchImage( {keyword:"late period temples"} );</em> - OK; no return set
		// </p>';
		
		// $res = $client->searchImage(new searchImage("late period temples")); // OK
		// print_r($res);
		// echo '<br /><br />';

		echo '<p>
			<em>$res = $client->searchImage( {keyword:"Late Period Temples"} );</em> - OK; no return set
		</p>';
		
		// $res = $client->searchImage(new searchImage("Late Period Temples")); // OK
		// print_r($res);
		// echo '<br /><br />';
		

		echo '<p>
			<em>$res = $client->getMetadata( {ark:"21198/zz000s5mbz"} );</em> - OK; no return set
		</p>';
		
		// $res = $client->getMetadata(new getMetadata("21198/zz000s5mbz")); // OK
		// print_r($res);
		// echo '<br /><br />';

		// echo '<p>
			// <em>$res = $client->setArticle( {id:"21198/zz000s5mbz"} );</em> - disappeared during testing
		// </p>';
		
		echo '<p>
			<em>$res = $client->findInBibliography( {id:"bib318"} );</em> - OK; no return set
		</p>';
		
		echo '<p>
			<em>$res = $client->listAllArticles();</em> - OK
		</p>';

		$res = $client->listAllArticles(); // OK
		foreach ($res->return as $row) {
			print_r($row);
			echo '<br /><br />';
		}
		
		echo '<p>
			<em>$res = $client->listAllImages();</em> - OK
		</p>';

		// $res = $client->listAllImages(); // OK
		// foreach ($res->return as $image)
		// {
			// print_r($image);
			// echo '<br /><br />';
		// }
		// echo '<br /><br />';
		
		echo '<p>
			<em>$res = $client->getMetadata( {ark:"21198/zz000s5mbz"} );</em> - OK; no return set
		</p>';
		
		echo '<p>
			<em>$res = $client->searchImage( {keyword:"img1034_01"} );</em> - OK; no return set
		</p>';
		
		// $res = $client->searchImage(new searchImage("img1034_01")); // OK
		// print_r($res);
		// echo '<br /><br />';		
				
		echo '<p>
			<em>$res = $client->listFrenchTitles( );</em> - OK; no return set
		</p>';
		
		// $res = $client->listFrenchTitles(); // OK
		// print_r($res);
		// echo '<br /><br />';		
				
		die();
				
		// echo '<p><strong>Last Request Headers</strong><br />';
		// echo '<em>$client->__getLastRequestHeaders());</em><br />';
		// echo htmlentities($client->__getLastRequestHeaders()).'</p>';
		
		// echo '<p><strong>Last Request</strong><br />';
		// echo '<em>$client->__getLastRequest());</em><br />';
		// echo htmlentities($client->__getLastRequest()).'</p>';
		
		// echo '<p><strong>Last Response Headers</strong><br />';
		// echo '<em>$client->__getLastResponseHeaders());</em><br />';
		// echo htmlentities($client->__getLastResponseHeaders()).'</p>';
		
		// echo '<p><strong>Last Response</strong><br />';
		// echo '<em>$client->__getLastResponse());</em><br />';
		// echo htmlentities($client->__getLastResponse()).'</p>';
		
		// if ($res)
		// {
			// foreach ($res as $row)
			// {
				// echo '<p>';
				// print_r($row);
				// echo '</p>';
			// }
		// }
		
	} catch (SoapFault $fault) {
		trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
	}		
	

	
?>
