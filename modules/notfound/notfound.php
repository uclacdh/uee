<?php
	// notfound.php
	// a generic module to handle when a requested module or event isn't found
	// Andrew Tai
	// October 29, 2008
	
	class notfound extends MVC_Authentication_None {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function __destruct() {
			parent::__construct();
		}
		
	}
?>