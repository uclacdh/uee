<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class search extends MVC_Authentication_View {
		public $soap;
		public $firstArticle;
		public $searchResults;
		public $keyword;
		public $placeId;
		public $placeName;
		public $glossName;
		public $placeResults;
		public $glossResults;
		public $fulltextResults;
		public $titles = array();
		public $articles = array();
		
		public function __construct() {
			parent::__construct();		
			// can I get my soap service?
			$wsdl_url = WSDL;
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				header('Location: /error/');
				exit();
				// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}
			$this->firstArticle=null;
			// validate the search
			$this->keyword = $_REQUEST['search']; // search can come from post or get (get is from timemap)...
			// did it come from get? if so strip the hyphens
			if ($_REQUEST['search'])
				$this->keyword = str_replace("_"," ",$this->keyword);
			if ($_REQUEST['placeId'])
				$this->placeId = $_REQUEST['placeId'];
			if (!$this->keyword) // no search string?
			{
				$this->keyword = '';
				// todo - process this better
			}
			if ($this->placeId) {
				// determine placeName
				$this->placeName = new MVC_Model_PlaceName(null);
				$query = "SELECT place_name.place_name FROM place_name WHERE place_name.place_id = {$this->placeId} and place_name.place_name_type_id = 1";
				$result = $this->placeName->query($query);
				while ($row = mysqli_fetch_array($result) )
				{
					$this->keyword = $row['place_name'];
				}				

				// get the matching articles
				$place_id = 'p'.str_pad($this->placeId,6,'0',STR_PAD_LEFT);
				$res = $this->soap->findPlace(new findPlace($place_id));
				$this->placeResults = $res;
				$this->processArticles();
			}
			else if ($this->keyword)
			{
				// 6/22/11 - prepending '$' for 'stemming' based on old basecamp request
				$res = $this->soap->searchArticle(new searchArticle($this->keyword));
				$this->searchResults = $res;
				// check for anything that matches this place....
				$this->placeName = new MVC_Model_PlaceName(null);
				$place_id = 0;
				$query = "SELECT place.old_id, place.place_id FROM place, place_name
				WHERE place.place_id = place_name.place_id AND place_name.place_name = '{$this->keyword}'";
				$result = $this->placeName->query($query);
				while ($row = mysqli_fetch_array($result) )
				{
					if ($row['old_id'])
					{
						$old_id = $row['old_id'];
						$place_id = $row['place_id'];
					}
					else {
						$place_id = $row['place_id'];					
					}
				}
				if ($old_id)
				{
					$res = $this->soap->findPlace(new findPlace($old_id));
					$this->placeResults = $res;
				}
				// check for anything that matches this as a gloss term....
				$this->glossName = new MVC_Model_GlossName(null);
				$gloss_id = 0;
				$query = "SELECT gloss.old_id, gloss.gloss_id FROM gloss, gloss_name
				WHERE gloss.gloss_id = gloss_name.gloss_id AND gloss_name.gloss_name = '{$this->keyword}'";
				$result = $this->glossName->query($query);
				while ($row = mysqli_fetch_array($result) )
				{
					if ($row['old_id'])
					{
						$old_id = $row['old_id'];
						$gloss_id = $row['gloss_id'];
					}
				}
				if ($gloss_id)
				{
					$res = $this->soap->findGloss(new findGloss($gloss_id));
					$this->glossResults = $res;
				}
				// lastly, try a fulltext search?
				// 6/22/11 - prepending '$' for 'stemming' based on old basecamp request
				$searchTerms = array('$'.$this->keyword);
				// if searchTerm happens to match a gloss or place name, get it's sibling names with it
				if ($gloss_id)
				{
					$query = "SELECT gloss_name.gloss_name FROM gloss_name WHERE gloss_name.gloss_id = {$gloss_id}";
					$result = $this->glossName->query($query);
					while ($row = mysqli_fetch_array($result) )
					{
						$searchTerms[] = $row['gloss_name'];
					}
				}
				if ($place_id)
				{
					$query = "SELECT place_name.place_name FROM place_name WHERE place_name.place_id = {$place_id}";
					$result = $this->placeName->query($query);
					while ($row = mysqli_fetch_array($result) )
					{
						$searchTerms[] = $row['place_name'];
					}			
				}
				$searchTerms = array_unique($searchTerms);
				$searchStr = implode(" OR ",$searchTerms);

				$res = $this->soap->findInText(new findInText($searchStr) );
				$this->fulltextResults = $res;
				$this->processArticles();
			}
		}
		
		private function processArticles() {
			$this->titles = array();
			// take the search results and put them into the titles array
			if ($this->searchResults->return and !is_array($this->searchResults->return) )
			{
				$article = $this->searchResults->return;
				$this->titles[] = $article->title;
			}
			else if ($this->searchResults->return)
			{
				foreach ($this->searchResults->return as $article)
					$this->titles[] = $article->title;
			}
			if ($this->placeResults->return and !is_array($this->placeResults->return) )
			{
				$article = $this->placeResults->return;
				$this->titles[] = $article->title;
			}
			else if ($this->placeResults->return)
			{
				foreach ($this->placeResults->return as $article)
					$this->titles[] = $article->title;
			}
			if ($this->glossResults->return and !is_array($this->glossResults->return) )
			{
				$article = $this->glossResults->return;
				$this->titles[] = $article->title;
			}
			else if ($this->glossResults->return)
			{
				foreach ($this->glossResults->return as $article)
					$this->titles[] = $article->title;
			}
			if ($this->fulltextResults->return and !is_array($this->fulltextResults->return) )
			{
				$article = $this->fulltextResults->return;
				$this->titles[] = $article->title;
			}
			else if ($this->fulltextResults->return)
			{
				foreach ($this->fulltextResults->return as $article)
					$this->titles[] = $article->title;
			}				
		}
		
		private function sortArticles() {
			// sort the titles alphabetically
			$this->titles = array_unique($this->titles);
			sort($this->titles);

		}
		
		public function listArticles() {
			// remove any duplicates
			$this->sortArticles();
			foreach ($this->titles as $title)
			{
				if (!$this->firstArticle)
				{
					if ($this->fulltextResults->return and !is_array($this->fulltextResults->return) and $this->fulltextResults->return->title == $title)
					{
						$article = $this->fulltextResults->return;
						$this->firstArticle=$article;
					}
					else if ($this->fulltextResults->return)
					{
						foreach ($this->fulltextResults->return as $article)
						{
							if ($article->title == $title)
								$this->firstArticle=$article;
						}
					}				
					if ($this->searchResults->return and !is_array($this->searchResults->return) and $this->searchResults->return->title == $title)
					{
						$article = $this->searchResults->return;
						$this->firstArticle=$article;
					}
					else if ($this->searchResults->return)
					{
						foreach ($this->searchResults->return as $article)
						{
							if ($article->title == $title)
								$this->firstArticle=$article;
						}
					}				
					if ($this->placeResults->return and !is_array($this->placeResults->return) and $this->placeResults->return->title == $title)
					{
						$article = $this->placeResults->return;
						$this->firstArticle=$article;
					}
					else if ($this->placeResults->return)
					{
						foreach ($this->placeResults->return as $article)
							if ($article->title == $title)
								$this->firstArticle=$article;
					}				
					if ($this->glossResults->return and !is_array($this->glossResults->return) and $this->glossResults->return->title == $title)
					{
						$article = $this->glossResults->return;
						$this->firstArticle=$article;
					}
					else if ($this->glossResults->return)
					{
						foreach ($this->glossResults->return as $article)
							if ($article->title == $title)
								$this->firstArticle=$article;
					}				
				}
				$permalink = $this->makePermalink($title);
				$linkPath = SITE_PATH."/articles/{$permalink}/";
				echo "<li>"."<a href=\"javascript:;\" >".$title."</a>"."</li>"."\n";
			}
		}
		
		private function makePermalink($title)
		{
			$permalink = str_replace(" ","_",$title);
			// $permalink = str_replace("(","",$permalink);
			// $permalink = str_replace(")","",$permalink);
			
			return $permalink;
		}
		
		public function displayBrief() {
			return;
			if (!$this->firstArticle)
				return;
			$xml = new DOMDocument;
			$file_exists = true;
			if (!@$xml->load($this->firstArticle->xmlURL)) {
				$cachePath = $_SERVER['DOCUMENT_ROOT'] .'/cache/articles/'.$this->makePermalink($this->firstArticle->title).'.xml';
				$file_exists = file_exists($cachePath);
				if (!$file_exists)
				    return;
//					echo("ERROR: No cached article file");
				elseif (!@$xml->load($cachePath))
					echo("ERROR: problem loading cached article");
			}
			$xpath = new DOMXPath($xml);
			$teiUrl = 'http://www.tei-c.org/ns/1.0';
			$xpath->registerNamespace('tei',$this->teiUrl);
			$titles = $xpath->query("//tei:titleStmt/tei:title");
			echo '<h3>';
			foreach($titles as $title)
			{
				if ($title->hasAttributes())
				{
					echo "<span lang='".$title->getAttribute('lang')."' xml:lang='".$title->getAttribute('lang')."' >".$title->nodeValue."</span><br />";
				}
				else
					echo $title->nodeValue .'<br />';
			}
			echo '</h3>';
			
			$authors = $xpath->query("//tei:div/tei:byline");
			foreach ($authors as $author)
				echo $author->nodeValue .'<br />';
			echo '</p>';
			
			echo '<p id="abstract">';
			$abstracts = $xpath->query("//tei:div[@type='english_abstract']");
			foreach ($abstracts as $abstract)
				echo $abstract->nodeValue .'<br /><br />';
			echo '</p>';
			echo '<p id="arabic_abstract" lang="ar" xml:lang="ar" >';
			$abstracts = $xpath->query("//tei:div[@type='arabic_abstract']");
			foreach ($abstracts as $abstract)
				echo $abstract->nodeValue .'<br /><br />';
			echo '</p>';
			echo '<p id="buttons">';
			echo '<form action="'.SITE_PATH.'/articles/'.$this->makePermalink($this->firstArticle->title).'/" method="GET" >';
			echo '<input type="image" src="/graphics/Read_Article.png" alt="Read Article &gt;&gt;" />';		
			echo '</form></p>';
			return;
		}
		
		public function __destruct() {
			$this->placeName = null;
			$this->glossName = null;
			// set the session var for last search
			$_SESSION['last_search'] = $this->keyword;
			$_SESSION['place_id'] = $this->placeId;
			session_write_close(); // make sure this gets written! (some weird errors w/o it)
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}
