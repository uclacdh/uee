<?php
	// getArticleShort.php
	// march 17, 2009
	// andrew tai, UCLA ATS
	
	// loads the appropriate article based on the title value
	require_once('../../../config.php');
	
	// what's the getparameter
	$gettitle = $_GET['title'];
	
	// what will I want to return?
	$titles = array(); // lang => title
	$titlelanguages = array(); 
	$authors = array();
	
	$abstracts = array();
	
	// vaildate REFERER url
	// generate query
	$wsdl_url = WSDL; // not final
	$soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
	$result = $soap->listAllArticles();
	foreach ($result->return as $article)
	{
		// absed on displayBrief code in titleindex module
		if (strtolower($article->title) == strtolower($gettitle))
		{
			$xml = new DOMDocument;
			$xml->load($article->xmlURL);
			$xpath = new DOMXPath($xml);
			$teiUrl = 'http://www.tei-c.org/ns/1.0';
			$xpath->registerNamespace('tei',$teiUrl);
			$articletitles = $xpath->query("//tei:titleStmt/tei:title");
			foreach($articletitles as $t)
			{
				// so javascript doesn't like newline unicode, we either need to decode or double encode the \ ?
				$t->nodeValue = str_replace("\n"," ",$t->nodeValue);
				$titles[] = '"'.htmlentities($t->nodeValue,ENT_COMPAT,'UTF-8').'"';
				if ($t->hasAttributes())
					$titlelanguages[] = '"'.$t->getAttribute('lang').'"';
				else
					$titlelanguages[] = '"'.'en-US'.'"'; // assume english
			}
			// $titles[] = '"'.htmlentities($articletitles->item(0)->nodeValue,ENT_COMPAT,'UTF-8').'"';
			
			$articleauthors = $xpath->query("//tei:div/tei:byline");
			foreach ($articleauthors as $a)
			{
				$a->nodeValue = str_replace("\n"," ",$a->nodeValue);
				$authors[] = '"'.$a->nodeValue.'"';
			}
			// $authors[] = '"'.htmlentities($articleauthors->item(0)->nodeValue,ENT_COMPAT,'UTF-8').'"';
			
			$eng_abstracts = $xpath->query("//tei:div[@type='english_abstract']");
			if (!$eng_abstracts)
			{
				$abstracts['english'] = '""';
			}
			$hasForeign = false;
			foreach ($eng_abstracts as $a)
			{
				/*
				// apply foreign tags
				if ($a->hasChildNodes())
				{
					foreach ($a->childNodes as $p) // should be p tags inside
					{
						if ($p->hasChildNodes())
						{
							foreach ($p->childNodes as $pchild)
							{
								$hasForeign = true;
								if ($pchild->localName == 'foreign')
								{
									// apply language wrapper
									$lang = $pchild->getAttribute('lang');
									$pchild->nodeValue = "<span lang='{$lang}' xml:lang='{$lang}' >".htmlentities($pchild->nodeValue,ENT_COMPAT,"UTF-8")."</span>";
								}
							}
						}
					}
				}
				$a->nodeValue = str_replace("\n"," ",$a->nodeValue);
				// apply any conversions
				$a->nodeValue = htmlentities($a->nodeValue,ENT_COMPAT,'UTF-8');
				// decode the spans
				if ($hasForeign)
					$a->nodeValue = htmlspecialchars_decode($a->nodeValue,ENT_COMPAT);
				$abstracts['english'] = '"'.$a->nodeValue.'"';
				*/
				$a->nodeValue = str_replace("\n"," ",$a->nodeValue);
				$abstracts['english'] = '"'.htmlentities($a->nodeValue,ENT_COMPAT,'UTF-8').'"';
			}
			$arab_abstracts = $xpath->query("//tei:div[@type='arabic_abstract']");
			if (!$arab_abstracts)
			{
				$abstracts['arabic'] = '""';
			}
			foreach ($arab_abstracts as $a)
			{
				$a->nodeValue = str_replace("\n"," ",$a->nodeValue);
				$abstracts['arabic'] = '"'.htmlentities($a->nodeValue,ENT_COMPAT,'UTF-8').'"';			
			}
		}
	}
	if (!$abstracts['english'])
	{
		echo ''; // basically, just die with no return
		return;
	}
	
	// construct our return object json string
	$json = "{";
	$json .= "\"titles\":[".implode(", ",$titles)."], ";
	$json .= "\"titlelanguages\":[".implode(", ",$titlelanguages)."], ";
	$json .= "\"authors\":[".implode(", ",$authors)."], ";
	$json .= "\"english_abstract\":{$abstracts['english']}, ";
	$json .= "\"arabic_abstract\":{$abstracts['arabic']},";
	$json .= "\"permalink\":\"".makePermalink($gettitle)."\"";
	$json .= "}";
	
	echo $json;
	
	return;
	
	function makePermalink($title)
	{
		$permalink = str_replace(" ","_",$title);
		// $permalink = str_replace("(","",$permalink);
		// $permalink = str_replace(")","",$permalink);
		
		return strtolower($permalink);
	}	
		
?>
