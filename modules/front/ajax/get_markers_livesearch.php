<?php
	// getMarkers.php
	// feb 6, 2009
	// andrew tai, UCLA ATS
	
	// getRows($start,$numRows);
	// gets all the gloss Rows (ajax call)
	// should only be called by 'input.php' (can validate!)
	
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	require_once('../../../modules/soap/libraryObjects.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}

	// set our start
	if ($_GET['startYear'])
		$start = $_GET['startYear'];
	else
		$start = -4500;
	// set our end
	if ($_GET['endYear'])
		$end = $_GET['endYear'];
	else
		$end = 500;
		
	// set our end
	if ($_GET['placeName'])
		$placeName = $_GET['placeName'];
		
	if ($placeName == 'placename')
		$placeName = '';
		
	// depth to search at?
	if ($_GET['depth'])
		$depth = $_GET['depth'];
	else $depth = 1; // start at region	
	
	
	// validate inputs
	if (!is_numeric($start) or !is_numeric($end))
		die();
		
	class myObj extends MVC_DB {}	
	$obj = new myObj();
	
	// does any place match this placeName?
	$topPids = getToplevel($obj,$placeName);
	
	// given all the high-level pids, recursively get the children sites/features AND any sites/features that match this placeName
	$retRows = getMarkers($obj,$placeName,$topPids,array());
	$jsonresp = '';
	$jsonresp .= "[";
	$row_arr = array();
	$sql_rows = array();
	foreach ($retRows as $row)
	{
		$rowResp = '';
		$rowResp .= '{';
		$fields = array();
		// TODO - corner cases; ie abu gharab at 1069 bce, when 1069 bce starts the third intermediate period?
		// think i resolved w/ row['year_end'] <= start
		if ($row['year_end'] <= $start or $row['year_start'] > $end)
			continue;
		foreach ($row as $key => $val)
		{			
			// mysql returns rows with numeric and text indices, let's use the fieldname indices this time
			if (!is_numeric($key))
			{
				// save the value
				if (is_numeric($val))
					$fields[] = '"'.$key.'":'.$val;
				else
					$fields[] = '"'.$key.'":"'.$val.'"';
			
			}
		}	
		$rowResp .= implode(", ",$fields);
		$rowResp .= '}';
		// we'd actually want to return this as a json array of objects
		$row_arr[] = $rowResp;
	}
	$jsonresp .= implode(", ",$row_arr);
	$jsonresp .= "]";
	echo $jsonresp;
	
	function getToplevel($db, $placeName, $depth = -1, $parentId = -1)
	{
		if ($depth > 5) // we're searched all possible high-level (no-geodata) placeTypes
			return;
		
		$query = "SELECT place_name.place_id
			FROM place_name, place
			WHERE place_name.place_id = place.place_id
				AND place_name.place_name LIKE '%{$placeName}%' ";
		
		// echo $query . '<br />';
		MVC_Log::write($query);			
		$result = $db->query($query);
		$retRows = array();
		$place_ids = array();
		
		if ($result)
		{
			while ($row = mysqli_fetch_array($result)) {
				$place_ids[] = $row['place_id'];
			}
		}
		return array_unique($place_ids);
	}
	
	function getMarkers($db, $placeName,$place_ids,$seen_ids = array(),$depth = 0) 
	{
		$query = "SELECT DISTINCT geo_time.place_id, time_period.year_start, time_period.year_end, place_name.place_name, geo_data.latitude, geo_data.longitude, place.place_type_id, place.old_id
				FROM geo_time, time_period, place_name, geo_data, place, place_tree
				WHERE geo_time.start_time_period_id = time_period.time_period_id
					AND geo_time.place_id = geo_data.place_id
					AND geo_time.time_type_id = 1
					AND place.place_id = geo_time.place_id
					AND place_name.place_id = geo_time.place_id
					AND place_name.place_name_type_id =1
					AND place_tree.place_relation_type_id = 1
					AND (place.place_id IN (SELECT DISTINCT place_tree.place_id_2 FROM place_tree WHERE place_tree.place_id_1 = ".implode(" OR place_tree.place_id_1=",$place_ids).
					") OR place_name.place_name LIKE '%{$placeName}%')";
		// echo str_replace( array("AND ", "FROM ", "IN ", "WHERE "),array("<br />AND ", "<br />FROM ", "<br />IN ", "<br />WHERE "),$query) .'<br /><br />';
		MVC_Log::write($query);			
		$result = $db->query($query);
		$retRows = array();
		$place_ids = array();
		$result_ids = array();
		
		if ($result)
		{
			while ($row = mysqli_fetch_array($result)) {
				// save this row?
				if (!in_array($row['place_id'],$seen_ids))
				{
					$retRows[$row['place_id'] ] = $row;
				}
				// print_r($row);
				// echo '<br /><br />';
				$result_ids[] = $row['place_id'];
			}
		}
		
		if ($result_ids and $depth < 5)
		{
			// echo 'result: ';
			// print_r($result_ids);
			// echo '<br /><br />';
			// what's the difference between place_ids and new_ids
			$new_ids = array_diff($result_ids,$seen_ids);
			// echo 'to process: ';
			// print_r($new_ids);
			// echo '<br /><br />';
			if ($new_ids)
			{
				$seen_ids = array_unique(array_merge($place_ids,$result_ids));
				// echo 'have seen: ';
				// print_r($seen_ids);
				// echo '<br /><br />';
				$retRows = array_merge($retRows,getMarkers($db,$placeName,$new_ids,$seen_ids,$depth+1) );
			}
		}
		return $retRows;
	}	
	
	$obj = null; // reset this so the db connection closes
	
?>
