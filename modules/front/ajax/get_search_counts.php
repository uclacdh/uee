<?php
	// getSearchCounts.php
	// feb 6, 2009
	// andrew tai, UCLA ATS
		
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	require_once('../../../modules/soap/libraryObjects.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		if (substr($className, 0, 4) === 'MVC_') {
		    $objectName = substr($className,4);
		    $objectName = str_replace("_","/",$objectName);
		} else {
		    $objectName = $className;
		}
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}
	
	function intToPlaceId($int) {
	    return 'p'.str_pad($int, 6, '0', STR_PAD_LEFT);
        }

	// set our values
	if ($_GET['place_name'])
		$place_name = str_replace("_"," ",$_GET['place_name']);
	else
		return;
	if ($_GET['old_id'])
		$old_id = $_GET['old_id'];
	else
		$old_id = ''; // karnak?
	if ($_GET['place_id'])
	{
		$place_id = $_GET['place_id'];
		// format it properly, this is just the numerical part
		$place_id = 'p'.str_pad($place_id,6,'0',STR_PAD_LEFT);
	} else
		$place_id = ''; // karnak?
	class myObj extends MVC_DB {}
	
	// let's do our soap service
	$wsdl_url = WSDL; // not final
	try {
		@$soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
	} catch (SoapFault $fault) {
		return; // can't do anything useful;
		trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
	}
	$ret = array();
	$query = "[SOAP] findPlace({$place_id})";
	MVC_Log::write($query);				
	$articles = $soap->findPlace(new findPlace($place_id) );
	$ret['articles'] = count($articles->return);
	// $images = $soap->searchImage(new searchImage($place_name) );
	// may 29, 2009 - use the old place_id to search for images
	// holding off until now...
	$query = "[SOAP] searchImage({$place_id})";
	MVC_Log::write($query);
	$placeId = intToPlaceId($_GET['place_id']);
	$images = $soap->searchImage(new searchImage($placeId));
	$ret['images'] = count($images->return);
	
	// we'd actually want to return this as a json array of objects
	$jsonresp = '';
	$jsonresp .= "{";
	// $jsonresp .= implode(", ",$ret);
	$first = true;
	foreach ($ret as $field=>$val)
	{
		if (!$first)
			$jsonresp .= ", ";
		$jsonresp .= "\"{$field}\":{$val}";
		$first = false;
	}
	$jsonresp .= "}";
	echo $jsonresp;
	return;
?>
