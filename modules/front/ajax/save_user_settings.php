<?php
	// save_user_settings.php
	// april 3, 2009
	// andrew tai, UCLA ATS
	
	session_start();
		
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die();
		require_once($objectPath);
	}
	
	if (!$_SESSION['user_id'] or $_SESSION['user_id'] <= 0)
		die();

	// load the logged in user, if any
	$user = new MVC_Model_UeeUser(null);
	$user->search['user_id'] = $_SESSION['user_id'];
	$user->dbSearchTable();
	if ($user->search_results and count($user->search_results) == 1)
	{
		$user->data = $user->search_results[0];
	}
	else
	{
		die();
	}
	// update the user preferences values
	$user->data['user_id'] = $_SESSION['user_id'];
	$user->data['link_on_flag'] = $_GET['links'];
	$user->data['gloss_on_flag'] = $_GET['glosses'];
	$user->data['bib_on_flag'] = $_GET['biblios'];
	$user->data['places_on_flag'] = $_GET['places'];
	
	$user->dbUpdateRow();
	
	echo "user settings saved";
	return;
?>
