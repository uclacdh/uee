<?php
	// getSearchCounts.php
	// feb 6, 2009
	// andrew tai, UCLA ATS
		
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	require_once('../../../modules/soap/libraryObjects.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}

	class myObj extends MVC_DB {}
	
	$ark = $_GET['ark'];
	// if (!$ark)
		// $ark = "21198/zz0014rnf4";
	
	// let's do our soap service
	$wsdl_url = WSDL; // not final
	try {
		@$soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
	} catch (SoapFault $fault) {
		// just return immediately and let the client handle it
		return;
		trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
	}
	$ret = array();
	$metadata = $soap->getMetadata(new getMetadata($ark));
	if ($metadata->return)
	{
		$dom = new DOMDocument();
		$dom->loadXML($metadata->return);
		$xpath = new DOMXPath($dom);
		$captions = $xpath->query("//caption");
		foreach ($captions as $node)
		{
			$caption = htmlentities($node->nodeValue,ENT_COMPAT,"UTF-8");
			// $caption = str_replace(")","\)",$caption);
			// $caption = str_replace("(","\(",$caption);
			// $caption = str_replace(":","\:",$caption);
		}
		if (!$caption)
			$caption = "No description available";
	}
	$jsonresp = $caption;
	echo $jsonresp;
	return;
?>
