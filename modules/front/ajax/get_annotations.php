<?php
	// save_user_settings.php
	// april 3, 2009
	// andrew tai, UCLA ATS
		
	// vaildate REFERER url
	// generate query
	
	session_start();
	
	// load any global config variables
	require_once('../../../config.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("need $className");
		require_once($objectPath);
	}
	// load the input info
	if (!$_SESSION['user_id'] or $_SESSION['user_id'] <= 0)
		die();
		
	$article_id = $_GET['article_id'];
	if (!$article_id)
		die();

	// load the logged in user, if any
	$annotation = new MVC_Model_MyueeAnnotation(null);
	// set the annotation data
	
	$annotation->search['user_id'] = $_SESSION['user_id'];
	$annotation->search['article_id'] = $article_id;
	$annotation->dbSearchTable();
	if (!$annotation->search_results)
		die('no results');
	$rowResps = array();
	foreach ($annotation->search_results as $row)
	{
		// print_r($row);
		$row['annotation'] = trim($row['annotation']);
		$rowResp = "{ \"id\":{$row['annotation_id']}, \"annotation\":\"{$row['annotation']}\" }";
		$rowResps[] = $rowResp;
	}
	$resp = "[".implode(",",$rowResps)."]";
	echo $resp;
	return;
?>
