<?php
	// let's write this as a rest service

        // load any global config variables
        require_once('../../../config.php');

	$method = $_GET['method'];
	$input = $_GET['input'];
	$input2 = $_GET['input2'];
	// TODO - validate input
	
	$result = "";
	switch ($method) {
		case 'getGlossary':
			$result = getGlossary($input);
			break;
		case 'getGlossary2':
			$result = getGlossary2($input,$input2);
			break;
		default:
			break;
	}
	echo $result;
	
	function getGlossary($glossNameInput) {
		$res = "";
		$db = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if ($db->error or mysqli_connect_error())
			return $res;
		$stmt = $db->prepare("
			SELECT g.gloss_id, gn.gloss_name, g.definition 
			FROM gloss g, gloss_name gn
			WHERE 
				g.gloss_id = gn.gloss_id
				AND gn.gloss_name = ?
				ORDER BY g.definition DESC
				LIMIT 1;
			");
		if ($db->error or !$stmt)
			return $res;
		$stmt->bind_param("s",$glossNameInput);
		$stmt->execute();
		$stmt->bind_result($glossId,$glossName,$glossDefinition);
		while ($stmt->fetch()) {
			$res = "{\"glossId\":{$glossId},\"glossName\":\"{$glossName}\",\"glossDefinition\":\"{$glossDefinition}\"}";
		}
		$stmt->close();
		return $res;
	}
	
	function getGlossary2($glossId, $glossNameInput) {
		$res = "";
		$gid = substr($glossId,1);
		$gid = ltrim($gid,"0");
		$glossNameInput = str_replace("\n","",$glossNameInput);
		$db = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if ($db->error or mysqli_connect_error())
			return $res;
		$stmt = $db->prepare("
			SELECT g.gloss_id, g.definition 
			FROM gloss g
			WHERE 
				g.gloss_id = ?
			LIMIT 1;
			");
		if ($db->error or !$stmt)
			return $res;
		$stmt->bind_param("i",$gid);
		$stmt->execute();
		$stmt->bind_result($glossId,$glossDefinition);
		while ($stmt->fetch()) {
			$res = "{\"glossId\":{$glossId},\"glossName\":\"{$glossNameInput}\",\"glossDefinition\":\"".html_entity_decode($glossDefinition)."\"}";
		}
		$stmt->close();
		return $res;
	}
	
	function searchSoapService($input) {
		// let's do our soap service
		$wsdl_url = WSDL; // not final
		try {
			@$soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
		} catch (SoapFault $fault) {
			return; // can't do anything useful;
			trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
		}
		$ret = array();
		// $articles = $soap->findPlace(new findPlace($place_id) );
		// $images = $soap->searchImage(new searchImage($place_id) );

	}

?>
