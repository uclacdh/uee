<?php
	// getMarkers.php
	// feb 6, 2009
	// andrew tai, UCLA ATS
	
	// getRows($start,$numRows);
	// gets all the gloss Rows (ajax call)
	// should only be called by 'input.php' (can validate!)
	
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	require_once('../../../modules/soap/libraryObjects.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}

	// set our start
	if ($_GET['startYear'])
		$start = $_GET['startYear'];
	else
		$start = -4500;
	// set our end
	if ($_GET['endYear'])
		$end = $_GET['endYear'];
	else
		$end = 500;
		
	// set our end
	if ($_GET['placeName'])
		$placeName = $_GET['placeName'];
		
	// validate inputs
	if (!is_numeric($start) or !is_numeric($end))
		die();
	
	class myObj extends MVC_DB {}
	
	$obj = new myObj();
	$query = "SELECT geo_time.place_id, time_period.year_start, time_period.year_end, place_name.place_name, geo_data.latitude, geo_data.longitude, place.place_type_id, place.old_id
				FROM geo_time, time_period, place_name, geo_data, place
				WHERE geo_time.start_time_period_id = time_period.time_period_id
					AND geo_time.place_id = geo_data.place_id
					AND geo_time.time_type_id = 1
					AND place.place_id = geo_time.place_id
					AND place_name.place_id = geo_time.place_id
					AND place_name.place_name_type_id =1";
	if ($placeName)
		$query .= " AND place_name.place_name LIKE '%{$placeName}%'";
	$query .= " UNION SELECT geo_time.place_id, time_period.year_start, time_period.year_end, place_name.place_name, geo_data.latitude, geo_data.longitude, place.place_type_id, place.old_id
				FROM geo_time, time_period, place_name, geo_data, place
				WHERE geo_time.end_time_period_id = time_period.time_period_id
				AND geo_time.place_id = geo_data.place_id
				AND geo_time.time_type_id = 1
				AND place.place_id = geo_time.place_id
				AND place_name.place_id = geo_time.place_id
				AND place_name.place_name_type_id =1";
	if ($placeName)
		$query .= " AND place_name.place_name LIKE '%{$placeName}%'";
	// echo $query . '<br />';
	MVC_Log::write($query);			
	$result = $obj->query($query);
	$retRows = array();
	$place_ids = array();
	
	if ($result)
	{
		while ($row = mysqli_fetch_array($result)) {
			// have i already seen this place_id?
			$index = str_replace(" ","",$row['place_name']);
			if (!in_array($row['place_id'],$place_ids))
			{
				$place_ids[] = $row['place_id'];
				$retRows[$index] = $row;
			}
			else
			{
				if ($retRows[$index]['year_start'] > $row['year_start'])
					$retRows[$index]['year_start'] = $row['year_start'];
				if ($retRows[$index]['year_end'] < $row['year_end'])
					$retRows[$index]['year_end'] = $row['year_end'];
			}
		}
	}
	
	// we'd actually want to return this as a json array of objects
	$jsonresp = '';
	$jsonresp .= "[";
	$row_arr = array();
	$sql_rows = array();
	foreach ($retRows as $row)
	{
		$rowResp = '';
		$rowResp .= '{';
		$fields = array();
		// TODO - corner cases; ie abu gharab at 1069 bce, when 1069 bce starts the third intermediate period?
		// think i resolved w/ row['year_end'] <= start
		if ($row['year_end'] <= $start or $row['year_start'] > $end)
			continue;
		foreach ($row as $key => $val)
		{			
			// mysql returns rows with numeric and text indices, let's use the fieldname indices this time
			if (!is_numeric($key))
			{
				// save the value
				if (is_numeric($val))
					$fields[] = '"'.$key.'":'.$val;
				else
					$fields[] = '"'.$key.'":"'.$val.'"';
			
			}
		}	
		$rowResp .= implode(", ",$fields);
		$rowResp .= '}';
		// we'd actually want to return this as a json array of objects
		$row_arr[] = $rowResp;
	}
	$jsonresp .= implode(", ",$row_arr);
	$jsonresp .= "]";
	echo $jsonresp;
	return;
?>
