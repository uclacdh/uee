<?php
	// get_dynasties.php
	// feb 6, 2009
	// andrew tai, UCLA ATS
	
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}

	// set our start
	if ($_GET['parentId'])
		$parentId = $_GET['parentId'];
	else
		die();
		
	// validate inputs
	if (!is_numeric($parentId))
		die();
	
	class myObj extends MVC_DB {}
	
	$obj = new myObj();
	$query = "SELECT time_period.*, time_period_name.time_period_name 
				FROM time_period, time_period_name
				WHERE time_period.time_period_id = time_period_name.time_period_id
					AND time_period.parent_time_period_id = $parentId
				ORDER BY time_period.year_start, time_period_name.time_period_name_id";
	// echo $query . '<br />';
	MVC_Log::write($query);			
	$result = $obj->query($query);
	$retRows = array();
	if ($result)
	{
		while ($row = mysqli_fetch_array($result)) {
			$retRows[] = $row;
		}
	}
	// we'd actually want to return this as a json array of objects
	$resp = '';
	$resp .= "[";
	$row_arr = array();
	$tp_ids = array();
	foreach ($retRows as $row)
	{
		$rowResp = '';
		$rowResp .= '{';
		$fields = array();
		if (in_array($row['time_period_id'], $tp_ids))
			continue;
		$tp_ids[] = $row['time_period_id'];
		foreach ($row as $key => $val)
		{
			if (!is_numeric($key))
			{
				if (is_numeric($val))
					$fields[] = '"'.$key.'":'.$val;
				else
					$fields[] = '"'.$key.'":"'.$val.'"';
			}
		}	
		$rowResp .= implode(", ",$fields);
		$rowResp .= '}';
		// we'd actually want to return this as a json array of objects
		$row_arr[] = $rowResp;
	}
	$resp .= implode(", ",$row_arr);
	$resp .= "]";
	echo $resp;
	return;
?>
