<?php
	// getPlaceGeodata.php
	// feb 24, 2009
	// andrew tai, UCLA ATS
	
	// loads the appropriate geodata where the place_id records match up
	
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}

	// set our start
	if ($_GET['place_ids'])
		$place_ids = trim($_GET['place_ids']);
		
	$placeArr = explode(",",$place_ids);
		
	// validate inputs
	if (!is_array($placeArr))
		die();
	
	class myObj extends MVC_DB {}
	
	$obj = new myObj();
	// THIS IS NOT A SECURE QUERY YET
	$query = "SELECT DISTINCT(place_name.place_name), place.place_id, geo_data.latitude, geo_data.longitude, geo_data.vertex_index, place.place_type_id, time_period.year_start, time_period.year_end, place.old_id
				FROM geo_data, place, place_name, geo_time, time_period
				WHERE 
					geo_time.place_id = place.place_id AND 
					(time_period.time_period_id = geo_time.start_time_period_id OR time_period.time_period_id = geo_time.end_time_period_id) AND
					geo_data.place_id = place.place_id AND 
					place_name.place_id = place.place_id AND
					place_name.place_name_type_id = 1 AND (0 ";
	foreach ($placeArr as $pid) {
		// $query .= "OR place.old_id='$pid' ";
		$new_id = substr($pid,1);
		$new_id = trim($new_id);
		$query .= "OR place.place_id='$new_id' ";
	}
	$query .= ")
				ORDER BY geo_data.place_id, geo_data.vertex_index";
				// LIMIT 100;";
	// echo $query . '<br />';
	MVC_Log::write($query);			
	$result = $obj->query($query);
	$retRows = array();
	$place_ids = array();
	
	if ($result)
	{
		while ($row = mysqli_fetch_array($result)) {
			// have i already seen this place_id?
			$index = str_replace(" ","",$row['place_name']);
			if (!in_array($row['place_id'],$place_ids))
			{
				$place_ids[] = $row['place_id'];
				$retRows[$index] = $row;
			}
			else
			{
				if ($retRows[$index]['year_start'] > $row['year_start'])
					$retRows[$index]['year_start'] = $row['year_start'];
				if ($retRows[$index]['year_end'] < $row['year_end'])
					$retRows[$index]['year_end'] = $row['year_end'];
			}
		}
	}
	// we'd actually want to return this as a json array of objects
	$jsonresp = '';
	$jsonresp .= "[";
	$row_arr = array();
	$sql_rows = array();
	foreach ($retRows as $row)
	{
		$rowResp = '';
		$rowResp .= '{';
		$fields = array();
		foreach ($row as $key => $val)
		{			
			// mysql returns rows with numeric and text indices, let's use the fieldname indices this time
			if (!is_numeric($key))
			{
				// save the value
				if (is_numeric($val))
					$fields[] = '"'.$key.'":'.$val;
				else
					$fields[] = '"'.$key.'":"'.$val.'"';
			
			}
		}	
		$rowResp .= implode(", ",$fields);
		$rowResp .= '}';
		// we'd actually want to return this as a json array of objects
		$row_arr[] = $rowResp;
	}
	$jsonresp .= implode(", ",$row_arr);
	$jsonresp .= "]";
	echo $jsonresp;
	return;
?>
