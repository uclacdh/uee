<?php
	// save_user_settings.php
	// april 3, 2009
	// andrew tai, UCLA ATS
		
	// vaildate REFERER url
	// generate query
	
	session_start();
	
	// load any global config variables
	require_once('../../../config.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT']."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("need $className");
		require_once($objectPath);
	}
	// load the input info
	if (!$_SESSION['user_id'] or $_SESSION['user_id'] <= 0)
		die("no session id");
		
	$annotationText = $_GET['annotation'];
	if (!$annotationText)
		die("no annotation text");
	$article_id = $_GET['article_id'];
	if (!$article_id)
		die("no artice_id");
	$annotation_id = $_GET['annotation_id'];

	// load the logged in user, if any
	$annotation = new MVC_Model_MyueeAnnotation(null);
	// set the annotation data
	
	$annotation->data['user_id'] = $_SESSION['user_id'];
	$annotation->data['annotation'] = $annotationText;
	$annotation->data['article_id'] = $article_id;
	$annotation->data['modify_datetime'] = date("Y-m-d H:i:s");
	$annotation->data['active_flag'] = true;
	
	if (!$annotation_id) {
		$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$stmt = $db->prepare("INSERT INTO myuee_annotation (annotation_id, annotation, user_id, article_id, create_datetime, modify_datetime, active_flag) VALUES(NULL,?,?,?, NOW(), NOW(), 1)");
		$stmt->bind_param("sis",$annotationText, $_SESSION['user_id'], $article_id);
		$stmt->execute();
		$stmt->close();
		$db->close();
	}
	else {
		$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$stmt = $db->prepare("UPDATE myuee_annotation SET annotation=? WHERE annotation_id=?");
		$stmt->bind_param("si",$annotationText, $annotation_id);
		$stmt->execute();
		$stmt->close();
		$db->close();
	}
	
	echo "user annotation saved";
	return;
?>
