<?php
	// myuee
	// a module that displays some user specific information
	// in particular, annotations of different articles?
	// requires: user login, soap service
	
	class myuee extends MVC_Authentication_View {
		
		public $user;
		public $openid;
		public $sess;
		public $edit;
		public $annotations;
		public $showAllAnnotations; // a getParam
		public $soap; // soap
		public $articletitles;
		
		public function __construct() {
			parent::__construct();	
			$this->user = new MVC_Model_UeeUser(null);
			$this->sess = $_SESSION;
			$this->user->search['user_id'] = $this->sess['user_id'];
			$this->user->dbSearchTable();
			if ($this->user->search_results and count($this->user->search_results) == 1)
			{
				$this->user->data = $this->user->search_results[0];
			}
			// set the openId
			$this->openid = new MVC_Model_OpenId(null);
			$this->openid->search['uee_user_id'] = $this->sess['user_id'];
			$this->openid->dbSearchTable();
			if ($this->openid->search_results and count($this->openid->search_results) == 1)
			{
				$this->openid->data = $this->openid->search_results[0];
			}
			$this->edit = false;
			$this->sess = $_SESSION;
			// load any existing annotations
			if ($this->user->data)
			{
				$this->annotations = new MVC_Model_MyueeAnnotation(null);
				$this->annotations->search['user_id'] = $this->user->data['user_id'];
				$this->annotations->dbSearchTable(); // so... they're in search results now
			}
			// can I get my soap service?
			$wsdl_url = WSDL;
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				header('Location: /error/');
				exit();
				// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}			
			// load the article titles
			$this->articletitles = array();
			$res = $this->soap->listAllArticles();
			foreach ($res->return as $article)
				$this->articletitles[$article->id] = $article->title;
		}
		
		public function listAnnotations() {				
			$ann = $this->sortAnnotations();
			foreach ($this->articletitles as $id=>$title)
			{
				$link = str_replace(" ","_",$title);
				if (empty($ann[$id]))
					continue;
				echo "<p>";
				echo "<strong><a href=\"/articles/{$link}/\">{$title}</a></strong>";
				echo "<ul>";
				foreach ($ann[$id] as $annote)
					echo "<li>{$annote}</li>";
				echo "</ul>";
				echo "</p>";
			}
			return;		
		}
		
		private function sortAnnotations() {
			$ann = array();
			if ($this->annotations->search_results) {
                foreach ($this->annotations->search_results as $row)
                {
                    if (!$ann[$row['article_id']])
                        $ann[$row['article_id']] = array();
                    $ann[$row['article_id']][] = $row['annotation'];
                }
            }
			return $ann;
		}
		
		public function displaySearchResults() {
			if ($this->annotations->search_results) {
				foreach ($this->annotations->search_results as $annotation) {
					echo '<li>Annotation</li>';
				}
			}
			else
				echo '<li>You haven\'t written any annotations yet!</li>';
		}
		
		public function __destruct() {
			$this->user = null;
			$this->sess = null;
			$this->openid = null;
			$this->annotations = null; // reset (make sure the db connection closes)
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}
