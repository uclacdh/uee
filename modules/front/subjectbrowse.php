<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class subjectbrowse extends MVC_Authentication_View {
		public $soap;
		public $category;
		
		public function __construct() {
			parent::__construct();
			$this->category = new MVC_Model_Category(null);
			
			// can I get my soap service?
			$wsdl_url = WSDL; // not final
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				header('Location: /error/');
				exit();
				// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}
		}
		
		public function listAllArticles() {
			$res = $this->soap->listAllArticles();
			foreach ($res->return as $article)
			{
				$permalink = $this->makePermalink($article->title);
				$linkPath = SITE_PATH."/articles/{$permalink}/";
				echo "<li>"."<a href=\"javascript:;\" >".$article->title."</a>"."</li>"."\n";
			}
		}
		
		public function listAllSubjects() {
			$flattree = $this->category->getTree3();
			foreach ($flattree as $cat_id => $row)
			{
				if (!$row['parent'])
					$this->displaySubject($cat_id,$row,$flattree);
			}
			return;
		}
		
		private function displaySubject($cat_id,$row,$flattree)
		{

			//<a href="javascript:;" class="selected" style="background-image: url(http://localhost/graphics/Expand.png);padding-left: 2em;background-repeat: no-repeat;padding-bottom: 0.2em; padding-top: 0.2em; ">Language, text, and writing</a>
			// this is a toplevel node
			if (!$row['parent']) {
				if (count($row['children']) > 0) {
					echo "<li id='{$row['xmlid']}' class='toplevel' ><a href='javascript:;' class='hasChildren'>".$row['category'] ."</a>";
					if (count($row['children']) > 0)
					{
						echo "<ul>";
						foreach ($row['children'] as $id)
							$this->displaySubject($id,$flattree[$id],$flattree);
						echo "</ul>";
					}
				}
				else
					echo "<li id='{$row['xmlid']}' class='toplevel hasNoChildren'>". $row['category'] ."";
			}
			else if (count($row['children']) <= 0) {
				if ($row['arkId'])
					echo "<li id='{$row['xmlid']}' class='endnode hasNoChildren' ><a href='javascript:;'>".$row['category'] ."</a>";
				else
					echo "<li id='{$row['xmlid']}' class='endnode hasNoChildren' >".$row['category'];
			}
			else {
				if ($row['arkId'] or count($row['children']) > 0) {
					echo "<li id='{$row['xmlid']}' ><a href='javascript:;' class='hasChildren'>".$row['category']."</a>";
					if (count($row['children']) > 0)
					{
						echo "<ul>";
						foreach ($row['children'] as $id)
							$this->displaySubject($id,$flattree[$id],$flattree);
						echo "</ul>";
					}
				}
				else
					echo "<li id='{$row['xmlid']}' >".$row['category'];
			}
			echo "</li>";
		}
		
		// generate teh stub section
		public function displayBrief() {
			echo '<h3>&nbsp;</h3>';
			
			echo '<p id="author"><strong>Author:</strong><br />&nbsp;</p>';
			
			echo '<p id="abstract">&nbsp;</p>';
			echo '<p id="arabic_abstract" lang="ar" xml:lang="ar" >&nbsp;</p>';
			echo '<p id="status" style="font-weight:bold;"></p>';
			echo '<p id="buttons">';
			echo '<a href="/"><img src="/graphics/Read_Article.png" alt="Read Article" /></a>';
			return;
		}
		
		private function makePermalink($title)
		{
			$permalink = str_replace(array(" ","/"),array("_","-"),$title);
			// $permalink = str_replace("(","",$permalink);
			// $permalink = str_replace(")","",$permalink);
			
			return $permalink;
		}
		
		public function __destruct() {
			$this->category = null;
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}
