<?php
	// profile
	// a module that displays some user profile information
	// requires: user login
	
	class profile extends MVC_Authentication_LoggedIn {
		
		public $user;
		public $openid;
		public $sess;
		public $edit;
		
		public function __construct() {
			parent::__construct();	
			// set the openId
			$this->openid = new MVC_Model_OpenId(null);
			$this->openid->search['uee_user_id'] = $_SESSION['user_id'];
			$this->openid->dbSearchTable();
			if ($this->openid->search_results and count($this->openid->search_results) == 1)
			{
				$this->openid->data = $this->openid->search_results[0];
			}
			$this->edit = false;
		}
				
		public function __destruct() {
			$this->user = null;
			$this->sess = null;
			$this->openid = null;
			parent::__destruct();
		}

		public function edit() {
			$this->edit = true;
		}
		
		// some custom events
		// 7/28/2011
		// users are automatically granted readonly access (approved=1)
		public function update() {
			// set fields based on the user input
			$this->openid->data['firstname'] = $_POST['firstname'];
			$this->openid->data['lastname'] = $_POST['lastname'];
			$this->openid->data['email'] = $_POST['email'];
			$this->openid->data['university_id'] = $_POST['university_id'];
			$this->openid->data['university_org'] = $_POST['university'];
			$this->openid->data['category'] = $_POST['category'];
			$this->openid->data['comments'] = $_POST['about_us'];
			$this->openid->data['keyphrase'] = $_POST['keyphrase'];
			$this->openid->data['completed_profile'] = 1;
			$this->openid->data['date_updated'] = date("Y-m-d H:i:s");

			// set the auto approved status and e-mail confirmation
			$this->openid->data['approved'] = 1;
			$this->openid->data['pending'] = 0;

			$this->openid->dbUpdateRow();

			$this->email_notification();
		}

		/**
		 * E-mail a notification to the user
		 * saying their account has been approved and they may begin
		 * using the website.
		 */
		protected function email_notification() {
			ini_set("SMTP","em2.ad.ucla.edu"); // set the outgoing mail server
				
			$to = $this->openid->data['email'];
			$subject = '[ueeweb] Completed user registration';
			$message = "Dear {$this->openid->data['firstname']}, \r\n".
				" Thank you for completing/updating a user profile on the UCLA Encyopedia of Egyptology. Your account has been approved and you may begin viewing the articles on the website. \r\n".
				"UEE Webmaster".
				"";
			$headers = 'From: webmaster@uee.ucla.edu' . "\r\n";
			$headers .= 'Reply-To: webmaster@uee.ucla.edu' . "\r\n";
			$headers .= 'X-Mailer: PHP/' . phpversion();
			$send_success = mail($to,$subject,$message, $headers);
			if (!$send_success)
				throw new Exception("Error sending e-mail");
			return $send_success;	
		}
		
	}
