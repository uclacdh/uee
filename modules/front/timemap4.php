<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class timemap4 extends MVC_Authentication_View {
		
		public $phasewidths = array();
		
		public function __construct() {
			parent::__construct();		
		}
		
		public function __destruct() {
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
		// displayPhases
		// display the blocks representing timeperiods...
		public function displayPhases() {
			$timeperiod = new MVC_Model_TimePeriod(null);
			$timeperiod->search['parent_time_period_id'] = NULL;
			$rows = $timeperiod->getAllRows('',array('year_start'));
			$phases = array();
			$sum = 0;
			foreach ($rows as $row)
			{
				if (!$row['parent_time_period_id'])
				{
					$sum += ( $row['year_end'] - ($row['year_start']<(-5000)?(-5000):$row['year_start']) );
					$phases[] = $row;
				}
			}
			$width = round(960*0.98);			
			// echo $sum . '<br />';
			$sectioncount = 0;
			$sectionclass = array('timesection_red','timesection_green','timesection_blue','timesection_yellow');
			$sum2 = 0;
			foreach ($phases as $phase)
			{
				$range = $phase['year_end'] - ($phase['year_start']<(-5000)?(-5000):$phase['year_start']);
				$pixels = round($width*$range/5500)-4; // 1px padding on both sides; 
				$left = round (960*0.98* ( ($phase['year_start']<-5000?-5000:$phase['year_start']) -(-5000)) / 5500);
				// we're gonna need a tp name
				$tpname = new MVC_Model_TimePeriodName(null);
				$tpname->search['time_period_id'] = $phase['time_period_id'];
				$tpname->dbSearchTable();
				if ($tpname->search_results)
					$name = $tpname->search_results[0]['time_period_name'];
				else
					$name = '';
				echo "<a id='phase-".str_replace(" ","",$name)."' class='".$sectionclass[$sectioncount%4]." phase-link' style='position:absolute;left:{$left}px;width:{$pixels}px;z-layer:{$sectioncount};opacity:0.75;' href='javascript:;' title='view places in the {$name} phase'>{$name}</a>";
				$this->phasewidths[$phase['time_period_id']] = array($sum2,$sum2+$pixels);
				$sum2 += $pixels + 2; // +2 accounts for border
				$sectioncount++;
			}
		}
		
		public function displayPhasewidths() {
			$first = true;
			foreach ($this->phasewidths as $tp_id => $width) {
				if (!$first)
					echo ", ";
				list($start,$stop) = $width;
				echo "{$tp_id}:[{$start},{$stop}]";				
				$first = false;
			}
		}
		
		public function displayTimelineMarkers($range = 500) {
			$start = -5000;
			$stop = 500;
			$count = $start;
			$percent = floor(100*$range/($stop-$start));
			$pixel = floor(900*$percent);
			while ($count < $stop)
			{
				echo "<div class='timeline-marker' style='width:{$percent}%;float:left;font-size:0.6em;' >";
				echo "|<br />";
				echo $count;
				if ($count > 0 )
					echo " CE";
				else
					echo " BCE";
				echo "</div>";
				$count += $range;
				$sectioncount++;
			}
		}
		
	}
