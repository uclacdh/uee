<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class timemap3 extends MVC_Authentication_View {
		
		public function __construct() {
			parent::__construct();		
		}
		
		public function __destruct() {
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
		// displayPhases
		// display the blocks representing timeperiods...
		public function displayPhases() {
			$timeperiod = new MVC_Model_TimePeriod();
			$conditions = array('parent_time_period_id' => NULL);
			$rows = $timeperiod->getAllRows(array("year_start"));
			$phases = array();
			$sum = 0;
			foreach ($rows as $row)
			{
				if (!$row['parent_time_period_id'])
				{
					$sum += ( $row['year_end'] - ($row['year_start']<(-5000)?(-5000):$row['year_start']) );
					$phases[] = $row;
				}
			}
			$width = round(960*0.98)-count($phases)*2;
			// echo $sum . '<br />';
			foreach ($phases as $phase)
			{
				$range = $phase['year_end'] - ($phase['year_start']<(-5000)?(-5000):$phase['year_start']);
				$pixels = round($width*$range/$sum)-4; // 1px padding on both sides; 
				// we're gonna need a tp name
				$tpname = new MVC_Model_TimePeriodName();
				$conditions2 = array('time_period_id' => $phase['time_period_id']);
				$results2 = $tpname->dbSearchTable($conditions2);
				if ($results2)
					$name = $results2[0]['time_period_name'];
				else
					$name = '';
				echo "<a id='phase-".str_replace(" ","",$name)."' class='phase-link' style='width:{$pixels}px;' href='javascript:;' title='view places in the {$name} phase'>&nbsp;</a>";
			}		
		}
		
		public function displayTimelineMarkers($range = 500) {
			$start = -5000;
			$stop = 500;
			$count = $start;
			$percent = floor(100*$range/($stop-$start));
			$pixel = floor(900*$percent);
			while ($count < $stop)
			{
				echo "<div class='timeline-marker' style='width:{$percent}%;float:left;font-size:0.6em;' >";
				echo "|<br />";
				if ($count == 0)
				{
					echo "1 CE </div>";
					$count += $range;
					continue;
				}
				echo $count;
				if ($count > 0 )
					echo " CE";
				else
					echo " BCE";
				echo "</div>";
				$count += $range;
			}
		}
		
	}
