<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	// one of those weird oddities - use lowercase classnames
	class advancedsearch extends MVC_Authentication_View {
		
		public function __construct() {
			parent::__construct();		
		}
		
		public function __destruct() {
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}
