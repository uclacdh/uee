<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class article5 extends MVC_Authentication_View {
		public $articletitle;
		public $article;
		public $article_id;
		public $articleXpath;
		public $tei;
		public $teiUrl = 'http://www.tei-c.org/ns/1.0';
		public $glossCount = 0;
		public $biblCount = 0;
		public $glossDefs = array();
		public $glossTerms = array();
		public $glossArticleCounts = array();
		public $biblRefs = array();
		public $biblTitles = array();
		public $biblArticleCounts = array();
		public $placeRefs = array();
		public $articleRefs = array();
		public $soap; // connection to library's soap service
		public $images = array();
		public $downloadURL;
		public $linksOn = false;
		public $glossesOn = false;
		public $bibliorefsOn = false;
		public $alreadyDisplayed = array();
		public $ueeUser;
		public $annotations;
		public $imageRefs;
		protected $lastImageFloat = 'right';
		public $lightboxIds;
		public $referenceIdsCount = array(); // we'll use this to keep track of how often a biblRef has appeared
		
		public function __construct() {
			parent::__construct();		
			// let's load the articletitle
			$atitle = strtolower($_GET['article']);

			// validation?
			if ($atitle == 'random')
			{
				// load a random article
				try {
					$tempSoap = @new SoapClient(WSDL, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
				} catch (SoapFault $fault) {
					// if it fails, and we have an article already, continue
					header('Location: /error/');
					echo $fault->getMessage() .'<br />';
					exit();
				}
				$articlesRes = $tempSoap->listAllArticles();
				// echo count($articlesRes->return).'<br />';
				$atitle = str_replace(array(" ","/"),array("_","-"),$articlesRes->return[rand(0,count($articlesRes->return)-1)]->title);
			}
			
			// if (!$atitle)
			// 	$this->articletitle = 'Late_Period_Temples';
			// else if (!ereg('[A-Za-z0-9-]+',$atitle) )
			if (!preg_match('/[A-Za-z0-9-]+/',$atitle) )
				throw new Exception("error loading specified article");
			else
				$this->articletitle = $atitle;

			//echo $this->articletitle .'<br />';
				
			$this->loadArticle();
			$this->loadImages();

			// having troubel loading	
			// let's preload the references
			if (!$this->articleXpath)
			{
				header('Location: /error');
				exit();
			}

			// load the places
			$results = $this->articleXpath->query("//tei:placeName");
			foreach ($results as $placename)
			{
				if ($placename->hasAttribute('key'))
				{
					$this->placerefs[] = $placename->getAttribute('key');
				}
			}
			if (!empty($this->placerefs))
				$this->placerefs = array_unique($this->placerefs);

			// article references
			$refs = $this->articleXpath->query("//tei:ref");
			$seen = array();
			foreach ($refs as $ref)
			{
				if ($ref->hasAttribute('target') and substr($ref->getAttribute('target'),0,3)=='uee')
				{
					$articleId = substr($ref->getAttribute('target'),3);
					if (!in_array($articleId, array_keys($this->articleRefs)) )
					{
						$this->articleRefs[$articleId] = '';
						// echo 'articleRef: '.$articleId .'<br />';
					} 
					; // something to do with linking to other articles
				}
			}
			$categoryObj = new MVC_Model_Category(null);
			$catRows = $categoryObj->getAllRows();
			foreach ($this->articleRefs as $escholar_id => $val) {
				$escholar_str = 'uee'.$escholar_id;
				foreach ($catRows as $catrow) {
					if ($catrow['escholar_article_id'] == $escholar_id) {
						// make a URL link
						$urlName = str_replace(array(" ","/"), array("_","-"),ucwords($catrow['category']));
						$this->articleRefs[$escholar_id] = $urlName;
					}
				}
			}
			$categoryObj = null;			
			
			// get the download URL?
			$this->getDownloadURL();
			// load the logged in user, if any
 			$this->ueeUser = new MVC_Model_UeeUser(null);
			$sess = MVC_Session::getInstance();
			$this->ueeUser->search['user_id'] = $sess->user_id;
			$this->ueeUser->dbSearchTable();
			if ($this->ueeUser->search_results and count($this->ueeUser->search_results) == 1)
			{
				$this->ueeUser->data = $this->ueeUser->search_results[0];
			}
			else
			{
				$this->ueeUser->data = null;
			}
			$this->annotations = new MVC_Model_MyueeAnnotation(null);
			// load the inline image refs (tei:figure)
			// note that this is not very efficient, as we're forced to get the short metadata for allimages.
			// would like to combine this with a search query?
			$inlineImagesRes = $this->articleXpath->query("//tei:graphic");
			if ($this->soap)
			{
				$allimages = $this->soap->listAllImages();
				$allimagesarr = array();
				$fullimagesarr = array();
				if ($allimages->return)
				{
					foreach ($allimages->return as $img)
					{
						$allimagesarr[$img->id] = $img->thumbnailURL;
					}
				}
				if ($inlineImagesRes)
				{
					foreach ($inlineImagesRes as $graphic)
					{
						if ($graphic->hasAttribute('url') )
						{
							$this->imageRefs[$graphic->getAttribute('url')] = $allimagesarr[$graphic->getAttribute('url')];
							foreach ($this->images as $i)
							{
								if ($i->id == $graphic->getAttribute('url') )
									$this->lightboxRefs[$graphic->getAttribute('url')] = "http://digital2.library.ucla.edu/imageResize.do?contentFileId={$i->contentFileId}";
							}
						}
					}
				}
			}
		}
		
		public function __destruct() {
			// any objects to close?
			$this->glossObj = null;
			$this->ueeUser = null;
			$this->annotations = null;
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
		public function getOutput($xpathQuery,$display=false) {
			// let's get the baseNodes based on the xpath query
			$return = '';
			$nodes = $this->articleXpath->query($xpathQuery);
			// echo $nodes->length .'<br />';
			if ($nodes)
			{
				foreach ($nodes as $node)
				{
					$return .= $this->getOutputHelper($node);
				}
			}
			if ($display)
				echo $return;
			else
				return $return;
		}
		
		// this is the recursive function piece, to do what getOutputHelper needs to do?
		private function getOutputHelper($node, $parentRef=false) {
			// add a checker - if this node is a ref, toggle a flag because we do not want to process refs nested inside
			$processingRef = false;
			$saveForeign = false;
			$foreignVal = "";
			if ($node->localName == 'ref')
				$processingRef = true;
			// add a checker, if this is a glossref, we'll want to 'preserve' the child's node value as best as possible
			if ($processingRef) {
				$target = $node->getAttribute('target');
				if (substr($target,0,1) == 'g' and !in_array($target,$this->alreadyDisplayed) and substr($target,0,4) != 'gold') {
					$saveForeign = true;
				}
			}
			// first, process all the children
			if ($node->hasChildNodes())
			{
				foreach ($node->childNodes as $child)
				{
					if ($saveForeign and $child->localName == 'foreign')
						$foreignVal = $child->nodeValue;
					$this->getOutputHelper($child, ($processingRef or $parentRef));
				}
			}
			// then, process this node
			// basially, what wrapper tag does this node need?
			// long if-else / switch stmt ?
			switch ($node->localName) {
				case 'head':
					$cleanNodeVal = str_replace(" ","", $node->nodeValue);
					$cleanNodeVal = str_replace("(","", $cleanNodeVal);
					$cleanNodeVal = str_replace(")","", $cleanNodeVal);
					$node->nodeValue = '<div class="section-head">
						<a id="'.strtolower($cleanNodeVal).'-toggle" class="section-toggle" href="javascript:;"><img alt="expand/collapse this section" src="/graphics/Contract.png" /></a>
						<h3 class="article-section-title">'.$node->nodeValue.'</h3>
						</div>';
					break;
				case 'p':
					$node->nodeValue = '<div class="tei-p">'.$node->nodeValue.'</div>';
					break;
				// may 13 2010 - add support for emphasis tags in TEI markup
				case 'emph':
					$node->nodeValue = '<em>'.$node->nodeValue.'</em>';
					break;
				case 'foreign':
					$lang = $node->getAttribute('lang');
					$node->nodeValue = '<span class="foreign" lang="'.$lang.'" xml:lang="'.$lang.'" >'.$node->nodeValue.'</span>';
					break;
				case 'ref': // references!
					// only process if it's not a nested ref
					if (!$parentRef) {
						// what's the target type?
						$target = $node->getAttribute('target');
						// if (in_array($target,$this->alreadyDisplayed) )
							// break;
						if (substr($target,0,1) == 'g' and !in_array($target,$this->alreadyDisplayed) and substr($target,0,4) != 'gold') // glossword
						{
							// which gloss are we going to be showing?
							$keyword = $node->nodeValue;
							$glossword = '<a id="glossword_'.$this->glossCount.'" name="'.$target.'" class="glossref" href="javascript:;" >'.trim($node->nodeValue).'</a>';
							// apply the wrapper, and append the popup
							$node->nodeValue = $glossword;
							$this->glossCount++;						
						}
						else if (substr($target,0,3) == 'uee' and !in_array($target,$this->alreadyDisplayed) ) // article link
						{
							$aid = substr($target,3);
							$node->nodeValue = '<a class="articleref" href="/articles/'.urlencode($this->articleRefs[$aid]).'/" >'.htmlspecialchars(htmlentities(trim($node->nodeValue),ENT_COMPAT,'UTF-8'),ENT_COMPAT,'UTF-8').'</a>';
						}
						else if (substr($target,0,3) == 'bib') // bibliography link
						{
							// generate the popup
							$biblref = '<a name="'.$target.'" class="biblioref" href="javascript:;" >'.$node->nodeValue.'</a>';
							// apply the wrapper and append the popup
							$node->nodeValue = $biblref;
							$this->biblCount++;
						}
						else if ($node->hasAttribute('type') and $node->getAttribute('type') == 'url') // external link 
						{
							$url = $node->nodeValue;
							$node->nodeValue = '<a class="external" href="'.$url.'">'.$url.'</a>';
						}
						$this->alreadyDisplayed[] = $target;
					}
					break;
				case 'placeName': // we get to ignore this one, no special markup (only for the minimap)
					// let's insert an icon
					$node->nodeValue .= '<img class=\'place-icon\' src=\'/graphics/place-icon.png\' alt=\'\' />';
					break; 
				case 'figure': //
					if ($this->lastImageFloat == 'left')
						$this->lastImageFloat = 'right';
					else
						$this->lastImageFloat = 'left';
					$node->nodeValue = '<div class=\'figure\' style="float:'.$this->lastImageFloat.';">'.$node->nodeValue.'</div>';
					break;
				case 'graphic':
					$target = $node->getAttribute('url');				
					$node->nodeValue = '<a class=\'lightbox-link\' href=\''.$this->lightboxRefs[$target].'\' ><img src=\''.$this->imageRefs[$target].'\' alt=\''.$target.'\' /></a>';
					break;
				default: // no modification
					break;
			}
			return $node->nodeValue ." "; // the extra space helps, sometimes things get cut off otherwise... 
		}
				
		// if we're not displaying, we don't put the div wrapper on it (we'll do it later, similar to how we do gloss popups)
		public function generateReferencesOutput($display = true) {
			// start by getting the reference count
			$res = $this->articleXpath->query("//tei:biblStruct");
			for ($inti = 1; $inti <= $res->length; $inti++)
			{
				$biblStruct = $this->articleXpath->query("//tei:biblStruct[$inti]");
				// we want that xml id attriubute
				if ($biblStruct->item(0)->hasAttributes())
				{
					$attrs = $biblStruct->item(0)->attributes;
					if ($attrs->item(0))
						$id = $attrs->item(0)->nodeValue;
				}
//				$output = "<a id='anchor_{$id}' name='anchor_{$id}'></a>\n";
				$output = "<a name='anchor_{$id}'></a>\n";
				if ($display)
					$output .= "<div id='{$id}' class='reference'>";
				// get me some authors
				$authors_query = "//tei:biblStruct[$inti]/tei:monogr/tei:author | //tei:biblStruct[$inti]/tei:analytic/tei:author";
				$authors = $this->articleXpath->query($authors_query);
				$author_arr = array();
				foreach ($authors as $authorNode)
				{
					$author_arr[] = trim($authorNode->nodeValue);
				}
				$output .= '<span class=\'authors\'>'.implode("; ",$author_arr).'</span><br />';
				
				$output .= "\n";
				// get the year
				$year_query = "//tei:biblStruct[$inti]/tei:monogr/tei:imprint/tei:date";
				$years = $this->articleXpath->query($year_query);
				$year_arr = array();
				foreach ($years as $yearNode)
					$year_arr[] = trim($yearNode->nodeValue);
				$output .= '<span class=\'year\'>'.$year_arr[0].'</span>'."\n";
				
				if ($display)
					$output .= '<p class=\'bibliography\'>';
				else
					$output .= '<div class=\'bibliography\'>';
				// get me some book titles
				// analytic title
				$titles_query = "//tei:biblStruct[$inti]/tei:analytic/tei:title[@level='a']";
				$titles = $this->articleXpath->query($titles_query);				
				$titles_arr = array();
				foreach ($titles as $titleNode)
					$titles_arr[] = $titleNode->nodeValue;
				$output .= "<span class='analytic-title'>".$titles_arr[0]."</span>";				
				// main title
				$titles_query = "//tei:biblStruct[$inti]/tei:monogr/tei:title";
				$titles = $this->articleXpath->query($titles_query);				
				$titles_arr = array();
				foreach ($titles as $titleNode)
				{
					if (!$titleNode->hasAttributes() )
						$titles_arr[] = $titleNode->nodeValue;
					else if ($titleNode->hasAttribute('level') and $titleNode->getAttribute('level')=='j')
						$titles_arr[] = $titleNode->nodeValue;
				}
				$output .= "<span class='monogr-title'>".implode(". ",$titles_arr)."</span>";
				// journal title - treat like a main title, but add info about volume pages
				$edition_query = "//tei:biblStruct[$inti]/tei:monogr/tei:imprint/tei:biblScope[@type='vol']";
				$editions = $this->articleXpath->query($edition_query);				
				$edition_arr = array();
				foreach ($editions as $editionNode)
					$edition_arr[] = $editionNode->nodeValue;					
				$edition_query = "//tei:biblStruct[$inti]/tei:monogr/tei:imprint/tei:biblScope[@type='pp']";
				$editions = $this->articleXpath->query($edition_query);
				foreach ($editions as $editionNode)
					$edition_arr[] = $editionNode->nodeValue;
				if (empty($edition_arr))
					$output .= ".";
				else
					$output .= ", ". implode(", ",$edition_arr) .".";
				// series title
				$titles_query = "//tei:biblStruct[$inti]/tei:monogr/tei:title[@level='s']";
				$titles = $this->articleXpath->query($titles_query);				
				$titles_arr = array();
				foreach ($titles as $titleNode)
					$titles_arr[] = $titleNode->nodeValue;
				$output .= "<span class='series-title'>".$titles_arr[0]."</span>";		
				// publication place and location
				$publication_query = "//tei:biblStruct[$inti]/tei:monogr/tei:imprint/tei:pubPlace";
				$publications = $this->articleXpath->query($publication_query);
				$publication_arr = array();
				foreach ($publications as $pubNode)
					$publication_arr[] = $pubNode->nodeValue;
				$publication_query = "//tei:biblStruct[$inti]/tei:monogr/tei:imprint/tei:publisher";
				$publications = $this->articleXpath->query($publication_query);
				foreach ($publications as $pubNode)
					$publication_arr[] = $pubNode->nodeValue;
				$output .= "<span class='publication-info'>".htmlspecialchars(implode(": ",$publication_arr))."</span>";
				$notes_query = "//tei:biblStruct[$inti]/tei:monogr/tei:note";
				$notes_arr = array();
				$notes = $this->articleXpath->query($notes_query);
				foreach ($notes as $noteNode)
					$notes_arr[] = $noteNode->nodeValue;
				$output .= "<span class='notes'>".implode("; ",$notes_arr)."</span>";
				// any related articles?
				if ($this->soap and $id)
				{
					$bibArts = $this->soap->findBibliography(new findBibliography($id) );
					if ($bibArts)
					{
						$this->bibArticleCounts[$id] = count($bibArts->return);
						if ($this->bibArticleCounts[$id] > 1) 
						{
							$output .= "<ul class='articlecount'>";
							$output .= "<li><a href='/search/".str_replace(" ","_",$this->biblTitles[$id])."'>articles ({$this->bibArticleCounts[$id]})</a></li>";
							$output .= "</ul>";
						}
					}
				}				
				if ($display)
					$output .= "</p>";
				else
					$output .= "</div>";
				if ($display)
					$output.= "</div>";
				if ($display)
					echo $output;
				else
					$this->biblRefs[$id] = $output;
			}
			return;
		}
		
		public function generateReferences2() {
			$res = $this->articleXpath->query("//tei:biblStruct");
			for ($inti = 1; $inti <= $res->length; $inti++)
			{
				$biblStruct = $this->articleXpath->query("//tei:biblStruct[{$inti}]");
				if ($biblStruct->item(0)->hasAttributes())
				{
					$id = $biblStruct->item(0)->attributes->item(0)->nodeValue;
					// echo "{$id} <br />";
				}
				$authors = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:author | //tei:biblStruct[{$inti}]/tei:analytic/tei:author");
				$authorArr = array();
				foreach ($authors as $authorNode) {
					$authorArr[] = trim($authorNode->nodeValue);
				}
				echo "<div id='{$id}' class='reference'>";
				echo "<span class='authors'>".implode(", ",$authorArr) .'</span><br />';
				$date = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:imprint/tei:date");
				$dateArr = array();
				foreach ($date as $dateNode)
					$dateArr[] = trim($dateNode->nodeValue);
				echo "<span class='year'>".$dateArr[0].'</span>';
				echo "<p class='bibliography'>";
				$title = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:analytic/tei:title[@level='a'] | //tei:biblStruct[{$inti}]/tei:monogr/tei:title");
				$titleArr = array();
				$emTitle = false;
				foreach ($title as $titleNode)
				{
					if ($titleNode->parentNode->nodeName == 'tei:monogr' and $titleNode->hasAttributes())
						; // do nothing
					else
					{
						if ($titleNode->parentNode->nodeName == 'tei:monogr')
							$emTitle = true;
						$titleArr[] = trim($titleNode->nodeValue);
					}
				} 
				if (!empty($titleArr) and $emTitle)
					echo "<em>".$titleArr[0].'.</em> ';
				else
					echo $titleArr[0].', ';
				
				$title2 = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:title[@level='m']");
				$title2Arr = array();
				foreach ($title2 as $title2Node)
				{
					$title2Arr[] = trim($title2Node->nodeValue);
				}
				if (!empty($title2Arr))
					echo "In <em>".implode(", ",$title2Arr)."</em>, ";
				$title2a = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:title[@level='s']");
				$title2aArr = array();
				foreach ($title2a as $title2aNode)
				{
					$title2aArr[] = trim($title2aNode->nodeValue);
				}
				if (!empty($title2aArr))
					echo implode(", ",$title2aArr).". ";
				$title3 = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:title[@level='j']");
				$title3Arr = array();
				foreach ($title3 as $title3Node)
				{
					$title3Arr[] = trim($title3Node->nodeValue);
				}
				if (!empty($title3Arr))
				{
					echo "<em>".implode(", ",$title3Arr)."</em>";
					$vol = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:imprint/tei:biblScope[@type='vol']");
					$volArr = array();
					foreach ($vol as $volNode)
						$volArr[] = trim($volNode->nodeValue);
					if (!empty($volArr))
					{
						echo " {$volArr[0]}";
						$issue = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:imprint/tei:biblScope[@type='issue']");
						$issueArr = array();
						foreach ($issue as $issueNode)
							$issueArr[] = trim($issueNode->nodeValue);
						if (!empty($issueArr))
							echo " ({$issueArr[0]})";					
					}
				}
				$editor = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:editor");
				$editorArr = array();
				foreach ($editor as $editorNode)
					$editorArr[] = trim($editorNode->nodeValue);
				if (!empty($editorArr))
					echo "ed. ".implode(", ",$editorArr)." ";
					
				$pp = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:imprint/tei:biblScope[@type='pp']");
				$ppArr = array();
				foreach ($pp as $ppNode)
					$ppArr[] = trim($ppNode->nodeValue);
				if (!empty($ppArr))
					echo ", pp. {$ppArr[0]}. ";
				
				$pubplace = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:imprint/tei:pubPlace");
				$pubplaceArr = array();
				foreach ($pubplace as $pubplaceNode)
					$pubplaceArr[] = trim($pubplaceNode->nodeValue);
				if (!empty($pubplaceArr))
					echo implode(", ",$pubplaceArr).": ";
				$publisher = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:imprint/tei:publisher");
				$publisherArr = array();
				foreach ($publisher as $publisherNode)
					$publisherArr[] = trim($publisherNode->nodeValue);
				if (!empty($publisherArr))
					echo htmlspecialchars(implode(", ",$publisherArr)).". ";
				$noteUrls = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:note/tei:ref[@type='url']");
				foreach ($noteUrls as $noteUrlNode) {
					$cleanUrl = trim($noteUrlNode->nodeValue);
					$noteUrlNode->nodeValue = '<a href="'.$cleanUrl.'">'.$cleanUrl.'</a>';
				}
				$note = $this->articleXpath->query("//tei:biblStruct[{$inti}]/tei:monogr/tei:note");
				$noteArr = array();
				foreach ($note as $noteNode)
					$noteArr[] = trim($noteNode->nodeValue);
				if (!empty($noteArr))
					echo implode(", ",$noteArr)." ";					
				// done? extra line break
				echo '</p></div>';
			}			
		}
		
		private function loadArticle() {
			// connect to the soap service and find the matching article
 			$path = '/cache/articles/'.$this->articletitle.'.xml';

			//echo $this->articletitle;
			//echo '<br>';
			//echo $_SERVER['DOCUMENT_ROOT'].$path;
			// do we have the article?
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$path))
			{
				//echo "file exist";
				$this->article = new DOMDocument();
				$this->article->load($_SERVER['DOCUMENT_ROOT'].$path);
				$this->articleXpath = new DOMXPath($this->article);
				if (!$this->articleXpath->registerNamespace('tei',$this->teiUrl))
					die("error registering teiNamespace");			
			}
							
			// can i get to the soap service?
			$wsdl_url = WSDL; // not final
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				// if it fails, and we have an article already, continue
				if (!$this->article)
				{
					header('Location: /error/');
					exit();
					// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
				}
			}
			// if I have the article and a soap service, I'm done
			if ($this->article)
				return;
			// otherwise, get the article
			$res = $this->soap->listAllArticles();
			if ($res->return)
			{
				foreach ($res->return as $article)
				{
					// echo " {$article->title} : {$this->articletitle} <br />";
					// echo " {$article->title} : ".str_replace("-", " ",$this->articletitle)." <br />";
					if (strtolower($article->title) == str_replace("_", " ",$this->articletitle)) // this is the article we want!
					{
						// echo "found match <br />";
						// echo "article hosted at {$article->xmlURL} <br />";
						$this->article_id = $article->id;
						$permalink = strtolower($this->makePermalink($article->title));
						$linkPath = SITE_PATH."/articles/{$permalink}/";
						$cachePath = $_SERVER['DOCUMENT_ROOT'].'/cache/articles/'.$permalink.'.xml';
						// does it exist in our cache? do we need to update it?
						if (file_exists($cachePath))
						{
							// echo $cachePath . " exists in the cache <br />";
							// we would need to update it if the lastmodified time on the library's side is more recent than ours?
							if ((@filemtime($article->xmlURL) > @filemtime($cachePath)) )
							{
								// echo "attempted to update the cache <br />";
								copy($article->xmlURL,$cachePath) or $this->displayError("Failed to cache requested article: {$article->title}");
							}
						}
						else // if it doesn't exist, gotta cache it!
						{
							// echo "attempted to add to the cache <br />";
							copy($article->xmlURL,$cachePath) or $this->displayError("Failed to cache requested article: {$article->title}");
						}
						// now that we have an updated copy, we can break
						break;
					}
				}
			}
			// can i get it from the soap service?
			// else
			// die('no file found <br />'.$this->articletitle);

			if (file_exists($_SERVER['DOCUMENT_ROOT'].$path))
			{
				$this->article = new DOMDocument();
				$this->article->load($_SERVER['DOCUMENT_ROOT'].$path);
				$this->articleXpath = new DOMXPath($this->article);
				if (!$this->articleXpath->registerNamespace('tei',$this->teiUrl))
					die("error registering teiNamespace");


			}
			return;
		}

		// this function should die() upon finishing
		private function displayError($errorMsg = "There was an error loading this article") {
			header("Location: /error");
			exit();
			echo "<h3>Title: {$title}</h3>";
			echo "<p>There was an error loading this article: <br />
				<span>{$errorMsg}</span></[>";
			die();
		}

		private function makePermalink($title)
		{
			$permalink = str_replace(" ","_",$title);
			// $permalink = str_replace("(","",$permalink);
			// $permalink = str_replace(")","",$permalink);

			return $permalink;
		}

		public function getGeodata() {
			if (empty($this->placerefs))
				return '';
			$json = implode(",",$this->placerefs);
			return $json;
		}

		private function loadImages() {
			$search = strtok($this->articletitle,"(");
			$search = str_replace("_"," ",$search);
			if ($this->soap)
			{
				$res = $this->soap->searchImage(new searchImage($search));
				if (!$res or !$res->return)
					return false;
				foreach ($res->return as $image)
				{
					$this->images[] = $image;
				}
			}
		}

		public function displayImages() {
			if (!$this->soap)
			{
				echo "<p><strong>Cannot connect to the image respository. No images will be displayed.</strong></p>";
				return;
			}
			if (is_object($this->images[0])) {
				foreach ($this->images as $image)
				{
					// do some height processing
					$res = imagecreatefromjpeg($image->thumbnailURL);
					$width = imagesx($res);
					$height = imagesy($res);
					$ratio = $height/$width;
					// we're doing a manual margin; the css vertical align didn't seem to want to cooperate;
					if ($width > $height)
					{
						$adjH = $ratio*120;
						$margin = round((120 - $adjH) / 2);
					}
					else
						$margin = 0;
					echo "<a class='image-wrapper' href='/imageresults/{$this->articletitle}' ><img class='related-image' src='{$image->thumbnailURL}' alt='{$image->title}' title='{$image->title}' style='margin-top:{$margin}px;' /></a>";
				}
			}
			else if($this->images) {
				// do some height processing
				$res = imagecreatefromjpeg($this->images[2]);
				$width = imagesx($res);
				$height = imagesy($res);
				$ratio = $height/$width;
				// we're doing a manual margin; the css vertical align didn't seem to want to cooperate;
				if ($width > $height)
				{
					$adjH = $ratio*120;
					$margin = round((120 - $adjH) / 2);
				}
				else
					$margin = 0;
				echo "<a class='image-wrapper' href='/imageresults/{$this->articletitle}' ><img class='related-image' src='{$this->images[2]}' alt='{$this->images[3]}' title='{$this->images[3]}' style='margin-top:{$margin}px;' /></a>";
			}
		}

		private function getDownloadURL() {

			$res = $this->soap->listAllArticles();
			if ($res->return)
			{
				foreach ($res->return as $article)
				{
					// echo " {$article->title} : {$this->articletitle} <br />";
					// echo " {$article->title} : ".str_replace("-", " ",$this->articletitle)." <br />";
					if (strtolower($article->title) == str_replace("_", " ",$this->articletitle)) // this is the article we want!
					{
						$this->downloadURL = $article->pdfURL;
					}
				}
			}

			return;
		}

		public function getAnnotations() {
			if (!$this->article_id and $this->soap)
			{
				$res = $this->soap->listAllArticles();
				if ($res->return)
				{
					foreach ($res->return as $article)
					{
						if (strtolower($article->title) == strtolower(str_replace("_", " ",$this->articletitle) ) ) // this is the article we want!
						{
							$this->article_id = $article->id;
							// now that we have a match, done
							break;
						}
					}
				}
			}
			if (!$this->article_id)
				return;
			$this->annotations = new MVC_Model_MyueeAnnotation(null);
			$this->annotations->search['article_id'] = $this->article_id;
			$this->annotations->search['user_id'] = $_SESSION['user_id'];
			$this->annotations->dbSearchTable();
			$annotations = array();
			if ($this->annotations->search_results)
			{
				foreach ($this->annotations->search_results as $row)
				{
					$text = $row['annotation'];
					$annotations[$row['annotation_id']] = $text;
				}
			}
			return array("article_id"=>$this->article_id,"annotations"=>$annotations);
		}

		public function listAnnotations() {
			$annotations = $this->getAnnotations();
			if (!$this->soap)
			{
				echo "<p><strong>No connection to the library exists, no annotations will be displayed.</strong></p>";
				return;
			}
			if (!$annotations)
				return;
			foreach ($annotations['annotations'] as $annotation_id => $text)
				echo '<p>'.$text.'&nbsp;<a name="'.$annotation_id.'" class="annotation_edit" href="javascript:;">Edit</a>'.'</p>';
		}

		public function getGlossCount($gloss_word, $gloss_id) {
			$keywordResults = $this->soap->searchArticle(new searchArticle($gloss_word));
			$glossIdStr = 'g'.str_pad($gloss_id,6,"0", STR_PAD_LEFT);
			// $oldIdStr = 'g'.str_pad($old_id,6,"0", STR_PAD_LEFT);
			$glossResults = $this->soap->findGloss(new findGloss($glossIdStr));

			// lastly, try a fulltext search?
			$searchTerms = array($gloss_word);
			// if searchTerm happens to match a gloss or place name, get it's sibling names with it
			$query = "SELECT gloss_name.gloss_name FROM gloss_name WHERE gloss_name.gloss_id = {$gloss_id}";
			$result = MVC_Model_Gloss::query($query);
			while ($row = mysqli_fetch_array($result) )
			{
				$searchTerms[] = $row['gloss_name'];
			}
			$fulltextResults = $this->soap->findInText(new findInText(implode(" OR ",$searchTerms)) );
			$resultsArr = array();
			if ($keywordResults->return and !is_array($keywordResults->return) )
			{
				$article = $keywordResults->return;
				$resultsArr[] = $article->title;
			}
			else if ($keywordResults->return)
			{
				foreach ($keywordResults->return as $article)
					$resultsArr[] = $article->title;
			}
			if ($glossResults->return and !is_array($glossResults->return) )
			{
				$article = $glossResults->return;
				$resultsArr[] = $article->title;
			}
			else if ($glossResults->return)
			{
				foreach ($glossResults->return as $article)
					$resultsArr[] = $article->title;
			}
			if ($fulltextResults->return and !is_array($fulltextResults->return) )
			{
				$article = $fulltextResults->return;
				$resultsArr[] = $article->title;
			}
			else if ($fulltextResults->return)
			{
				foreach ($fulltextResults->return as $article)
					$resultsArr[] = $article->title;
			}
			$resultsArr = array_unique($resultsArr);
			return count($resultsArr);

		}
	}

