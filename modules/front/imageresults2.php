<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class imageresults2 extends MVC_Authentication_View {
		public $articletitle;
		public $article;
		public $articleXpath;
		public $tei;
		public $teiUrl = 'http://www.tei-c.org/ns/1.0';
		public $soap; // connection to library's soap service
		public $images;
		public $keyword_search;
		
		public function __construct() {
			parent::__construct();		
			// let's load the articletitle
			$atitle = $_GET['article'];			
			$this->keyword_search = str_replace("_"," ",$_GET['search']);
			$this->place_name = $this->getPlaceName($this->keyword_search);
			// echo 'atitle:'.$atitle .'<br />';
			// echo 'keyword_search:'.$this->keyword_search .'<br />';
			// if (!$atitle)
				// $atitle = $_GET['search'];
			// validation?
			if (!$atitle)
				$this->articletitle = '';
			else if (!ereg('[A-Za-z0-9-]+',$atitle) )
				throw new Exception("error loading specified article");
			else
				$this->articletitle = $atitle;
			// echo $this->articletitle .'<br />';
			
			if ($this->keyword_search)
			{
				// echo $this->keyword_search .'<br />';
				$this->keyword_search = strtok($this->keyword_search,"(");
				$this->loadSearch();
			}
			else
			{
				$this->keyword_search = str_replace("_"," ",$atitle);
				$this->loadArticle();
				$this->loadImages();
			}
		}
		
		public function __destruct() {
			// any objects to close?
			$this->glossObj = null;
			parent::__destruct();
		}

		// Get the main place_name given place_id
		private function getPlaceName($place_id){
			$tpname = new MVC_Model_PlaceName();
			$conditions = array('place_id' => $place_id, 'place_name_type_id' => 1);
			$results = $tpname->dbSearchTable($conditions);
			if ($results)
				$name = $results[0]['place_name'];
			else
				$name = $this->keyword_search;
			return $name;	
		}

		public static function intToPlaceId($int) {
		    return 'p'.str_pad($int, 6, '0', STR_PAD_LEFT);
                }
		// some custom events
		// let's use this to toggle the code file in question		
		private function loadSearch() {
			// connect to the soap service and find the matching article
			$wsdl_url = WSDL; // not final
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				header('Location: /error/');
				exit();
				// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}
			echo "Keyword search:";
			$res = $this->soap->searchImage(new searchImage(self::intToPlaceId($this->keyword_search)));
			if (!$res or !$res->return) {
                $this->images = array();
                return false;
            }
			if (!is_array($res->return)) // only one item?
				$this->images[] = $res->return;
			else
			{
				foreach ($res->return as $image)
				{
					$this->images[] = $image;
				}
			}
			return;
		}

		private function loadArticle() {
			// connect to the soap service and find the matching article
			try {
				$this->soap = new SoapClient(WSDL, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}
			$res = $this->soap->listAllArticles();
			if ($res->return)
			{	
				foreach ($res->return as $article)
				{
					if ($article->title == str_replace("_", " ",$this->articletitle)) // this is the article we want!
					{
						$permalink = $this->makePermalink($article->title);
						$linkPath = SITE_PATH."/articles/{$permalink}/";
						$cachePath = $_SERVER['DOCUMENT_ROOT'].'/cache/articles/'.$permalink.'.xml';
						// does it exist in our cache? do we need to update it?
						if (file_exists($cachePath))
						{
							// we would need to update it if the lastmodified time on the library's side is more recent than ours?
							if ((@filemtime($article->xmlURL) > @filemtime($cachePath)) )
							{
								copy($article->xmlURL,$cachePath) or die("Failed to cache requested article: {$article->title}");
							}
						} 
						else // if it doesn't exist, gotta cache it!
						{
							copy($article->xmlURL,$cachePath) or die("Failed to cache requested article: {$article->title}");
						}
						// now that we have an updated copy, we can break
						break;
					}
				}
			}
			
			// does this article's xml already exist in the cache?
			$path = '/cache/articles/'.$this->articletitle.'.xml';
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$path))
			{
				$this->article = new DOMDocument();
				$this->article->load($_SERVER['DOCUMENT_ROOT'].$path);
				$this->articleXpath = new DOMXPath($this->article);
				if (!$this->articleXpath->registerNamespace('tei',$this->teiUrl))
					die("error registering teiNamespace");			
			}
			// can i get it from the soap service?
			else
				die('no file found <br />'.$this->articletitle);
			return;
		}
		
		private function makePermalink($title)
		{
			$permalink = str_replace(" ","_",$title);
			// $permalink = str_replace("(","",$permalink);
			// $permalink = str_replace(")","",$permalink);
			
			return $permalink;
		}
		
		private function loadImages() {
			$search = strtok($this->articletitle,"(");
			$search = str_replace("_"," ",$search);
			$res = $this->soap->searchImage(new searchImage($search) );
			if (!$res or !$res->return)
				return false;
			if (!is_array($res->return))
				$this->images = $res->return;
			else {
				foreach ($res->return as $image)
				{
					$this->images[] = $image;
				}
			}
		}
		
		public function displayImages() {
			foreach ($this->images as $image)
			{
				// display the thumbnail
				// do some height processing
				$res = imagecreatefromjpeg($image->thumbnailURL);
				if (!$res)
					continue;
				$width = imagesx($res);
				$height = imagesy($res);
				$ratio = $height/$width;
				// we're doing a manual margin; the css vertical align didn't seem to want to cooperate;
				if ($width > $height)
				{
					$adjH = $ratio*120;
					$margin = round((120 - $adjH) / 2);
				}
				else
					$margin = 0;
				echo "<a class='image-wrapper' href='javascript:' ><img id='{$image->contentFileId}' class='related-image' src='{$image->thumbnailURL}' alt='{$image->title}' title='{$image->title}' style='margin-top:{$margin}px;' /></a><span style='display:none;'>{$image->id}</span>";
			}
		}
	}
