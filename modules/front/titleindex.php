<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class titleindex extends MVC_Authentication_View {
		public $soap;
		public $firstArticle;
		public $titles = array();
		public $arabicTitles = array();
		public $frenchTitles = array();
		public $germanTitles = array();
		
		public function __construct() {
			parent::__construct();		
			
			// can I get my soap service?
			$wsdl_url = WSDL;
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
				
			} catch (SoapFault $fault) {
				header('Location: /error/');
				exit();
				// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}
			$this->firstArticle=null;
			$this->loadTitles();
		}
		
		private function loadTitles() {
			$res = $this->soap->listAllArticles();
			if ($res->return) {
				foreach ($res->return as $article)
				{
					if (!$this->firstArticle)
						$this->firstArticle=$article;
					$this->titles[$article->id] = $article->title;
				}
			}
			$res = $this->soap->listFrenchTitles(); 
			if ($res->return) {			
				foreach ($res->return as $article)
				{
					$this->frenchTitles[$article->id] = $article->title;
				}
			}
			$res = $this->soap->listGermanTitles();
			if ($res->return) {
				foreach ($res->return as $article)
				{
					$this->germanTitles[$article->id] = $article->title;
				}
			}
			$res = $this->soap->listArabicTitles();
			if ($res->return) {
				foreach ($res->return as $article)
				{
					$this->arabicTitles[$article->id] = $article->title;
				}
			}
		}
		
		public function listAllArticles() {
			foreach ($this->titles as $title)
			{
				$permalink = strtolower($this->makePermalink($title));
				$linkPath = SITE_PATH."/articles/{$permalink}/";
				echo "<li>"."<a class=\"{$title}\" href=\"javascript:;\" >".$title."</a>"."</li>"."\n";
			}
		}
		
		public function listFrenchArticles() {
			// asort($this->frenchTitles);
			foreach ($this->frenchTitles as $id => $french)
			{
				$permalink = $this->titles[$id];
				echo "<li>"."<a class=\"{$permalink}\" href=\"javascript:;\" >".$french."</a>"."</li>"."\n";
			}
		}
		
		public function listGermanArticles() {
			// asort($this->germanTitles);
			foreach ($this->germanTitles as $id => $german)
			{
				$permalink = $this->titles[$id];
				echo "<li>"."<a class=\"{$permalink}\" href=\"javascript:;\" >".$german."</a>"."</li>"."\n";
			}
		}
		
		public function listArabicArticles() {
			// asort($this->arabicTitles);
			foreach ($this->arabicTitles as $id => $arabic)
			{
				$permalink = $this->titles[$id];
				echo "<li>"."<a class=\"{$permalink}\" href=\"javascript:;\" >".$arabic."</a>"."</li>"."\n";
			}
		}
		
		private function makePermalink($title)
		{
			$permalink = str_replace(array(" ","/"),array("_","-"),$title);
			// $permalink = str_replace("(","",$permalink);
			// $permalink = str_replace(")","",$permalink);
			
			return $permalink;
		}
		
		public function displayBrief() {
			$xml = new DOMDocument;
			if (!@$xml->load($this->firstArticle->xmlURL)) {
				$cachePath = $_SERVER['DOCUMENT_ROOT'] .'/cache/articles/'.$this->makePermalink($this->firstArticle->title).'.xml';
				if (!file_exists($cachePath))
					return("ERROR: No cached article file");
				if (!@$xml->load($cachePath))
					return("ERROR: problem loading cached article");
			}
			$xpath = new DOMXPath($xml);
			$teiUrl = 'http://www.tei-c.org/ns/1.0';
			$xpath->registerNamespace('tei',$this->teiUrl);
			$titles = $xpath->query("//tei:titleStmt/tei:title");
			echo '<h3>';
			foreach($titles as $title)
			{
				if ($title->hasAttributes())
				{
					echo "<span lang='".$title->getAttribute('lang')."' xml:lang='".$title->getAttribute('lang')."' >".$title->nodeValue."</span><br />";
				}
				else
					echo $title->nodeValue .'<br />';
			}
			echo '</h3>';
			
			echo '<p id="author">Author:<br />';
			$authors = $xpath->query("//tei:div/tei:byline");
			foreach ($authors as $author)
				echo $author->nodeValue .'<br />';
			echo '</p>';
			
			echo '<p id="abstract">';
			$abstracts = $xpath->query("//tei:div[@type='english_abstract']/tei:p");
			foreach ($abstracts as $abstract)
			{
				// note: the abstract can have foreign tags, apply the lang attribute properly
				if ($abstract->hasChildNodes())
				{
					foreach ($abstract->childNodes as $cNode)
					{
						if ($cNode->localName == 'foreign')
						{
							$cNode->nodeValue = '<span lang="'.$cNode->getAttribute('lang').'" xml:lang="'.$cNode->getAttribute('lang').'" >'.$cNode->nodeValue.'</span>';
						}
					}
				}
				echo $abstract->nodeValue .'<br /><br />';
			}
			echo '</p>';
			echo '<p id="arabic_abstract" lang="ar" xml:lang="ar" >';
			$abstracts = $xpath->query("//tei:div[@type='arabic_abstract']");
			foreach ($abstracts as $abstract)
				echo $abstract->nodeValue;
			echo '</p>';
			echo '<p id="status" style="font-weight:bold;">';
			$notes = $xpath->query("//tei:notesStmt/tei:note");
			$statuses = array();
			if ($notes->length > 0)
				foreach ($notes as $note)
					$statuses[] = $note->nodeValue;
			echo implode(", ",$statuses);
			echo '</p>';			
			echo '<p id="buttons">';
			echo '<form action="'.SITE_PATH.'/articles/'.$this->makePermalink($this->firstArticle->title).'/" method="GET" >';
			echo '<input type="image" src="/graphics/Read_Article.png" alt="Read Article &gt;&gt;" />';		
			echo '</form></p>';
			return;
		}
		
		public function getForeignTitles() {
			// $res = $this->soap->listAllArticles();
			// foreach ($res->return as $article)
			// {
				// $english = $article->title;
				// $id = $article->id;
				// $metadatasearch = $this->soap->getMetadata(new getMetadata($id));
				// $recordXML = new DOMDocument();
				// $recordXML->loadXML($metadatasearch->return);
				// $xpath = new DOMXPath($recordXML);
				// echo htmlentities($metadatasearch->return).'<br /><br />';
				// $titles = $xpath->query("//altTitle/*");
				// foreach ($titles as $title)
					// echo $title->nodeName .': '.$title->nodeValue.'<br />';
				// echo '<br />';			
			// }		
		}
		
		public function __destruct() {
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}

