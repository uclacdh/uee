		<article class="post" id="post-5">

			<h2>Editors and Staff</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/editors/" title="2:32 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T14:32:14+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<h3>Editors</h3>
<p><strong>Willeke Wendrich</strong> &#8211; Editor-in-Chief, UCLA<br />
<strong>Jacco Dieleman</strong> &#8211; Editor, UCLA<br />
<strong>Elizabeth Frood</strong> &#8211; Editor, Oxford<br />
<strong>John Baines</strong> &#8211; Senior Editorial Consultant, Oxford For the list of authors, click <a href="/authors">here</a></p>
<h3>Area Editors</h3>
<p><strong>Jacco Dieleman</strong> (UCLA, USA) &#8211; Area Editor, Religion<br />
<strong>Elizabeth Frood</strong> (University of Oxford, UK) &#8211; Area Editor, Individual and Society<br />
<strong>Wolfram Grajetzki</strong> (University College London, UK) – Area Editor, Time and History<br />
<strong>Juan Carlos Moreno Garcia</strong> (Université Charles-de-Gaulle, France) &#8211; Area Editor, Economy<br />
<strong>Andréas Stauder</strong> (Swiss National Science Foundation –Universität Basel) &#8211; Area Editor, Language<br />
<strong>Julie Stauder-Porchet</strong> (Swiss National Science Foundation – Universität Basel) &#8211; Area Editor, Language<br />
<strong>Willeke Wendrich</strong> (UCLA, USA) &#8211; Area Editor, Material Culture, Art and Architecture; Geography; Natural Environment</p>
<h3></h3>
<p>&nbsp;</p>
<h3>Editorial Staff</h3>
<p><strong>Gina Dubois</strong> &#8211; Copy Editor<br />
<strong>Justine Gesell</strong> &#8211; Copy Editor<br />
<strong>Menna el-Dorry</strong> &#8211; Translator, Arabic<br />
<strong>Hisham Hussein</strong> &#8211; Translator, Arabic<br />
<strong>Mohsen Kamel</strong> &#8211; Copy Editor, Arabic<br />
<strong>Kandace Pansire</strong> &#8211; Editorial Assistant <strong> </strong></p>
<h3>Mark-up assistants</h3>
<p><strong>Amy Chan<br />
Evelyn Bonilla<br />
Angelic Gonzalez<br />
</strong><strong>John Yung<br />
</strong><strong>Katherine Zalog</strong></p>
<h3>Technical Staff</h3>
<p><strong>Stephen Davison</strong>, Digital Librarian, Coordinator<br />
<strong>Lisa McAulay</strong>, Digital Librarian, Mark-up specialist<br />
<strong>Christina Patterson</strong>, Lead programmer<br />
<strong>Henry Chiong</strong> &#8211; Digital Library Programmer</p>
<h3>Previous Staff of the UEE</h3>
<p><strong>Anne Austin</strong> &#8211; Editorial Assistant<br />
<strong>Sonali Gupta-Agarwal</strong> &#8211; Editorial Assistant<br />
<strong>Krystal Lords</strong> &#8211; Editorial Assistant<br />
<strong>Kamelia Loukipoudis</strong> &#8211; Editorial Assistant<br />
<strong>Adam Stojanik</strong> &#8211; Editorial Assistant<br />
<strong>Elaine Sullivan</strong> &#8211; Coordinator, Karnak Virtual Reality Project<br />
<strong>Elizabeth Waraksa</strong> &#8211; CLIR Postdoctoral Fellow, UCLA Library, Copy Editor<br />
<strong>Carrie Zarnoch</strong> &#8211; Editorial Assistant, Karnak Virtual Reality Project</p>
<h3>Technical Development Staff</h3>
<p><strong>Dr. Zoe Borovsky</strong> &#8211; Development Coordinator, CDH/UDHIG/Library<br />
<strong>Lisa Snyder</strong> &#8211; Virtual Reality Coordinator, IDRE<br />
<strong>Yoh Kawano</strong> &#8211; GIS and time-map specialist<br />
<strong>Jennifer Dillon</strong> – Web designer, IDRE<br />
<strong>Shawn Higgins</strong> &#8211; Web designer, CDH</p>
<h3>Academic Editorial Committee</h3>
<p>The Academic Editorial Committee consists of leading scholars of Egyptology. Its tasks are to establish and maintain the overall concept of the encyclopedia.</p>
<h3>Members:</h3>
<p><strong>Dorothea Arnold</strong> &#8211; Curator, Egyptian Dept., Metropolitan Museum of Art , NY<br />
<strong>John Baines</strong> &#8211; Professor, Egyptology, Oxford University , Great Britain<br />
<strong>Joris F. Borghouts</strong> &#8211; Professor, Egyptology, Leiden University , the Netherlands<br />
<strong>Jacco Dieleman</strong> &#8211; Assistant Professor, Egyptology, NELC, UCLA<br />
<strong>Elizabeth Frood</strong> &#8211; Lecturer in Egyptology, Oriental Institute, University of Oxford, Great Britain<br />
<strong>Fayza Haikal</strong> &#8211; Professor, Egyptology, American University in Cairo , Egypt<br />
<strong>Janet Johnson</strong> &#8211; Professor, Egyptology, Oriental Institute, University of Chicago Middle Egyptian Text Editions for Online Research ( METEOR ), and XML System for Textual and Archaeological Research (XSTAR )<br />
<strong>Antonio Loprieno</strong> &#8211; Rector, Universität Basel, Switzerland<br />
<strong>Stephan Seidlmayer</strong> &#8211; Professor, Egyptology, Free Berlin University , editor web-based Altägyptisches Wörterbuch<br />
<strong>Willeke Wendrich</strong> &#8211; Professor, Egyptian Archaeology, NELC, UCLA<br />
<strong>Jean Winand</strong> &#8211; Professor, Université de Liège<br />
<strong>Alain Zivie</strong> &#8211; Director, CNRS Egyptology, Paris , France   <strong> </strong></p>
<h3>Technology Committee</h3>
<p>The Technology Committee gave advice in the initial stages of developing the UEE.</p>
<h4>Members:</h4>
<p><strong>Colin Allen</strong> Associate editor Stanford Encyclopedia of Philosophy<br />
<strong>Hans van den Berg</strong> Programmer Analyst, Annual Egyptological Bibliography<br />
<strong>Zoe P. Borovsky</strong> Academic Services Manager, Center for Digital Humanities<br />
<strong>Robert Englund Professor</strong>, Near Eastern Languages and Cultures, UCLA, Director of the Cuneiform Digital Library Initiative<br />
<strong>Diane Favro</strong>, School of Architecture and Urban Design, UCLA, Director of the Experiential Technologies Center<br />
<strong>Eugene T. Horikawa</strong> Programmer Analyst, Center for Digital Humanities<br />
<strong>Willem Hovestreydt</strong> Editor of the Annual Egyptological Bibliography<br />
<strong>Pete Nielsen</strong> Coordinator, Server Development and Support, Academic Technology Services , UCLA<br />
<strong>Janice Reiff</strong> Associate Professor, History, UCLA and co-editor Encyclopedia of Chicago<br />
<strong>Margo Reveil</strong> Manager, Academic Technology Services, UCLA<br />
<strong>Stephen Schwartz</strong> Computer Resource Manager, Library Information Systems<br />
<strong>Edward N. Zalta</strong> Editor-in-chief, Stanford Encyclopedia of Philosophy</p>
<h3></h3>

				
			</div>

			
		</article>
		
