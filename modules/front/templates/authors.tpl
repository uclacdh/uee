		<article class="post" id="post-59">

			<h2>The Authors</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/authors/" title="7:37 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T19:37:08+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<p>Authors are contribution to the UEE by invitation. Criteria for the selection of authors and area editors are their academic record and standing, as well as their publication record on a specific sub-field or particular subject.</p>
<p>&nbsp;</p>
<p>In alphabetical order the authors whose articles have been published to date are:</p>
<p><strong>Baines, John</strong> (UK)<br />
<strong>Bloxam, Elizabeth</strong> (UK)<br />
<strong>Borg, Barbara E.</strong> (Germany)<br />
<strong>Brand, Peter</strong> (USA)<br />
<strong>Budde, Dagmar</strong> (Germany)<br />
<strong>Campagno, Marcelo P.</strong> (Argentina)<br />
<strong>Cooney, Kathlyn M</strong> (USA)<br />
<strong>Coppens, Filip</strong> (Czech Republic)<br />
<strong>Coulon, Laurent</strong> (France)<br />
<strong>Cruz-Uribe, Eugene</strong> (USA)<br />
<strong>Darnell, John C.</strong> (USA)<br />
<strong>Dodson, Aidan</strong>  (UK)<br />
<strong>De Meyer, Marleen</strong> (Belgium)<br />
<strong>Duqesne, Terence</strong> (UK)<br />
<strong>Emerit, Sibylle</strong> (France)<br />
<strong>Enmarch, Roland</strong> (UK)<br />
<strong>Exell, Karen</strong> (UK)<br />
<strong>Emery, Virginia L.</strong> (USA)<br />
<strong>Fiore Marochetti, Elisa</strong> (Italy)<br />
<strong>Gallet, Laetitia </strong>(France)<br />
<strong>Gillam, Robyn</strong> (Canada)<br />
<strong>Grajetzki, Wolfram</strong> (UK)<br />
<strong>Guilhou, Nadine</strong> (France)<br />
<strong>Hallof, Jochen</strong> (Germany)<br />
<strong>Harrell, James A.</strong> (USA)<br />
<strong>Hays, Harold</strong> (Netherlands)<br />
<strong>Haring, Ben</strong> (Netherlands)<br />
<strong>Harvey, Julia </strong>(Netherlands)<br />
<strong>Hikade, Thomas</strong> (Canada)<br />
<strong>Huyge, Dirk</strong> (Belgium)<br />
<strong>Ikram, Salima</strong> (Egypt)<br />
<strong>Janák, Jíří</strong> (Czech Republic)<br />
<strong>Kahl, Jochem</strong> (Germany)<br />
<strong>Katary, Sally</strong> (Canada)<br />
<strong>Kockelman, Holger</strong> (Germany)<br />
<strong>Köpp-Junk, Heidi</strong> (Germany)<br />
<strong>Kucharek, Andrea</strong> (Germany)<br />
<strong>Kuhlman, Klaus P.</strong> (Germany)<br />
<strong>Laboury, Dimitri</strong> (Belgium)<br />
<strong>Lazaridis, Nikolaos</strong> (UK, USA)<br />
<strong>Leprohon, Ronald</strong> (Canada)<br />
<strong>Lippert, Sandra</strong> (Germany)<br />
<strong>Leach, Bridget</strong> (UK)<br />
<strong>Manniche, Lise</strong> (Denmark)<br />
<strong>Loprieno, Antonio</strong> (Switzerland)<br />
<strong>Lucarelli, Rita</strong> (Germany)<br />
<strong>Luiselli, Michela </strong><strong>Manassa, Colleen</strong> (USA)<br />
<strong>Masson, Aurélia</strong> (France)<br />
<strong>McClain, Brett</strong> (USA)<br />
<strong>Meyer-Dietrich, Erika</strong> (Germany)<br />
<strong>Milde, Henk</strong> (Netherlands)<br />
<strong>Millet, Marie</strong> (France)<br />
<strong>Minas-Nerpel, Martina</strong> (UK)<br />
<strong>Moeller, Nadine </strong>(USA)<br />
<strong>Moreno Garcia, Juan Carlos</strong> (France)<br />
<strong>Muhlestein, Kerry</strong> (USA)<br />
<strong>Naguib, Saphinaz-Amal</strong> (Norway)<br />
<strong>Nicholson, Paul</strong> (UK)<br />
<strong>Pantalacci, Laure</strong> (France)<br />
<strong>Phillips, Jacke S.</strong> (UK)<br />
<strong>Pinch, Geraldine</strong> (UK)<br />
<strong>Poo, Mu-Chou </strong>(Taiwan)<br />
<strong>Riggs, Christina</strong> (UK)<br />
<strong>Roth, Silke</strong> (UK)<br />
<strong>Schulz, Regine</strong> (Germany)<br />
<strong>Servajean, Frédéric</strong> (France)<br />
<strong>Shortland, Andrew</strong> (UK)<br />
<strong>Smith, Mark</strong> (UK)<br />
<strong>Spencer, Neal</strong> (UK)<br />
<strong>Spieser, Cathie</strong> (Switzerland)<br />
<strong>Stadler, Martin A.</strong> (Germany)<br />
<strong>Stevens, Anna</strong> (UK)<br />
<strong>Stevenson, Alice</strong> (UK)<br />
<strong>Sullivan, Elaine</strong> (USA)<br />
<strong>Sweeny, Deborah</strong> (Israel)<br />
<strong>Teeter, Emily </strong>(USA)<br />
<strong>Toivari-Viitala, Jaana</strong> (Finland)<br />
<strong>Uljas, Sami</strong> (Sweden)<br />
<strong>Veldmeijer, André J.</strong> (Netherlands)<br />
<strong>Vinson, Steve</strong> (USA)<br />
<strong>Vittmann, Günter</strong> (Germany)<br />
<strong>von Lieven, Alexandra</strong> (Germany)<br />
<strong>Waraksa, Elizabeth A.</strong> (USA)<br />
<strong>Wengrow, David</strong> (UK)<br />
<strong>Wilkinson, Richard H.</strong> (USA)<br />
<strong>Zivie-Coche, Christiane </strong>(France)</p>

				
			</div>

			
		</article>
		
		
