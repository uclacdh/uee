	<div id='results' class='column_half'>
		<h3>title index (a-z)</h3>
		<p id='sort-wrapper'>
			Show titles in:
			<a href="javascript:;">English</a> |
			<a href="javascript:;">French</a> |
			<a href="javascript:;">German</a> |
			<a href="javascript:;">Arabic</a> <br />
			<div id='letter-filters'>
				<a class='letter-filter' href='javascript:;'>All</a>
			<?php
				foreach (range('A','Z') as $letter) {
					echo "<a class='letter-filter' href='javascript:;'>{$letter}</a>&nbsp";
				}
			?>
			</div>
		</p>
		<ul id='titles-english' class='titles'>
		<?php
			$this->listAllArticles();
		?>
		</ul>
		<ul id='titles-french' class='titles' lang='fr' xml:lang='fr' >
		<?php
			$this->listFrenchArticles();
		?>
		</ul>
		<ul id='titles-german' class='titles' lang='de' xml:lang='de' >
		<?php
			$this->listGermanArticles();
		?>
		</ul>
		<ul id='titles-arabic' class='titles' lang='ar' xml:lang='ar' >
		<?php
			$this->listArabicArticles();
		?>
		</ul>
	</div>
	
	<div id='brief' class='column_half' >
	</div>


	<div class='clearfix'></div>
	
	<div id='foreign-titles'>
		<?php $this->getForeignTitles(); ?>
	</div>

<script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/titleindex.js" ></script>
