<div style='padding:2%;padding-top:0;'>

<h2>Error</h2>
<p>Whoops! Looks like the page you wanted is unavailable at this time. <br />
Possible reasons include: </p>
<ul>
	<li><strong>You may have acecssed an article which is still <em>in progress</em> and not available for viewing.</strong>
	<li>Please check the <span style='color:red; font-style:italic;'>red italicized text</span> at the top for any notices from the development team.</li>
	<li>Do you have the right URL?</li>
	<li>One of the necessary services could be down:
		<ul>
			<li>UCLA Library Soap Service</li>
			<li>ATS Metadata System</li>
		</ul>
	</li>
</ul>

<p>Please contact the webmaster for additional assistance</p>
</div>