<article class="post" id="post-29">

			<h2>Preffered Chronology</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/chronology/" title="6:31 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T18:31:15+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<p>We request contributors of the UEE to use the following chronology as basis. The editors are well aware that many of the rounded dates represented here are a simplification, or represent a particular view. Whenever relevant, authors are encouraged to refine or comment on the chronology for a specific period or subject, but should be aware that in the next phase of development of the UEE, more detailed chronologies will be provided, and several articles will be commissioned discussing the development of chronology, the sources, and their interpretation. We encourage authors to comment on the chronology listed below, or to suggest encyclopedia articles related to chronological issues. Overlapping dates indicate co-regencies.</p>
<p>The chronology is the work of Thomas Schneider (University of British Columbia). The dates before 664 BCE are approximate.</p>
<table class="tbl_stylem" width="450">
<tbody>
<tr>
<td width="72"><strong>Phase</strong></td>
<td width="65"><strong>Dynasty</strong></td>
<td colspan="2"><strong>Period or Ruler</strong></td>
<td><strong>Years BCE</strong></td>
</tr>
<tr>
<td rowspan="7">Prehistory</td>
<td rowspan="7"></td>
<td colspan="2">Lower Palaeolithic</td>
<td width="49">700,000-175,000</td>
</tr>
<tr>
<td colspan="2">Middle Palaeolithic</td>
<td width="49">175,000-40,000</td>
</tr>
<tr>
<td colspan="2">Upper Palaeolithic</td>
<td width="49">40,000-35,000</td>
</tr>
<tr>
<td colspan="2">Final Palaeolithic</td>
<td width="49">25,000-7000</td>
</tr>
<tr>
<td colspan="2">Epi-palaeolithic</td>
<td width="49">7000-6000</td>
</tr>
<tr>
<td colspan="2">Neolithic</td>
<td width="49">6000-4500</td>
</tr>
<tr>
<td colspan="2">Badarian (Middle Egypt)</td>
<td width="49">4500-4000</td>
</tr>
<tr>
<td rowspan="5" bgcolor="#CCCCCC">Predynastic</td>
<td rowspan="4"></td>
<td colspan="2">Badarian (Middle Egypt)</td>
<td>4500-4000</td>
</tr>
<tr>
<td colspan="2">Early A–-group (Lower Nubia)<br />
Naqada I–early II (Upper Egypt)<br />
Maadi/Buto I (Lower Egypt)</td>
<td>4000-3400</td>
</tr>
<tr>
<td colspan="2">Middle A–group (Lower Nubia)<br />
Late Naqada II (Upper Egypt)<br />
Buto II (Lower Egypt)</td>
<td>3400-3300</td>
</tr>
<tr>
<td width="77">Late/Final A-group (Lower Nubia)<br />
Naqada IIIa (Upper Egypt)<br />
Buto III (Lower Egypt)</td>
<td width="178">  Tomb U-j ruler</td>
<td>3300-3150</td>
</tr>
<tr>
<td>‘Dynasty 0’</td>
<td>Naqada IIIb<br />
Buto IV (Lower Egypt)</td>
<td>Irihor(?), Ka / Sekhen, Crocodile (?), Scorpion</td>
<td>3150-2950</td>
</tr>
<tr>
<td rowspan="2">Early dynastic</td>
<td>Dynasty 1</td>
<td>Naqada IIIc<br />
Buto V (Lower Egypt)</td>
<td>Narmer, Aha [=Menes?], Djer, Wadj / Djet, Merneith (Queen), Den / Dewen, Anedjib, Semerkhet, Qaa</td>
<td>2950-2800</td>
</tr>
<tr>
<td>Dynasty 2</td>
<td>Naqada IIId</td>
<td>Hetepsekhemwy, Raneb, Ninetjer, Peribsen, Khasekhem(wy)</td>
<td>2800-2670</td>
</tr>
<tr>
<td style="background: #ccc;" rowspan="21">Old Kingdom</td>
<td>Dynasty 3</td>
<td></td>
<td>Netjerykhet (Djoser), Sekhemkhet, Khaba, Huni</td>
<td>2670-2600</td>
</tr>
<tr>
<td rowspan="6">Dynasty 4</td>
<td>Sneferu</td>
<td></td>
<td>2600-2480</td>
</tr>
<tr>
<td>Khufu</td>
<td>(Cheops)</td>
<td></td>
</tr>
<tr>
<td>Radjedef</td>
<td>(Djedefra)</td>
<td></td>
</tr>
<tr>
<td>Khafra</td>
<td>(Rakhaef, Chephren)</td>
<td></td>
</tr>
<tr>
<td>Menkaura</td>
<td>(Mycerinus, Mykerinos)</td>
<td></td>
</tr>
<tr>
<td>Shepseskaf</td>
<td></td>
<td></td>
</tr>
<tr>
<td rowspan="9">Dynasty 5</td>
<td>Userkaf</td>
<td></td>
<td>2480-2350</td>
</tr>
<tr>
<td>Sahura</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Neferirkara Kakai</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Shepseskara Ini</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Raneferef</td>
<td>(Neferefra)</td>
<td></td>
</tr>
<tr>
<td>Neuserra Isi</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Menkauhor</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Djedkara Isesi</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Unas</td>
<td></td>
<td></td>
</tr>
<tr>
<td rowspan="4">Dynasty 6</td>
<td>Tety</td>
<td></td>
<td>2350-2200</td>
</tr>
<tr>
<td>Pepy I</td>
<td>(Meryra)</td>
<td></td>
</tr>
<tr>
<td>Merenra Nemtyemsaf</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Pepy II</td>
<td>(Neferkara)</td>
<td></td>
</tr>
<tr>
<td>(Dynasty 7)<br />
Dynasty 8</td>
<td colspan="2">Brief rules of several kings<br />
(including Neferkara)</td>
<td>2200-2168</td>
</tr>
<tr>
<td rowspan="7">First Intermediate Period</td>
<td>Dynasty 9<br />
Dynasty 10</td>
<td colspan="2">Herakleopolitan dynasties, brief rules of several kings (Khety, Merykara, Ity).</td>
<td>2168-2030</td>
</tr>
<tr>
<td rowspan="6">Dynasty 11</td>
<td colspan="2">Theban dynasty in divided Egypt</td>
<td>2130-1990</td>
</tr>
<tr>
<td colspan="2">[Mentuhotep Tepy-a (I)]</td>
<td></td>
</tr>
<tr>
<td>Intef I</td>
<td>(Sehertawy)</td>
<td></td>
</tr>
<tr>
<td>Intef II</td>
<td>(Wahankh)</td>
<td></td>
</tr>
<tr>
<td>Intef III</td>
<td>(Nakhtnebtepnufer)</td>
<td></td>
</tr>
<tr>
<td colspan="2" rowspan="2">Nebhepetra Mentuhotep (II)(unification of Egypt in 2040)</td>
<td>2061-2010</td>
</tr>
<tr>
<td rowspan="13" bgcolor="#CCCCCC">Middle Kingdom</td>
<td rowspan="3"></td>
<td></td>
</tr>
<tr>
<td colspan="2">Seankhkara Mentuhotep (III)</td>
<td>2010-1998</td>
</tr>
<tr>
<td colspan="2">Nebtawyra Mentuhotep (IV)</td>
<td>1998-1990</td>
</tr>
<tr>
<td rowspan="8">Dynasty 12</td>
<td>Amenemhat I</td>
<td>(Sehetepibra)</td>
<td>1990-1961</td>
</tr>
<tr>
<td>Senusret I</td>
<td>(Kheperkara)</td>
<td>1971-1926</td>
</tr>
<tr>
<td>Amenemhat II</td>
<td>(Nubkaura)</td>
<td>1928-1893</td>
</tr>
<tr>
<td>Senusret II</td>
<td>(Khakheperra)</td>
<td>1896-1887</td>
</tr>
<tr>
<td>Senusret III</td>
<td>(Khakaura)</td>
<td>1887-1848</td>
</tr>
<tr>
<td>Amenemhat III</td>
<td>(Nimaatra)</td>
<td>1868-1822</td>
</tr>
<tr>
<td>Amenemhat IV</td>
<td>(Maakherura)</td>
<td>1822-1812</td>
</tr>
<tr>
<td>Neferusobek</td>
<td>(Sobekkara), Queen</td>
<td>1812-1809</td>
</tr>
<tr>
<td>Dynasty 13</td>
<td colspan="2">Approximately 70 kings, among whom:<br />
Wegaf, Amenemhat V-VII, Sobekhotep I-V, Neferhotep I-III, Hor, Khendjer, Mentuemsaf.</td>
<td>1809-1656</td>
</tr>
<tr>
<td>(Dynasty 14)</td>
<td colspan="2">Several kings, probably contemporary with the 13th or the 15th dynasty.</td>
<td></td>
</tr>
<tr>
<td rowspan="6">Second Intermediate Period</td>
<td>Dynasty 15</td>
<td colspan="2">Hyksos kings:<br />
Salitis, Sheshi, Khian, Apep, Khamudi</td>
<td>1640-1532</td>
</tr>
<tr>
<td>Dynasty 16</td>
<td colspan="2">Minor Hyksos rulers, contemporary with the 15th dynasty</td>
<td></td>
</tr>
<tr>
<td rowspan="4">Dynasty 17</td>
<td colspan="2">Theban dynasty in divided Egypt, numerous kings, among whom:<br />
Intef V, Sobekemsaf I and II, and the last three:</td>
<td>1690-1548</td>
</tr>
<tr>
<td>Tao I</td>
<td>(Senakhtenra)</td>
<td></td>
</tr>
<tr>
<td>Tao II</td>
<td>(Seqenenra)</td>
<td></td>
</tr>
<tr>
<td>Kamose</td>
<td>(Wadjkheperra)</td>
<td>1555-1548</td>
</tr>
<tr>
<td rowspan="32" bgcolor="#CCCCCC">New Kingdom</td>
<td rowspan="14">Dynasty 18</td>
<td>Ahmose</td>
<td>(Nebpehtyra)</td>
<td>1548-1523</td>
</tr>
<tr>
<td>Amenhotep I</td>
<td>(Djeserkara)</td>
<td>1523-1502</td>
</tr>
<tr>
<td>Thutmose I</td>
<td>(Aakheperkara)</td>
<td>1502-1492</td>
</tr>
<tr>
<td>Thutmose II</td>
<td>(Aakheperenra)</td>
<td>1492-1479</td>
</tr>
<tr>
<td>Thutmose III</td>
<td>(Menkheperra)</td>
<td>1479-1425</td>
</tr>
<tr>
<td>Hatshepsut</td>
<td>(Maatkara), Queen</td>
<td>1473-1458</td>
</tr>
<tr>
<td>Amenhotep II</td>
<td>(Aakheperura)</td>
<td>1425-1399</td>
</tr>
<tr>
<td>Thutmose IV</td>
<td>(Menkheperura)</td>
<td>1399-1389</td>
</tr>
<tr>
<td>Amenhotep III</td>
<td>(Nebmaatra)</td>
<td>1389-1349</td>
</tr>
<tr>
<td>Amenhotep IV / Akhenaten</td>
<td>(Neferkheperura Waenra)</td>
<td>1349-1332</td>
</tr>
<tr>
<td>Semenkhkara</td>
<td>(Ankh kheperura)</td>
<td>1332-1328</td>
</tr>
<tr>
<td>Tutankhamen</td>
<td>(Nebkheperura)</td>
<td>1328-1319</td>
</tr>
<tr>
<td>Aye</td>
<td>(Kheperkheperura)</td>
<td>1319-1316</td>
</tr>
<tr>
<td>Horemheb</td>
<td>(Djeserkheperura)</td>
<td>1316-1302</td>
</tr>
<tr>
<td rowspan="8">Dynasty 19</td>
<td>Ramesses I</td>
<td>(Menpehtyra)</td>
<td>1302-1301</td>
</tr>
<tr>
<td>Sety I</td>
<td>(Menmaatra)</td>
<td>1301-1290</td>
</tr>
<tr>
<td>Ramesses II</td>
<td>(Usermaatra Setepenra)</td>
<td>1290-1224</td>
</tr>
<tr>
<td>Merenptah</td>
<td>(Baenra Hotephermaat)</td>
<td>1224-1214</td>
</tr>
<tr>
<td>Sety II</td>
<td>(Userkheperura Setepenra)</td>
<td>1214-1208</td>
</tr>
<tr>
<td>Amenmesse</td>
<td>(Menmira)</td>
<td>1208-1206</td>
</tr>
<tr>
<td>Siptah</td>
<td>(Akhenra Setepenra)</td>
<td>1206-1198</td>
</tr>
<tr>
<td>Tauseret</td>
<td>(Sitra Merytamen)</td>
<td>1206-1198</td>
</tr>
<tr>
<td rowspan="10">Dynasty 20</td>
<td>Sethnakhte</td>
<td>(Userkhaura Meryamen)</td>
<td>1198-1195</td>
</tr>
<tr>
<td>Ramesses III</td>
<td>(Usermaatra Meryamen)</td>
<td>1195-1164</td>
</tr>
<tr>
<td>Ramesses IV</td>
<td>(Hekamaatra Setepenamen)</td>
<td>1164-1156</td>
</tr>
<tr>
<td>Ramesses V</td>
<td>(Usermaatra Sekheperenra)</td>
<td>1156-1152</td>
</tr>
<tr>
<td>Ramesses VI</td>
<td>(Nebmaatra Meryamen)</td>
<td>1152-1144</td>
</tr>
<tr>
<td>Ramesses VII</td>
<td>(Usermaatra Setepenra Merytamen)</td>
<td>1144-1137</td>
</tr>
<tr>
<td>Ramesses VIII</td>
<td>(Usermaatra Akhenamen)</td>
<td>1137</td>
</tr>
<tr>
<td>Ramesses IX</td>
<td>(Neferkara Setepenra)</td>
<td>1137-1118</td>
</tr>
<tr>
<td>Ramesses X</td>
<td>(Khepermaatra Setepenra)</td>
<td>1118-1115</td>
</tr>
<tr>
<td>Ramesses XI</td>
<td>(Menmaatra Setepenptah)</td>
<td>1115-1086</td>
</tr>
<tr>
<td rowspan="29">Third Intermediate Period</td>
<td rowspan="7">Dynasty 21(Tanis)</td>
<td>Smendes</td>
<td>(Hedjkheperra Setepenra)</td>
<td>1086-1060</td>
</tr>
<tr>
<td>Amenemnisu</td>
<td>(Neferkara)</td>
<td>1060-1056</td>
</tr>
<tr>
<td>Psusennes I</td>
<td>(Aakheperra Setepenamen)</td>
<td>1056-1010</td>
</tr>
<tr>
<td>Amenemope</td>
<td>(Usermaatra Setepenamen)</td>
<td>1010-1001</td>
</tr>
<tr>
<td>Osorkon I</td>
<td>“the elder” (Aakheperra Setepenra)</td>
<td>1001-995</td>
</tr>
<tr>
<td>Siamun</td>
<td>(Netjerkheperra Setepenamen)</td>
<td>995-976</td>
</tr>
<tr>
<td>Psusennes II</td>
<td>(Titkheperura Setepenra)</td>
<td>976-962</td>
</tr>
<tr>
<td rowspan="9">Dynasty 22(Bubastis)</td>
<td>Shoshenq I</td>
<td>(Hedjkheperra Setepenra)</td>
<td>962-941</td>
</tr>
<tr>
<td>Osorkon I</td>
<td>(Sekhemkehperra Setepenra)</td>
<td>941-906</td>
</tr>
<tr>
<td>Shoshenq IIa-c</td>
<td>(Hekakheperra Setepenra)</td>
<td>906-881</td>
</tr>
<tr>
<td>Takelot I</td>
<td>(Usermaatra Setepenamen)</td>
<td>881-868</td>
</tr>
<tr>
<td>Osorkon II</td>
<td>(Usermaatra Setepenamen)</td>
<td>868-833</td>
</tr>
<tr>
<td>Shoshenq III</td>
<td>(Usermaatra Setepenra/amen)</td>
<td>833-794</td>
</tr>
<tr>
<td>Shoshenq IV</td>
<td></td>
<td>794-781</td>
</tr>
<tr>
<td>Pami</td>
<td>(Usermaatra Setepenra/amen)</td>
<td>781-774</td>
</tr>
<tr>
<td>Shoshenq V</td>
<td>(Aakheperra)</td>
<td>774-736</td>
</tr>
<tr>
<td rowspan="4">Dynasty 23<br />
(Tanis/<br />
Libyan)</td>
<td colspan="2">Kings recognized in Thebes, Hermopolis, Herakleopolis, Leontopolis and Tanis.</td>
<td></td>
</tr>
<tr>
<td>Pedubaste I</td>
<td>(Usermaatra)</td>
<td>828-803</td>
</tr>
<tr>
<td>Osorkon IV</td>
<td>(Usermaatra Setepenamen)</td>
<td>777-749</td>
</tr>
<tr>
<td>Peftjauawybast</td>
<td>(Neferkara)</td>
<td>740-725</td>
</tr>
<tr>
<td rowspan="2">Dynasty 24(Sais)</td>
<td>Tefnakhte</td>
<td>(Shepsesra?)</td>
<td>724-717</td>
</tr>
<tr>
<td>Bocchoris</td>
<td>Bakenrenef (Wahkara)</td>
<td>717-712</td>
</tr>
<tr>
<td rowspan="6">Dynasty 25(Kushite)</td>
<td>Kashta</td>
<td>(Nimaatra)</td>
<td>770-750</td>
</tr>
<tr>
<td>Piankhy / Piye</td>
<td>(Usermaatra)</td>
<td>750-722/21</td>
</tr>
<tr>
<td>Shabaqo</td>
<td>(Neferkara)</td>
<td>722/21-706</td>
</tr>
<tr>
<td>Shabitqo</td>
<td>(Djedkaura)</td>
<td>706-690</td>
</tr>
<tr>
<td>Taharqo</td>
<td>(Khunefertemra)</td>
<td>690-664</td>
</tr>
<tr>
<td>Tanutamani</td>
<td>(Bakara)</td>
<td>664-656</td>
</tr>
<tr>
<td>Dynasty 26</td>
<td>[Necho I]</td>
<td></td>
<td>672-664</td>
</tr>
<tr>
<td rowspan="26" bgcolor="#CCCCCC">Late Period</td>
<td rowspan="6">(Sais)</td>
<td>Psammetichus I</td>
<td>(Wahibra)</td>
<td>664-610</td>
</tr>
<tr>
<td>Necho II</td>
<td>(Wehemibra)</td>
<td>610-595</td>
</tr>
<tr>
<td>Psammetichus II</td>
<td>(Neferibra)</td>
<td>595-589</td>
</tr>
<tr>
<td>Apries</td>
<td>(Haibra)</td>
<td>589-570</td>
</tr>
<tr>
<td>Amasis / Ahmose II</td>
<td>(Khnemibra)</td>
<td>570-526</td>
</tr>
<tr>
<td>Psammetichus III</td>
<td>(Ankhkaenra)</td>
<td>526-525</td>
</tr>
<tr>
<td rowspan="8">Dynasty 27(Persian)</td>
<td>Cambyses</td>
<td></td>
<td>525-522</td>
</tr>
<tr>
<td>Darius I</td>
<td></td>
<td>521-486</td>
</tr>
<tr>
<td>Xerxes I</td>
<td></td>
<td>486-466</td>
</tr>
<tr>
<td>Artaxerxes I</td>
<td></td>
<td>465-424</td>
</tr>
<tr>
<td>Xerxes II</td>
<td></td>
<td>424</td>
</tr>
<tr>
<td>Sogdianos</td>
<td></td>
<td>424</td>
</tr>
<tr>
<td>Darius II</td>
<td></td>
<td>424-404</td>
</tr>
<tr>
<td>Artaxerxes II</td>
<td>Memnon</td>
<td>404-402</td>
</tr>
<tr>
<td>Dynasty 28</td>
<td>Amyrtaios</td>
<td></td>
<td>404-399</td>
</tr>
<tr>
<td rowspan="4">Dynasty 29</td>
<td>Nepherites I</td>
<td>(Baenra Merynetjeru)</td>
<td>399-393</td>
</tr>
<tr>
<td>Psammuthis</td>
<td>(Userra Setepenptah)</td>
<td>393</td>
</tr>
<tr>
<td>Hakoris</td>
<td>(Khnemmaatra)</td>
<td>393-380</td>
</tr>
<tr>
<td>Nepherites II</td>
<td></td>
<td>380</td>
</tr>
<tr>
<td rowspan="3">Dynasty 30</td>
<td>Nectanebo I</td>
<td>(Kheperkara)</td>
<td>380-363</td>
</tr>
<tr>
<td>Teos</td>
<td>(Irmaatenra)</td>
<td>362-361</td>
</tr>
<tr>
<td>Nectanebo II</td>
<td>(Senedjemibra Setepenanhur)</td>
<td>360-343</td>
</tr>
<tr>
<td rowspan="3">Second Persian Period</td>
<td>Artaxerxes III</td>
<td>Ochus</td>
<td>343-338</td>
</tr>
<tr>
<td>Arses</td>
<td></td>
<td>338-336</td>
</tr>
<tr>
<td>Darius III</td>
<td>Codoman</td>
<td>335-332</td>
</tr>
<tr>
<td>(Indigenous ruler)</td>
<td>Khababash</td>
<td>(Senentanen Setepenptah)</td>
<td>337/6-</td>
</tr>
<tr>
<td rowspan="78">Greco-Roman<br />
Period</td>
<td rowspan="3">Macedonian</td>
<td>Alexander III</td>
<td>the Great</td>
<td>332-323</td>
</tr>
<tr>
<td>Philip Arrhidaeus</td>
<td></td>
<td>323-316</td>
</tr>
<tr>
<td>Alexander IV</td>
<td></td>
<td>316-304</td>
</tr>
<tr>
<td rowspan="5">Ptolemaic</td>
<td>Ptolemy I</td>
<td>Soter I</td>
<td>304-284</td>
</tr>
<tr>
<td>Ptolemy II</td>
<td>Philadelphus</td>
<td>285-246</td>
</tr>
<tr>
<td>Ptolemy III</td>
<td>Euergetes I</td>
<td>246-221</td>
</tr>
<tr>
<td>Ptolemy IV</td>
<td>Philopator</td>
<td>221-205</td>
</tr>
<tr>
<td>Ptolemy V</td>
<td>Epiphanes</td>
<td>205-180</td>
</tr>
<tr>
<td>(Indigenous ruler)</td>
<td>Harwennefer</td>
<td></td>
<td>205-199</td>
</tr>
<tr>
<td>(Indigenous ruler)</td>
<td>Ankhwennefer</td>
<td></td>
<td>199-186</td>
</tr>
<tr>
<td rowspan="3"></td>
<td>Ptolemy VI</td>
<td>Philometor</td>
<td>180-164<br />
163-145</td>
</tr>
<tr>
<td>Ptolemy VII</td>
<td>Neos Philopator</td>
<td>145</td>
</tr>
<tr>
<td>Ptolemy VIII</td>
<td>Euergetes II (Physkon)</td>
<td>170-163<br />
145-116</td>
</tr>
<tr>
<td>(Indigenous ruler)</td>
<td>Harsiese</td>
<td></td>
<td>131</td>
</tr>
<tr>
<td rowspan="12"></td>
<td>Cleopatra III<br />
Ptolemy IX</td>
<td>Queen, and<br />
Soter II (Latyros)</td>
<td>116-107</td>
</tr>
<tr>
<td>Cleopatra III<br />
Ptolemy X</td>
<td>Queen, and<br />
Alexander I</td>
<td>107-88</td>
</tr>
<tr>
<td>Ptolemy IX</td>
<td>Soter II</td>
<td>88-81</td>
</tr>
<tr>
<td>Cleopatra</td>
<td>Berenike</td>
<td>81-80</td>
</tr>
<tr>
<td>Ptolemy XI</td>
<td>Alexander II</td>
<td>80</td>
</tr>
<tr>
<td>Ptolemy XII</td>
<td>Neos Dionysos (Auletes)</td>
<td>80-58</td>
</tr>
<tr>
<td>Berenike IV</td>
<td></td>
<td>58-55</td>
</tr>
<tr>
<td>Ptolemy XII</td>
<td>Neos Dionysos (Auletes)</td>
<td>55-51</td>
</tr>
<tr>
<td>Cleopatra VII</td>
<td>Queen</td>
<td>51-30</td>
</tr>
<tr>
<td>Ptolemy XIII</td>
<td></td>
<td>51-47</td>
</tr>
<tr>
<td>Ptolemy XIV</td>
<td></td>
<td>47-44</td>
</tr>
<tr>
<td>Ptolemy XV</td>
<td>Caesarion</td>
<td>44-30</td>
</tr>
<tr>
<td rowspan="51">Roman</td>
<td colspan="2">Augustus</td>
<td>30 BCE – 14 CE</td>
</tr>
<tr>
<td colspan="2">Tiberius</td>
<td>14-37</td>
</tr>
<tr>
<td colspan="2">Gaius (Caligula)</td>
<td>37-41</td>
</tr>
<tr>
<td colspan="2">Claudius</td>
<td>41-54</td>
</tr>
<tr>
<td colspan="2">Nero</td>
<td>54-68</td>
</tr>
<tr>
<td colspan="2">Galba</td>
<td>68-69</td>
</tr>
<tr>
<td colspan="2">Otho</td>
<td>69</td>
</tr>
<tr>
<td colspan="2">Vespasian</td>
<td>69-79</td>
</tr>
<tr>
<td colspan="2">Titus</td>
<td>79-81</td>
</tr>
<tr>
<td colspan="2">Domitian</td>
<td>81-96</td>
</tr>
<tr>
<td colspan="2">Nerva</td>
<td>96-98</td>
</tr>
<tr>
<td colspan="2">Trajan</td>
<td>98-117</td>
</tr>
<tr>
<td colspan="2">Hadrian</td>
<td>117-138</td>
</tr>
<tr>
<td colspan="2">Antoninus Pius</td>
<td>138-161</td>
</tr>
<tr>
<td colspan="2">Marcus Aurelius</td>
<td>161-180</td>
</tr>
<tr>
<td colspan="2">Lucius Verus</td>
<td>161-169</td>
</tr>
<tr>
<td colspan="2">Commodus</td>
<td>180-192</td>
</tr>
<tr>
<td colspan="2">Septimius Severus</td>
<td>193-211</td>
</tr>
<tr>
<td colspan="2">Caracalla</td>
<td>198-217</td>
</tr>
<tr>
<td colspan="2">Geta</td>
<td>209-212</td>
</tr>
<tr>
<td colspan="2">Macrinus</td>
<td>217-218</td>
</tr>
<tr>
<td colspan="2">Elegabalus</td>
<td>218-222</td>
</tr>
<tr>
<td colspan="2">Severus Alexander</td>
<td>222-235</td>
</tr>
<tr>
<td colspan="2">Gordian III</td>
<td>238-244</td>
</tr>
<tr>
<td colspan="2">Philip</td>
<td>244-249</td>
</tr>
<tr>
<td colspan="2">Decius</td>
<td>249-251</td>
</tr>
<tr>
<td colspan="2">Gallus and Volusianus</td>
<td>251-253</td>
</tr>
<tr>
<td colspan="2">Valerian</td>
<td>253-260</td>
</tr>
<tr>
<td colspan="2">Gallienus</td>
<td>253-268</td>
</tr>
<tr>
<td colspan="2">Macrianus and Quietus</td>
<td>260-261</td>
</tr>
<tr>
<td colspan="2">Aurelian</td>
<td>270-275</td>
</tr>
<tr>
<td colspan="2">Probus</td>
<td>276-282</td>
</tr>
<tr>
<td colspan="2">Diocletian</td>
<td>284-305</td>
</tr>
<tr>
<td colspan="2">Maximian</td>
<td>286-305</td>
</tr>
<tr>
<td colspan="2">Galerius</td>
<td>293-311</td>
</tr>
<tr>
<td colspan="2">Constantine I</td>
<td>306-337</td>
</tr>
<tr>
<td colspan="2">Maxentius</td>
<td>306-312</td>
</tr>
<tr>
<td colspan="2">Maximinus Daia</td>
<td>307-324</td>
</tr>
<tr>
<td colspan="2">Licinius</td>
<td>308-324</td>
</tr>
<tr>
<td colspan="2">Constantine II</td>
<td>337-340</td>
</tr>
<tr>
<td colspan="2">Constans</td>
<td>337-350</td>
</tr>
<tr>
<td colspan="2">Constantius II</td>
<td>337-361</td>
</tr>
<tr>
<td colspan="2">Magnetius</td>
<td>350-353</td>
</tr>
<tr>
<td colspan="2">Julian the Apostate</td>
<td>361-363</td>
</tr>
<tr>
<td colspan="2">Jovian</td>
<td>363-364</td>
</tr>
<tr>
<td colspan="2">Valentinian I</td>
<td>364-375</td>
</tr>
<tr>
<td colspan="2">Valens</td>
<td>364-378</td>
</tr>
<tr>
<td colspan="2">Gratian</td>
<td>375-383</td>
</tr>
<tr>
<td colspan="2">Theodosius the Great</td>
<td>379-395</td>
</tr>
<tr>
<td colspan="2">Valentinian II</td>
<td>383-392</td>
</tr>
<tr>
<td colspan="2">Eugenius</td>
<td>392-394</td>
</tr>
<tr>
<td colspan="3">Division of the Roman Empire<br />
Late Roman or Byzantine Period</td>
<td>395</td>
</tr>
</tbody>
</table>

				
			</div>

			
		</article>
