	<div id='welcome-column'>
	<div id='welcome-div' >
		<p id='welcome-message' >Welcome</p>
		<p id='welcome-close' >to the UCLA Encylopedia of Egyptology</p>
	</div>
	<div id='subject-browse' >
		<h3><a href="/subjectbrowse/">Subject Browse</a></h3>
		<ul id='subject-list' >
			<?php
				$categories = array("natural environment","individual and society","geography","language, text and writing","time and history","material culture, art and architecture","persons","domains of knowledge","religion","egyptology");
				foreach ($categories as $category)
				{
					$href = SITE_PATH."/subjectbrowse/".urlencode($category);
					$src = SITE_PATH."/graphics/home/welcome-".str_replace(" ","_",str_replace(",","",$category)).".jpg";
					echo "<li><a href=\"{$href}\">\n
						<img src='{$src}' alt='' /></a>\n
						<a href=\"{$href}\">".ucwords($category)."</a></li>";
				}
			?>		
		</ul>
		<div class='clearfix'></div>
	</div>
	</div>
	
	<div id='timemap' >
		<h3><a href="<?php echo SITE_PATH; ?>/timemap/">Time map</a></h3>
		<a href="/timemap/" title="view a map of places and their related content">
			<img id='timemap-image' src='<?php echo SITE_PATH; ?>/graphics/home/timemap.jpg' alt='' /><br />
		</a>
		<form action="<?php echo SITE_PATH; ?>/search/advanced" method="POST" >
			<input type='image' src='/graphics/Advanced_Search.png' alt='Advanced Search' />
		</form>
		<form action="/articles/random/" method="POST" >
			<input type='image' src='/graphics/Surprise_Me.png' alt='Surprise Me!' /><br />
		</form>
	</div>
		
	<div style='clear:both;'></div>
