
        <h2>Welcome</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/" title="10:02 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-29T22:02:53+00:00" pubdate>August 29, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

        <div class="entry">

            <div id="welcome-column">
                <h2>Browse by Subject</h2>
                <div id="subject-browse">
                    <ul id="subject-list">
                        <li><a href="/subjectbrowse/natural+environment"><img src="/graphics/home/welcome-natural_environment.jpg" alt="" width="117" height="55" />Natural Environment</a></li>
                        <li><a href="/subjectbrowse/individual+and+society"><img src="/graphics/home/welcome-individual_and_society.jpg" alt="" />Individual And Society</a></li>
                        <li><a href="/subjectbrowse/geography"><img src="/graphics/home/welcome-geography.jpg" alt="" />Geography</a></li>
                        <li><a href="/subjectbrowse/language%2C+text+and+writing"><img src="/graphics/home/welcome-language_text_and_writing.jpg" alt="" />Language, Text And Writing</a></li>
                        <li><a href="/subjectbrowse/time+and+history"><img src="/graphics/home/welcome-time_and_history.jpg" alt="" />Time And History</a></li>
                        <li><a href="/subjectbrowse/material+culture%2C+art+and+architecture"><img src="/graphics/home/welcome-material_culture_art_and_architecture.jpg" alt="" />Material Culture, Art And Architecture</a></li>
                        <li><a href="/subjectbrowse/persons"><img class="inactive" src="/graphics/home//welcome-persons.jpg" alt="" />Persons</a></li>
                        <li><a href="/subjectbrowse/domains+of+knowledge"><img class="inactive" src="/graphics/home//welcome-domains_of_knowledge.jpg" alt="" />Domains Of Knowledge</a></li>
                        <li><a href="/subjectbrowse/religion"><img src="/graphics/home/welcome-religion.jpg" alt="" />Religion</a></li>
                        <li><a href="/subjectbrowse/egyptology"><img class="inactive" src="/graphics/home/welcome-egyptology.jpg" alt="" />Egyptology</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div id="welcome-column">
                <h2>Explore the Time Map</h2>
                <div align="center"><a href="/timemap"><img src="/graphics/home/timemap_sm.png" alt="" /></a></div>
                <div id="welcome-div">
                    <p align="center">The Time Map allows users to explore the UEE through geographical and chronological coordinates.</p>
                    <p align="center">
                </div>
            </div>
            <div id="welcome-column">
                <h2>About the UEE</h2>
                <div id="welcome-div">
                    <p>The UCLA Encyclopedia of Egyptology is an international cooperative project to provide high quality peer reviewed information on ancient Egypt. It is a resource in development and will grow steadily.</p>
                    <ul>
                        <li><a title="Editors and Staff" href="/editors/">Who Writes the UEE?</a></li>
                        <li><a title="Submission Style Guide" href="/style/">Information for Authors</a></li>
                        <li><a title="Tutorial" href="/tutorial/">Tutorial</a></li>
                        <li><a title="Citing the UEE" href="/citing/">Citing the UEE</a></li>
                    </ul>
                <div>
                    <div class="donate">
                        <a title="Donate to UEE" href="https://giving.ucla.edu/EncyclopediaOfEgyptology" target="_blank">
                            <img style="float: left; padding: 4%; border-right: #ccc solid 1px;" src="/graphics/home/donating_btn.png" alt="donating to the uee" />
                        </a>
                    </div>
                    <div class="showme"><img style="padding: 4%;" src="/graphics/home/showme_btn.png" alt="show me an article" /></div>
                </div>
            </div>
            </div>
<div class="clearfix"></div>

				
			</div>

			

