<?php
	if (substr_count($_SERVER['REQUEST_URI'],'/articles/') ) :
?>
<a href="/articles/<?php echo $this->articletitle;?>/">back to <?php echo str_replace("_"," ",$this->articletitle); ?></a><br />
<?php
	endif;
?>
<a id='thumbsonly' href="javascript:;">Thumbnails only</a> | <a id='theatre' href="javascript:;">Show Featured Image</a>
<div id='imageresults'>
	<h3>image results for &quot;<?php echo $this->place_name; ?>&quot;</h3>
	<span id='imagecount'><?php echo count($this->images);?> images found</span>
	<div id='thumbs'>
		<?php $this->displayImages(); ?>
	</div>
</div>
<div id='toprow'>
	<div id='metadata'>
	</div>
	<div id='featured'>
		<span class='wrapper'>
			<img src='missing.gif' alt='loading...' />
		</span>
		<a href='#enlarge-anchor' >enlarge</a>
	</div>
</div>
<script type='text/javascript' src='/js/imageresults2.js'></script>
<div id="enlarged-image-wrapper">
	<div id="enlarged-image">
		<a id="enlarge-anchor" name="enlarge-anchor"></a>
		<a href="javascript:;">[x] close</a><br />
		<span class='caption'>Image caption goes here</span><br />
		<img src="missing.gif" alt="loading" title=""/>
	</div>
</div>
