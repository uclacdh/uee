		<article class="post" id="post-39">

			<h2>Preferred Spelling</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/spelling/" title="6:34 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T18:34:32+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<p>The UEE uses American spelling (see the <a href="/style">UEE style sheet</a>). For Egyptian terms and names (ancient and modern) the preferred spelling is listed here. The most common alternatives are indicated, but only the preferred spelling will be used in the articles. This list is in development and will be gradually expanded. For key terms and names contributors are requested to include, in addition to the English spelling, the hieroglyphic spelling (including variants), and transcription.</p>
<p><a href="#A">A</a>&nbsp;&nbsp;<a href="#B">B</a>
&nbsp;&nbsp;<a href="#C">C</a>
&nbsp;&nbsp;<a href="#D">D</a>
&nbsp;&nbsp;<a href="#E">E</a>
&nbsp;&nbsp;<a href="#F">F</a>
&nbsp;&nbsp;<a href="#G">G</a>
&nbsp;&nbsp;<a href="#H">H</a>
&nbsp;&nbsp;<a href="#I">I</a>
&nbsp;&nbsp;<a href="#J">J</a>
&nbsp;&nbsp;<a href="#K">K</a>
&nbsp;&nbsp;<a href="#L">L</a>
&nbsp;&nbsp;<a href="#M">M</a>
&nbsp;&nbsp;<a href="#N">N</a>
&nbsp;&nbsp;<a href="#O">O</a>
&nbsp;&nbsp;<a href="#P">P</a>
&nbsp;&nbsp;<a href="#Q">Q</a>
&nbsp;&nbsp;<a href="#R">R</a>
&nbsp;&nbsp;<a href="#S">S</a>
&nbsp;&nbsp;<a href="#T">T</a>
&nbsp;&nbsp;<a href="#U">U</a>
&nbsp;&nbsp;<a href="#V">V</a>
&nbsp;&nbsp;<a href="#W">W</a>
&nbsp;&nbsp;<a href="#X">X</a>
&nbsp;&nbsp;<a href="#Y">Y</a>
&nbsp;&nbsp;<a href="#Z">Z</a></p>
<table class="tbl_style" border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="113"><strong><a href="a">A</a><a name="a"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">el-Abadiya</td>
<td width="126">Abadiya</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abbasiya</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abu Ghurab</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abu Rawash</td>
<td width="126">Abu Rowash</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abu Simbel</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abqiq</td>
<td width="126">Abgig</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abusir</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abusir el-Melek</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Abuqir</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Kanopos</td>
</tr>
<tr>
<td width="113">Abydos</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Adaima</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Aha</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ahmose</td>
<td width="126">‘Ahmose</td>
<td width="54">Person</td>
<td width="150">Nebpehtyra</td>
</tr>
<tr>
<td width="113">Ahmose-Aametju</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ahmose-Sapair</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ahmose-Turo</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Akhenra Setepenra</td>
<td width="126">Akhenre‘ setepenre‘</td>
<td width="54">Person</td>
<td width="150">Siptah</td>
</tr>
<tr>
<td width="113">Aakheperra Setepenamen</td>
<td width="126">‘Akheperre‘ setepenamun</td>
<td width="54">Person</td>
<td width="150">Psusennes I</td>
</tr>
<tr>
<td width="113">Aakheperra Setepenra</td>
<td width="126">‘Akheperre‘ setepenre‘</td>
<td width="54">Person</td>
<td width="150">Osorkon I “the elder”</td>
</tr>
<tr>
<td width="113">Aakheperkara</td>
<td width="126">‘Akheperkare‘</td>
<td width="54">Person</td>
<td width="150">Thutmose I</td>
</tr>
<tr>
<td width="113">Aakheperenra</td>
<td width="126">‘Akheperenre‘</td>
<td width="54">Person</td>
<td width="150">Thutmose II</td>
</tr>
<tr>
<td width="113">Akhetaten</td>
<td width="126">Akhetaton</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Aakheperra</td>
<td width="126">‘Akheperre‘</td>
<td width="54">Person</td>
<td width="150">Shoshenq V</td>
</tr>
<tr>
<td width="113">Aakheperra Setepenamen</td>
<td width="126">‘Akheperre‘ setepenamun</td>
<td width="54">Person</td>
<td width="150">Osorkon V</td>
</tr>
<tr>
<td width="113">Aakheperura</td>
<td width="126">‘Akheperure‘</td>
<td width="54">Person</td>
<td width="150">Amenhotep II</td>
</tr>
<tr>
<td width="113">Aghurmi</td>
<td width="126">el-Aghurmi</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Agilkia</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Aia</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ain Amur</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ain Manawir</td>
<td width="126">Ayn Manawir</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Aker</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Akhenaten</td>
<td width="126">Akhenaton<br />
Ikhenaten<br />
Ikhenaton<br />
Echnaton<br />
Achnaton</td>
<td width="54">Person</td>
<td width="150">Amenhotep IV<br />
Neferkheperura Waenra</td>
</tr>
<tr>
<td width="113">Akhet-hotep</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Akhmenu</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Akhmim</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Akoris</td>
<td width="126">Hakor, Hakoris</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Alexander</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Amada</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Amarah (Stela)</td>
<td width="126"></td>
<td width="54">Object</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Amarna</td>
<td width="126"></td>
<td width="54">Term</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Amarna</td>
<td width="126">el-‘Amarna, Tell el-Amarna</td>
<td width="54">Place</td>
<td width="150">Akhetaten</td>
</tr>
<tr>
<td width="113">Amasis</td>
<td width="126">Ahmose II</td>
<td width="54">Person</td>
<td width="150">Khnemibra</td>
</tr>
<tr>
<td width="113">Amenemhat</td>
<td width="126">Amenemhet</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Amenemnisu</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Neferkara</td>
</tr>
<tr>
<td width="113">Amenemope</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Usermaatra Setepenamen</td>
</tr>
<tr>
<td width="113">Amenhotep</td>
<td width="126">Amenophis</td>
<td width="54">Person</td>
<td width="150">Djeserkara (I)<br />
Aakheperura (II)<br />
Nebmaatra (III)<br />
Neferkheperura Waenra (IV)<br />
Akhenaten (IV)</td>
</tr>
<tr>
<td width="113">Amenmesse</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Menmira</td>
</tr>
<tr>
<td width="113">Amheida</td>
<td width="126">Amhada</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Amra</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Amunherkhepshef</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Amyrtaios</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">An-Her</td>
<td width="126">Anhur, Han-Her</td>
<td width="54">God</td>
<td width="150">Arensnuphis, Onuris</td>
</tr>
<tr>
<td width="113">Anedjib</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Aniba</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ankhtifi</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ankhu</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ankhwennefer</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Antinoopolis</td>
<td width="126">Antinopolis</td>
<td width="54">Place</td>
<td width="150">Establ Antar, Speos Artemidos</td>
</tr>
<tr>
<td width="113">Anuket</td>
<td width="126">Anuqet, Anukis</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Apep</td>
<td width="126">Apophis, Apepi, Apopi</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Apries</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Haibra</td>
</tr>
<tr>
<td width="113">Arensnuphis</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150">Onuris, An-Her</td>
</tr>
<tr>
<td width="113">Argo</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Arikeamenote</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Armant</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Hermonthis</td>
</tr>
<tr>
<td width="113">Arses</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Arsinoe</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Arsinoe (1)</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Cleopatris, Suez</td>
</tr>
<tr>
<td width="113">Arsinoe (2)</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Crocodilopolis, Medinet el-Fayum, Kiman Faris, Shehdet</td>
</tr>
<tr>
<td width="113">Artaxerxes</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Asasif</td>
<td width="126">‘Asasif</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Ashmunein</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Assiut</td>
<td width="126">Asyut</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Aswan</td>
<td width="126">Assuan, Assouan</td>
<td width="54">Place</td>
<td width="150">Syene</td>
</tr>
<tr>
<td width="113">Athribis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ausim</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Letopolis, Khem</td>
</tr>
<tr>
<td width="113">Awibra</td>
<td width="126">Awibre‘</td>
<td width="54">Person</td>
<td width="150">Hor</td>
</tr>
<tr>
<td width="113">Aye</td>
<td width="126">Aya, Eje</td>
<td width="54">Person</td>
<td width="150">Kheperkheperura</td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style" width="100%">
<tbody>
<tr>
<td width="113"><strong>B<a id="B" name="B"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred Spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">ba</td>
<td width="126"></td>
<td width="54">Term</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Bacchias</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Kom el-Atl</td>
</tr>
<tr>
<td width="113">Badari</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Baenra Merynetjeru</td>
<td width="126">Baenre‘ merynetjeru</td>
<td width="54">Person</td>
<td width="150">Nepherites I</td>
</tr>
<tr>
<td width="113">Baenra Hotephermaat</td>
<td width="126">Baenre‘ hotephirma‘at</td>
<td width="54">Person</td>
<td width="150">Merenptah</td>
</tr>
<tr>
<td width="113">Bahariya oasis</td>
<td width="126">Bahriya</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Bakara</td>
<td width="126">Bakare‘</td>
<td width="54">Person</td>
<td width="150">Tanutamani</td>
</tr>
<tr>
<td width="113">Bakenrenef</td>
<td width="126">Bocchoris</td>
<td width="54">Person</td>
<td width="150">Wahkara</td>
</tr>
<tr>
<td width="113">Balat</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ballana</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Batn el-Haggar</td>
<td width="126">Batn el-Hagar</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Batn Ihrit</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Theadelphia</td>
</tr>
<tr>
<td width="113">Bebi</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Behbet el-Haggar</td>
<td width="126">Behbeit el-Hagar</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Behdet</td>
<td width="126">Behedet</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Beni Hassan</td>
<td width="126">Beni Hasan</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Beni Suef Region</td>
<td width="126">Beni Sueif, Beni Sueef</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Beqenu</td>
<td width="126">Baqan</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Berenike</td>
<td width="126">Berenice</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Berenike</td>
<td width="126">Berenice</td>
<td width="54">Place</td>
<td width="150">Medinet el-Haras</td>
</tr>
<tr>
<td width="113">Beyt Khallaf</td>
<td width="126">Beit Khallaf</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Beyt el-Wali</td>
<td width="126">Beit el-Waly</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Biga</td>
<td width="126">Bigga</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Birket Habu</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Bubastis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Tell Basta</td>
</tr>
<tr>
<td width="113">Buchis</td>
<td width="126">Bakh</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Byahmu</td>
<td width="126">Biyahmu</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style" width="100%">
<tbody>
<tr>
<td width="113"><strong>C<a id="C" name="C"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113">Preferred spelling</td>
<td width="126">Alternative spelling</td>
<td width="54">Category</td>
<td width="150">Equivalent</td>
</tr>
<tr>
<td width="113">Cleopatra</td>
<td width="126">Kleopatra</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Coptos</td>
<td width="126">Koptos</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113"></td>
<td width="126"></td>
<td width="54"></td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style" width="100%">
<tbody>
<tr>
<td width="113"><strong>D<a id="D" name="D"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Dabud</td>
<td width="126">Dabod</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dahshur</td>
<td width="126">Dashur</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dakhla oasis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Dakka</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dangeil</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Darius</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Debeheni</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dedumose</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dedusobek</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Den</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dendara</td>
<td width="126">Denderah</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deir el-Bahri</td>
<td width="126">Deir el-Bahari</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deir el-Ballas</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deir el-Bersha</td>
<td width="126">Deir el-Bersheh</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deir el-Gabrawi</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deir el-Medina</td>
<td width="126">Deir el-Medineh</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deir el-Shelwit</td>
<td width="126">Deir el-Shalwit</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dendur</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Denen</td>
<td width="126">Danuna</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Derr</td>
<td width="126">Derr</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Deshasha</td>
<td width="126">Dishasha</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dimai</td>
<td width="126">Dime, Dimai el-Sebakh</td>
<td width="54">Place</td>
<td width="150">Soknopaiou Nesos</td>
</tr>
<tr>
<td width="113">Diocletian</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dionysias</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Qasr Qarun</td>
</tr>
<tr>
<td width="113">Djehutihotep</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Djedkara</td>
<td width="126">Djedkare‘</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Djedkaura</td>
<td width="126">Djedkaure‘</td>
<td width="54">Person</td>
<td width="150">Shebitqo</td>
</tr>
<tr>
<td width="113">Djer</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Djeserkara</td>
<td width="126">Djeserkare‘</td>
<td width="54">Person</td>
<td width="150">Amenhotep I</td>
</tr>
<tr>
<td width="113">Djeserkheperura</td>
<td width="126">Djeserkheprure‘</td>
<td width="54">Person</td>
<td width="150">Horemheb</td>
</tr>
<tr>
<td width="113">Dra Abu el-Naga</td>
<td width="126">Dra‘ Abu el-Naga‘</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Dush</td>
<td width="126">Qasr Dush</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style" width="100%">
<tbody>
<tr>
<td width="113"><strong>E<a id="E" name="E"></a></strong></td>
<td width="124"><strong> </strong></td>
<td width="56"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="124"><strong>Alternative spelling</strong></td>
<td width="56"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Edfu</td>
<td width="124"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Elephantine</td>
<td width="124"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Elkab</td>
<td width="124">el-Kab</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Esna</td>
<td width="124"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Esnun</td>
<td width="124"></td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Establ Antar</td>
<td width="124">Istabl Antar, Estable &#8216;Antar</td>
<td width="56">Place</td>
<td width="150">Speos Artemidos, Antinoopolis</td>
</tr>
<tr>
<td width="113">Euhemeria</td>
<td width="124"></td>
<td width="56">Place</td>
<td width="150">Qasr el-Banat</td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style" width="100%">
<tbody>
<tr>
<td width="113"><strong>F<a id="F" name="F"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Farafra</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Fayum</td>
<td width="126">el-Fayum, Faiyum, Fayyum, Fayoum</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style" width="100%">
<tbody>
<tr>
<td width="113"><strong>G<a id="G" name="G"></a></strong></td>
<td width="139"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="139"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Gebel</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel el-Asr</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150">&#8220;Chephren&#8217;s Quarry&#8221;</td>
</tr>
<tr>
<td width="113">Gebel el-Ahmar</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel Hafafit</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel el-Mawta</td>
<td width="139">Gebel el-Mawata, Gabal el-Mawta</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel Migif</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel el-Silsila</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel el-Teir</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel Uweinat</td>
<td width="139">Gabal &#8216;Owainat</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebel Zabara</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gebelein</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gerf Hussein</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Gerra</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150">Mahammediya</td>
</tr>
<tr>
<td width="113">Gerza</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Giza</td>
<td width="139"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>H<a id="H" name="H"></a></strong></td>
<td width="127"><strong> </strong></td>
<td width="53"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="127"><strong>Alternative spelling</strong></td>
<td width="53"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Hadrian</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Haibra</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150">Apries</td>
</tr>
<tr>
<td width="113">Hakoris</td>
<td width="127">Hakor, Akoris</td>
<td width="53">Person</td>
<td width="150">Khemmaatra</td>
</tr>
<tr>
<td width="113">el-Halfaya Bahari</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Halfaya Qibli</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Hammam</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hammamiya</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hapi</td>
<td width="127">Hapy</td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Haraga</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Harendotes</td>
<td width="127"></td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Harnedjheriotef</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150">Hetepibra</td>
</tr>
<tr>
<td width="113">Harpara</td>
<td width="127"></td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hathor</td>
<td width="127"></td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hatnub</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hatshepsut</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150">Maatkara</td>
</tr>
<tr>
<td width="113">Hawara</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Hawawish</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Hayz</td>
<td width="127">el-Haiyz</td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hecataeus</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Heliopolis</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150">Iunu</td>
</tr>
<tr>
<td width="113">Helwan</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hekakheperra Setepenra</td>
<td width="127">Heqakheperre‘ setepenre‘</td>
<td width="53">Person</td>
<td width="150">Shoshenq II</td>
</tr>
<tr>
<td width="113">Hekamaatra Setepenamen</td>
<td width="127">Heqama‘atre‘ setepenamun</td>
<td width="53">Person</td>
<td width="150">Ramesses IV</td>
</tr>
<tr>
<td width="113">Hekanakht</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Henuy</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Heqaib</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Herakleion</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Herishef</td>
<td width="127">Harsaphes</td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hermonthis</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150">Armant</td>
</tr>
<tr>
<td width="113">Henqu</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hesu</td>
<td width="127">Hasa</td>
<td width="53">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hetepibra</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150">Harnedjheriotef</td>
</tr>
<tr>
<td width="113">Hetepsekhemwy</td>
<td width="127">Hotepsekhemuy</td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hedjkheperra Setepenra</td>
<td width="127">Hedjkheperre‘ setepenre‘</td>
<td width="53">Person</td>
<td width="150">Smendes<br />
Shoshenq I<br />
Takelot II</td>
</tr>
<tr>
<td width="113">Hierakon</td>
<td width="127"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Hiba</td>
<td width="127">el-Heba, el-Hibeh, Hibeh</td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hiw</td>
<td width="127">Hiu</td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Hor</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150">Awibra</td>
</tr>
<tr>
<td width="113">Horemheb</td>
<td width="127">Haremhab</td>
<td width="53">Person</td>
<td width="150">Djeserkheperura</td>
</tr>
<tr>
<td width="113">Horus</td>
<td width="127"></td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Huni</td>
<td width="127"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>I<a id="I" name="I"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Ibi</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ihnasya el-Medina</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Imhotep</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Intef</td>
<td width="126">Inyotef</td>
<td width="54">Person</td>
<td width="150">Sehertawy (I)<br />
Wahankh (II)<br />
Nakhtnebtepnefer (III)Nebkheperra (V)</td>
</tr>
<tr>
<td width="113">Ipet-resyt</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Iqer</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Irmaatenra</td>
<td width="126">Irma‘atenre‘</td>
<td width="54">Person</td>
<td width="150">Teos</td>
</tr>
<tr>
<td width="113">Isbetu</td>
<td width="126">Asbata</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ismailiya</td>
<td width="126">Ismalia</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Itytawy</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Iunu</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Heliopolis<br />
el-Matariya</td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>J<a id="J" name="J"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">&#8211;</td>
<td width="126"></td>
<td width="54"></td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>K<a id="K" name="K"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">ka</td>
<td width="126"></td>
<td width="54">Term</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Elkab</td>
<td width="126">el-Kab</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kalabsha</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kamose</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Wadjkheperra</td>
</tr>
<tr>
<td width="113">Kamutef</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kanopos</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Abuqir</td>
</tr>
<tr>
<td width="113">Karanis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Kom Aushim</td>
</tr>
<tr>
<td width="113">Karanog</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Karnak</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kashta</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Nimaatra</td>
</tr>
<tr>
<td width="113">Kellis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kematef</td>
<td width="126"></td>
<td width="54">Deity</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kerkeosiris</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">el-Kharag el-Sultani?</td>
</tr>
<tr>
<td width="113">Khaankhra</td>
<td width="126">Kha‘ankhre‘</td>
<td width="54">Person</td>
<td width="150">Sobekhetep I</td>
</tr>
<tr>
<td width="113">Khaba</td>
<td width="126">Kha‘ba</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khababash</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Senentanen Setepenptah</td>
</tr>
<tr>
<td width="113">Khaemwaset</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khafra</td>
<td width="126">Chephren, Khephren, Re‘kha‘ef</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Kharag el-Sultani</td>
<td width="126">el-Charag el-Sultani</td>
<td width="54">Place</td>
<td width="150">Kerkeosiris?</td>
</tr>
<tr>
<td width="113">Kharga oasis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khasekhem</td>
<td width="126">Kha‘sekhem</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khasekhemwy</td>
<td width="126">Kha‘sekhemuy</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khebeny</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khem</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Letopolis<br />
Ausim</td>
</tr>
<tr>
<td width="113">Khemmaatra</td>
<td width="126">Khemma‘atre‘</td>
<td width="54">Person</td>
<td width="150">Hakor</td>
</tr>
<tr>
<td width="113">Kheperkara</td>
<td width="126">Kheperkare‘</td>
<td width="54">Person</td>
<td width="150">Nectanebo I</td>
</tr>
<tr>
<td width="113">Kheperkheperura</td>
<td width="126">Kheperkheperure‘</td>
<td width="54">Person</td>
<td width="150">Aye</td>
</tr>
<tr>
<td width="113">Khepermaatra Setepenra</td>
<td width="126">Kheperma‘atre‘ Setepenre‘</td>
<td width="54">Person</td>
<td width="150">Ramesses X</td>
</tr>
<tr>
<td width="113">Khety</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khian</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khnemibra</td>
<td width="126">Khnemibre‘</td>
<td width="54">Person</td>
<td width="150">Amasis<br />
Ahmose II</td>
</tr>
<tr>
<td width="113">Khnum</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khons</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khufu</td>
<td width="126">Cheops</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khunefertemra</td>
<td width="126">Khure nefertem</td>
<td width="54">Taharqo</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Khutawyra</td>
<td width="126">Khutawyre‘</td>
<td width="54">Person</td>
<td width="150">Wegaf</td>
</tr>
<tr>
<td width="113">el-Khokha</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kom Abu Billo</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Terenuthis</td>
</tr>
<tr>
<td width="113">Kom el-Ahmar</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kom el-Atl</td>
<td width="126">Kom el-Asl</td>
<td width="54">Place</td>
<td width="150">Bacchias</td>
</tr>
<tr>
<td width="113">Kom Aushim</td>
<td width="126">Kom Oshim</td>
<td width="54">Place</td>
<td width="150">Karanis</td>
</tr>
<tr>
<td width="113">Kom el-Kharaba el-Kebir</td>
<td width="126">Kom el-Charaba el-Kebir</td>
<td width="54">Place</td>
<td width="150">Philadelphia</td>
</tr>
<tr>
<td width="113">Kom el-Kuffar</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kom el-Mumenein</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kom Medinet Ghurab</td>
<td width="126">Gurob</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kom Ombo</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kom Ruqaya</td>
<td width="126">Kom Ruqaiya</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Kor</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Krokodilopolis</td>
<td width="126">Crocodilopolis</td>
<td width="54">Place</td>
<td width="150">ShedetArsinoe<br />
Kiman Faris<br />
Medinet el-Fayum</td>
</tr>
<tr>
<td width="113">Kurkur oasis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>L<a name="L"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preffered spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">el-Lahun</td>
<td width="126">Kahun</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Lake Moeris</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Latopolis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Esna</td>
</tr>
<tr>
<td width="113">Letopolis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Khem<br />
Ausim</td>
</tr>
<tr>
<td width="113">el-Lisht</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Libu</td>
<td width="126">Labu</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Luxor</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>M<a name="M"></a></strong></td>
<td width="152"><strong> </strong></td>
<td width="57"><strong> </strong></td>
<td width="149"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="152"><strong>Alternative spelling</strong></td>
<td width="57"><strong>Category</strong></td>
<td width="149"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Maadi</td>
<td width="152">el-Ma&#8217;adi</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Maakherura</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149">Amenemhat IV</td>
</tr>
<tr>
<td width="113">Maat</td>
<td width="152">Ma‘at</td>
<td width="57">God</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">maat</td>
<td width="152">ma‘at</td>
<td width="57">Term</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Maatkara</td>
<td width="152">Ma‘atkare‘</td>
<td width="57">Person</td>
<td width="149">Hatshepsut</td>
</tr>
<tr>
<td width="113">Magdola</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Medinet Nahas</td>
</tr>
<tr>
<td width="113">el-Mahamid Qibli</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mahammediya</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Gerra</td>
</tr>
<tr>
<td width="113">el-Mahasna</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Maidum</td>
<td width="152">Maydum, Meidum</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Malqata</td>
<td width="152">el-Malqata</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mandulis</td>
<td width="152"></td>
<td width="57">God</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Marina el-Alamein</td>
<td width="152">Marina el-Al&#8217;amein</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Marsa Gawasis</td>
<td width="152">Mersa Gawasis</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">el-Matariya</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Heliopolis<br />
Iunu</td>
</tr>
<tr>
<td width="113">Matmar</td>
<td width="152">el-Matmar</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">May</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Medamud</td>
<td width="152">Naga el-Medamud, Nag&#8217; el-Medamud</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Medinet el-Fayum</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Krokodilopolis<br />
Kiman Faris</td>
</tr>
<tr>
<td width="113">Medinet Habu</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Medinet el-Haras</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Berenike</td>
</tr>
<tr>
<td width="113">Medinet Madi</td>
<td width="152">Medinet Maadi</td>
<td width="57">Place</td>
<td width="149">Narmuthis</td>
</tr>
<tr>
<td width="113">Medinet Nahas</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Magdola</td>
</tr>
<tr>
<td width="113">Medinet Watfa</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Philoteris</td>
</tr>
<tr>
<td width="113">Meir</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Meket-Ra</td>
<td width="152">Meketre</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Memphis</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Menes</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Menkaura</td>
<td width="152">Mycerinus, Mykerinos</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Menkauhor</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Menkheperra</td>
<td width="152">Menkheperre‘</td>
<td width="57">Person</td>
<td width="149">Thutmose III</td>
</tr>
<tr>
<td width="113">Menkheperura</td>
<td width="152">Menkheprure‘</td>
<td width="57">Person</td>
<td width="149">Thutmose IV</td>
</tr>
<tr>
<td width="113">Menmaatra</td>
<td width="152">Menma‘atre‘</td>
<td width="57">Person</td>
<td width="149">Sety I</td>
</tr>
<tr>
<td width="113">Menmaatra Setepenptah</td>
<td width="152">Menma‘atre‘ Setepenptah</td>
<td width="57"></td>
<td width="149">Ramesses XI</td>
</tr>
<tr>
<td width="113">Menmira</td>
<td width="152">Menmire‘</td>
<td width="57">Person</td>
<td width="149">Amenmesse</td>
</tr>
<tr>
<td width="113">Menpehtyra</td>
<td width="152">Menpehtire‘</td>
<td width="57">Person</td>
<td width="149">Ramesses I</td>
</tr>
<tr>
<td width="113">Mentuemhat</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mentuemsaf</td>
<td width="152">Mentuemzaf</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mentuhotep</td>
<td width="152">Mentuhetep, Mentuhotpe</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Menuthis</td>
<td width="152">Menouthis</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Merenra</td>
<td width="152">Merenre‘</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Merenra Nemtyemsaf</td>
<td width="152">Merenre‘ Nemtyemzaf</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mereruka</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Meroe</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Merykara</td>
<td width="152">Merykare‘</td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Meryra</td>
<td width="152">Meryre‘</td>
<td width="57">Person</td>
<td width="149">Pepy I</td>
</tr>
<tr>
<td width="113">Merenptah</td>
<td width="152">Merneptah</td>
<td width="57">Person</td>
<td width="149">Baenra Hotephermaat</td>
</tr>
<tr>
<td width="113">Merimde Beni-Salame</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Meshwesh</td>
<td width="152"></td>
<td width="57">Person/Tribe</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Metjetji</td>
<td width="152"></td>
<td width="57">Person</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Minshat Abu Omar</td>
<td width="152">Minshat Abu ‘Omar</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Minya</td>
<td width="152">Miniya</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mirgissa</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149">Shalfak</td>
</tr>
<tr>
<td width="113">Mit Rahina</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">el-Moalla</td>
<td width="152">el-Mo‘alla</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Montu</td>
<td width="152">Monthu</td>
<td width="57">God</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Muqattam</td>
<td width="152">Mokattam</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Myos Hormos</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mons Claudianus</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mons Porphyrites</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mons Smaragdus</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mostagedda</td>
<td width="152">An Nazlah el-Mustajiddah</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Muharraqa</td>
<td width="152">Maharraqa</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Musawwarat el-Sufra</td>
<td width="152">Musawwaret el-Sufara</td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
<tr>
<td width="113">Mut el-Kharab</td>
<td width="152"></td>
<td width="57">Place</td>
<td width="149"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>N<a name="N"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Nadura</td>
<td width="126">Kom el-Nadura</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Naga el-Deir</td>
<td width="126">Nag-ed-Der, Nag&#8217; el-Deir</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nakht</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nakhtnebtepnefer</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Intef III</td>
</tr>
<tr>
<td width="113">Napata</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Naqa</td>
<td width="126">Naq&#8217;a</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Naqada</td>
<td width="126">Nagadah</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Narmuthis</td>
<td width="126">Narmouthis</td>
<td width="54">Place</td>
<td width="150">Medinet Madi</td>
</tr>
<tr>
<td width="113">Naukratis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nebkheperura</td>
<td width="126">Nebkheperure‘</td>
<td width="54">Person</td>
<td width="150">Tutankhamen</td>
</tr>
<tr>
<td width="113">Necho</td>
<td width="126">Nekau</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nectanebo I</td>
<td width="126">Netanebes, Nakhtnebef</td>
<td width="54">Person</td>
<td width="150">Kheperkara</td>
</tr>
<tr>
<td width="113">Nectanebo II</td>
<td width="126">Nektanebos, Nakhtnebef</td>
<td width="54">Person</td>
<td width="150">Senedjemibra Setepenanhur</td>
</tr>
<tr>
<td width="113">Neferibra</td>
<td width="126">Neferibre‘</td>
<td width="54">Person</td>
<td width="150">Psamtik II</td>
</tr>
<tr>
<td width="113">Neferkara</td>
<td width="126">Neferkare‘</td>
<td width="54">Person</td>
<td width="150">Pepy II<br />
Amenemnisu<br />
Peftjauawybast<br />
Shabaqo</td>
</tr>
<tr>
<td width="113">Neferkheperura Waenra</td>
<td width="126">Neferkheperure‘ Wa‘enre‘</td>
<td width="54">Person</td>
<td width="150">Akhenaten, Amenhotep IV</td>
</tr>
<tr>
<td width="113">Nebmaatra Meryamen</td>
<td width="126">Nebma‘atre‘ Meryamun</td>
<td width="54">Person</td>
<td width="150">Ramesses VI</td>
</tr>
<tr>
<td width="113">Nebpehtyra</td>
<td width="126">Nebpehtire‘</td>
<td width="54">Person</td>
<td width="150">Ahmose</td>
</tr>
<tr>
<td width="113">Nebtawyra</td>
<td width="126">Nebtawyre‘</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Neferhotep</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Neferkara Setepenra</td>
<td width="126">Neferkare‘ setepenre‘</td>
<td width="54">Person</td>
<td width="150">Ramesses IX</td>
</tr>
<tr>
<td width="113">Neferneferuaten</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nefertari</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nefertiti</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Neferusobek</td>
<td width="126">Nefrusobk</td>
<td width="54">Person</td>
<td width="150">Sobekkara</td>
</tr>
<tr>
<td width="113">Neith</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nekhbet</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nekhen</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nemtyemsaf</td>
<td width="126">Nemtyemzaf</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nepherites</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Baenra Merynetjeru (I)</td>
</tr>
<tr>
<td width="113">Neskhons</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Netjerkheperra Setepenamen</td>
<td width="126">Netjerkheperre‘  Setepenamun</td>
<td width="54">Person</td>
<td width="150">Siamen</td>
</tr>
<tr>
<td width="113">Nikaura</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nimaatra</td>
<td width="126">Nima‘atre‘</td>
<td width="54">Person</td>
<td width="150">Amenemhat III<br />
Kashta</td>
</tr>
<tr>
<td width="113">Ninetjer</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Niuserra</td>
<td width="126">Niuserre, Neuserre‘</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nubet</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Nuri</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>O<a name="O"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">el-Omari</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ombos</td>
<td width="126">Nbwt</td>
<td width="54">Place</td>
<td width="150">Kom Ombo</td>
</tr>
<tr>
<td width="113">Onuris</td>
<td width="126">Onouris</td>
<td width="54">God</td>
<td width="150">Arensnuphis, An-Her</td>
</tr>
<tr>
<td width="113">Osorkon</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Aakheperra Setepenra (I)<br />
Sekhemkheperra Setepenra (II)<br />
Usermaatra Setepenamen (III)<br />
Usermaatra Setepenamen (IV)<br />
Aakheperra Setepenamen (V)</td>
</tr>
<tr>
<td width="113">Oxyrhynchus</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>P<a name="P"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Pami</td>
<td width="126">Pimay</td>
<td width="54">Person</td>
<td width="150">Usermaatra Setepenra/amen</td>
</tr>
<tr>
<td width="113">Panebtawy-pa-khered</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Parthenios</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Pathyris</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Petubaste</td>
<td width="126">Petubastis</td>
<td width="54">Person</td>
<td width="150">Usermaatra (I)</td>
</tr>
<tr>
<td width="113">Peftjauawybast</td>
<td width="126">peftjau‘awybast</td>
<td width="54">Person</td>
<td width="150">Neferkara</td>
</tr>
<tr>
<td width="113">Pelusion</td>
<td width="126">Pelusium</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Pepiankh-hery-ib</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Pepy</td>
<td width="126">Pepi</td>
<td width="54">Person</td>
<td width="150">Meryra (Pepy I)</td>
</tr>
<tr>
<td width="113">Peribsen</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Perim</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Philadelphia</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Kom el-Kharaba el-Kebir</td>
</tr>
<tr>
<td width="113">Philae</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Philoteris</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Medinet Watfa</td>
</tr>
<tr>
<td width="113">Piankhy</td>
<td width="126">Piye</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Pinedjem</td>
<td width="126">Pinudjem</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Pi-Ramesse</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Pi-Sopdu</td>
<td width="126">Per-Soped, Per-Sopdu, Pi-Soped</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Piye</td>
<td width="126">Piankhy</td>
<td width="54">Person</td>
<td width="150">Usermaatra</td>
</tr>
<tr>
<td width="113">Psammuthis</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Userra Setepenptah</td>
</tr>
<tr>
<td width="113">Psammetichus</td>
<td width="126">Psamtek, Psamtik</td>
<td width="54">Person</td>
<td width="150">Wahibra (I)<br />
Neferibra (II)</td>
</tr>
<tr>
<td width="113">Psusennes</td>
<td width="126">Pasebakhaenniut</td>
<td width="54">Person</td>
<td width="150">Aakheperra Setepenamen (I)<br />
Titkheperura Setepenra (II)</td>
</tr>
<tr>
<td width="113">Ptolemies</td>
<td width="126">Lagides</td>
<td width="54">Term</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ptolemy</td>
<td width="126">Ptolemeus, Ptolemaeus, Ptolemaios</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Peleset</td>
<td width="126">Pulasti</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>Q<a name="Q"></a></strong></td>
<td width="144"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="144"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Qaa</td>
<td width="144">Qa‘a</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Qala</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Qara el-Hamra</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qarat el-Muzawwaqa</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qaret Rusas</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qasr el-Aquz</td>
<td width="144">Qasr el- &#8216;Aquz</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qasr el-Banat</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150">Euhemeria</td>
</tr>
<tr>
<td width="113">Qasr el-Ghuweida</td>
<td width="144">Qasr el-Ghueida, Qasr el-Ghuweita</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qasr Ibrim</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qasr Qarun</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150">Dionysias</td>
</tr>
<tr>
<td width="113">Qasr el-Sagha</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qasr Zaiyan</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qau el-Kebir</td>
<td width="144">Qaw el-Kebir</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qena Region</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qeyqeshu</td>
<td width="144">Qayqisha</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qirtasi</td>
<td width="144">Kertassi</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qubbet el-Hawa</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Quft</td>
<td width="144">Qift</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qurna</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qurnet Murai</td>
<td width="144">Qurnet Mura‘i</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qus</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Quseir el-Qadim</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Qustul</td>
<td width="144"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>R</strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Ra</td>
<td width="126">Re</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Radjedef</td>
<td width="126">Djedefra, Re‘djedef</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ramesses</td>
<td width="126">Ramses, Ramsis, Rameses</td>
<td width="54">Person</td>
<td width="150">Menpehtyra (I)<br />
Usermaatra Setepenra (II)<br />
Usermaatra Meryamen (III)<br />
Hekamaatra Setepenamen (IV)<br />
Usermaatra Sekheperenra (V)<br />
Nebmaatra Meryamen (VI)<br />
Usermaatra Setepenra Merytamen (VII)<br />
Usermaatra Akhenamen (VIII)<br />
Neferkara Setepenra (IX)<br />
Khepermaatra Setepenra (X)<br />
Menmaatra Setepenptah (XI)</td>
</tr>
<tr>
<td width="113">Ramessesemperra</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Ramesside</td>
<td width="126"></td>
<td width="54">Term</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Ranan Canal</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Raneb</td>
<td width="126">Re‘neb</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Raneferef</td>
<td width="126">Neferefra, Ra‘neferef</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Renenutet</td>
<td width="126">Renenet</td>
<td width="54">God</td>
<td width="150">Thermuthis</td>
</tr>
<tr>
<td width="113">Roma-Roy</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>S<a name="S"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Sa el-Haggar</td>
<td width="126">Sa el-Hagar</td>
<td width="54">Place</td>
<td width="150">Sais</td>
</tr>
<tr>
<td width="113">el-Saff</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Saft el-Henna</td>
<td width="126">Saft el-Hina, Saft el-Hinna, Saft el- Hennah</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sais</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Sa el-Haggar</td>
</tr>
<tr>
<td width="113">Sakhmet</td>
<td width="126">Sekhmet</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Salitis</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Samanud</td>
<td width="126">Samannud</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Samannud</td>
<td width="126">Asammanud</td>
<td width="54">Place</td>
<td width="150">Sebennytos</td>
</tr>
<tr>
<td width="113">San el-Haggar</td>
<td width="126">San el-Hagar</td>
<td width="54">Place</td>
<td width="150">Tanis</td>
</tr>
<tr>
<td width="113">Sanakht</td>
<td width="126">Zanakht</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sanam</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Saqqara</td>
<td width="126">Sakkara, Saqara, Saqqarah</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sarenput</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Satet</td>
<td width="126">Satis</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sayala</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Seankhkara</td>
<td width="126">S‘ankhkare‘</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sebennytos</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Samannud</td>
</tr>
<tr>
<td width="113">Sedeinga</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sedment</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sehel</td>
<td width="126">Geziret Sehel</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sehertawy</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Intef I</td>
</tr>
<tr>
<td width="113">Sehertawy</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sehetepibra</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Seila</td>
<td width="126">Sila</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sekhemkheperra Setepenra</td>
<td width="126">Sekhemkheperre‘ setepenre‘</td>
<td width="54">Person</td>
<td width="150">Osorkon II</td>
</tr>
<tr>
<td width="113">Semaina</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Semenkhkara</td>
<td width="126">Smenkhkare‘</td>
<td width="54">Person</td>
<td width="150">Ankh kheperura</td>
</tr>
<tr>
<td width="113">Semerkhet</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Senakhtenra</td>
<td width="126">Senakhtenre‘</td>
<td width="54">Person</td>
<td width="150">Tao I</td>
</tr>
<tr>
<td width="113">Seneferu</td>
<td width="126">Snofru</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Senenmut</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Senentanen Setepenptah</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Khababash</td>
</tr>
<tr>
<td width="113">Sennefer</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Senusret</td>
<td width="126">Senwosret</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Seqenenra</td>
<td width="126">Seqenenre‘</td>
<td width="54">Person</td>
<td width="150">Tao II</td>
</tr>
<tr>
<td width="113">Serabit el-Khadim</td>
<td width="126">Sarabit el-Khadim</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Serapis</td>
<td width="126">Sarapis</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Serefka</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sesebi</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sethnakhte</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Userkhaura Meryamen</td>
</tr>
<tr>
<td width="113">Sety</td>
<td width="126">Sethos, Seti</td>
<td width="54">Person</td>
<td width="150">Menmaatra (I)<br />
Userkheperura Setepenra (II)</td>
</tr>
<tr>
<td width="113">Shabaqo</td>
<td width="126">Shabaka</td>
<td width="54">Person</td>
<td width="150">Neferkara</td>
</tr>
<tr>
<td width="113">Shabitqo</td>
<td width="126">Shebitku</td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">shabti</td>
<td width="126"></td>
<td width="54">Object</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shalfak</td>
<td width="126">Shelfak</td>
<td width="54">Place</td>
<td width="150">Mirgissa</td>
</tr>
<tr>
<td width="113">Sharuhen</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Sheikh Ibada</td>
<td width="126">el-Sheikh ‘Ibada</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">el-Sheikh Said</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shekelesh</td>
<td width="126">Shakalusha</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shemay</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shenhur</td>
<td width="126">Shanhur</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shepseskaf</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shepsesra</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Tefnakhte?</td>
</tr>
<tr>
<td width="113">Sheshi</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shesmetet</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Shoshenq</td>
<td width="126">Sheshonq, Sheshonk</td>
<td width="54">Person</td>
<td width="150">Hedjkheperra Setepenra (I)<br />
Hekakheperra Setepenra (II)<br />
Usermaatra Setepenra/amen (III)<br />
Aakheperra (V)</td>
</tr>
<tr>
<td width="113">Sheytepu</td>
<td width="126">Shayu</td>
<td width="54">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Siamen</td>
<td width="126">Siamun</td>
<td width="54">Person</td>
<td width="150">Netjerkheperra Setepenamen</td>
</tr>
<tr>
<td width="113">Sikait</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Simut-Kyky</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sinai</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Siptah</td>
<td width="126">Saptah</td>
<td width="54">Person</td>
<td width="150">Akhenra Setepenra</td>
</tr>
<tr>
<td width="113">Sitra Merytamen</td>
<td width="126">Sitre‘ Meritamun</td>
<td width="54">Person</td>
<td width="150">Tauseret</td>
</tr>
<tr>
<td width="113">Siwa oasis</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Smendes</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150">Hedjkheperra Setepenra</td>
</tr>
<tr>
<td width="113">Sobekhotep</td>
<td width="126">Sebekhotep</td>
<td width="54">Person</td>
<td width="150">Kha ankhra</td>
</tr>
<tr>
<td width="113">Sobekkara</td>
<td width="126">Sebekkare‘</td>
<td width="54">Person</td>
<td width="150">Neferusobek</td>
</tr>
<tr>
<td width="113">Sokar</td>
<td width="126"></td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Soknopaiou Nesos</td>
<td width="126">Socnopaiou Nesos</td>
<td width="54">Place</td>
<td width="150">Dimai</td>
</tr>
<tr>
<td width="113">Soleb</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Sopdu</td>
<td width="126">Soped</td>
<td width="54">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Speos Artemidos</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Establ Antar, Antinoopolis</td>
</tr>
<tr>
<td width="113">Suez</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Arsinoe<br />
Kleopatris</td>
</tr>
<tr>
<td width="113">Syene</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150">Aswan</td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>T<a name="T"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="56"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="56"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Taharqo</td>
<td width="126">Taharqa, Taharka</td>
<td width="56">Person</td>
<td width="150">Khunefertemra</td>
</tr>
<tr>
<td width="113">Takelot</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150">Usermaatra Setepenamen (I)<br />
Hedjkheperra Setepenra (II)</td>
</tr>
<tr>
<td width="113">Tanis</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150">San el-Haggar</td>
</tr>
<tr>
<td width="113">Tanutamani</td>
<td width="126">Tantamani</td>
<td width="56">Person</td>
<td width="150">Bakara</td>
</tr>
<tr>
<td width="113">Tao</td>
<td width="126">Ta‘o</td>
<td width="56">Person</td>
<td width="150">Senakhtenra (Tao I)</td>
</tr>
<tr>
<td width="113">el-Tarif</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tauseret</td>
<td width="126">Tausret, Twosret</td>
<td width="56">Person</td>
<td width="150">Sitra Merytamen</td>
</tr>
<tr>
<td width="113">Tebtunis</td>
<td width="126">Tebtynis</td>
<td width="56">Place</td>
<td width="150">Umm el-Baraqat</td>
</tr>
<tr>
<td width="113">Tefnakhte</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150">Shepsesra?</td>
</tr>
<tr>
<td width="113">Tell Atrib</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell Basta</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150">Bubastis</td>
</tr>
<tr>
<td width="113">Tell el-Ajjul</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Balamun</td>
<td width="126">el-Balamun, Balamun</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Dabaa</td>
<td width="126">Tell el-Daba‘a</td>
<td width="56">Place</td>
<td width="150">Avaris</td>
</tr>
<tr>
<td width="113">Tell el-Farain</td>
<td width="126">Tell el-Fara‘in</td>
<td width="56">Place</td>
<td width="150">Buto</td>
</tr>
<tr>
<td width="113">Tell el-Farkha</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Hebwa</td>
<td width="126">Tell el-Heboua</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Herr</td>
<td width="126">Tell el-Heir</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell Ibrahim Awad</td>
<td width="126">Ezbet Ibrahim &#8216;Awad</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Maskhuta</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Muqdam</td>
<td width="126">Tell el-Moqdam</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Ruba</td>
<td width="126">Tell el-Rub&#8217;</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Timai</td>
<td width="126">Tell Timai</td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tell el-Yahudiya</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Teos</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150">Irmaatenra</td>
</tr>
<tr>
<td width="113">Terenuthis</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150">Kom Abu Billo</td>
</tr>
<tr>
<td width="113">Tetian</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tetisheri</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tety</td>
<td width="126">Teti</td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Theadelphia</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150">Batn Ihrit</td>
</tr>
<tr>
<td width="113">Thebaid</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Thebes</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Thermuthis</td>
<td width="126"></td>
<td width="56">God</td>
<td width="150">Renenutet</td>
</tr>
<tr>
<td width="113">Thinis</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Thoth</td>
<td width="126">Thot</td>
<td width="56">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Thutmose</td>
<td width="126">Thutmoses, Thutmosis, Djehutymes</td>
<td width="56">Person</td>
<td width="150">Aakheperkara (I)<br />
Aakheperenra (II)<br />
Menkheperra (III)<br />
Menkheperura (IV)</td>
</tr>
<tr>
<td width="113">Tihna el-Gebel</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Titkheperura Setepenra</td>
<td width="126">Titkheprure‘ setepenre‘</td>
<td width="56">Person</td>
<td width="150">Psusennes II</td>
</tr>
<tr>
<td width="113">Tiy</td>
<td width="126">Tiye</td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tjaru</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tjeker</td>
<td width="126">Sikala, Sikels</td>
<td width="56">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tod</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Trajan</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tukh</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tuna el-Gebel</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tura</td>
<td width="126"></td>
<td width="56">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Tutankhamen</td>
<td width="126">Tutankhamun, Tutankhamon</td>
<td width="56">Person</td>
<td width="150">Nebkheperura</td>
</tr>
<tr>
<td width="113">Tuya</td>
<td width="126"></td>
<td width="56">Person</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>U<a name="U"></a></strong></td>
<td width="125"><strong> </strong></td>
<td width="55"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="125"><strong>Alternative spelling</strong></td>
<td width="55"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Umm el-Baraqat</td>
<td width="125">Umm el-Baragat Umm el-Brigat</td>
<td width="55">Place</td>
<td width="150">Tebtunis</td>
</tr>
<tr>
<td width="113">Umm el-Qaab</td>
<td width="125">Umm el-Qa&#8217;ab</td>
<td width="55">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Umm el-Sawan</td>
<td width="125"></td>
<td width="55">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Umm Ubeida</td>
<td width="125">Umm el-Ebeida, Umm &#8216;Ubeida</td>
<td width="55">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Unas</td>
<td width="125">Wenis</td>
<td width="55">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Uronarti</td>
<td width="125"></td>
<td width="55">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Userhat</td>
<td width="125"></td>
<td width="55">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Userkaf</td>
<td width="125"></td>
<td width="55">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Userkhaura Meryamen</td>
<td width="125"></td>
<td width="55">Person</td>
<td width="150">Sethnakhte</td>
</tr>
<tr>
<td width="113">Userkheperura Setepenra</td>
<td width="125">Userkheprure‘ Setepenre‘</td>
<td width="55">Person</td>
<td width="150">Sety II</td>
</tr>
<tr>
<td width="113">Usermaatra</td>
<td width="125">Userma‘atre‘</td>
<td width="55">Person</td>
<td width="150">Pedubaste I<br />
Piye</td>
</tr>
<tr>
<td width="113">Usermaatra Akhenamen</td>
<td width="125">Userma‘atre‘ Akhenamun</td>
<td width="55">Person</td>
<td width="150">Ramesses VIII</td>
</tr>
<tr>
<td width="113">Usermaatra Meryamen</td>
<td width="125">Userma‘atre‘ Meryamun</td>
<td width="55">Person</td>
<td width="150">Ramesses III</td>
</tr>
<tr>
<td width="113">Usermaatra Sekheperenra</td>
<td width="125">Userma‘atre‘ Sekheperenre‘</td>
<td width="55">Person</td>
<td width="150">Ramesses V</td>
</tr>
<tr>
<td width="113">Usermaatra Setepenamen</td>
<td width="125">Userma‘atre‘ Setepenamun</td>
<td width="55">Person</td>
<td width="150">Amenemope<br />
Takelot I<br />
Osorkon III<br />
Shoshenq III<br />
Pami<br />
Osorkon IV</td>
</tr>
<tr>
<td width="113">Usermaatra Setepenra</td>
<td width="125">Userma‘atre‘ Setepenre‘</td>
<td width="55">Person</td>
<td width="150">Ramesses II<br />
Shoshenq III<br />
Pami</td>
</tr>
<tr>
<td width="113">Usermaatra Setepenra Meryamen</td>
<td width="125">Userma‘atre‘ Setepenre‘ Meryamun</td>
<td width="55">Person</td>
<td width="150">Ramesses VII</td>
</tr>
<tr>
<td width="113">Userra Setepenptah</td>
<td width="125">Userre‘ setepenptah</td>
<td width="55">Person</td>
<td width="150">Psammuthis</td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>V<a name="V"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">&#8211;</td>
<td width="126"></td>
<td width="54"></td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>W<a name="W"></a></strong></td>
<td width="139"><strong> </strong></td>
<td width="53"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="139"><strong>Alternative spelling</strong></td>
<td width="53"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Wadi</td>
<td width="139"></td>
<td width="53">Term</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Abbad</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Digla</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Fayrouz</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Gawasis</td>
<td width="139">Wadi Gasus</td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Hammamat</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Hof</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi el-Hol</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi el-Hudi</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Kubaniya</td>
<td width="139">Wadi Kubbaniya, Wadi Kubaniyya</td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Maghara</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi el-Muluk</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150">Valley of the Kings</td>
</tr>
<tr>
<td width="113">Wadi el-Qash</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi el-Sebua</td>
<td width="139">Wadi es-Subua, el-Sebu&#8217;a</td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Sikait</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadi Tumilat</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadj</td>
<td width="139"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wadjkheperra</td>
<td width="139">Wadjkheperre‘</td>
<td width="53">Person</td>
<td width="150">Kamose</td>
</tr>
<tr>
<td width="113">Wahankh</td>
<td width="139">Wah ankh Wah‘ankh</td>
<td width="53">Person</td>
<td width="150">Intef II</td>
</tr>
<tr>
<td width="113">Wahibra</td>
<td width="139">Wahibre‘</td>
<td width="53">Person</td>
<td width="150">Psamtik I</td>
</tr>
<tr>
<td width="113">Wahkara</td>
<td width="139">Wahkare‘</td>
<td width="53">Person</td>
<td width="150">Bakenrenef / Bocchoris</td>
</tr>
<tr>
<td width="113">Wannina</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wegaf</td>
<td width="139"></td>
<td width="53">Person</td>
<td width="150">Khutawyra</td>
</tr>
<tr>
<td width="113">Wehemibra</td>
<td width="139">Wehemibre‘</td>
<td width="53">Person</td>
<td width="150">Nekau II</td>
</tr>
<tr>
<td width="113">Weni</td>
<td width="139"></td>
<td width="53">Person</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Wennefer</td>
<td width="139">Onophris</td>
<td width="53">God</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Weshesh</td>
<td width="139">Washasha</td>
<td width="53">Person/Tribe</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Widan el-Faras</td>
<td width="139"></td>
<td width="53">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>X<a name="X"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">&#8211;</td>
<td width="126"></td>
<td width="54"></td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>Y<a name="Y"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Yuya</td>
<td width="126"></td>
<td width="54">Person</td>
<td width="150"></td>
</tr>
</tbody>
</table>
<p><a href="#top">Return to Top</a></p>
<table class="tbl_style">
<tbody>
<tr>
<td width="113"><strong>Z<a name="Z"></a></strong></td>
<td width="126"><strong> </strong></td>
<td width="54"><strong> </strong></td>
<td width="150"><strong> </strong></td>
</tr>
<tr>
<td width="113"><strong>Preferred spelling</strong></td>
<td width="126"><strong>Alternative spelling</strong></td>
<td width="54"><strong>Category</strong></td>
<td width="150"><strong>Equivalent</strong></td>
</tr>
<tr>
<td width="113">Zawiyet Umm el-Rakham</td>
<td width="126">Zawyet Umm el-Rakham</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Zawyet</td>
<td width="126"></td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Zawyet el-Aryan</td>
<td width="126">Zawyet el-‘Aryan</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
<tr>
<td width="113">Zawyet el-Mayitin</td>
<td width="126">Zawyet el-Maiyitin</td>
<td width="54">Place</td>
<td width="150"></td>
</tr>
</tbody>
</table>

				
			</div>

			
		</article>
		
