<article class="post" id="post-64">

			<h2>Tutorial</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/tutorial/" title="7:45 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T19:45:20+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<p>UEE is in development. There are several ways to search UEE content, of which at the moment (2014) the best options are: using full text search through the search box, or using the time map.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Search Box</h3>
<p>The start page and the header have a “Search UEE” box. Anything typed in the “search” box will give results of a full text search in all UEE articles. A search for “Amenhotep III” for instance, will result in over 10 articles, presented in alphabetical order. The article on <em>Cartouche</em> comes up, and when you go to the full text of the article you can find the term within the article through using the search function of the browser. Firefox, for instance, has a “Find” function under the “Edit” tab in the Menu bar at the top of the browser page.</p>
<p>&nbsp;</p>
<p>A search for the Egyptologist “Stadler” will give you articles authored by Stadler, but also articles in which his work is quoted (and his name shows up in the references list).</p>
<p>&nbsp;</p>
<h3>Time Map</h3>
<p>The time map has its own search box, indicated with “Filter Results by Keyword”. Typing in a place name (e.g. “Amarna”) will show that place on the map (if you do not see it appear, then zoom out until you do). Clicking on the place will show how many articles mention this particular location. there are two types of marker: a circle with a cross (the <em>nyut</em> hieroglyph for city) indicates sites, a stela indicates an element of that site. This can be a building (e.g. a particular house or tomb), or even a part of a building (e.g. the pylon of the Temple of Edfu). Place indicators may be clustered (indicated with a small “+” sign) and will divide up in individual markers when zooming in.</p>
<p>&nbsp;</p>
<h3>Time Slider</h3>
<p>The time slider at the top of the time map indicates periods of Egyptian history in different levels of detail. The first level indicates years from 5000 BCE (Before Common Era) to 500 CE (=AD). The second level has distinct periods from Prehistory to the Late Roman Period. Clicking on any of these periods, e.g. “Old Kingdom Phase” (the periods show up when you mouse over), brings up a third line which indicates dynasties of the Old Kingdom. Clicking on “View Places in Dyasty 5” brings up individual king’s names. Clicking on “Userkaf Menkau” will show places on the time map which were in use during the reign of this king. Zooming in on Bubastis and clicking on the marker will show that there are four articles that feature Bubastis.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Title Index</h3>
<p>The title index gives a list of articles that have been published, or are in development. Not all of these are at this moment available. If you click on an article that is available, the author name, title in four languages (English, Arabic, German and French), as well as the English and Arabic abstracts appear at the top of the page to the right, together with a button “read article” that will bring up the full article text. If you click on a title and nothing appears, then scroll up with the bar on the right until the abstract, or the text “This article is currently in development and not available at this time.” appears.</p>
<p>&nbsp;</p>
<h3>Advanced Search</h3>
<p>This functionality is in development and cannot be used at present.</p>
<p>&nbsp;</p>
<h3>Subject Browse</h3>
<p>The UEE is divided in 10 different subjects:</p>
<p>1.   Natural Environment</p>
<p>2.   Geography</p>
<p>3.   Time and History</p>
<p>4.   Persons</p>
<p>5.   Religion</p>
<p>6.   Individual and Society</p>
<p>7.   Language, Text and Writing</p>
<p>8.   Material Culture, Arts and Architecture</p>
<p>9.   Domains of Knowledge</p>
<p>10. Egyptology</p>
<p>&nbsp;</p>
<p>Clicking on any of these categories brings the organizational structure of the UEE to the fore. In 2014 the categories for which articles have been commissioned are: Natural Environment (only 2 articles available at this time); Time and History; Religion; Individual and Society; Language, text and writing; and Geography. The subject browse is under development and at present not the easiest way to access information in the UEE.</p>

				
			</div>

			
		</article>
		
		
