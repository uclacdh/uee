	<div>
		<h3>My Profile</h3>
		<?php if ($this->openid->data['completed_profile'] == true and $this->edit == false) { ?>
		<p><a href="<?php echo SITE_PATH; ?>/?module=front&class=profile&event=edit">Edit my profile</a></p>
		<p><strong>Name:</strong> <?php echo "{$this->openid->data['firstname']} {$this->openid->data['lastname']}"; ?></p>
		<p><strong>Email:</strong> <?php echo "{$this->openid->data['email']}"; ?></p>
		<p><strong>Status:</strong> <?php echo "{$this->openid->data['category']}, {$this->openid->data['university_org']}"; ?></p>
		<?php 
			} // endif
			else {
		?>
		<form action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=profile&event=update" method="POST">
			<h3>Create / Update a Profile</h3>
			<fieldset>
				<legend>Basic Information</legend>
				<p>
					<label for="firstname">First name *:</label>
					<input type="text" id="firstname" name="firstname" value="<?php echo $this->openid->data['firstname']; ?>" />
				</p>
				<p>
					<label for="lastname">Last name *:</label>
					<input type="text" id="lastname" name="lastname" value="<?php echo $this->openid->data['lastname']; ?>" />
				</p>
				<p>
					<label for="email">E-mail *:</label>
					<input type="text" id="email" name="email" value="<?php echo $this->openid->data['email']; ?>" />
				</p>
				<p>
					<label for="university">University / Organization *:</label>
					<input type="text" id="university" name="university" value="<?php echo $this->openid->data['university_org']; ?>" />
				</p>
				<p>
					<label for="university_id">UCLA ID: (only if you are a UCLA student)</label>
					<input type="text" id="university_id" name="university_id" value="<?php echo $this->openid->data['university_id']; ?>" />
				</p>
				<p>
					<label for="category">Your affiliation to UEE *:</label>
					<select id="category" name="category">
						<option value="professional" <?php echo ($this->openid->data['category'] == 'professional'?"selected=selected":""); ?> >Professional Egyptologist</option>
						<option value="student" <?php echo ($this->openid->data['category'] == 'student'?"selected=selected":""); ?> >Egyptology Student</option>
						<option value="education_other" <?php echo ($this->openid->data['category'] == 'education_other'?"selected=selected":""); ?> >Other Student</option>
						<option value="public" <?php echo ($this->openid->data['category'] == 'public'?"selected=selected":""); ?> >Interested Public</option>
					</select>
				</p>				
				<p>
					<label for="about_us">How did you find out about us?</label>
					<textarea id="about_us" name="about_us" ><?php echo $this->openid->data['comments']; ?></textarea>
				</p>
				<p>
					<label for="keyphrase">Secret Phrase (used for securing your account):</label>
					<input type="text" id="keyphrase" name="keyphrase" value="" />
				</p>
				<p>
					<input type="submit" id="submit_button" name="submit_button" value="Submit" />
				</p>
			</fieldset>
		</form>
		<?php 
			} // end else;
		?>
	
	</div>
	
	<div class='clearfix'></div>
