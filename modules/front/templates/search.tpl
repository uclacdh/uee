	<div id='results' >
		<?php if (!$this->keyword) echo "<!-- "; ?>
		<h3>search results for &quot;<?php echo $this->keyword; ?>&quot;</h3>
		<?php 
			if (!$this->keyword) 
			{
				echo "-->";
			}
			if (!$this->keyword and !$this->placeId) {			
				echo '<p><strong>Oops!!! Looks like you didn\'t enter a search phrase. </strong></p>';
			}
		?>
		<?php if (empty($this->titles)) : ?>
		<p><strong>No search results found</strong></p>
		<?php endif; ?>
		<ul id='titles'>
		<?php
			if ($this->keyword or $this->placeId)
				$this->listArticles();
		?>
		</ul>
	</div>
	
	<div id='brief' >
		<p><?php $this->displayBrief(); ?></p>
	</div>
	
	<div class='clearfix'></div>

<script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/search.js" ></script>