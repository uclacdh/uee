	<div id='leftcol'>
		<div id='toc-wrapper'>
			<div class='section-head'>
				<h3 class='article-section-title'>Table of Contents</h3>
			</div>
			<div id='toc'>
				<ul>
					<li>loading...</li>
				</ul>
			</div>
		</div>
	</div>	
	
	<div id='maincol'  style='width:58%;padding;1%;float:left;'>
		<h3 id='article-names' style='clear:right;'>
		<?php
			$results = $this->articleXpath->query("//tei:titleStmt/tei:title");
			if ($results->length > 0){
				foreach ($results as $element)
				{
					if ($element->hasAttribute('type')) {
						echo '<span lang="'.$element->getAttribute('lang').'" xml:lang="'.$element->getAttribute('lang').'" >'.$element->nodeValue.'</span><br />';
                	} else { // main one, add italics
			?>
                    <h3 class="english-title">
                        <?=$element->nodeValue?><br />
                    </h3>
			<?php
					}
				//		echo $element->nodeValue.'<br />';
				}	
			}
			
		?>
		</h3>
		<p id='article-author'>
		<?php
			$results = $this->articleXpath->query("//tei:byline");
			if ($results->length > 0){
				$nodes = $results->item(0)->childNodes;
				foreach ($nodes as $element)
				{
					echo $element->nodeValue .'<br />';
				}	
			}			
		?>
		</p>
		<div id='article-keywords' class='article-section'>
			Keywords:<br />
			<span>
			<?php
				$results = $this->articleXpath->query("//tei:textClass/tei:keywords/tei:term");
				$first = true;
				foreach ($results as $res) {
					if (!$first)
						echo ", ";
					$first = false;
					echo $res->nodeValue;
				}
			?>
			</span>
		</div><br />	
		<div style='font-size:0.8em;margin-top:0;'>
			<?php
				$results = $this->articleXpath->query("//tei:notesStmt/tei:note");
				if ($results->length > 0):
			?>
			<p class='tei-p' style='margin-top:0;'><strong>Editorial Notes:</strong><br/>
			<?php				
				foreach ($results as $res)
					echo $res->nodeValue .'<br />';
			?>
			</p>
			<?php endif; ?>
		</div>
		<div class='section-head'>
			<a id='abstract-toggle' name='abstract-toggle' class='section-toggle' href="javascript:;"><img src="/graphics/Contract.png" alt="expand/collapse this section" /></a>
			<h3 class='article-section-title'>Abstract</h3>
		</div>
		<div id='abstract-section' class='article-section' >
		<?php	
			echo $this->getOutput("//tei:div[@type='english_abstract']");		
		?>
		</div>
		<div class='section-head'>
			<a id='arabic-toggle' name='arabic-toggle' class='section-toggle' href="javascript:;"><img src="/graphics/Contract.png" alt="expand/collapse this section" /></a>
			<h3 class='article-section-title'>Abstract in Arabic</h3>
		</div>
		<div id='arabic-section' class='article-section' >
		<?php
			$results = $this->articleXpath->query("//tei:div[@type='arabic_abstract']");
			if ($results->length > 0){
				$nodes = $results->item(0)->childNodes;
				foreach ($nodes as $element)
				{
					if ($element->localName == 'p')
						echo '<p style="direction:rtl;" lang="ar" xml:lang="ar" >'. $element->nodeValue.'</p>';
					else
						; //echo $element->nodeValue;
				}
			}
		?>
		</div>
		<div class='section-head'>
			<a id='entrytext-toggle' name='entrytext-toggle' class='section-toggle' href="javascript:;"><img src="/graphics/Contract.png" alt="expand/collapse this section" /></a>
			<h3 class='article-section-title' >Entry text</h3>
		</div>
		<div id='entrytext-section' class='article-section' >
		<?php
			echo $this->getOutput("//tei:div[@type='article']/tei:div[@type='article']");			
		?>
		</div>
		<div id='additional-article-sections'>
		<?php
			// echo $this->getOutput("//tei:body/tei:div[@type='article']/tei:div[not(@type)]");
			
			$otherSections = $this->articleXpath->query("//tei:body/tei:div[@type='article']/tei:div[not(@type)]");
			for ($inti = 1; $inti <= $otherSections->length; $inti++)
			{
				$query = "//tei:body/tei:div[@type='article']/tei:div[not(@type)][$inti]/tei:head";
				// echo $query . '<br />';
				$header = $this->getOutput($query);
				// what is the id to use for the section div?
				// 2017-08-17: Dave changed 'ereg' (deprecated) to 'preg_match'. Look here for bugs.
				if (preg_match('/id="(.*)"/',$header,$matches))
				{
					$id = $matches[1];
					$id = strtok($id,"-");
				}
				else
					$id = '';
				// display the header
				echo $header;
				$query = "//tei:body/tei:div[@type='article']/tei:div[not(@type)][$inti]/tei:p";
				// use a section wrapper, then display the section
				echo '<div id="'.$id.'-section" class="article-section" >';
				echo $this->getOutput($query);
				echo '<p style="clear:both;" />';
				echo '</div>';
			}
			
			
		?>
		</div>
		<?php if (!empty($this->placerefs)) : ?>
		<div class='section-head'>
			<a id='minimap-toggle' name='minimap-toggle' class='section-toggle' href='javascript:;' ><img src='/graphics/Contract.png' alt='expand/collapse this section' /></a>
			<h3 class='article-section-title' >Mini Map</h3>
		</div>
		<div id='place_ids' style='display:none;'>
		<?php
			echo $this->getGeodata();
		?>
		</div>
		<div id='minimap-section'>
			<p><em>places referenced in this article:</em></p>
			<div id='mini-map' >
			</div>
		</div>
		<?php endif; ?>
		<?php if ($this->articleXpath->query("//tei:div[@type='bibliographic_notes']/tei:p")->length > 0): ?>
		<div class='section-head' style='padding-top:1em;' >
			<a id='bibnotes-toggle' name='bibnotes-toggle' class='section-toggle' href='javascript:;' ><img src='/graphics/Contract.png' alt='expand/collapse this section' /></a>
			<h3 class='article-section-title' >Bibliographic Notes</h3>
		</div>
		<div id='bibnotes-section' class='article-section'>
			<?php echo $this->getOutput("//tei:div[@type='bibliographic_notes']/tei:p"); ?>
		</div>
		<?php endif; ?>
		<div class='section-head'>
			<a id='references-toggle' name='references-toggle' class='section-toggle' href='javascript:;' ><img src='/graphics/Contract.png' alt='expand/collapse this section' /></a>
			<h3 class='article-section-title' >References</h3>
		</div>
		<div id='article-references' class='article-section'>
		<?php
			// echo $this->generateReferencesOutput();
		?>
		<?php
			echo $this->generateReferences2();
		?>	
		</div>
		<!-- image credits, if any -->
		<?php if ($this->articleXpath->query("//tei:div[@type='image_credits']")->length > 0): ?>
		<?php echo $this->getOutput("//tei:div[@type='image_credits']/tei:head"); ?>
		<div id='imagecredits-section'  class='article-section'>
			<?php echo $this->getOutput("//tei:div[@type='image_credits']/tei:p"); ?>
		</div>
		<?php endif; ?>
		<a id='related-images' name='related-images'></a>
		<?php if (!empty($this->images)) : ?>
		<div class='section-head'>
			<a id='relatedimages-toggle' name='relatedimages-toggle' class='section-toggle' href='javascript:;' ><img src='/graphics/Contract.png' alt='expand/collapse this section' /></a>
			<h3 class='article-section-title' >Related Images</h3>
		</div>
		<div id='relatedimages-section' class='article-section'>
		<div id='related-image-div'>
			<?php $this->displayImages(); ?>
		</div>
		<form action="/imageresults/articles/<?php echo $this->articletitle; ?>" method="post" > 
			<p><input type='submit' id='more-images-button' name='more-images-button' value='MORE IMAGES >>' /></p>
		</form>
		</div>
		<?php endif; ?>
	</div>	

	<div id='rightcol' style='width:18%;padding;1%;float:left;'>
		<div id='popup-wrapper'>
			<div id='article-actions'>
				<p>
				<a href='javascript:;' id='tocColumn-toggle'><img src="/graphics/TOC_Hide.png" alt="Hide Table of Contents" /></a>
				<a href='javascript:;' id='myuee-toggle'><img src="/graphics/Annotations.png" alt="Annotations" /></a>
				<?php
					if (substr_count($_SERVER['HTTP_REFERER'],"search") and substr_count($_SERVER['HTTP_REFERER'],"placeId") ) :
				?>
				<a href='/search/placeId=<?php echo $_SESSION['place_id']; ?>'><img src="/graphics/Back_Search.png" alt="Search Results" /></a>
				<?php
					elseif (substr_count($_SERVER['HTTP_REFERER'],"search")): 
				?>
				<a href='/search/<?php echo $_SESSION['last_search']; ?>'><img src="/graphics/Back_Search.png" alt="Search Results" /></a>
				<?php
					endif;
				?>
				<?php if ($this->downloadURL) : ?>
				<!-- Every time a user clicks the button to download the PDF, a report will be sent to Google Analytics -->
				<a href='<?php echo htmlentities($this->downloadURL); ?>'  onclick="ga('send', 'event', 'button', 'click', '<?php echo htmlentities($this->downloadURL); ?>');">
					<img src="/graphics/Download_PDF.png" alt="Download PDF" />
				</a>
				<?php endif; ?>
				</p>
			</div>
			<div class='clearfix'></div>
			<!-- <p>Content will be displayed here after clicking on a glossary term or bibliographic reference</p> -->
			<div id='popup-content'>
			</div>
		</div>
	</div>
	
	<div class='clearfix'></div>
	
	<div id='myuee'>
		<a id='close-myuee' href="javascript:;">close</a>
		<h3 class='article-section-title'>my UEE</h3>
		<form id='myueeform' action='javascript:;'>
			<h4>preferences</h4>
			<p>
			<?php if (!$this->ueeUser->data or $this->ueeUser->data['link_on_flag']) :?>
				<input type='checkbox' id='links-toggle' name='links-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='links-toggle' name='links-toggle' />
			<?php endif;?>
			<label for='links-toggle' >links</label><br />
			<?php if (!$this->ueeUser->data or $this->ueeUser->data['gloss_on_flag']) :?>
				<input type='checkbox' id='glosses-toggle' name='glosses-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='glosses-toggle' name='glosses-toggle' />
			<?php endif;?>
			<label for='glosses-toggle' >glosses</label><br />
			<?php if (!$this->ueeUser->data or $this->ueeUser->data['bib_on_flag']) :?>
				<input type='checkbox' id='biblio-toggle' name='biblio-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='biblio-toggle' name='biblio-toggle' />
			<?php endif;?>
			<label for='biblio-toggle' >bibliographic references</label><br />
			<?php if (!$this->ueeUser->data or $this->ueeUser->data['places_on_flag']) :?>
				<input type='checkbox' id='places-toggle' name='places-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='places-toggle' name='places-toggle' />
			<?php endif;?>
			<label for='places-toggle' >places icons</label><br />
			<input type='button' id='save-toggles-button' name='save-toggles-button' value='SAVE' />
			</p>
			<h4>new annotation</h4>
			<?php 
				$annotations = $this->getAnnotations();
			?>
			<p>
				<input type='hidden' name='article_id' id='article_id' value='<?php echo $annotations['article_id']; ?>' />
				<input type='hidden' name='annotation_id' id='annotation_id' value='' />
				<textarea id='myuee-comments' name='myuee-comments' rows='3' cols='20'>Enter text comments here..</textarea><br />
				<input type='button' id='save-comments-button' name='save-comments-button' value='SAVE' />
			</p>
		</form>
		<br />
		<label id='toggles-save-status'></label><br />
		<h3 class='article-section-title'>Your Annotations for this Article</h3>
		<div style='clear:both;' id='myannotations' >
		<?php
			$annotations = null;
			$this->listAnnotations();
		?>
		</div>
	</div>
	
	<div style='clear:both;'></div>
	<div class="lightbox-layer">
		<p>
			<a class='lightbox-close' href="javascript:;">[x] close</a><br />
			<a class='lightbox-zoomin' href="javascript:;">Zoom In</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class='lightbox-zoomout' href="javascript:;">Zoom Out</a>
		</p>
		<p class='lightbox-wrapper'><img class="inline-lightbox" src="missing.jpg" alt="" /></p>
	</div>
	
	<script type="text/javascript" src="/js/ClusterMarker.js" ></script>
	<script type='text/javascript' src='/js/article5.js' ></script>
	<script type='text/javascript' src='/js/popups.js' ></script>
