
	<div id='leftcol' >
		<div id='welcome-div' >
			<p id='welcome-message' >Welcome,  <span id="name"><?php echo $this->openid->data['firstname']; ?></span></p>
			<p id='welcome-close' >to the UCLA Encylopedia of Egyptology</p>
			<?php if ($this->openid->data['completed_profile'] == true and $this->edit == false) { ?>
			<p><a href="<?php echo SITE_PATH; ?>/?module=front&class=profile&event=edit">Edit my profile</a></p>
			<?php } // endif ?>
		</div>	
		<h3>My Article Preferences</h3>
		<form id='myueeform'>
			<span class='block'>
			<?php if (!$this->user->data or $this->user->data['link_on_flag']) :?>
				<input type='checkbox' id='links-toggle' name='links-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='links-toggle' name='links-toggle' />
			<?php endif;?>
			<label for='links-toggle' >Show links to other articles</label>
			</span>
			<span class='block'>
			<?php if (!$this->user->data or $this->user->data['gloss_on_flag']) :?>
				<input type='checkbox' id='glosses-toggle' name='glosses-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='glosses-toggle' name='glosses-toggle' />
			<?php endif;?>
			<label for='glosses-toggle' >Show popups with glossary definitions</label>
			</span>
			<span class='block'>
			<?php if (!$this->user->data or $this->user->data['bib_on_flag']) :?>
				<input type='checkbox' id='biblio-toggle' name='biblio-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='biblio-toggle' name='biblio-toggle' />
			<?php endif;?>
			<label for='biblio-toggle' >Show popups with bibliographic references</label>
			</span>
			<span class='block'>
			<?php if (!$this->user->data or $this->user->data['places_on_flag']) :?>
				<input type='checkbox' id='places-toggle' name='places-toggle' checked='checked' />
			<?php else :?>
				<input type='checkbox' id='places-toggle' name='places-toggle' />
			<?php endif;?>
			<label for='places-toggle' >Show icons identifying places</label>
			</span>
			<a href='javascript:;' id='save-toggles-button' name='save-toggles-button'><img src='/graphics/Save.png' alt='SAVE' /></a>
		</form>
		<br />
		<label id='toggles-save-status' for=''></label><br />
	</div>
	
	<div id='rightcol' >
		<h3>My Annotations</h3>
		<?php $this->listAnnotations(); ?>
	</div>
	
	<div class='clearfix'></div>
	<script type='text/javascript' src='/js/myuee.js' ></script>	
