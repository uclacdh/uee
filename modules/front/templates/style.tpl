		<article class="post" id="post-32">

			<h2>Citing the UEE</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/citing/" title="6:32 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T18:32:00+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<p>The UEE is published under Creative Commons license Attribution-Non-Commercial-NoDerivs 3.0 Unported</p>
<p>&nbsp;</p>
<p>Articles can be cited as regular publications, with addition of their unique digital identification number. Example of the full citation is incorporated on the cover sheet of the article PDF, under the names of the editors in the following format:</p>
<p>&nbsp;</p>
<p>Exell, Karen, 2008, Ancestor Bust. In Willeke Wendrich (ed.), <em>UCLA Encyclopedia of Egyptology</em>, LosAngeles.<br />
<a href="http://digital2.library.ucla.edu/viewItem.do?ark=21198/zz000s5mbz"> http://digital2.library.ucla.edu/viewItem.do?ark=21198/zz000s5mbz</a></p>
<p>&nbsp;</p>
<p>In addition, each article is stored and can be found in three ways:</p>
<p>&nbsp;</p>
<p><a title="https://uee.ats.ucla.edu/articles/ancestor_bust/?x=53&amp;y=14" href="https://uee.ats.ucla.edu/articles/ancestor_bust/?x=53&amp;y=14">https://uee.ats.ucla.edu/articles/Ancestor_Bust/?x=53&amp;y=14</a><br />
points at an article text in the interactive UEE website</p>
<p>&nbsp;</p>
<p><a href="http://escholarship.org/uc/item/59k7832w">http://escholarship.org/uc/item/59k7832w<br />
</a>is the permanent link to the PDF of the article in the eScholarship repository</p>
<p>&nbsp;</p>
<p><a href="http://digital2.library.ucla.edu/viewItem.do?ark=21198/zz000s5mbz">http://digital2.library.ucla.edu/viewItem.do?ark=21198/zz000s5mbz<br />
</a>is the permanent link to the PDF of the article in the UCLA Digital Library Collection System</p>

				
			</div>

			
		</article>
		
