	<div id='time-slider' >
		<div id='min-time-period' >
			<label for='min-time-period-val' style='display:none;' >Begin:</label>
			<input id='min-time-period-val' name='min-time-period-val' type='text' value='-5000' size='5' />
		</div>
		<div style="width:75%;margin:1%;float:left;">
		</div>
		<div id='max-time-period' >
			<label for='max-time-period-val' style='display:none;' >End:</label>
			<input id='max-time-period-val' name='max-time-period-val' type='text' value='500' size='5' />
		</div>
		<div class='clearfix'></div>
		<div id='slider' class='ui-slider-1' >
			<div class='ui-slider-handle'></div>	
		</div>
		<div class='clearfix'></div>
		<div id='timeline-markers' >
			<?php // $this->displayTimelineMarkers(); ?>
		</div>
		<div class='clearfix'></div>
		<div id='phases' >
			<?php $this->displayPhases(); ?>
		</div>
		<div id='phasewidths' class='clearfix'>
			<?php // $this->displayPhasewidths(); ?>
		</div>
		<div class='clearfix'></div>	
		<div id='dynasties' >
		</div>	
		<div class='clearfix'></div>	
		<div id='rulers' >
		</div>	
		<div style='padding:0.5%;text-align:center;clear:both;display:none;'>
			<a id='reset-timeline' href='javascript:;' style='font-size:0.8em;' >Reset the timeline</a><br />
			<a id='expand-timeline-link' href='javascript:;' style='font-size:0.8em;' >View Expanded timeline</a>
		</div>
	</div>
	<div id='search-map' class='clearfix'>
		<form id='search-map-form' style='text-align:right;'>
			<label id='search-status' for='status'></label>
			<label for='keyword'>Filter results by keyword: </label>
			<input type='text' id='keyword' name='keyword' value='placename' /> 
			<input type='submit' id='search-map-submit' name='search-map-submit' value='Filter' />
		</form>
	</div>	
	<div id='marker-queries' >		
	</div>
	
	<div id='time-map-wrapper' >
		<!--
		<div id='marker-details-box' >
			<p>Marker details will go here</p>
		</div>
		-->
		<div id='time-map' ></div>
		<div class='clearfix'></div>	
	</div>
	
	<script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/jquery-ui-personalized-1.6rc6.min.js"></script>
	<!--
	<script type="text/javascript" src="http://ui.jquery.com/testing/ui/ui.core.js"></script>		
	<script type="text/javascript" src="http://ui.jquery.com/testing/ui/ui.slider.js"></script>		
	-->
	
	<script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/ClusterMarker.js" ></script>
	<script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/timemap4.js" ></script>
	