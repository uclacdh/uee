<article class="post" id="post-36">

			<h2>About the UEE</h2>

			<!---// <span class="sep">Posted </span><a href="http://uee.ucla.edu/about/" title="6:33 pm" rel="bookmark"><time class="entry-date" datetime="2014-08-28T18:33:20+00:00" pubdate>August 28, 2014</time></a> by <span class="byline author vcard">ueedevadmin</span> //-->

			<div class="entry">

				<p>The <em>UCLA Encyclopedia of Egyptology</em> is a steadily growing resource of high quality articles on ancient Egypt, written in jargon-free language. The UEE articles are written by the world’s leading <a title="The Authors" href="/authors/">Egyptologists</a> and are all peer reviewed before being published. They provide in-depth information, relevant illustrations, and an overview of recent literature. When an article is updated, the older version will always remain available in the archive with the same stable URL. UEE articles can, therefore, be quoted as any other scholarly publication. Online publication enables not only that information can be regularly updated, but it allows for interactive searches, for instance through the UEE Time Map interface (see the <a title="Tutorial" href="/tutorial/">Tutorial</a>). The articles are also made available as PDF through <a href="http://escholarship.org/uc/nelc_uee">eScholarship</a>.</p>
<p>&nbsp;</p>
<p>The publication process makes use of the eScholarship repository features, which enables an automated double blind review process. <a title="Editors and Staff" href="/editors/">Area editors</a> request authors and reviewers through this online system, which also tracks editorial decisions and the progress of an article through copy-editing, lay-out and galley proofs. The copy editors check the consistency of the references and the spelling, which is vital to enable the search functionality of the UEE. The article is published as a PDF and staff members then take the text and images and mark these up to enable the enhanced functionality of, for instance, the Time Map. All texts and images are stored in the Digital Library Collection System at UCLA, and combined into interactive articles when they fit the search criteria.</p>
<p>&nbsp;</p>
<p>Egyptology has as its object of study the history, practices, and conceptual categories of a culture, which was remarkably prolific in terms of written texts, art, architecture, and other forms of material culture. The richness of this culture, of which we find ample traces, allows us to reconstruct religious thinking, economic systems, intimate details of daily life, as well as ancient pathology, to name just a few aspects. The knowledge of Egyptologists, archaeologists, linguists, geologists and all other professions that are involved in research in Egypt , reflect the interdisciplinary approach that is needed to make sense of such a wealth of information. Truly encyclopedic knowledge in this day and age means to have access to every relevant source, of guaranteed quality, with clear guidelines on how to search for the desired material with the potential to open unexplored research paths.</p>
<p>&nbsp;</p>
<p>The establishment and initial growth of the UEE has been made possible by two generous grants from the National Endowment for the Humanities (PA 51941-06, $ 325 000 and PW-50095-08, $ 350 000).</p>

				
			</div>

			
		</article>
