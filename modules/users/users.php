<?php 

	/*
	login.php

	Allows users to log in for pages which require authentication
	Some other insightful comment here.
	*/

class users extends MVC_Authentication_ManageUsers {
	
	public $openId;		//need funtionality to look up users / compare login info for validation and authentication
	
	public function __construct() {
		parent::__construct();
		$this->openId = new MVC_Model_OpenId(); // assign the model
	}
	
	// display available permissions for the given user
	public function permissions($row) {
		$p = array();
		if ($row['approved'] == 0 and $row['pending'] == 0)
			$p[] = 'not approved';
		if ($row['pending'] == 1)
			$p[] = 'pending approval';
		if ($row['approved'] == 1)
			$p[] = 'view';
		if ($row['can_edit'] == 1)
			$p[] = 'edit';
		if ($row['manage_users'] == 1)
			$p[] = 'manage users';
		if ($row['completed_profile'] == 1)
			$p[] = 'completed profile';
		if ($row['admin'] == 1)
			$p[] = 'administrator';
		return $p;
	}
	
	public function approve() {
		// set the openId
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['uee_user_id'] = $_GET['id'];
		$this->openId->dbSearchTable();
		if ($this->openId->search_results and count($this->openId->search_results) == 1)
		{
			$this->openId->data = $this->openId->search_results[0];
		}
		$this->openId->data['pending'] = 0;
		$this->openId->data['approved'] = 1;
		$this->openId->data['date_updated'] = date("Y-m-d H:i:s");
		$this->openId->dbUpdateRow();		
		$this->openId = new MVC_Model_OpenId(); // assign the model
		$this->openId->dbSearchTable();
	}

	public function reject() {
		// set the openId
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['uee_user_id'] = $_GET['id'];
		$this->openId->dbSearchTable();
		if ($this->openId->search_results and count($this->openId->search_results) == 1)
		{
			$this->openId->data = $this->openId->search_results[0];
		}
		$this->openId->data['pending'] = 0;
		$this->openId->data['approved'] = 0;
		$this->openId->data['date_updated'] = date("Y-m-d H:i:s");
		$this->openId->dbUpdateRow();		
		$this->openId = new MVC_Model_OpenId(); // assign the model
		$this->openId->dbSearchTable();
	}

	public function allowEdit() {
		// set the openId
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['uee_user_id'] = $_GET['id'];
		$this->openId->dbSearchTable();
		if ($this->openId->search_results and count($this->openId->search_results) == 1)
		{
			$this->openId->data = $this->openId->search_results[0];
		}
		$this->openId->data['can_edit'] = 1;
		$this->openId->data['date_updated'] = date("Y-m-d H:i:s");
		$this->openId->dbUpdateRow();		
		$this->openId = new MVC_Model_OpenId(); // assign the model
		$this->openId->dbSearchTable();
	}

	public function removeEdit() {
		// set the openId
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['uee_user_id'] = $_GET['id'];
		$this->openId->dbSearchTable();
		if ($this->openId->search_results and count($this->openId->search_results) == 1)
		{
			$this->openId->data = $this->openId->search_results[0];
		}
		$this->openId->data['can_edit'] = 0;
		$this->openId->data['date_updated'] = date("Y-m-d H:i:s");
		$this->openId->dbUpdateRow();		
		$this->openId = new MVC_Model_OpenId(); // assign the model
		$this->openId->dbSearchTable();
	}

	public function allowManageUsers() {
		// set the openId
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['uee_user_id'] = $_GET['id'];
		$this->openId->dbSearchTable();
		if ($this->openId->search_results and count($this->openId->search_results) == 1)
		{
			$this->openId->data = $this->openId->search_results[0];
		}
		$this->openId->data['manage_users'] = 1;
		$this->openId->data['date_updated'] = date("Y-m-d H:i:s");
		$this->openId->dbUpdateRow();		
		$this->openId = new MVC_Model_OpenId(); // assign the model
		$this->openId->dbSearchTable();
	}

	public function removeManageUsers() {
		// set the openId
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['uee_user_id'] = $_GET['id'];
		$this->openId->dbSearchTable();
		if ($this->openId->search_results and count($this->openid->search_results) == 1)
		{
			$this->openId->data = $this->openId->search_results[0];
		}
		$this->openId->data['manage_users'] = 0;
		$this->openId->data['date_updated'] = date("Y-m-d H:i:s");
		$this->openId->dbUpdateRow();		
		$this->openId = new MVC_Model_OpenId(); // assign the model
		$this->openId->dbSearchTable();
	}

	public function loadPendingUsers() {
		// load the pending users
		$this->openId = new MVC_Model_OpenId(null);
		$this->openId->search['pending'] = 1;
		$this->openId->dbSearchTable();
	}

	public function loadAllUsers() {
		$this->openId = new MVC_Model_OpenId(null);
        $this->openId->search_results = $this->openId->getAllRows('', array('uee_user_id desc'));
	}

	/**
	 * email_confirmation()
	 * e-mail a user confirmation that his/her account has been approved
	 */
	public function email_notification() {
		return false; // for now - do nothing
	}

	public function __destruct() {
		parent::__destruct();
	}
	
}
?>
