<h1>OpenId Users</h1>
<p>Below is a list of users who have logged in to the UEE website and their current access level. You may change access levels as desired.</p>


<div id="approval">
<?php $this->loadAllUsers(); ?>
<table class="users dbrows" style="width:960px;margin:0 auto;">
<thead>
	<tr class='odd'>
		<th scope='col' style='width:5%;'>UEE User Id</th>
		<!--<th scope='col' style='width:35%;'>Identity</th> -->
		<th scope='col' style='width:20%;'>Name + Email, UCLA ID</th>
		<th scope='col' style='width:10%;'>Title, University / Organization</th>
		<th scope='col' style='width:10%;'>Status</th>
		<th scope='col' style='width:10%;'>Actions</th>
	</tr>
</thead>
<tbody>
	<?php $count = 0; ?>
	<?php foreach ($this->openId->search_results as $row): ?>
	<tr class='<?php echo ($count%2==0?'even':'odd'); ?>' >
		<td valign="top"><?php echo $row['uee_user_id']; ?></td>
		<!-- <td valign="top"><?php echo $row['identity']; ?></td> -->
		<td valign="top">
			<?php echo "{$row['firstname']} {$row['lastname']}"; ?> <br />
			<?php echo $row['email']; ?> <br />
			<?php echo $row['university_id']; ?>
		</td>
		<td valign="top"><?php echo "{$row['category']}, {$row['university_org']}"; ?></td>
		<td valign="top">
		<?php
			$p = $this->permissions($row);
			echo implode(",<br/>",$p);
		?>
		</td>
		<td valign="top">
			<a href="<?php echo SITE_PATH; ?>/?module=users&id=<?php echo $row['uee_user_id']; ?>&event=approve">Approve</a><br />
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=reject">Reject</a><br />
			<?php
				if ($this->hasPermission("manage users") && !$this->hasPermission('admin')) {
            ?>
                <a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=delete">Delete User</a><br />
			<?php
                }
			?>
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=allowEdit">Grant Admin Access</a><br />
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=removeEdit">Revoke Admin Access</a><br />
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=allowEdit">Can Edit</a><br />
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=removeEdit">Remove Editing privileges</a><br />
			<?php if ($this->hasPermission("admin")): ?>
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=allowManageUsers">Can Manage Users</a><br />
			<a href="<?php echo SITE_PATH; ?>?module=users&id=<?php echo $row['uee_user_id']; ?>&event=removeManageUsers">Revoke User Management Privileges</a><br />
			<?php endif; ?>
		</td>
		
	</tr>
	<?php $count++; ?>
	<?php endforeach; ?>
</tbody>
</table>
</div>

