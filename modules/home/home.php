<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class home extends MVC_Authentication_Edit {
		
		public $modules = array();
		private $ignores = array('gloss_name','feature_type','geo_data','geo_time','myuee_annotation', 'open_id',
			'place_name','place_name_type','place_tree','time_period_name',
			'geodata_type','place_relation_type','place_transition_type','place_type','time_type',
			'uee_user'); // are there any modules we want to ignore?
		
		public function __construct() {
			parent::__construct();
			
			// hrm...populate modules,yea?
			$this->prepare("SHOW TABLES");
			$this->execute();
			$meta = $this->result_metadata();			
			while ($field = $meta->fetch_field())
				$params[] = &$row[$field->name];
			call_user_func_array(array($this->stmt,'bind_result'),$params);
			while ($this->fetch()) {
				foreach ($row as $key => $val)
					$c[$key] = $val;
				$result[] = $c;
			}
			
			if ($result)
			{
				foreach ($result as $row)
				{
					if (!in_array($row['Tables_in_'.DB_NAME], $this->ignores) )
						$this->modules[] = $row['Tables_in_'.DB_NAME];
				}
			}			
		}
		
		public function __destruct() {
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}

