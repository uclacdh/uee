<style type="text/css">
	div#head #banner-wrapper {
		background-image: none;
	}
</style>
	<h1>Egyptology Database Entry</h1>
	<h2>Modules</h2>
	<ul>
		<?php
			foreach ($this->modules as $module)
			{
				echo "<li><a href='".SITE_PATH."/?module=".strtolower(str_replace("_","",$module))."&class=input'>{$module}</a></li>";
			}
		?>
		<li><a href='<?php echo SITE_PATH; ?>/?module=endnote'>Endnote XML converter</a></li>
		<li><a href='<?php echo SITE_PATH; ?>/?module=refcheck&class=refcheck'>Article references checking utility</a></li>
		<li><a href='<?php echo SITE_PATH; ?>/checkCache.php'>Article Cache utility</a></li>
		<?php if ($this->hasPermission('manage users')): ?>
		<li><a href="/users">Manage User accounts</a></li>
		<?php endif; ?>
	</ul>	
	<ul>
		<li><a href='/'>Public Site</a></li>
	</ul>
