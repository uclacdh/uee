<?php 

	/*
	input.php
	Contains the input module for the ueeuser db table
	
	Usage: actual module for this application
	*/

class input extends MVC_Authentication_Edit {

	public $category; 
	public $parentCategory; // placeholder category has a parent;
	public $catnames;
	public $ignore_fields = array('category_id','create_datetime','modify_datetime'); // what fields do we want to ignore when doing data entry?
	
	public function __construct($id=null) {
		parent::__construct();
		$this->category = new MVC_Model_Category($id); // assign the model
		$allrows = $this->category->getAllRows();
		$this->catnames = array(); // cat_id => cat_name
		foreach ($allrows as $cat)
		{
			$this->catnames[$cat['category_id']] = $cat['category'];
		}
	}
	
	public function __destruct() {
		parent::__destruct();
	}

	// event listeners
	public function insertRow() {
		// validate fields?
		// some validation handled by dbInsertRow()		
		// TODO
		$data = array();
		
		$data['category_id'] = NULL;
		$data['category'] = $_POST['category'];
		$data['escholar_article_id'] = $_POST['escholar_article_id'];
		$data['escholar_url'] = $_POST['escholar_url'];
		$data['escholar_comment'] = $_POST['escholar_comment'];
		$data['arkId'] = $_POST['arkId'];
		$data['parent_category_id'] = $_POST['parent_category_id'];
		$data['active_flag'] = true;
		$data['create_datetime'] = $data['modify_datetime'] = date("Y-m-d H:i:s");
		// insert to the database
		$this->category->dbInsertRow($data);
	}
	
	public function updateRow() {
		// validate fields?
		// TODO
		// migrate from $post to $gloss
		$data = array();
		$conditions = array();
		
		$conditions['category_id'] = $_POST['category_id'];
		$data['category'] = $_POST['category'];
		$data['escholar_article_id'] = $_POST['escholar_article_id'];
		$data['escholar_url'] = $_POST['escholar_url'];
		$data['escholar_comment'] = $_POST['escholar_comment'];
		$data['arkId'] = $_POST['arkId'];
		$data['parent_category_id'] = $_POST['parent_category_id'];
		$data['sort_order'] = $_POST['sort_order'];
		$data['modify_datetime'] = date("Y-m-d H:i:s");

		// insert to the database
		$this->category->dbUpdateRow($data,$conditions);
	}

	// TODO 4/11/11 - what are we using this for?	
	public function editRow() {
		// presumably, we need a row ID
		if ($_POST['row_id'] and is_numeric($_POST['row_id']) )
			$id = $_POST['row_id'];
		$this->category = new MVC_Model_Category($id); // assign the model
	}
	
	// some display helper functions	
	public function displayCatTree() {
		$catTree = $this->category->getTree2();
		foreach ($catTree as $key=>$branch) {
			$this->displayTreeBranch($branch,0,$this->category->data['parent_category_id']);
		}
	}

	public function deleteRow() {
		$conditions = array("category_id"=>$_POST['category_id']);
		$this->category->deleteRow($conditions);
	}
	
	public function displayTreeBranch($branch,$depth=0,$true_val) {
		// a branch has category_id, category_name, and children
		echo "<option value='{$branch['category_id']}' ".($true_val==$branch['category_id']?"selected='selected'":"").">";
		echo str_pad($branch['category_name'],strlen($branch['category_name'])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($branch['children']))
			foreach ($branch['children'] as $key=>$lowerBranch)
				$this->displayTreeBranch($lowerBranch,$depth+1,$true_val);
		return;
	}	

}
