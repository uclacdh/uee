<!--
	editcategory.tpl
	An input template for the gloss module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Update a Category</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=updateRow" id="categoryform" class="dataentry">

<p id="catname_0" class="catname_row">
	<label for="name_field_0">Name</label>
	<input type="text" id="name_field_0" name="category" maxlength="100" value="<?php echo $this->category['category']; ?>" />
</p>
<p>
	<label for="parent_category_id">Parent Category ID</label>
	<select id="parent_category_id" name="parent_category_id" >
		<option value=""></option>
		<?php $this->displayCatTree($this->category['parent_category_id']); ?>
	</select>
</p>
<p>
	<label for="sort_order">Sort Order</label>
	<input type="text" id="sort_order" name="sort_order" maxlength="2" value="<?php echo $this->category['sort_order']; ?>" />
</p>

<fieldset><legend>Main Article</legend>
<p>
	<label for="escholar_id">eScholar Article ID</label>
	<input type="text" id="escholar_id" name="escholar_article_id" maxlength="10" value="<?php echo $this->category['escholar_article_id']; ?>" />
</p>
<p>
	<label for="escholar_url">eScholar URL</label>
	<input type="text" id="escholar_url" name="escholar_url" maxlength="500" value="<?php echo $this->category['escholar_url']; ?>" />
</p>
<p>
	<label for="escholar_comment">eScholar Comment (1000 character max)</label>
	<textarea id="escholar_comment" name="escholar_comment" ><?php echo $this->category['escholar_comment']; ?></textarea>
</p>
<p>
	<label for="arkId">UCLA Library DLCS arkId</label>
	<input type="text" id="arkId" name="arkId" maxlength="100" value="<?php echo $this->category['arkId']; ?>" />
</p>
</fieldset>
<input type="hidden" name="category_id" value="<?php echo $this->category['category_id']; ?>" />
<p id="categoryform_submit" class="submit" ><input type="submit" name="insert_record" value="Update" /></p>

</form>

