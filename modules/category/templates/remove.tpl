<!--
	remove.tpl
	An input template for the category module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Confirm Deletion</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=deleteRow" id="categoryform" class="dataentry">

<p>Please confirm you want to delete this record</p>
<?php
	foreach ($this->category as $fieldname=>$val)
		echo "<p>{$fieldname}: $val</p>";
?>

<input type="hidden" name="category_id" value="<?php echo $this->category['category_id']; ?>" />
<p id="categoryform_submit" class="submit" ><input type="submit" name="insert_record" value="Delete" /></p>
</form>
