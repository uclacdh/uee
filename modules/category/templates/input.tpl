<!--
	input.tpl
	A input template for the category module
	
	Andrew Tai
	Nov 4, 2008
	
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>
<h1 class="tablename"><?php echo $this->moduleName; ?> table</h1>
<div style="width:50%;float:left;">
	<a id="toTable" style='display:none;' href="javascript:;">Table View</a>
	<a id="toTree" href="javascript:;">Tree View</a>
</div>
<p id="insertbox" style="width:50%;float:left;text-align:right;margin-top:0;padding-top:0;"><a href="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=newcategory"><img src="<?php echo SITE_PATH; ?>/graphics/plus.gif" alt=""></a> <a href="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=newcategory">Add a new category</a></p>
<div class='clearfix'></div>
<div id="tableView">
<table class="dbrows" id="category-table">
	<thead>
	<tr>
		<th scope='col'>ID</th>
		<th scope='col'>Name(s)</th>
		<th scope='col'>eScholar Main Article</th>
		<th scope='col'>UCLA Library DLCS arkId</th>
		<th scope='col'>Parent Category</th>
		<th scope='col' style="visibility:hidden;">Edit</th>
		<th scope='col' style="visibility:hidden;">Delete</th>
	</tr>
	</thead>
	<tbody>
	<?php
		$rows=$this->category->getAllRows();
		if (empty($rows))
			echo "<tr><td colspan='5' >No results found </td></tr>";
		else
			foreach ($rows as $row)
			{
				echo "<tr>";
				echo "<td>{$row['category_id']}</td>";
				// echo "<td>".htmlentities($row['category'])."</td>";
				echo "<td>".$row['category']."</td>";
				echo "<td>";
				echo "Article ID: ".$row['escholar_article_id'] . "<br />";
				echo "URL: <a href='{$row['escholar_url']}' target='_new'>".htmlentities($row['escholar_url']) . "</a><br />";
				echo "Comment: ".htmlentities($row['escholar_comment']);
				echo "</td>";
				echo "<td>{$row['arkId']}</td>";				
				echo "<td>".htmlentities($row['parent_category_id']?$this->catnames[$row['parent_category_id']]:'---')."</td>";
				// access to edit
				echo "<td>";
				echo "<form action='".SITE_PATH."/?module=category&class=editcategory' method='POST' >";
				// NOTE - have to identify which field to populate in value for this row_id
				echo "<input type='hidden' name='row_id' value='{$row['category_id']}' />";
				echo "<input type='submit' name='edit_row' value='edit' /> ";
				echo "</form>";
				echo "</td>";
				echo "<td>";
				echo "<form action='".SITE_PATH."/?module=category&class=remove' method='POST' >";
				echo "<input type='hidden' name='row_id' value='{$row['category_id']}' />";
				echo "<input type='submit' name='delete_row' value='del' />";
				echo "</form>";
				echo "</td>";
				echo "</tr>";
			}
	?>
	</tbody>
</table>
</div>
<!-- tree view -->
<div id="treeView" style="display:none;">
<?php
	// $this->category->displayCatTree();
	$this->category->displayCatTree2();
?>
</div>
