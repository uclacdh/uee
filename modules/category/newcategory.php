<?php 

	/*
	newcategory.php
	Contains a variant input module for creating new category records
	
	Oct 28, 2008 
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class newcategory extends MVC_Authentication_Edit {

	public $category;
	// You might need to add some other fields here
	public $catnames;		
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
		$this->category = new MVC_Model_Category($id); // assign the model
		$allrows = $this->category->getAllRows();
		$this->catnames = array(); // cat_id => cat_name
		foreach ($allrows as $cat)
		{
			$this->catnames[$cat['category_id']] = $cat['category'];
		}
	}

	public function __destruct() {
		parent::__destruct();
	}
	
	// some display helper functions	
	public function displayCatTree() {
		$catTree = $this->category->getTree3();
		foreach ($catTree as $cat_id=>$row) {
			if (!$row['parent']) // top level
			{
				$this->displayTreeBranch($cat_id,$depth=0,$catTree);
			}
		}
	}	
	
	protected function displayTreeBranch($cat_id,$depth,$catTree,$true_val = '') {
		// a branch has category_id, category_name, and children
		$row = $catTree[$cat_id];
		echo "<option value='{$cat_id}' ".($true_val==$cat_id?"selected='selected'":"").">";
		echo str_pad($this->catnames[$cat_id],strlen($this->catnames[$cat_id])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayTreeBranch($child_id,$depth+1,$catTree, $true_val);
		return;
	}		
	
}
