<?php 

	/*
	editcategory.php
	Contains a variant input module for updating category records
	
	Nov 4, 2008 
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class editcategory extends MVC_Authentication_Edit {

	public $category;
	public $catnames;
	private $cat_db;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
                if (!$id and !$_POST['row_id'])
                        die("no category_id to remove");
                $id = $_POST['row_id'];
                $conditions = array("category_id"=>$id);
                $this->cat_db = new MVC_Model_Category();
                $matches = $this->cat_db->dbSearchTable($conditions);
                $this->category = $matches[0];
		$allrows = $this->cat_db->getAllRows();
		$this->catnames = array(); // cat_id => cat_name
		foreach ($allrows as $cat)
		{
			$this->catnames[$cat['category_id']] = $cat['category'];
		}
	}

	public function __destruct() {
		parent::__destruct();
	}

	// some display helper functions	
	public function displayCatTree($true_val='') {
		$catTree = $this->cat_db->getTree3();
		foreach ($catTree as $cat_id=>$row) {
			if (!$row['parent']) // top level
			{
				$this->displayTreeBranch($cat_id,$depth=0,$catTree,$true_val);
			}
		}
	}	
	
	protected function displayTreeBranch($cat_id,$depth,$catTree,$true_val = '') {
		// a branch has category_id, category_name, and children
		$row = $catTree[$cat_id];
		echo "<option value='{$cat_id}' ".($true_val==$cat_id?"selected='selected'":"").">";
		echo str_pad($this->catnames[$cat_id],strlen($this->catnames[$cat_id])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayTreeBranch($child_id,$depth+1,$catTree, $true_val);
		return;
	}	
	
}
