<!--
	edittimeperiod.tpl
	An input template for the gloss module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Update a Time Period</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=updateRow" id="timeperiodform" class="dataentry">
<p>
	<label for="year_start">Year Start</label>
	<input type="text" id="year_start" name="year_start" maxlength="6" value="<?php echo ( ($this->timeperiod_record['year_start'] < 0)?substr($this->timeperiod_record['year_start'],1):$this->timeperiod_record['year_start']); ?>" />
	<label for="year_start_bce" style="display:none;">BCE/CE?</label>
	<select id="year_start_bce" name="year_start_bce" >
		<option value="1" <?php echo ( ($this->timeperiod_record['year_start'] < 0)?'selected="selected"':''); ?> >BCE</option>
		<option value="0" <?php echo ( ($this->timeperiod_record['year_start'] >= 0)?'selected="selected"':''); ?> >CE</option>
	</select>
</p>
<p>
	<label for="year_end">Year Start</label>
	<input type="text" id="year_end" name="year_end" maxlength="6" value="<?php echo ( ($this->timeperiod_record['year_end'] < 0)?substr($this->timeperiod_record['year_end'],1):$this->timeperiod_record['year_end']); ?>" />
	<label for="year_end_bce" style="display:none;">BCE/CE?</label>
	<select id="year_end_bce" name="year_end_bce" >
		<option value="1" <?php echo ( ($this->timeperiod_record['year_end'] < 0)?'selected="selected"':''); ?> >BCE</option>
		<option value="0" <?php echo ( ($this->timeperiod_record['year_end'] >= 0)?'selected="selected"':''); ?> >CE</option>
	</select>
</p>
<p style="width:100%;">
	<label for="parent_time_period_id">Parent Time Period</label>
	<select id="parent_time_period_id" name="parent_time_period_id">
		<option value=""></option>
		<?php $this->displayTimeTree($this->timeperiod_record['parent_time_period_id']); ?>
	</select>
</p>
<p><em>* blank names = will be removed</em></p>
<?php
	$this->listTimePeriodNames();
?>

<input type="hidden" name="time_period_id" value="<?php echo $this->timeperiod_record['time_period_id']; ?>" />
<p id="timeperiodform_submit" class="submit" ><input type="submit" name="update_record" value="Update" /></p>

</form>

