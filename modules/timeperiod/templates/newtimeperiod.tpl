<!--
	newgloss.tpl
	An input template for the gloss module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Add a Time Period</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=insertRow" id="timeperiodform" class="dataentry">

<p>
	<label for="year_start">Year Start</label>
	<input type="text" id="year_start" name="year_start" maxlength="6" />
	<label for="year_start_bce" style="display:none;">BCE/CE?</label>
	<select id="year_start_bce" name="year_start_bce" >
		<option value="1" selected="selected" >BCE</option>
		<option value="0">CE</option>
	</select>
</p>
<p>
	<label for="year_end">Year End</label>
	<input type="text" id="year_end" name="year_end" maxlength="6" />
	<label for="year_end_bce" style="display:none;">BCE/CE?</label>
	<select id="year_end_bce" name="year_end_bce" >
		<option value="1" selected="selected" >BCE</option>
		<option value="0">CE</option>
	</select>
</p>
<p style="width:100%;">
	<label for="parent_time_period_id">Parent Time Period</label>
	<select id="parent_time_period_id" name="parent_time_period_id">
		<option value=""></option>
		<?php $this->displayTimeTree(); ?>
	</select>
</p>
<p id="timeperiodname_0" class="timeperiodname_row">
	<label for="name_field_0">Name</label>
	<input type="text" id="name_field_0" name="timeperiod_name[]" maxlength="100" />
	<a href="javascript:;"><img src="<?php echo SITE_PATH; ?>/graphics/plus.gif" alt="" /></a> <a href="javascript:;">More</a>
</p>
<p id="timeperiodform_submit" class="submit" ><input type="submit" name="insert_record" value="Add" /></p>

</form>

