<?php 

	/*
	edittimeperiod.php
	Contains a variant input module for updating timeperiod records
	
	Nov 4, 2008 
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class edittimeperiod extends MVC_Authentication_Edit {

	public $timeperiod;
	public $timeperiod_db;
	public $timeperiod_record;
	public $timeperiod_name;
	public $timeperiodname_db;
	public $tpnames;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();

		$this->timeperiod_db = new MVC_Model_TimePeriod();
		$this->timeperiodname_db = new MVC_Model_TimePeriodName();
		if (!$_POST['row_id'] and !$id)
			die("error: no timeperiod to update");
		$id = $_POST['row_id'];
		// load the record
		$results = $this->timeperiod_db->dbSearchTable(array("time_period_id"=>$id));
		$this->timeperiod_record = $results[0];
		$this->tpnames = array(); // let's preload this; time_period_id => array('time_period_name',...)
		$allnames = $this->timeperiodname_db->getAllRows();
		foreach ($allnames as $tname)
		{
			if (!in_array($tname['time_period_id'],array_keys($this->tpnames)) )
				$this->tpnames[$tname['time_period_id']] = array();
			$this->tpnames[$tname['time_period_id']][] = $tname['time_period_name'];
		}			
	}

	public function __destruct() {
		parent::__destruct();
	}

	public function listTimePeriodNames($timeperiod_id='')
	{
		if (!$timeperiod_id and !($this->timeperiod_record['time_period_id']))
			die("error: no timeperiod_id");
		if (!$timeperiod_id)
			$timeperiod_id = $this->timeperiod_record['time_period_id'];
		$timeperiod_names = $this->timeperiod_db->getTimePeriodNames($timeperiod_id);
		$names = array();
		$count = 0;
		if (!$timeperiod_names)
		{
			echo "<p id='timeperiodname_{$count}' class='timeperiodname_row'>";
			$names[] = $timeperiod_names->data['time_period_name'];
			echo "<label for='name_field_{$count}'>Name</label>";
			echo "<input type='text' id='name_field_{$count}' name='timeperiod_name[]' maxlength='100' value='{$timeperiod_names->data['time_period_name']}' />";
			echo "<input type='hidden' name='timeperiod_name_id[]' value='{$timeperiod_names->data['time_period_name_id']}' />";
			echo "<a href='javascript:;'><img src='".SITE_PATH."/graphics/plus.gif' alt='' /></a> <a href='javascript:;'>More</a>";
			echo "</p>";		
		}
		foreach ($timeperiod_names as $tpname)
		{
			echo "<p id='timeperiodname_{$count}' class='timeperiodname_row'>";
			$names[] = $tpname->data['time_period_name'];
			echo "<label for='name_field_{$count}'>Name</label>";
			echo "<input type='text' id='name_field_{$count}' name='timeperiod_name[]' maxlength='100' value='{$tpname}' />";
			echo "<input type='hidden' name='time_period_name_id[]' value='{$tpname}' />";
			if ($count == 0)
			{
				echo "<a href='javascript:;'><img src='".SITE_PATH."/graphics/plus.gif' alt='' /></a> <a href='javascript:;'>Add</a>";
			}
			echo "</p>";
			$count++;
		}
		return true;
	}
	
	public function displayTimeTree($true_val) {
		$timeTree = $this->timeperiod_db->getTree3($this->tpnames);
		foreach ($timeTree as $tp_id=>$row) {
			if (!$row['parent']) // top level
			{
				$this->displayTreeBranch($tp_id,$depth=0,$timeTree,$true_val);
			}
		}
	}
	
	protected function displayTreeBranch($tp_id,$depth,$timeTree,$true_val = '') {
		// a branch has category_id, category_name, and children
		$row = $timeTree[$tp_id];
		echo "<option value='{$tp_id}' ".($true_val==$tp_id?"selected='selected'":"").">";
		echo str_pad($this->tpnames[$tp_id][0],strlen($this->tpnames[$tp_id][0])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayTreeBranch($child_id,$depth+1,$timeTree, $true_val);
		return;
	}
	
}
?>
