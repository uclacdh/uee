<?php 

	/*
	newtimeperiod.php
	Contains a variant input module for creating new timeperiod records
	
	Nov 4, 2008 
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class newtimeperiod extends MVC_Authentication_Edit {

	public $timeperiod;
	public $timeperiod_name;
	public $tpnames;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
		$this->timeperiod = new MVC_Model_TimePeriod($id); // assign the model
		$this->timeperiod_name = new MVC_Model_TimePeriodName(null); // assign the model
		$this->tpnames = array(); // let's preload this; time_period_id => array('time_period_name',...)
		$allnames = $this->timeperiod_name->getAllRows();
		foreach ($allnames as $tname)
		{
			if (!in_array($tname['time_period_id'],array_keys($this->tpnames)) )
				$this->tpnames[$tname['time_period_id']] = array();
			$this->tpnames[$tname['time_period_id']][] = $tname['time_period_name'];
		}
	}

	public function __destruct() {
		parent::__destruct();
	}
	
	// some display helper functions	
	public function displayTimeTree($true_val) {
		$timeTree = $this->timeperiod->getTree3($this->tpnames);
		foreach ($timeTree as $tp_id=>$row) {
			if (!$row['parent']) // top level
			{
				$this->displayTreeBranch($tp_id,$depth=0,$timeTree,$true_val);
			}
		}
	}
	
	protected function displayTreeBranch($tp_id,$depth,$timeTree,$true_val = '') {
		// a branch has category_id, category_name, and children
		$row = $timeTree[$tp_id];
		echo "<option value='{$tp_id}' ".($true_val==$tp_id?"selected='selected'":"").">";
		echo str_pad($this->tpnames[$tp_id][0],strlen($this->tpnames[$tp_id][0])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayTreeBranch($child_id,$depth+1,$timeTree, $true_val);
		return;
	}	
}
?>
