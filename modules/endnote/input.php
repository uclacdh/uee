<?php 

	/*
	input.php
	Contains the input module for the endnote db table
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class input extends MVC_Authentication_Edit {

	public $file;
	// You might need to add some other fields here
	
	public function __construct($id=null) {
		parent::__construct();
		$this->file = '';
		// did we get a post result
		if ($_POST['submitbutton'] == 'Convert') // todo: add more checks
		{
			if (count($_FILES) > 1)
				die("How did you upload more than one file?");
			$file = $_FILES['filepath'];
			if ($file['type'] != 'text/xml')
				die("You must enter a file with a proper XML format");
			if (!is_uploaded_file($file['tmp_name']))
				die("There was an error processing your file");
			// let's try converting this to an XML DOM object
			$this->file = $file;
			
			// print_r($file);
			// echo '<br />';

		}
	}

	public function __destruct() {
		parent::__destruct();
	}
	
	// event listener
	public function upload() {
		;
	}
	
	public function convertEndnoteXml() {
		if (!$_POST['submitbutton'] == 'Convert')
		{
			echo 'You must select a file first';
			return false;
		} 
		if (!file_exists($this->file['tmp_name']))
			die("There was an error processing your file: Cannot find specified file"."<br />");

		$teiUrl = 'http://www.tei-c.org/ns/1.0';
		$xmlUrl = 'http://www.w3.org/XML/1998/namespace';
		// open the file, want to manipulate and setup a xml document
		$dom = new DOMDocument();
		$dom->load($this->file['tmp_name']);
		// let's set up an xpath because we'll want to specifically attack certain pieces of the xml
		$xpath = new DOMXPath($dom);	
		$convertedDom = new DOMDocument('1.0','utf-8'); // this is where we'll place our conversion
		// let's start the tei references object
		$refDiv = $convertedDom->createElementNS($teiUrl,'tei:div');
		$refType = $convertedDom->createAttribute('type');
		$refType->nodeValue='references';
		$refDiv->appendChild($refType);
		$head = $convertedDom->createElementNS($teiUrl,'tei:head');
		$head->nodeValue = 'References';
		$refDiv->appendChild($head);
		// let's start a listBibl object
		$listBibl = $convertedDom->createElementNS($teiUrl,'tei:listBibl');
		$records = $xpath->query("//record");
		// echo $records->length . "\n"; 
		$searchXpath = new DOMXPath($dom);
		$count = 1; // xpath starts at 1, not 0 (so weird!)
		foreach ($records as $record)
		{
			// get the bibliographic reference type, determine how to process this record
			$query = "//record[$count]//ref-type";
			$refType = $searchXpath->query($query);
			foreach ($refType as $rtype) // should only ever be 1
			{
				$type = $rtype->getAttributeNode('name')->value;
			}
			$biblStruct = $convertedDom->createElementNS($teiUrl,'tei:biblStruct');
			// apply the id value
			$idList = $searchXpath->query("//record[$count]/rec-number");
			// $biblStruct->appendChild($idAttr);
			foreach ($idList as $node)
			{
				// this doesn't actually seem to attach the xml namespace... blah
				// temporary hack - handled in output...
				$biblStruct->setAttributeNS($xmlUrl,"xml:id",$node->nodeValue);
			}
			$analytic = $convertedDom->createElementNS($teiUrl,'tei:analytic');
			$monogr = $convertedDom->createElementNS($teiUrl,'tei:monogr','');
			// get the authors?
			$query = "//record[$count]//contributors/*";
			$contributors = $searchXpath->query($query);
			foreach ($contributors as $authorType)
			{
				// sometimes we have empty nodes
				if (!$authorType->hasChildNodes())
					continue;
				// here, we have a set of authors
				foreach ($authorType->childNodes as $author)
				{
					if ($author->nodeName != 'author')
						continue;
					// echo $author->nodeName."\t".$author->nodeValue."\n";
					$author = $convertedDom->createElementNS($teiUrl,'tei:author',trim($author->nodeValue));
					$editor = $convertedDom->createElementNS($teiUrl,'tei:editor',trim($author->nodeValue));
					if ($authorType->nodeName == 'translated-authors')
					{
						// create a resp statement
						$respStmt = $convertedDom->createElementNS($teiUrl,'tei:respStmt','');
						// resp
						$resp = $convertedDom->createElementNS($teiUrl,'tei:resp','Translated by');
						$respStmt->appendChild($resp);
						// name
						$transName = $convertedDom->createElementNS($teiUrl,'tei:name',trim($author->nodeValue));
						$respStmt->appendChild($transName);
						$monogr->appendChild($respStmt);
					}
					else if ($type == 'Book')
						$monogr->appendChild($author);
					else if ($type == 'Journal Article')
					{
						// which title is this?
						// echo $titleType->localName.'<br />';
						if ($authorType->localName == 'authors')
						{
							$analytic->appendChild($author);
						}
						else 
						{
							$monogr->appendChild($author);
						}		
					}
					else if ($type == 'Book Section')
					{
						if ($authorType->localName == 'authors')
						{
							$analytic->appendChild($author);
						}
						else // secondary authors
						{
							$monogr->appendChild($editor);
						}						
					}		
					else if ($type == 'Edited Book') // all authors become editors
					{
						$monogr->appendChild($editor);
					}
					else
					{
						$monogr->appendChild($author);
					}
				}
			}
			// get the titles?
			$query = "//record[$count]//titles/*";
			$titles = $searchXpath->query($query);
			foreach ($titles as $titleType)
			{
				if (!$titleType->hasChildNodes()) // filter out empty nodes
					continue;
				// here, we have a titleType with a style tag underneath it
				foreach ($titleType->childNodes as $style)
				{
					if ($style->nodeName != 'style')
						continue;
					// question about title's level attribute: http://www.tei-c.org/release/doc/tei-p5-doc/en/html/CO.html#COBICO
					// echo $style->nodeName."\n";
					$title = $convertedDom->createElementNS($teiUrl,'tei:title',trim($style->nodeValue));
					// note; what to do with titles changes depending on type of this record
					if ($type == 'Book')
					{
						// is this a book in a series?
						if ($titleType->localName == 'secondary-title')
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);
							$title->setAttribute('level','s');
							// do we need to add a number?
							$query2 = "//record[$count]/number/*";
							$numbers = $searchXpath->query($query2);
							foreach ($numbers as $number)
							{							
								$title->nodeValue .= ' '.$number->nodeValue;
							}
						}
						else if ($titleType->localName == 'tertiary-title')
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);
							$title->setAttribute('level','m');							
						}
						$monogr->appendChild($title);
					}
					else if ($type == 'Journal Article')
					{
						// which title is this?
						// echo $titleType->localName.'<br />';
						if ($titleType->localName == 'title')
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);
							$title->setAttribute('level','a');
							$analytic->appendChild($title);
						}
						else 
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);						
							$title->setAttribute('level','j');
							$monogr->appendChild($title);
						}		
					}
					else if ($type == 'Book Section')
					{
						// which title is this?
						// echo $titleType->localName.'<br />';
						if ($titleType->localName == 'title')
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);
							$title->setAttribute('level','a');						
							$analytic->appendChild($title);						
						}
						// is this a book in a series?
						if ($titleType->localName == 'secondary-title')
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);
							$title->setAttribute('level','m');
							// do we need to add a number?
							$query2 = "//record[$count]/number/*";
							$numbers = $searchXpath->query($query2);
							foreach ($numbers as $number)
							{							
								$title->nodeValue .= ' '.$number->nodeValue;
							}
							$monogr->appendChild($title);						
						}
						else if ($titleType->localName == 'tertiary-title')
						{
							$level = $convertedDom->createAttribute('level');
							$title->appendChild($level);
							$title->setAttribute('level','s');							
							$monogr->appendChild($title);						
						}
					}
					else
					{
						$monogr->appendChild($title);					
					}
				}
			}
			// notes?
			$query = "//record[$count]//notes/*";
			$notes = $searchXpath->query($query);
			foreach ($notes as $note) {
				if ($note->nodeName != 'style') // empty text node
					continue;
				$noteTag = $convertedDom->createElementNS($teiUrl,'tei:note', trim($note->nodeValue));
				$monogr->appendChild($noteTag);
			}
			// do i need to add translator info?
			$query = "//record[$count]//work-type/*";
			$worktypes = $searchXpath->query($query);
			foreach ($worktypes as $worktype)
			{
				if ($worktype->nodeName != 'style')
					continue;
				if ($worktype->nodeValue != 'Translation')
					continue;
				;
			}
			// get the publishing info
			$imprint = $convertedDom->createElementNS($teiUrl,'tei:imprint');
			// publishing place?
			$query = "//record[$count]//pub-location/*";
			$locations = $searchXpath->query($query);
			foreach ($locations as $location)
			{
				if ($location->nodeName != 'style') // empty text node
					continue;
				$pubPlace = $convertedDom->createElementNS($teiUrl,'tei:pubPlace', trim($location->nodeValue));
				$imprint->appendChild($pubPlace);
			}
			// publisher?
			$query = "//record[$count]//publisher/*";
			$publishers = $searchXpath->query($query);
			foreach ($publishers as $publisherNode)
			{
				if ($publisherNode->nodeName != 'style') // empty text node
					continue;
				$publisher = $convertedDom->createElementNS($teiUrl,'tei:publisher', htmlspecialchars(trim($publisherNode->nodeValue),ENT_COMPAT,'UTF-8') );
				$imprint->appendChild($publisher);
			}	
			// publishing date
			$query = "//record[$count]//dates/year/*";
			$years = $searchXpath->query($query);
			foreach ($years as $year)
			{
				if ($year->nodeName != 'style') // empty text node
					continue;
				$date = $convertedDom->createElementNS($teiUrl,'tei:date', trim($year->nodeValue));
				$imprint->appendChild($date);
			}
			// some additional info?
			// pages?
			$query = "//record[$count]//pages/*";
			$pages = $searchXpath->query($query);
			foreach ($pages as $pagerange)
			{
				if ($pagerange->nodeName != 'style') // empty text node
					continue;
				$page = $convertedDom->createElementNS($teiUrl,'tei:biblScope', trim($pagerange->nodeValue));
				$biblScopeType = $convertedDom->createAttribute('type');
				$page->appendChild($biblScopeType);
				$page->setAttribute('type','pp');
				$imprint->appendChild($page);				
			}
			// volume?
			// number?
			$query = "//record[$count]//volume/*";
			$volumes = $searchXpath->query($query);
			foreach ($volumes as $volumeNode)
			{
				if ($volumeNode->nodeName != 'style') // empty text node
					continue;
				$volume = $convertedDom->createElementNS($teiUrl,'tei:biblScope', trim($volumeNode->nodeValue));
				$biblScopeType = $convertedDom->createAttribute('type');
				$volume->appendChild($biblScopeType);
				$volume->setAttribute('type','vol');
				$imprint->appendChild($volume);				
			}
			$query = "//record[$count]//number/*";
			if ($type == 'Journal Article')
			{
				$issueNumbers = $searchXpath->query($query);
				foreach ($issueNumbers as $issueNode)
				{
					if ($issueNode->nodeName != 'style') // empty text node
						continue;
					$issueNum = $convertedDom->createElementNS($teiUrl,'tei:biblScope', trim($issueNode->nodeValue));
					$biblScopeType = $convertedDom->createAttribute('type');
					$issueNum->appendChild($biblScopeType);
					$issueNum->setAttribute('type','issue');
					$imprint->appendChild($issueNum);
				}
			}
			
			
			$monogr->appendChild($imprint);
			// set up the biblStruct object
			if ($analytic->hasChildNodes())
				$biblStruct->appendChild($analytic);
			$biblStruct->appendChild($monogr);
			$listBibl->appendChild($biblStruct);	
			
			$count++;
			// echo "\n";
		}
		$refDiv->appendChild($listBibl);
		$convertedDom->appendChild($refDiv);
		// debugging to see 
		// would like the double_encode, not upgraded enough (need php 5.2.3)

		
		// $str = html_entity_decode($convertedDom->saveXML());
		// $str = htmlentities($str,ENT_COMPAT,'UTF-8');
		// $str = str_replace(htmlentities("><"),htmlentities(">").'<br />'.htmlentities("<"),$str);
		// echo $str;
		
		
		$this->displayDom($convertedDom);
		return true;
	}
	
	private function processBookRecord($dom,$convertedDom) {
		$teiUrl = 'http://www.tei-c.org/ns/1.0';
		$searchXpath = new DOMXPath($dom);	
		return $convertedDom;
	}
	
	private function displayDom($xmlDom) {
		$lines = '';
		$xmlDom->normalize();
		foreach ($xmlDom->childNodes as $childNode)
			$lines .= $this->displayDomNodes($childNode);
		echo $lines;
		return true;
	}
	
	private function displayDomNodes($node, $depth = 0) {
		$lines = '';
		$tag = "<".$node->nodeName;
		if ($node->hasAttributes())
		{
			foreach ($node->attributes as $attrName => $attrNode)
			{
				// temporary hack, having trouble with getting xml:id inserted into the dom properly
				if ($node->nodeName == 'tei:biblStruct' and $attrName == "id")
					$tag .= " xml:$attrName=\"bib{$attrNode->nodeValue}\"";				
				else
					$tag .= " $attrName=\"{$attrNode->nodeValue}\"";
			}
		}
		$tag .= ">";
		$endtag = "</".$node->nodeName .">";
		if (!$node->hasChildNodes())
		{
			if ($node->nodeValue)
			{
				if ($node->nodeName != "#text")
					$lines .= str_repeat('&nbsp;',($depth*4)).htmlentities($tag);
				$value = html_entity_decode($node->nodeValue,ENT_COMPAT,'UTF-8');
				// regarding double encoding: 
				// mar 18, 2009
				// 5 double encodings: http://uclaidre.projectpath.com/P20689458
				// matches htmlspecialchars!
				// $value = str_replace("&","&amp;",$value);
				// $value = str_replace(">","&gt;",$value);
				// $value = str_replace("<","&lt;",$value);
				// $value = str_replace("'","&apos;",$value);
				// $value = str_replace("\"","&quot;",$value);
				$value = htmlspecialchars($value,ENT_COMPAT,'UTF-8'); // testing double encoding
				$lines .= str_repeat('&nbsp;',(($depth)*4)).htmlentities($value,ENT_COMPAT,'UTF-8').'<br />';
				if ($node->nodeName != "#text")
					$lines .= str_repeat('&nbsp;',($depth*4)).htmlentities($endtag).'<br />';
			}
			else
				return '';
		}
		else
		{
			if ($node->nodeName != "#text")
				$lines .=  str_repeat('&nbsp;',($depth*4)).htmlentities($tag).'<br />';
			foreach ($node->childNodes as $childNode)
			{
				$lines .= $this->displayDomNodes($childNode,$depth+1);
			}
			if ($node->nodeName != "#text")
				$lines .= str_repeat('&nbsp;',($depth*4)).htmlentities($endtag).'<br />';
		}
		return $lines;
	}
	
}
?>
