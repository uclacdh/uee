	<h1>UEE Article References Checking Utility</h1>
	<form action='/?module=refcheck&class=refcheck' method='post'>
	<p><label for='articletitle'>Article: </label>
	<select id='articletitle' name='articletitle'>
		<?php
			foreach ($this->allArticleTitles as $title) {
				$formatted = str_replace(" ","_",$title);
				$selected = "";
				if ($formatted == $_POST['articletitle'])
					$selected = "selected='selected'";
				echo "<option value='{$formatted}' {$selected} >{$title}</option>";
			}
		?>
	</select>&nbsp;&nbsp;<input type='submit' id='analyzeButton' name='analyzeButton' value='Analyze' /></p>
	</form>
	<?php if ($this->loadedArticle): ?>
	<h2 style='font-size:1.2em;'>Analyzed Results</h2>
	<p style='font-size:0.8em;'><em>references appear in the order in which they are found in the TEI:XML markup. However, you can sort on the type by clicking on the 'Type' column header</em></p>
	<?php 
		$this->analyzeArticle();
	?>
	<?php endif; ?>
