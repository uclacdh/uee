<?php
	// a default Class file
	// invoked by using the URL /
	// typically extends from a generic Module object which has certain base behaviors 
	// this is an example that views don't have to connect to data models
	
	class refcheck extends MVC_Authentication_Edit {
		
		public $loadedArticle = null;
		public $glossRefs = null;
		public $bibRefs = null;
		public $placeRefs = null;
		
		
		public $glossNames = array();
		
		public $allArticleTitles = array();
		public $allImages = array();
		public $errors = array();
		
		
		public function __construct() {
			parent::__construct();		
			
			// can I get my soap service?
			$wsdl_url = WSDL;
			try {
				@$this->soap = new SoapClient($wsdl_url, array("cache_wsdl"=>WSDL_CACHE_NONE, "trace"=>true) );
			} catch (SoapFault $fault) {
				header('Location: /error/');
				exit();
				// trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
			}
			$this->loadTitles();
			$this->loadGlossNames();
			$this->loadPlaceNames();
			$this->loadImages();
			if ($_POST['articletitle']) {
				$this->loadedArticle = $_POST['articletitle'];
			}
		}
		
		private function loadGlossNames() {
			$glossObj = new MVC_Model_GlossName(null);
			$glossRows = $glossObj->getAllRows();
			foreach ($glossRows as $grow) {
				if (!$this->glossNames[$grow['gloss_id']])
					$this->glossNames[$grow['gloss_id']] = array();
				$this->glossNames[$grow['gloss_id']][] = $grow['gloss_name'];
			}
		}

		private function loadPlaceNames() {
			$placeObj = new MVC_Model_PlaceName(null);
			$placeRows = $placeObj->getAllRows();
			foreach ($placeRows as $prow) {
				if (!$this->placeNames[$prow['place_id']])
					$this->placeNames[$prow['place_id']] = array();
				if ($prow['place_name'])
					$this->placeNames[$prow['place_id']][] = $prow['place_name'];
			}
		}
		
		private function loadImages() {
			$allimages = $this->soap->listAllImages();
			$allimagesarr = array();
			$fullimagesarr = array();
			if ($allimages->return)
			{
				foreach ($allimages->return as $img)
				{
					$allimagesarr[$img->id] = $img->thumbnailURL;
				}
			}
			$this->allImages = $allimagesarr;
			if ($inlineImagesRes = false)
			{
				foreach ($inlineImagesRes as $graphic)
				{
					if ($graphic->hasAttribute('url') )
					{
						$this->imageRefs[$graphic->getAttribute('url')] = $allimagesarr[$graphic->getAttribute('url')];
					}
				}
			}
		
		}

		private function loadTitles() {
			$res = $this->soap->listAllArticles();
			if ($res->return) {
				foreach ($res->return as $article)
				{
					$this->allArticleTitles[] = $article->title;
				}
			}			
		}
		
		protected function analyzeArticle() {
			$targetsSeen = array();
			try {
				$xml = new DOMDocument;
				$filepath = $_SERVER['DOCUMENT_ROOT'].'/cache/articles/'.$this->loadedArticle.'.xml';
				if (!(file_exists($filepath)) )
					throw new Exception("requested article not found");
				$xml->load($filepath);
				$xpath = new DOMXPath($xml);
				$teiUrl = 'http://www.tei-c.org/ns/1.0';
				$xpath->registerNamespace('tei',$this->teiUrl);
				$refs = $xpath->query("//tei:ref | //tei:placeName | //tei:graphic");
				echo "<table id='referenceChecks' class='dbrows'>";
				echo "<thead>";
				echo "<tr><th scope='col'>Ref</th><th scope='col'>Type</th><th scope='col'>Target</th><th scope='col'>reference text</th><th scope='col'>database/expected value(s)</th></tr>";
				echo "</thead>";
				echo "<tbody>";
				for ($inti = 0; $inti < $refs->length; $inti++) {
					$type;
					$target;
					if ($inti % 2)
						$row = "odd";
					else
						$row = "even";
					echo "<tr class='{$row}'>";
					echo "<td>{$inti}</td>";
					$ref = $refs->item($inti);
					$attrCount = $ref->attributes->length;
					echo "<td>";
					for ($intj = 0; $intj < $attrCount; $intj++) {
						$attr = $ref->attributes->item($intj);
						if ($attr->name == 'type') {
							echo "{$attr->nodeValue}";
							$type = $attr->nodeValue;
						}
						if ($attr->name == 'key') {
							echo "placeName";
							$type = "placeName";
						}
						if ($attr->name == 'url') {
							echo "graphic";
							$type = "graphic";
						}
					}
					echo "</td>";
					echo "<td>";
					for ($intj = 0; $intj < $attrCount; $intj++) {
						$attr = $ref->attributes->item($intj);
						if ($attr->name == 'target' or $attr->name == 'key' or $attr->name == 'url') {
							echo "{$attr->nodeValue}";
							$target = $attr->nodeValue;
						}
					}
					echo "</td>";
					echo "<td>{$ref->nodeValue}</td>";
					// db/expected value, if any
					echo "<td>";
					switch ($type) {
						case "gloss":
							if (substr($target,0,4) != 'gold')
								echo str_replace($ref->nodeValue,"<strong>".$ref->nodeValue."</strong>",implode(", ",$this->glossNames[intval(substr($target,1))]) );
							break;
						case "placeName":
							if (substr($target,0,4) != 'pold')
								if ($ref->nodeValue)
									echo str_replace($ref->nodeValue,"<strong>".$ref->nodeValue."</strong>",implode(", ",$this->placeNames[intval(substr($target,1))]) );
								else
									echo implode(", ",$this->placeNames[intval(substr($target,1))]);									
							break;
						case "graphic":
							$imgUrl = $this->allImages[$target];
							echo "<img src='{$imgUrl}' alt='' />";
							break;
						default:
							echo "&nbsp;";
							break;
					}
					echo "</td>";
					echo "</tr>";
				}
				echo "</tbody>";
				echo "</table>";
			} catch (Exception $e) {
				$this->errors[] = $e->getMessage();
			}
		}
		
		public function __destruct() {
			parent::__destruct();
		}

		// some custom events
		// let's use this to toggle the code file in question
		
	}
?>
