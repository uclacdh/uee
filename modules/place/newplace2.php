<?php 

	/*
	newplace.php
	Contains the insert input module for the place db table
	
	Nov 4, 2008
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class newplace2 extends MVC_Authentication_Edit {

	public $place;
	public $place_db;
	public $placetypes;
	public $placenametypes;
	public $featuretypes;
	public $timetypes;
	public $timeperiod;
	public $timeperiod_name;
	public $tpnames;	
	public $timetree;
	public $featuretree;

	public $allplacenames;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
		$this->place = new MVC_Model_Place($id); // assign the model
		$this->place_db = new MVC_Model_Place();
		$this->placenames = new MVC_Model_PlaceName(null);
		$this->placenametypes = new MVC_Model_PlaceNameType(null);
		$this->placetypes = new MVC_Model_PlaceType(null);
		$this->featuretypes = new MVC_Model_FeatureType(null);
		$this->timetypes = new MVC_Model_TimeType(null);
		$this->timeperiod = new MVC_Model_TimePeriod(null);
		$this->timeperiod_name = new MVC_Model_TimePeriodName(null);
		// some preloading
		$this->timetyperows = $this->timetypes->getAllRows();
		$this->timetree = $this->timeperiod->getTree3($this->tpnames);
		$this->placerows = $this->place_db->getAllRows();
		$this->placenametyperows = $this->placenametypes->getAllRows();		
		$this->allplacenames = array(); // place_id => main_name
		$placenamerows = $this->placenames->getAllRows();
		foreach ($placenamerows as $row)
		{
			if ($row['place_name_type_id'] == 1) // MAIN
			{
				$this->allplacenames[ $row['place_id'] ] = $row['place_name'];
			}
		}
		$this->tpnames = array(); // let's preload this; time_period_id => array('time_period_name',...)
		$allnames = $this->timeperiod_name->getAllRows();
		foreach ($allnames as $tname)
		{
			if (!in_array($tname['time_period_id'],array_keys($this->tpnames)) )
				$this->tpnames[$tname['time_period_id']] = array();
			$this->tpnames[$tname['time_period_id']][] = $tname['time_period_name'];
		}
		$this->timeperiod_name = null;
		$this->featuretree = $this->featuretypes->getTree3();
		
	}

	public function __destruct() {
		$this->place = null;
		$this->placenames = null;
		$this->placenametypes = null;
		$this->placetypes = null;
		$this->featuretypes = null;
		$this->timetypes = null;
		$this->timeperiod = null;
		$this->timeperiod_name = null;
		parent::__destruct();
	}
	
	// event listeners
	// TODO
	
	
	// display helpers
	// TODO
	public function displayTimeTree() {
		$timeTree = $this->timetree;
		foreach ($timeTree as $tp_id=>$row) {
			if (!$row['parent']) // top level
			{
				$this->displayTimeTreeBranch($tp_id,$depth=0,$timeTree,$true_val='');
			}
		}
	}
	
	protected function displayTimeTreeBranch($tp_id,$depth,$timeTree,$true_val = '') {
		// a branch has category_id, category_name, and children
		$row = $timeTree[$tp_id];
		echo "<option value='{$tp_id}' ".($true_val==$tp_id?"selected='selected'":"").">";
		echo str_pad($this->tpnames[$tp_id][0],strlen($this->tpnames[$tp_id][0])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayTimeTreeBranch($child_id,$depth+1,$timeTree, $true_val);
		return;
	}	
	
	public function displayFeatureTypeTree() {
		$featureTypeTree = $this->featuretree;
		$toplevel = $this->featuretypes->getToplevel();
		foreach ($toplevel as $ft_id=>$name) {
			$row = $featureTypeTree[$ft_id];
			if (!$row['parent']) // top level
			{
				$this->displayFeatureTypeTreeBranch($ft_id,$depth=0,$featureTypeTree,$true_val='');
			}
		}
	}
	
	public function displayFeatureTypeTreeBranch($ft_id,$depth=0,$featureTypeTree,$true_val) {
		// a branch has category_id, category_name, and children
		$row = $featureTypeTree[$ft_id];
		echo "<option value='{$ft_id}' ".($true_val==$ft_id?"selected='selected'":"").">";
		echo str_pad($row['feature_type'],strlen($row['feature_type'])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayFeatureTypeTreeBranch($child_id,$depth+1,$featureTypeTree, $true_val);
		return;
	}	
	
	public function displayPlaceNameTypes() {
		$placeTypes = $this->placenametyperows;
		foreach ($placeTypes as $pnt)
		{
			echo "<option value='{$pnt['place_name_type_id']}' >".ucwords($pnt['place_name_type'])."</option>";
		}
		return;
	}
	
	public function displayPlaceTypes() {
		$placeTypes = $this->placetypes->getAllRows();
		foreach ($placeTypes as $pt)
		{
			echo "<option value='{$pt['place_type_id']}' >".ucwords($pt['place_type'])."</option>";
		}
		return;
	}
	
	public function displayTimeTypes() {
		$timeTypes = $this->timetyperows;
		foreach ($timeTypes as $tt)
		{
			echo "<option value='{$tt['time_type_id']}' >".ucwords($tt['time_type'])."</option>";
		}
		return;
	}
	
	public function displayRegions() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_REGION)
				{
/*					$children = $this->getChildrenPlaces($row['place_id']);
					$kidnames = array();
					foreach ($children as $child)
						$kidnames[] = $child['name'];
					$kidstr = implode(", ",$kidnames);
					echo '<p><input class="parent-place" type="checkbox" id="parent_region_'.$count.'" name="parent_regions[]" value="'.$row['place_id'].'" /> ';
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '<label class="parent-label" for="parent_region_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->placemainnames[$row['place_id']].'</label></p>';						
					else
						echo '<label class="parent-label" for="parent_region_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->place->getMainName($row['place_id']).'</label></p>';					
*/
					// TODO 4/11/11 - Fix the title display
					echo '<p><input class="parent-place" type="checkbox" id="parent_region_'.$count.'" name="parent_regions[]" value="'.$row['place_id'].'" />';
					echo '<label class="parent-label" for="parent_region_'.$count.'" >'.$this->allplacenames[ $row['place_id'] ].'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayGovernates() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_GOVERNATE)
				{
					echo '<p><input class="parent-place" type="checkbox" id="parent_governate_'.$count.'" name="parent_governates[]" value="'.$row['place_id'].'" /> ';
					echo '<label class="parent-label" for="parent_governate_'.$count.'" >'.$this->allplacenames[ $row['place_id'] ].'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayNomes() {
		$rows = $this->placerows;
		echo '<select id="parent_nomes_select" name="parent_nomes[]" >';
		echo '<option value=""></option>';
		if ($rows)
		{
			$count = 0;
			
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_NOME)
				{
					$count++;
					echo '<option value="'.$row['place_id'].'" >'.$this->allplacenames[ $row['place_id'] ].'</option>';					
				}
			}
		}
		echo '</select>';
		return;
	}
	
	public function displayAltNomes() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_ALTNOME OR $row['place_type_id'] == PLACE_TYPE_NOME)
				{
					echo '<p><input type="checkbox" id="parent_altnome_'.$count.'" name="parent_altnomes[]" value="'.$row['place_id'].'" /> ';
					echo '<label class="parent-label" for="parent_altnome_'.$count.'" >'.$this->allplacenames[ $row['place_id'] ].'</label></p>';	
					$count++;
				}
			}
		}
		return;
	}
	
	public function displaySites() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_SITE)
				{
/*
					$parents = $this->getParentPlaces($row['place_id']);
					$parnames = array();
					foreach ($parents as $parent)
						$parnames[] = $parent['name'];
					$parname = implode(", ",$parnames);	
*/
					echo '<p><input type="checkbox" id="parent_site_'.$count.'" name="parent_sites[]" value="'.$row['place_id'].'" /> ';
					echo '<label class="parent-label" for="parent_site_'.$count.'" >'.$this->allplacenames[ $row['place_id'] ].'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayFeatures() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_FEATURE)
				{
					echo '<p><input type="checkbox" id="parent_feature_'.$count.'" name="parent_features[]" value="'.$row['place_id'].'" /> ';
					echo '<label class="parent-label" for="parent_feature_'.$count.'" >'.$this->allplacenames[ $row['place_id'] ].'</label></p>';	
					$count++;
				}
			}
		}
		return;
	}
	
	// for the select box in the 'other' section
	public function displayOtherPlaces() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
					echo '<option value="'.$row['place_id'].'" >';
					echo htmlentities( $this->allplacenames[ $row['place_id'] ] );
					echo '</option>';
			}
		}
		return;		
	}
	
	public function displayChildrenPlaces($place_id, $depth = 5) {
		return;
		$children = $this->getChildrenPlaces($place_id);
		$names = array();
		foreach ($children as $child)
			$names[] = $child['name'];
		echo "<ul style='padding-left:1.5em;'>";
		echo "<li style='font-size:0.8em;padding-bottom:0.5em;'>".implode(", ",$names)."</li>";
		foreach ($children as $child) {
			$morechildren = $this->getChildrenPlaces($child['id'], $depth-1);
			if (!empty($morechildren))
			{
				echo "<li style='list-style-type:none;'>";
				echo $child['name']."<br />";
				$this->displayChildrenPlaces($child['id']);
				echo "</li>";
			}
		}
		echo "</ul>";
	}
	
	public function getChildrenPlaces($place_id, $depth = 3) {
		$children = array();
		if ($depth == 0)
			return $children;
		$rows = $this->place->query("SELECT place_id, place_name FROM place_name WHERE place_id IN (SELECT place_id_2 FROM place_tree WHERE place_id_1=$place_id AND place_relation_type_id=1) AND place_name_type_id=1 ORDER BY place_name");
		while ($row = mysqli_fetch_array($rows))
			$children[] = array('name'=>$row['place_name'],'id'=>$row['place_id']);
		return $children;
	}
	
	public function displayParentPlaces($place_id, $depth = 5) {
		$parents = $this->getParentPlaces($place_id);
		echo "<ul style='padding-left:1.5em;margin-bottom:0;margin-top:0;'>";
		foreach ($parents as $parent) {
			$moreparents = $this->getParentPlaces($parent['id'], $depth-1);
			if (!empty($moreparents))
			{
				$this->displayParentPlaces($parent['id']);
			}
			echo "<li>";
			echo $parent['name']."<br />";
			echo "</li>";
		}
		echo "</ul>";
	}	
	
	public function getParentPlaces($place_id, $depth = 3) {
		$parents = array();
		if ($depth == 0)
			return $children;
		$rows = $this->place->query("SELECT place_id, place_name FROM place_name WHERE place_id IN (SELECT place_id_1 FROM place_tree WHERE place_id_2=$place_id AND place_relation_type_id=1) AND place_name_type_id=1 ORDER BY place_name");
		while ($row = mysqli_fetch_array($rows))
			$parents[] = array('name'=>$row['place_name'],'id'=>$row['place_id']);
		return $parents;
	}		
		
}
?>
