<?php 

	/*
	editplace.php
	Contains the insert input module for the place db table
	
	Dec 22, 2008
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class editplace extends MVC_Authentication_Edit {

	public $place;
	public $place_db;
	public $place_record;
	public $placenames;
	public $placenames_db;
	public $placemainnames;
	public $geotimes;
	public $placetypes;
	public $placenametypes;
	public $featuretypes;
	public $timetypes;
	public $timetypes_db;
	public $timeperiod;
	public $timeperiod_name;
	public $tpnames;	
	public $timetree;
	public $featuretree;	
	public $placetree;
	public $geodata;
	public $place_parents;
	public $place_children;
	public $place_other;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
		if ($_POST['row_id'] and is_numeric($_POST['row_id']) )
			$id = $_POST['row_id'];
		else
			die("error loading place record");
		$this->place_db = new MVC_Model_Place();
		$conditions = array('place_id' => $id);
		$results = $this->place_db->dbSearchTable($conditions);
		$this->place_record = $results[0];
		$this->placenames_db = new MVC_Model_PlaceName();
		$this->placenametypes = new MVC_Model_PlaceNameType();
		$this->placenametyperows = $this->placenametypes->getAllRows();
		// let's cache the placenames to reduce DB load
		$this->timetypes_db = new MVC_Model_TimeType();
		$this->timetyperows = $this->timetypes_db->getAllRows();
		$this->timeperiod_db = new MVC_Model_TimePeriod();
		$this->timetree = $this->timeperiod_db->getTree3($this->tpnames);
		$this->placerows = $this->place_db->getAllRows();

		$conditions = array('place_name_type_id'=>PLACE_NAME_TYPE_MAIN);
		$pnames = $this->placenames_db->dbSearchTable($conditions);
		$this->placemainnames = array();
		foreach ($pnames as $row) {
			$this->placemainnames[ $row['place_id'] ] = $row['place_name'];
		}
		$this->tpnames = array(); // let's preload this; time_period_id => array('time_period_name',...)
		$this->timeperiodnames_db = new MVC_Model_TimePeriodName();
		$allnames = $this->timeperiodnames_db->getAllRows();
		foreach ($allnames as $tname)
		{
			if (!in_array($tname['time_period_id'],array_keys($this->tpnames)) )
				$this->tpnames[$tname['time_period_id']] = array();
			$this->tpnames[$tname['time_period_id']][] = $tname['time_period_name'];
		}
		$this->featuretypes_db = new MVC_Model_FeatureType();
		$this->featuretree = $this->featuretypes_db->getTree3();
	}

	public function __destruct() {
		parent::__destruct();
	}
	
	// event listeners
	// TODO
	
	
	// display helpers
	// TODO
	public function displayTimeTree($true_val) {
		$timeTree = $this->timetree;
		foreach ($timeTree as $tp_id=>$row) {
			if (!$row['parent']) // top level
			{
				$this->displayTimeTreeBranch($tp_id,$depth=0,$timeTree,$true_val);
			}
		}
	}
	
	protected function displayTimeTreeBranch($tp_id,$depth,$timeTree,$true_val = '') {
		// a branch has category_id, category_name, and children
		$row = $timeTree[$tp_id];
		echo "<option value='{$tp_id}' ".($true_val==$tp_id?"selected='selected'":"").">";
		echo str_pad($this->tpnames[$tp_id][0],strlen($this->tpnames[$tp_id][0])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayTimeTreeBranch($child_id,$depth+1,$timeTree, $true_val);
		return;
	}	
	
	public function displayFeatureTypeTree($true_val) {
		$featureTypeTree = $this->featuretree;
		$toplevel = $this->featuretypes_db->getToplevel();
		foreach ($toplevel as $ft_id=>$name) {
			$row = $featureTypeTree[$ft_id];
			if (!$row['parent']) // top level
			{
				$this->displayFeatureTypeTreeBranch($ft_id,$depth=0,$featureTypeTree,$true_val);
			}
		}
	}
	
	public function displayFeatureTypeTreeBranch($ft_id,$depth=0,$featureTypeTree,$true_val) {
		// a branch has category_id, category_name, and children
		$row = $featureTypeTree[$ft_id];
		echo "<option value='{$ft_id}' ".($true_val==$ft_id?"selected='selected'":"").">";
		echo str_pad($row['feature_type'],strlen($row['feature_type'])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($row['children']))
			foreach ($row['children'] as $child_id)
				$this->displayFeatureTypeTreeBranch($child_id,$depth+1,$featureTypeTree, $true_val);
		return;
	}	
	
	public function displayPlaceNames() {
		$conditions = array('place_id' => $this->place_record['place_id']);
		$results = $this->placenames_db->dbSearchTable($conditions);
		// display existing records
		if ($results)
		{
			// start with the main name;
			$this->displayMainPlaceName($results);
			// any additional records
			$this->displayOtherPlaceNames($results);
		}
		// display a 'new' record...
		if (count($results) <= 1)
			$this->displayExtraPlaceName($results);
	}

	private function displayMainPlaceName($results) {
		if ($results)
		{
			// start with the main name;
			foreach ($results as $row)
			{
				if ($row['place_name_type_id'] == 1) // MAIN
				{
					echo '<p>
						<label for="main_name">* Main Name</label>
						<input type="text" id="main_name" name="place_name[]" maxlength="100" ';
						echo 'value="'.$row['place_name'].'" ';
						echo '/>
						<!-- this should be the id for the place_name_type \'main\' -->
						<input type="hidden" id="main_name_type" name="place_name_type_id[]" value="'.$row['place_name_type_id'].'" />
						<input type="hidden" id="place_name_id_0" name="place_name_id[]" value="'.$row['place_name_id'].'" />
						<input type="hidden" id="place_name_create_datetime_0" name="place_name_create_datetime[]" value="'.$row['create_datetime'].'" />
						<input type="hidden" id="place_name_modify_datetime_0" name="place_name_modify_datetime[]" value="'.$row['create_datetime'].'" />
					</p>';					
				}
			}
		}	
	}
	
	private function displayOtherPlaceNames($results) {
		if ($results)
		{
			$count = 0;
			foreach ($results as $row)
			{
				if ($row['place_name_type_id'] != 1)
				{
					echo '
					<p id="placename_'.$count.'" class="placename_row">
						<label for="name_field_'.$count.'">&nbsp;&nbsp;Alternate Name</label>
						<input type="text" id="name_field_'.$count.'" name="place_name[]" maxlength="100" ';
						echo 'value="'.$row['place_name'].'" ';
						echo '/>
						<label for="nametype_field_'.$count.'" style="display:none;">Type:</label>
						<select id="nametype_field_'.$count.'" name="place_name_type_id[]" >
							<option value=\'\'>Type</option>';
							$this->displayPlaceNameTypes($row['place_name_type_id']);
					echo '</select>';
					echo '<input type="hidden" id="place_name_id_'.($count+1).'" name="place_name_id[]" value="'.$row['place_name_id'].'" />';
					echo '<input type="hidden" id="place_name_create_datetime_'.($count+1).'" name="place_name_create_datetime[]" value="'.$row['create_datetime'].'" />';
					echo '<input type="hidden" id="place_name_modify_datetime_'.($count+1).'" name="place_name_modify_datetime[]" value="'.$row['modify_datetime'].'" />';
					if ($count == 0)	
						echo '<a class=\'addbutton\' href="javascript:;"><img src="'.SITE_PATH.'/graphics/plus.gif" alt="" /></a> <a class=\'addtext\' href="javascript:;">Add</a>';
					echo '</p>';
					$count++;
				}
			}
		}	
	}
	
	private function displayExtraPlaceName($results) {
		if ($results)
		{
			$count = count($results);
			echo '
			<p id="placename_'.$count.'" class="placename_row">
				<label for="name_field_'.$count.'">&nbsp;&nbsp;Alternate Name</label>
				<input type="text" id="name_field_'.$count.'" name="place_name[]" maxlength="100" />
				<label for="nametype_field_'.$count.'" style="display:none;">Type:</label>
				<select id="nametype_field_'.$count.'" name="place_name_type_id[]" >
					<option value=\'\'>Type</option>';
					$this->displayPlaceNameTypes(); 
			echo '</select>';
			if ($count == 0)	
				echo '<a class=\'addbutton\' href="javascript:;"><img src="'.SITE_PATH.'/graphics/plus.gif" alt="" /></a> <a class=\'addtext\' href="javascript:;">Add</a>';
			echo '</p>';
		}	
	}
	
	public function displayPlaceNameTypes($pnt_id = -1) {
		$placeTypes = $this->placenametyperows;
		foreach ($placeTypes as $pnt)
		{
			echo "<option value='{$pnt['place_name_type_id']}' ";
			if ($pnt['place_name_type_id'] == $pnt_id)
				echo " selected='selected' ";			
			echo ">".ucwords($pnt['place_name_type'])."</option>";
		}
		return;
	}
	
	public function displayPlaceTypes() {
		$placetypes_db = new MVC_Model_PlaceType();
		$placeTypes = $placetypes_db->getAllRows();
		foreach ($placeTypes as $pt)
		{
			echo "<option value='{$pt['place_type_id']}' ";
			if ($pt['place_type_id'] == $this->place_record['place_type_id'])
				echo " selected='selected' ";
			echo ">".ucwords($pt['place_type'])."</option>";
		}
		return;
	}
	
	public function displayTimeTypes($tt_id = -1) {
		$timeTypes = $this->timetyperows;
		foreach ($timeTypes as $tt)
		{
			echo "<option value='{$tt['time_type_id']}' ";
			if ($tt['time_type_id'] == $tt_id)
				echo " selected='selected' ";			
			echo "> ".ucwords($tt['time_type'])."</option>";
		}
		return;
	}
	
	public function displayGeoTimes() {
		$this->geotimes = new MVC_Model_GeoTime();
		$conditions = array('place_id' => $this->place_record['place_id']);
		$results = $this->geotimes->dbSearchTable($conditions);
		// display existing records
		if ($results)
		{
			// start with the main name;
			$this->displayGeoTimeRows($results);
		}
		// display a 'new' record...
		// if (count($results) <= 0)
		// 	$this->displayExtraGeoTimeRow();
	}
	
	private function displayGeoTimeRows($results) {
		$count = 0;
		foreach ($results as $row)
		{
			echo '
				<div id=\'timerow_'.$count.'\' class="timerow">
					<div class="quarter_column" id="time_type_col" >
						<label for="time_type_'.$count.'">Time Type</label><br />
						<select id=\'time_type'.$count.'\' name=\'time_type_id[]\' >';
							$this->displayTimeTypes($row['time_type_id']);
						echo '</select>
					</div>
					<div class="quarter_column" id="start_time_period_col">
						<label for="start_time_period_id_'.$count.'">Start</label><br />
						<select id=\'start_time_period_id_'.$count.'\' name=\'start_time_period_id[]\' >
							<option value=""></option>';
							$this->displayTimeTree($row['start_time_period_id']);
						echo '</select>
					</div>
					<div class="quarter_column" id="end_time_period_col">
						<label for="End_time_period_id_'.$count.'">End</label><br />
						<select id=\'end_time_period_id_'.$count.'\' name=\'end_time_period_id[]\' >
							<option value=""></option>';
							$this->displayTimeTree($row['end_time_period_id']);
						echo '</select>
					</div>
					<div class="quarter_column">
						';
						if ($count == 0)
							echo '
							<br /><a href="javascript:;"><img src="'.SITE_PATH.'/graphics/plus.gif" alt="ADD" /></a>
							<a href="javascript:;">Add</a>';
					echo '</div>
				</div>
			';
			$count++;
		}
	}
	
	public function displayPlaceTreeTypes() {
		return array();
	}
		
	public function loadGeoData() {
		$this->geodata = new MVC_Model_GeoData();
		$conditions = array('place_id' => $this->place_record['place_id']);
		$results = $this->geodata->dbSearchTable($conditions);
		// display existing records
		if ($results)
		{
			// start with the main name;
			$this->displayGeoData($results);
		}		
	}
	
	private function displayGeoData($results) {
		if ($results)
		{
			// print_r($this->geodata->search_results);
			foreach ($results as $row)
			{
				$lat = $row['latitude'];
				$lng = $row['longitude'];
				// we display latlong initially
				echo $lat.", ".$lng."\n";
			}
		}
	}
	
	public function loadPlaceTree() {
		// we're gonna want the parents;
		$conditions = array();
		$conditions['place_id_2'] = $this->place_record['place_id'];
		$conditions['place_relation_type_id'] = 1; // parent/child
		$this->placetree = new MVC_Model_PlaceTree();
		$this->place_parents = $this->placetree->dbSearchTable($conditions);;
		// children
		$conditions = array();
		$conditions['place_id_1'] = $this->place_record['place_id'];
		$conditions['place_relation_type_id'] = 1;
		$this->place_children = $this->placetree->dbSearchTable($conditions);
		// other?
		$conditions['place_relation_type_id'] = 2; // other
		$other1 = $this->placetree->dbSearchTable($conditions);
		if (!$other1)
			$other1 = array();

		$conditions = array();
		$conditions['place_id_2'] = $this->place_record['place_id'];
		$conditions['place_relation_type_id'] = 2;
		$other2 = $this->placetree->dbSearchTable($conditions);
		if (!$other2)
			$other2 = array();
		$this->place_other = array_merge($other1,$other2);
	}
	
	public function setMarkerMode() {
		if ($this->place_record['geodata_type_id'])
			echo 'document.getElementById("geodata_type_id").value='.$this->place_record['geodata_type_id'].';';
		else {
			// assume it's a point
			echo 'document.getElementById("geodata_type_id").value=1';
		}
		echo 'geomap_mode=';
		switch ($this->place_record['geodata_type_id']) {
			case 1:
				echo '"point_mode";';
				break;
			case 2:
				echo '"line_mode";';
				break;
			case 3:
				echo '"polygon_mode";';
				break;
			default:
				echo '\'\';';
				break;
		};
	}
	
	
	// public function displayParentsLists() {
		// $places = $this->place->getAllRows();
		// print_r($places);
	// }
	public function displayRegions() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_REGION and $row['place_id'] != $this->place_record['place_id'])
				{
					$children = $this->getChildrenPlaces($row['place_id']);
					$kidnames = array();
					foreach ($children as $child)
						$kidnames[] = $child['name'];
					$kidstr = implode(", ",$kidnames);
					echo '<p><input type="checkbox" id="parent_region_'.$count.'" name="parent_regions[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id']) // place_id_1 is always the parent
								echo 'checked="CHECKED" ';
					}
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '/> <label for="parent_region_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->placemainnames[$row['place_id']].'</label></p>';						
					else
						echo '/> <label for="parent_region_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayGovernates() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_GOVERNATE and $row['place_id'] != $this->place_record['place_id'])
				{
					$children = $this->getChildrenPlaces($row['place_id']);
					$kidnames = array();
					foreach ($children as $child)
						$kidnames[] = $child['name'];
					$kidstr = implode(", ",$kidnames);
					echo '<p><input type="checkbox" id="parent_governate_'.$count.'" name="parent_governates[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id']) // place_id_1 is always the parent
								echo 'checked="CHECKED" ';
					}
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '/> <label for="parent_governate_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->placemainnames[$row['place_id']].'</label></p>';						
					else
						echo '/> <label for="parent_governate_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayNomes() {
		$rows = $this->placerows;
		if ($rows)
		{
			echo '<select id="parent_nomes_select" name="parent_nomes[]" >';
			echo '<option value=""></option>';
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_NOME and $row['place_id'] != $this->place_record['place_id'] )
				{
					$children = $this->getChildrenPlaces($row['place_id']);
					$kidnames = array();
					foreach ($children as $child)
						$kidnames[] = $child['name'];
					$kidstr = implode(", ",$kidnames);
					echo '<option value="'.$row['place_id'].'"  title="immediate children: '.$kidstr.'"  ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id'] and $tree_row['is_main_nome'] == 1 ) {
							// place_id_1 is always the parent
								echo 'selected="SELECTED" ';
						}
					}					
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '>'.$this->placemainnames[$row['place_id']].'</option>';						
					else
						echo '>'.$this->place_db->getMainName($row['place_id']).'</option>';
				}
			}
			echo '</select>';
		}
		return;
	}
	
	public function displayAltNomes() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_NOME and $row['place_id'] != $this->place_record['place_id'])
				{
					$children = $this->getChildrenPlaces($row['place_id']);
					$kidnames = array();
					foreach ($children as $child)
						$kidnames[] = $child['name'];
					$kidstr = implode(", ",$kidnames);
					echo '<p><input type="checkbox" id="parent_altnome_'.$count.'" name="parent_altnomes[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id'] and $tree_row['is_main_nome'] == 0 ) // place_id_1 is always the parent
								echo 'checked="CHECKED" ';
					}
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '/> <label for="parent_altnome_'.$count.' title="immediate children: '.$kidstr.'" >'.$this->placemainnames[$row['place_id']].'</label></p>';						
					else
						echo '/> <label for="parent_altnome_'.$count.'" title="immediate children: '.$kidstr.'" >'.$this->place_db->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displaySites() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_SITE and $row['place_id'] != $this->place_record['place_id'])
				{
					$parents = $this->getParentPlaces($row['place_id']);
					$parnames = array();
					foreach ($parents as $parent)
						$parnames[] = $parent['name'];
					$parname = implode(", ",$parnames);
					echo '<p><input type="checkbox" id="parent_site_'.$count.'" name="parent_sites[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id']) // place_id_2 is always the child
								echo 'checked="CHECKED" ';
					}
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '/> <label for="parent_site_'.$count.'" title="immediate parents: '.$parname.'" >'.$this->placemainnames[$row['place_id']].'</label></p>';						
					else
						echo '/> <label for="parent_site_'.$count.'" title="immediate parents: '.$parname.'" >'.$this->place_db->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayFeatures() {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
			
				if ($row['place_type_id'] == PLACE_TYPE_FEATURE and $row['place_id'] != $this->place_record['place_id'])
				{
					$parents = $this->getParentPlaces($row['place_id']);
					$parnames = array();
					foreach ($parents as $parent)
						$parnames[] = $parent['name'];
					$parname = implode(", ",$parnames);	
					echo '<p><input type="checkbox" id="parent_feature_'.$count.'" name="parent_features[]" value="'.$row['place_id'].'" ';
					if ($this->place_children)
					{
						foreach ($this->place_children as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id']) // place_id_2 is always the child
								echo 'checked="CHECKED" ';
					}
					// is the name cached?
					if (in_array($row['place_id'], array_keys($this->placemainnames)) )
						echo '/> <label for="parent_feature_'.$count.'" title="immediate parents: '.$parname.'" >'.$this->placemainnames[$row['place_id']].'</label></p>';						
					else
						echo '/> <label for="parent_feature_'.$count.'" title="immediate parents: '.$parname.'" >'.$this->place_db->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	// display the proper values for the 'Other box'
	public function displayOtherPlaces() {
		// print_r($this->place_other);
		// echo '<br /><br />';
		$this->displayExistingOtherPlaces();
		if (count($this->place_other) < 1)
			$this->displayExtraOtherPlace();
		return;
	}
	
	private function displayExistingOtherPlaces() {
		if ($this->place_other)
		{
			$count = 0;
			foreach ($this->place_other as $row)
			{
				print_r($row);
				echo '<br /><br />';
				echo "
	<div class='other-box' id='other-box_0' style='clear:both;padding-top:0.5em;' >
		<div class='quarter_column' style='' >
			<label for='other_places-select_0'>
			This place also existed as: 
			</label>
			<div id='other-places-box' class='places-select-box' >
				<select id='other_places-select_0' name='other_places[]' class='other_places-select' >
					<option value=''></option>
					";
					// load other places
					if ($row['place_id_1'] == $this->place_record['place_id'])
						$this->displayOtherPlaceOptions($row['place_id_2']);
					else
						$this->displayOtherPlaceOptions($row['place_id_1']);
				echo
				"</select>
			</div>
		</div>
		<div class='quarter_column' >
			<p><br /> in the</p>
		</div>
		<div class='quarter_column' >
			<p><br />
			<select id='other-places-pastpresent-toggle_0' name='pastpresent-toggle[]' class='pastpresent-toggle' >
				<option value=''></option>
				";
				if ($row['place_id_1'] == $this->place_record['place_id']) // this means the other place is the later one
					echo "<option value='past'>Past</option>";
				else
					echo "<option value='past' selected='SELECTED' >Past</option>";
				if ($row['place_id_1'] == $this->place_record['place_id']) // this means the other place is the later one				
					echo "<option value='future' selected='SELECTED' >Future</option>";
				else			
					echo "<option value='future' >Future</option>";
				echo "
			</select></p>
		</div>
		<div class='quarter_column' >
			<p><label for='transition_type_id_0'>What happened?</label><br />
			<select id='transition_type_id_0' name='transition_type_id[]' class='transition_type_id' >";
				$this->displayTransitionTypeOptions($row['place_transition_type_id']);
			echo "
			</select>
			</p>
		</div>
		<div class='quarter_column'><label for='other-descriptions_0'>Is this what you meant? <em>(if not, try to fix it by adjusting the values)</em></label></div>
		<div class='quarter_column'><p id='other-descriptions_0'></p></div>
		<div class='quarter_column'><a id='newotherbox' href='javascript:;'><img src='/graphics/plus.gif' alt=''> Add another relation?</a><div>
		<p style='clear:both;'>&nbsp;</p>
				";
				$count++;
			}
			;
		}
		return;
	}
	
	// we only use this when no existing other relations are present
	// otherwise, an add toggle will already be provided by the JS
	public function displayExtraOtherPlace() { 
		echo "
	<div class='other-box' id='other-box_0' style='clear:both;padding-top:0.5em;' >
		<div class='quarter_column' style='' >
			<label for='other_places-select_0'>
			This place also existed as: 
			</label>
			<div id='other-places-box' class='places-select-box' >
				<select id='other_places-select_0' name='other_places[]' class='other_places-select' >
					<option value=''></option>
					";
					$this->displayOtherPlaceOptions();
				echo "
				</select>
			</div>
		</div>
		<div class='quarter_column' >
			<p><br /> in the</p>
		</div>
		<div class='quarter_column' >
			<p><br />
			<select id='other-places-pastpresent-toggle_0' name='pastpresent-toggle[]' class='pastpresent-toggle' >
				<option value=''></option>
				<option value='past'>Past</option>
				<option value='future'>Future</option>
			</select></p>
		</div>
		<div class='quarter_column' >
			<p><label for='transition_type_id_0'>What happened?</label><br />
			<select id='transition_type_id_0' name='transition_type_id[]' class='transition_type_id' >
				<option value='1'>It shifted</option>
				<option value='2'>It contracted</option>
				<option value='3'>It expanded</option>
			</select>
			</p>
		</div>
		<div class='quarter_column'><label for='other-descriptions_0'>Is this what you meant? <em>(if not, try to fix it by adjusting the values)</em></label></div>
		<div class='quarter_column'><p id='other-descriptions_0'></p></div>
		<div class='quarter_column'><a id='newotherbox' href='javascript:;'><img src='/graphics/plus.gif' alt=''> Add another relation?</a><div>
		<p style='clear:both;'>&nbsp;</p>
	</div>
		";
		return;	
	}

	// for the select box in the 'other' section
	private function displayOtherPlaceOptions($trueval = -1) {
		$rows = $this->placerows;
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				echo '<option value="'.$row['place_id'].'" ';
				if ($trueval == $row['place_id'])
					echo 'selected=\'SELECTED\'';
				echo ' >'.htmlentities($this->placemainnames[$row['place_id']]).'</option>';
			}
		}
		return;		
	}	
		
	private function displayTransitionTypeOptions($trueval = -1) {
		echo "<option value='1' ";
		if ($trueval == PLACE_TRANSITION_TYPE_SHIFT)
			echo 'selected=\'SELECTED\' ';
		echo ">It shifted</option>";
		echo "<option value='2' ";
		if ($trueval == PLACE_TRANSITION_TYPE_CONTRACT)
			echo 'selected=\'SELECTED\' ';
		echo ">It contracted</option>";
		echo "<option value='3' ";
		if ($trueval == PLACE_TRANSITION_TYPE_EXPAND)
			echo 'selected=\'SELECTED\' ';
		echo ">It expanded</option>";
		return;
	}
	
	public function displayChildrenPlaces($place_id, $depth = 5) {
		$children = $this->getChildrenPlaces($place_id);
		$names = array();
		foreach ($children as $child)
			$names[] = $child['name'];
		echo "<ul style='padding-left:1.5em;'>";
		echo "<li style='font-size:0.8em;padding-bottom:0.5em;'>".implode(", ",$names)."</li>";
		foreach ($children as $child) {
			$morechildren = $this->getChildrenPlaces($child['id'], $depth-1);
			if (!empty($morechildren))
			{
				echo "<li style='list-style-type:none;'>";
				echo $child['name']."<br />";
				$this->displayChildrenPlaces($child['id']);
				echo "</li>";
			}
		}
		echo "</ul>";
	}
	
	public function getChildrenPlaces($place_id, $depth = 3) {
		$children = array();
		if ($depth == 0)
			return $children;
		$rows = $this->place_db->query("SELECT place_id, place_name FROM place_name WHERE place_id IN (SELECT place_id_2 FROM place_tree WHERE place_id_1=$place_id AND place_relation_type_id=1) AND place_name_type_id=1 ORDER BY place_name");
		while ($row = mysqli_fetch_array($rows))
			$children[] = array('name'=>$row['place_name'],'id'=>$row['place_id']);
		return $children;
	}
	
	public function displayParentPlaces($place_id, $depth = 5) {
		$parents = $this->getParentPlaces($place_id);
		echo "<ul style='padding-left:1.5em;margin-bottom:0;margin-top:0;'>";
		foreach ($parents as $parent) {
			$moreparents = $this->getParentPlaces($parent['id'], $depth-1);
			if (!empty($moreparents))
			{
				$this->displayParentPlaces($parent['id']);
			}
			echo "<li>";
			echo $parent['name']."<br />";
			echo "</li>";
		}
		echo "</ul>";
	}	
	
	public function getParentPlaces($place_id, $depth = 3) {
		$parents = array();
		if ($depth == 0)
			return $children;
		$rows = $this->place_db->query("SELECT place_id, place_name FROM place_name WHERE place_id IN (SELECT place_id_1 FROM place_tree WHERE place_id_2=$place_id AND place_relation_type_id=1) AND place_name_type_id=1 ORDER BY place_name");
		while ($row = mysqli_fetch_array($rows))
			$parents[] = array('name'=>$row['place_name'],'id'=>$row['place_id']);
		return $parents;
	}	
	
}
?>
