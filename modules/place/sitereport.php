<?php 

	/*
	editplace.php
	Contains the insert input module for the place db table
	
	Dec 22, 2008
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class sitereport extends MVC_Authentication_Edit {

	public $place;
	public $place_db;
	public $place_record;
	public $placenames;
	public $placenames_db;
	public $geotimes;
	public $placetypes;
	public $placenametypes;
	public $featuretypes;
	public $timetypes;
	public $timeperiod;
	public $timeperiodnames;
	public $placetree;
	public $geodata;
	public $place_parents;
	public $place_children;
	public $place_other;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=154) {
		parent::__construct();
		if ($_POST['row_id'] and is_numeric($_POST['row_id']) )
			$id = $_POST['row_id'];
		else if (!$id)
			die("error loading place record");
		// $this->place = new MVC_Model_Place($id); // assign the model
		$this->place_db = new MVC_Model_Place();
		$conditions = array('place_id' => $id);
		$results = $this->place_db->dbSearchTable($conditions);
		$this->place_record = $results[0];
		// print_r($this->place->data);
		// echo '<br /><br />';
		$this->placenames_db = new MVC_Model_PlaceName();
		$this->placenames = $this->placenames_db->dbSearchTable($conditions);
	}

	public function __destruct() {
		parent::__destruct();
	}
	
	// event listeners
	// TODO
	
	
	// display helpers
	// TODO
	public function displayTimeTree($tt_id) {
		$timeTree = $this->timeperiod->getTree2();
		foreach ($timeTree as $key=>$branch) {
			$this->displayTimeTreeBranch($branch,0,$tt_id);
		}
	}
	
	public function displayTimeTreeBranch($branch,$depth=0,$true_val) {
		// a branch has category_id, category_name, and children
		echo "<option value='{$branch['time_period_id']}' ".($true_val==$branch['time_period_id']?"selected='selected'":"").">";
		echo str_pad($branch['timeperiod_name'],strlen($branch['timeperiod_name'])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($branch['children']))
			foreach ($branch['children'] as $key=>$lowerBranch)
				$this->displayTimeTreeBranch($lowerBranch,$depth+1,$true_val);
		return;
	}	
	
	public function displayFeatureTypeTree() {
		$featureTypeTree = $this->featuretypes->getTree2();
		// print_r($featureTypeTree);
		foreach ($featureTypeTree as $key=>$branch) {
			$this->displayFeatureTypeTreeBranch($branch,0,$this->place->data['feature_type_id']);
		}
	}
	
	public function displayFeatureTypeTreeBranch($branch,$depth=0,$true_val) {
		// a branch has category_id, category_name, and children
		echo "<option value='{$branch['feature_type_id']}' ".($true_val==$branch['feature_type_id']?"selected='selected'":"").">";
		echo str_pad($branch['feature_type'],strlen($branch['feature_type'])+3*$depth,"-- ",STR_PAD_LEFT);
		echo "</option>";
		if (!empty($branch['children']))
			foreach ($branch['children'] as $key=>$lowerBranch)
				$this->displayFeatureTypeTreeBranch($lowerBranch,$depth+1,$true_val);
		return;
	}	
	
	public function displayMainPlaceName() {
		if ($this->placenames)
		{
			// start with the main name;
			foreach ($this->placenames as $row)
			{
				if ($row['place_name_type_id'] == 1) // MAIN
				{
					echo $row['place_name'];				
				}
			}
		}	
	}
	
	public function displayOtherPlaceNames($place_name_type_id) {
		if ($this->placenames)
		{
			$count = 0;
			foreach ($this->placenames as $row)
			{
				if ($row['place_name_type_id'] == $place_name_type_id)
				{
					echo htmlentities($row['place_name'], ENT_COMPAT, 'UTF-8') .'<br />';
				}
			}
		}	
	}
	
	public function displayPlaceType() {
		switch ($this->place_record['place_type_id']) {
			case 1:
				echo 'Region';
				break;
			case 2: 
				echo 'Governate';
				break;
			case 3: 
				echo 'Nome';
				break;
			case 4:
				echo 'Site';
				break;
			case 5:
				echo 'Feature';
				break;						
			default:
				break;
		}
		return;
	}
	
	public function displayTimeTypes($tt_id = -1) {
		$timeTypes = $this->timetypes->getAllRows();
		foreach ($timeTypes as $tt)
		{
			echo "<option value='{$tt['time_type_id']}' ";
			if ($tt['time_type_id'] == $tt_id)
				echo " selected='selected' ";			
			echo "> ".ucwords($tt['time_type'])."</option>";
		}
		return;
	}
	
	public function displayGeoTimes() {
		$this->geotimes = new MVC_Model_GeoTime();
		$conditions = array('place_id' => $this->place_record['place_id']);
		$results = $this->geotimes->dbSearchTable($conditions);
		// display existing records
		if ($results)
		{
			foreach ($results as $row)
			{
				// what's the usage?
				switch ($row['time_type_id']) {
					case 1:
						$usage = 'in use';
						break;
					case 2:
						$usage = 'construction';
						break;
					case 3:
						$usage = 'destruction';
						break;
					case 4:
						$usage = 'defunct';
						break;
					default:
						break;
				}
				// start tp_id
				$this->timeperiodnames = new MVC_Model_TimePeriodName();
				$startname = '';
				$tconditions = array('time_period_id' => $row['start_time_period_id'] );
				$tresults = $this->timeperiodnames->dbSearchTable($tconditions);
				if ($tresults)
					$startname = $tresults[0]['time_period_name'];
				// end tp_id
				$tconditions = array('time_period_id' => $row['end_time_period_id'] );
				$endname = '';
				$tresults = $this->timeperiodnames->dbSearchTable($tconditions);
				if ($tresults)
					$endname = $tresults[0]['time_period_name'];
				echo ucwords($usage) . ": " . $startname . " to " . $endname . "<br />";
			}
		}
	}
		
	public function loadGeoData() {
		$this->geodata->search['place_id'] = $this->place->data['place_id'];
		$this->geodata->dbSearchTable();
		// display existing records
		if ($this->geodata->search_results)
		{
			// start with the main name;
			$this->displayGeoData();
		}		
	}
	
	private function displayGeoData() {
		if ($this->geodata->search_results)
		{
			// print_r($this->geodata->search_results);
			foreach ($this->geodata->search_results as $row)
			{
				$lat = $row['latitude'];
				$lng = $row['longitude'];
				// we display latlong initially
				echo $lat.", ".$lng."\n";
			}
		}
	}
	
	public function loadPlaceTree() {
		// we're gonna want the parents;
		$this->placetree->search['place_id_2'] = $this->place->data['place_id'];
		$this->placetree->search['place_relation_type_id'] = 1; // parent/child type relation
		$this->placetree->dbSearchTable();
		$this->place_parents = $this->placetree->search_results;
		// print_r($this->place_parents);
		// echo '<br /><br />';
		// children
		$this->placetree->resetSearch(); // reset the search
		$this->placetree->search['place_id_1'] = $this->place->data['place_id'];
		$this->placetree->search['place_relation_type_id'] = 1; // parent/child type relation
		$this->placetree->dbSearchTable();
		$this->place_children = $this->placetree->search_results;
		// other?
		$this->placetree->resetSearch(); // reset the search
		$this->placetree->search['place_id_1'] = $this->place->data['place_id'];
		$this->placetree->search['place_relation_type_id'] = 2; // other type relation
		$this->placetree->dbSearchTable();
		$other1 = $this->placetree->search_results;
		if (!$other1)
			$other1 = array();
		$this->placetree->resetSearch(); // reset the search
		$this->placetree->search['place_id_2'] = $this->place->data['place_id'];
		$this->placetree->search['place_relation_type_id'] = 2; // other type relation
		$this->placetree->dbSearchTable();
		$other2 = $this->placetree->search_results;
		if (!$other2)
			$other2 = array();
		$this->place_other = array_merge($other1,$other2);
	}
	
	
	
	public function displayRegions() {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_REGION and $row['place_id'] != $this->place->data['place_id'])
				{
					echo '<p><input type="checkbox" id="parent_region_'.$count.'" name="parent_regions[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id']) // place_id_1 is always the parent
								echo 'checked="CHECKED" ';
					}
					echo '/> <label for="parent_region_'.$count.'">'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayGovernates() {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_GOVERNATE and $row['place_id'] != $this->place->data['place_id'])
				{
					echo '<p><input type="checkbox" id="parent_governate_'.$count.'" name="parent_governates[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id']) // place_id_1 is always the parent
								echo 'checked="CHECKED" ';
					}
					echo '/> <label for="parent_governate_'.$count.'">'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayNomes() {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			echo '<select id="parent_nomes_select" name="parent_nomes[]" >';
			echo '<option val=""></option>';
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_NOME and $row['place_id'] != $this->place->data['place_id'] )
				{
					// echo '<p><input type="checkbox" id="parent_nome_'.$count.'" name="parent_nomes[]" value="'.$row['place_id'].'" ';
					// if ($this->place_parents)
					// {
						// foreach ($this->place_parents as $tree_row)
							// if ($tree_row['place_id_1'] == $row['place_id']) // place_id_1 is always the parent
								// echo 'checked="CHECKED" ';
					// }
					// echo '/> <label for="parent_nome_'.$count.'">'.$this->place->getMainName($row['place_id']).'</label></p>';
					// $count++;
					echo '<option value="'.$row['place_id'].'"  ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id'] and $row['is_main_nome'] == 1 ) // place_id_1 is always the parent
								echo 'selected="SELECTED" ';
					}					
					echo '>'.$this->place->getMainName($row['place_id']).'</option>';
				}
			}
			echo '</select>';
		}
		return;
	}
	
	public function displayAltNomes() {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_NOME and $row['place_id'] != $this->place->data['place_id'])
				{
					echo '<p><input type="checkbox" id="parent_altnome_'.$count.'" name="parent_altnomes[]" value="'.$row['place_id'].'" ';
					if ($this->place_parents)
					{
						foreach ($this->place_parents as $tree_row)
							if ($tree_row['place_id_1'] == $row['place_id'] and $row['is_main_nome'] == 0 ) // place_id_1 is always the parent
								echo 'checked="CHECKED" ';
					}
					echo '/> <label for="parent_altnome_'.$count.'">'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displaySites() {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_SITE and $row['place_id'] != $this->place->data['place_id'])
				{
					echo '<p><input type="checkbox" id="child_site_'.$count.'" name="child_sites[]" value="'.$row['place_id'].'" ';
					if ($this->place_children)
					{
						foreach ($this->place_children as $tree_row)
							if ($tree_row['place_id_2'] == $row['place_id']) // place_id_2 is always the child
								echo 'checked="CHECKED" ';
					}
					echo '/> <label for="child_site_'.$count.'">'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	public function displayFeatures() {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				if ($row['place_type_id'] == PLACE_TYPE_FEATURE and $row['place_id'] != $this->place->data['place_id'])
				{
					echo '<p><input type="checkbox" id="child_feature_'.$count.'" name="child_features[]" value="'.$row['place_id'].'" ';
					if ($this->place_children)
					{
						foreach ($this->place_children as $tree_row)
							if ($tree_row['place_id_2'] == $row['place_id']) // place_id_2 is always the child
								echo 'checked="CHECKED" ';
					}
					echo '/> <label for="child_feature_'.$count.'">'.$this->place->getMainName($row['place_id']).'</label></p>';
					$count++;
				}
			}
		}
		return;
	}
	
	// display the proper values for the 'Other box'
	public function displayOtherPlaces() {
		// print_r($this->place_other);
		// echo '<br /><br />';
		$this->displayExistingOtherPlaces();
		if (count($this->place_other) < 1)
			$this->displayExtraOtherPlace();
		return;
	}
	
	private function displayExistingOtherPlaces() {
		if ($this->place_other)
		{
			$count = 0;
			foreach ($this->place_other as $row)
			{
				print_r($row);
				echo '<br /><br />';
				echo "
	<div class='other-box' id='other-box_0' style='clear:both;padding-top:0.5em;' >
		<div class='quarter_column' style='' >
			<label for='other_places-select_0'>
			This place also existed as: 
			</label>
			<div id='other-places-box' class='places-select-box' >
				<select id='other_places-select_0' name='other_places[]' class='other_places-select' >
					<option value=''></option>
					";
					// load other places
					if ($row['place_id_1'] == $this->place->data['place_id'])
						$this->displayOtherPlaceOptions($row['place_id_2']);
					else
						$this->displayOtherPlaceOptions($row['place_id_1']);
				echo
				"</select>
			</div>
		</div>
		<div class='quarter_column' >
			<p><br /> in the</p>
		</div>
		<div class='quarter_column' >
			<p><br />
			<select id='other-places-pastpresent-toggle_0' name='pastpresent-toggle[]' class='pastpresent-toggle' >
				<option value=''></option>
				";
				if ($row['place_id_1'] == $this->place->data['place_id']) // this means the other place is the later one
					echo "<option value='past'>Past</option>";
				else
					echo "<option value='past' selected='SELECTED' >Past</option>";
				if ($row['place_id_1'] == $this->place->data['place_id']) // this means the other place is the later one				
					echo "<option value='future' selected='SELECTED' >Future</option>";
				else			
					echo "<option value='future' >Future</option>";
				echo "
			</select></p>
		</div>
		<div class='quarter_column' >
			<p><label for='transition_type_id_0'>What happened?</label><br />
			<select id='transition_type_id_0' name='transition_type_id[]' class='transition_type_id' >";
				$this->displayTransitionTypeOptions($row['place_transition_type_id']);
			echo "
			</select>
			</p>
		</div>
		<div class='quarter_column'><label for='other-descriptions_0'>Is this what you meant? <em>(if not, try to fix it by adjusting the values)</em></label></div>
		<div class='quarter_column'><p id='other-descriptions_0'></p></div>
		<div class='quarter_column'><a id='newotherbox' href='javascript:;'><img src='/graphics/plus.gif' alt=''> Add another relation?</a><div>
		<p style='clear:both;'>&nbsp;</p>
				";
				$count++;
			}
			;
		}
		return;
	}
	
	// we only use this when no existing other relations are present
	// otherwise, an add toggle will already be provided by the JS
	public function displayExtraOtherPlace() { 
		echo "
	<div class='other-box' id='other-box_0' style='clear:both;padding-top:0.5em;' >
		<div class='quarter_column' style='' >
			<label for='other_places-select_0'>
			This place also existed as: 
			</label>
			<div id='other-places-box' class='places-select-box' >
				<select id='other_places-select_0' name='other_places[]' class='other_places-select' >
					<option value=''></option>
					";
					$this->displayOtherPlaceOptions();
				echo "
				</select>
			</div>
		</div>
		<div class='quarter_column' >
			<p><br /> in the</p>
		</div>
		<div class='quarter_column' >
			<p><br />
			<select id='other-places-pastpresent-toggle_0' name='pastpresent-toggle[]' class='pastpresent-toggle' >
				<option value=''></option>
				<option value='past'>Past</option>
				<option value='future'>Future</option>
			</select></p>
		</div>
		<div class='quarter_column' >
			<p><label for='transition_type_id_0'>What happened?</label><br />
			<select id='transition_type_id_0' name='transition_type_id[]' class='transition_type_id' >
				<option value='1'>It shifted</option>
				<option value='2'>It contracted</option>
				<option value='3'>It expanded</option>
			</select>
			</p>
		</div>
		<div class='quarter_column'><label for='other-descriptions_0'>Is this what you meant? <em>(if not, try to fix it by adjusting the values)</em></label></div>
		<div class='quarter_column'><p id='other-descriptions_0'></p></div>
		<div class='quarter_column'><a id='newotherbox' href='javascript:;'><img src='/graphics/plus.gif' alt=''> Add another relation?</a><div>
		<p style='clear:both;'>&nbsp;</p>
	</div>
		";
		return;	
	}

	// for the select box in the 'other' section
	private function displayOtherPlaceOptions($trueval = -1) {
		$rows = $this->place->getAllRows();
		if ($rows)
		{
			$count = 0;
			foreach ($rows as $row)
			{
				echo '<option value="'.$row['place_id'].'" ';
				if ($trueval == $row['place_id'])
					echo 'selected=\'SELECTED\'';
				echo ' >'.htmlentities($this->place->getMainName($row['place_id'])).'</option>';
			}
		}
		return;		
	}	
		
	private function displayTransitionTypeOptions($trueval = -1) {
		echo "<option value='1' ";
		if ($trueval == PLACE_TRANSITION_TYPE_SHIFT)
			echo 'selected=\'SELECTED\' ';
		echo ">It shifted</option>";
		echo "<option value='2' ";
		if ($trueval == PLACE_TRANSITION_TYPE_CONTRACT)
			echo 'selected=\'SELECTED\' ';
		echo ">It contracted</option>";
		echo "<option value='3' ";
		if ($trueval == PLACE_TRANSITION_TYPE_EXPAND)
			echo 'selected=\'SELECTED\' ';
		echo ">It expanded</option>";
		return;
	}
}
?>
