<?php
	// get_dynasties.php
	// feb 6, 2009
	// andrew tai, UCLA ATS
	
	// vaildate REFERER url
	// generate query
	
	// load any global config variables
	require_once('../../../config.php');
	
	// this autoload relies on a 'good' file structure
	// all objects in this application go in
	// webroot/includes/
	function __autoload($className) {
		// let's truncate the 'MVC_' from the object name
		$objectName = substr($className,4);
		$objectName = str_replace("_","/",$objectName);
		// where's the object file located?
		$objectDir = $_SERVER['DOCUMENT_ROOT'].'/uee'."/includes/"; 
		$objectPath = $objectDir.$objectName.'.php';
		// echo $objectPath .'<br />';
		if (!file_exists($objectPath))
			die("You tried to load a file that doesn't exist: $objectPath");
		require_once($objectPath);
	}
	
	class myObj extends MVC_DB {}
	
	$obj = new myObj();
	$query = "SELECT place.place_id, place_tree.place_id_1, place_tree.place_id_2, place_tree.place_relation_type_id, place_name.place_name, place_tree.place_transition_type_id
FROM place
LEFT OUTER JOIN place_tree ON place.place_id = place_tree.place_id_1, place_name
WHERE ( 1 OR
place_tree.place_relation_type_id = 1
OR place_tree.place_relation_type_id IS NULL
)
AND (
place_name.place_id = place.place_id
AND place_name.place_name_type_id = 1
)
ORDER BY place.place_id, place_name.place_name";
	// echo $query . '<br />';
	MVC_Log::write($query);			
	$result = $obj->query($query);
	$retRows = array();
	if ($result)
	{
		while ($row = mysqli_fetch_array($result)) {
			// print_r($row);
			// echo '<br /><br />';
			$r = array(
				'place_name' => $row['place_name'],
				'parent_place_id' => $row['place_id_1'],
				'child_place_id' => $row['place_id_2']
				);
			switch ($row['place_relation_type_id']) {
				case 1:
					$r['place_relation_type'] = 'Parent/Child';
					break;
				case 2:
					$r['place_relation_type'] = 'Shift';
					break;
				default:
					break;
			}
			// print_r($r);
			// echo '<br /><br />';
			// if (!in_array($row['place_id'],$placeIds))
			// {
				// $placeIds[] = $row['place_id'];
				// $retRows[$row['place_id']] = $row;
				// $retRows[$row['place_id']]['main_name'] = '';
				// $retRows[$row['place_id']]['alt_names'] = array();
			// }
			// if ($row['place_name_type_id'] == 1)
				// $retRows[$row['place_id']]['main_name'] = $row['place_name'];
			// else
				// $retRows[$row['place_id']]['alt_names'][] = $row['place_name'];
			$retRows[] = $r;
		}
	}
	$treeArr = array();
	// place ids I've already seen
	$placeIds = array();
	foreach ($retRows as $row)
	{
		// print_r($row);
		// echo '<br /><br />';
		
		if ($row['place_relation_type'] == 'Parent/Child')
		{
			if (!in_array($row['parent_place_id'],$placeIds))
			{	
				$placeIds[] = $row['parent_place_id'];
				$treeArr[$row['parent_place_id']] = array();
			}
			$treeArr[$row['parent_place_id']][] = $row['child_place_id'];
		}
	}
	foreach ($treeArr as $key=>$row)
	{
		echo $key.'<br />';
		print_r($row);
		echo "<br /><br />";
	}
	// we'd actually want to return this as a json array of objects
	$resp = '';
	// $resp .= "[";
	// $row_arr = array();
	// $tp_ids = array();
	// foreach ($retRows as $row)
	// {
		// $rowResp = '';
		// $rowResp .= '{';
		// $fields = array();
		// if (in_array($row['time_period_id'], $tp_ids))
			// continue;
		// $tp_ids[] = $row['time_period_id'];
		// foreach ($row as $key => $val)
		// {
			// if (!is_numeric($key))
			// {
				// if (is_numeric($val))
					// $fields[] = '"'.$key.'":'.$val;
				// else
					// $fields[] = '"'.$key.'":"'.$val.'"';
			// }
		// }	
		// $rowResp .= implode(", ",$fields);
		// $rowResp .= '}';
		// we'd actually want to return this as a json array of objects
		// $row_arr[] = $rowResp;
	// }
	// $resp .= implode(", ",$row_arr);
	// $resp .= "]";
	// echo $resp;
	return;
?>