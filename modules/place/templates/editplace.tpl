<!--
	editplace.tpl
	A customized input template for the place module
	
	Dec 22, 2008
	Andrew Tai
	
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/?module=place" >Places table</a>
<h1 class="tablename">Edit a Place</h1>
<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=updateRow" id="placeform" class="dataentry">
<div style="width:75%;float:left;">
<p><em>* denotes required field</em></p>
<?php 
	$this->displayPlaceNames(); 
?>
</div>
<div style='width:24%;float:left;font-size:0.9em;border:1px dotted gray;'>
	<h5 style='margin-top:0;margin-bottom:0.5em;'>Parents &ndash;&gt; Children</h5>
	<p style='margin-bottom:0;'>
		<?php
			echo $this->place_db->getMainName($this->place_record['place_id']).' <em>('.$this->place_db->getPlaceTypeName($this->place_record['place_type_id']).')</em> has children:<br />';
			$this->displayChildrenPlaces($this->place_record['place_id']);	
		?>
	</p>
	<h5 style='margin-top:0;margin-bottom:0.5em;'>Children &lt;&ndash; Parents</h5>
		<?php
			$this->displayParentPlaces($this->place_record['place_id']);	
			echo '<p style="margin-top:0;">'.$this->place_db->getMainName($this->place_record['place_id']).' <em>('.$this->place_db->getPlaceTypeName($this->place_record['place_type_id']).')</em> has the above parents:</p>';
		?>	
</div>
<p id="place_type_buffer" style="border-bottom:1px dashed black;width:100%;">&nbsp;</p>
<p>
	<label for="old_id">&nbsp;&nbsp;Old Place ID</label>
	<input type="text" id="old_id" name="old_id" maxlength="20" value="<?php echo $this->place_record['old_id']; ?>" />
</p>
<p id="place_type_wrapper">
	<label for="place_type_id">* This is a</label>
	<select id="place_type_id" name="place_type_id">
		<?php $this->displayPlaceTypes(); ?>
	</select>
	<label for="feature_type" style="display:none;" >Feature Type:</label>
	<select id="feature_type" name="feature_type_id" style="float:left; display:none;" >
		<option value="">Feature Type</option>
		<?php $this->displayFeatureTypeTree($this->place_record['feature_type_id']); ?>		
	</select>
</p>
<fieldset id="geotime-data" style='clear:both;display:none;' >
	<legend>Map It</legend>
	<div id='marker-type' style='width:15em; float:left; font-size:0.8em;' >
		<label for="timemap" >Map It <br />
		<a id="point_mode" class="geomarker_mode" href="javascript:;">Point <img src="http://google.com/mapfiles/ms/t/Bmu.png" alt="" /></a><br />
		<a id="line_mode" class="geomarker_mode" href="javascript:;">Line <img src="http://google.com/mapfiles/ms/t/Blu.png" alt="" /></a><br />
		<a id="polygon_mode" class="geomarker_mode" href="javascript:;">Polygon <img src="http://google.com/mapfiles/ms/t/Bpu.png" alt="" /></a><br />
		</label>
		<input type="hidden" id="geodata_type_id" name="geodata_type_id" value="" />
		<div id='map-tooltip' style='padding-right:0.5em;' >
			<p><strong>Map Instructions</strong></p>
			<p>Click on the map to start creating points.</p>
			<p>When making lines and polygons, dragging on the gray-colored boxes to create new points </p>
			<p>Clicking without dragging on a white-colored point will remove that point.</p>
			<p>To use the geodata textbox for entry: <br />
			Use a one line for one geodata coordinate. Separate latitude and longitude with commas, use a newline (enter key) to separate coordinates.<br />
			Example: <br />
			31, 32 <br />
			31.05, 32.004			
			</p>
		</div>
	</div>
	<div id='map' style='width:750px;height:420px;' >
		<img src="<?php echo SITE_PATH.'/graphics/missing.gif'; ?>" alt='timemap' /><br />
	</div>
	<div id='geomarker-data' style='width:750px; margin-left:12em;'>
		<label for='geodata'>Geo Data</label>
		<label style='width:25em; font-size:0.8em;height:1.5em;' >This data is in the form: <a id='latlong_mode' href='javascript:;' onclick="mapSwitchCoordMode(this.id);" >Lat, Long</a> | <a id='longlat_mode' href='javascript:;' onclick="mapSwitchCoordMode(this.id);" >Long, Lat</a></label>
		<input type='hidden' name='geocoord_mode' value='latlong_mode' />
		<!--<label><a href="javascript:;" onclick="swapCoordinates();" >Swap Lat/Long data</a></label>-->
		<br />
		<textarea id='geodata' name='geodata' style='width:750px;height:100px;overflow:auto;float:left;clear:right;' onchange='mapUpdateFromText();' ><?php $this->loadGeoData(); ?></textarea>
	</div>
</fieldset>
<fieldset id='time' style='clear:both;'>
	<legend>Time</legend>
	<?php $this->displayGeoTimes(); ?>
</fieldset>
<fieldset id='parent_places' style="clear:both;">
	<legend>Parents</legend>
	<div style="clear:both;">	
		<p><em>This place is considered part of: </em></p>
		<p><label for="parents-search">LiveSearch:</label><input type='text' id='parents-search' name='parents-search' /></p>
		<ul id='parents_tabs' class='tabs'>
			<li class='selected'><a href="javascript:;">Regions</a></li>
			<li><a href="javascript:;">Governates</a></li>
			<li><a href="javascript:;">Nomes</a></li>
			<li><a href="javascript:;">AltNomes</a></li>
			<li><a href="javascript:;">Sites</a></li>
			<li><a href="javascript:;">Features</a></li>
		</ul>
		<?php $this->loadPlaceTree(); ?>
		<div id="parent_regions" style='clear:both;'>
			Regions
			<div id="parent_regions-box" class='places-checkbox-box' >
				<?php $this->displayRegions(); ?>
			</div>
		</div>
		<div id="parent_governates" style='clear:both;'>
			Governates
			<div id="parent_governates-box" class='places-checkbox-box' >
				<?php $this->displayGovernates(); ?>
			</div>
		</div>
		<div id="parent_nomes" style='clear:both;'>
			Nome
			<div id="parent_nomes-box" class='places-checkbox-box' >
				<?php $this->displayNomes(); ?>
			</div>
		</div>
		<div id="parent_altnomes" style='clear:both;'>
			<abbrev title="Alternate">Alt.</abbrev> Nomes
			<div id="parent_altnomes-box" class='places-checkbox-box' >
				<?php $this->displayAltNomes(); ?>
			</div>
		</div>
		<div id="parent_sites" style='clear:both;'>
			Sites
			<div id="parent_sites-box" class='places-checkbox-box' >
				<?php $this->displaySites(); ?>
			</div>
		</div>
		<div id="parent_features" style='clear:both;'>
			Features
			<div id="parent_features-box" class='places-checkbox-box' >
				<?php $this->displayFeatures(); ?>
			</div>
		</div>
	</div>
</fieldset>
<!--
<fieldset id='child-places' style="clear:both;">
	<legend>Children</legend>
	<div style="clear:both;">	
		<p><em>Check the places below that are part of this place:</em></p>
		<div id="child-sites" class='quarter_column' >
			Sites
			<div id="child_sites-box" class='places-checkbox-box' >
				<?php ; // $this->displaySites(); ?>	
			</div>			
		</div>
		<div id="child-features" class='quarter_column'>
			Features
			<div id="child_features-box" class='places-checkbox-box' >
				<?php ; // $this->displayFeatures(); ?>	
			</div>			
		</div>
	</div>
</fieldset>
-->

<fieldset id='other-places' style="clear:both;">
	<legend>Other</legend>
	<?php $this->displayOtherPlaces(); ?>
</fieldset>

<fieldset>
	<legend>Comments</legend>
	<p>
		<label for="comments" style="visibility:hidden;">Comments</label>
		<textarea id='comments' name='comments' ><?php echo $this->place_record['comments']; ?></textarea>
	</p>
</fieldset>
<input type='hidden' name='place_id' value='<?php echo $this->place_record['place_id']; ?>' />
<input type='hidden' name='date_added' value='<?php echo $this->place_record['create_datetime']; ?>' />
<input type='hidden' name='date_modified' value='<?php echo $this->place_record['modify_datetime']; ?>' />
<p id="placeform_submit" class="submit" style="width:60%;text-align:right;" ><input type="submit" name="insert_record" value="Update" /></p>
</form>
<p style='clear:both;' ><?php // $this->loadPlaceTree(); ?></p>
<script type='text/javascript'>
	// we want to load the map shape properly
	// is there even a map to display?
	<?php $this->setMarkerMode(); ?>
	showMap();
	showFeatureType();
	loadOtherDescription();
</script>
