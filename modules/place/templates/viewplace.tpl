<!--
	editplace.tpl
	A customized input template for the place module
	
	Dec 22, 2008
	Andrew Tai
	
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/?module=place" >Places table</a>
<h1 class="tablename">View a Place</h1>
<div>
<div style="width:75%;float:left;">
<?php $this->displayPlaceNames(); ?>
<p>
	<label for="old_id">Old Place ID: </label>
	<span><?php echo $this->place_record['old_id']?$this->place_record['old_id']:"<em>no value</em>"; ?></span>
</p>
<p id='other-places' style="clear:both;">
	<strong>Other</strong>
	<?php $this->displayOtherPlaces(); ?>
</p>
<p><strong>Comments</strong></p>
<p id='comments' name='comments' ><?php echo $this->place_record['comments']; ?></p>
</div>
<div style='width:24%;float:left;font-size:0.9em;border:1px dotted gray;'>
	<h5 style='margin-top:0;margin-bottom:0.5em;'>Parents &ndash;&gt; Children</h5>
	<p style='margin-bottom:0;'>
		<?php
			echo $this->place_db->getMainName($this->place_record['place_id']).' <em>('.$this->place_db->getPlaceTypeName($this->place_record['place_type_id']).')</em> has children:<br />';
			$this->displayChildrenPlaces($this->place_record['place_id']);	
		?>
	</p>
	<h5 style='margin-top:0;margin-bottom:0.5em;'>Children &lt;&ndash; Parents</h5>
		<?php
			$this->displayParentPlaces($this->place_record['place_id']);	
			echo '<p style="margin-top:0;">'.$this->place_db->getMainName($this->place_record['place_id']).' <em>('.$this->place_db->getPlaceTypeName($this->place_record['place_type_id']).')</em> has the above parents:</p>';
		?>	
</div>
<div id="geotime-data" style='clear:both;display:none;' >
	<h3>Map It</h3>
	<p><strong>Coordinates (latlong)</strong></p>
	<p id='geodata' name='geodata' ><?php $this->loadGeoData(); ?></p>
	<input type='hidden' id='place_type_id' name='place_type_id' value='<?php echo $this->place->data['place_type_id']; ?>' />
	<input type='hidden' id='latlong_mode' name='geocoord_mode' value='latlong_mode' />
	<input type="hidden" id="geodata_type_id" name="geodata_type_id" value="" />
	<div id='map' style='width:750px;height:420px;' >
		<img src="<?php echo SITE_PATH.'/graphics/missing.gif'; ?>" alt='timemap' /><br />
	</div>
</div>
<div id='time' style='clear:both;padding-top:1em;padding-bottom:1em;'>
	<p><strong>This place&rsquo;s history</strong></p>
	<?php $this->displayGeoTimes(); ?>
	<div style='clear:both;'></div>
</div>
<script type='text/javascript'>
	// we want to load the map shape properly
	// is there even a map to display?
	<?php $this->setMarkerMode(); ?>
	showMap();
	showFeatureType();
	loadOtherDescription();
</script>
