<!--
	input.tpl
	A customized input template for the place module
	
	Nov 4, 2008
	Andrew Tai
	
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/?module=place" >Places table</a>
<h1 class="tablename">Add a Place</h1>
<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=insertRow" id="placeform" class="dataentry">
<p><em>* denotes required field</em></p>
<p>
	<label for="main_name">* Main Name</label>
	<input type="text" id="main_name" name="place_name[]" maxlength="100" />
	<!-- this should be the id for the place_name_type 'main' -->
	<input type="hidden" id="main_name_type" name="place_name_type_id[]" value="1" />
</p>
<p id="placename_0" class="placename_row">
	<label for="name_field_0">&nbsp;&nbsp;Alternate Name</label>
	<input type="text" id="name_field_0" name="place_name[]" maxlength="100" />
	<label for="nametype_field_0" style="display:none;">Type:</label>
	<select id="nametype_field_0" name="place_name_type_id[]" >
		<option value=''>Type</option>
		<?php $this->displayPlaceNameTypes(); ?>
	</select>
	<a class='addbutton' href="javascript:;"><img src="<?php echo SITE_PATH; ?>/graphics/plus.gif" alt="" /></a> <a class='addtext' href="javascript:;">Add</a>	
</p>
<p id="place_type_buffer" style="border-bottom:1px dashed black;width:100%;">&nbsp;</p>
<p>
	<label for="old_id">&nbsp;&nbsp;Old Place ID</label>
	<input type="text" id="old_id" name="old_id" maxlength="20" />
</p>
<p id="place_type_wrapper">
	<label for="place_type_id">* This is a</label>
	<select id="place_type_id" name="place_type_id">
		<?php $this->displayPlaceTypes(); ?>
	</select>
	<label for="feature_type" style="display:none;" >Feature Type:</label>
	<select id="feature_type" name="feature_type_id" style="float:left; display:none;" >
		<option value="">Feature Type</option>
		<?php $this->displayFeatureTypeTree(); ?>		
	</select>
</p>
<fieldset id="geotime-data" style='clear:both;display:none;' >
	<legend>Map It</legend>
	<div id='marker-type' style='width:15em; float:left; font-size:0.8em;' >
		<label for="timemap" >Map It <br />
		<a id="point_mode" class="geomarker_mode" href="javascript:;">Point <img src="http://google.com/mapfiles/ms/t/Bmu.png" alt="" /></a><br />
		<a id="line_mode" class="geomarker_mode" href="javascript:;">Line <img src="http://google.com/mapfiles/ms/t/Blu.png" alt="" /></a><br />
		<a id="polygon_mode" class="geomarker_mode" href="javascript:;">Polygon <img src="http://google.com/mapfiles/ms/t/Bpu.png" alt="" /></a><br />
		</label>
		<input type="hidden" id="geodata_type_id" name="geodata_type_id" value="" />
		<div id='map-tooltip' style='padding-right:0.5em;' >
			<p><strong>Map Instructions</strong></p>
			<p>Click on the map to start creating points.</p>
			<p>When making lines and polygons, dragging on the gray-colored boxes to create new points </p>
			<p>Clicking without dragging on a white-colored point will remove that point.</p>
			<p>To use the geodata textbox for entry: <br />
			Use a one line for one geodata coordinate. Separate latitude and longitude with commas, use a newline (enter key) to separate coordinates.<br />
			Example: <br />
			31, 32 <br />
			31.05, 32.004			
			</p>
		</div>
	</div>
	<div id='map' style='width:750px;height:420px;' >
		<img src="<?php echo SITE_PATH.'/graphics/missing.gif'; ?>" alt='timemap' /><br />
	</div>
	<div id='geomarker-data' style='display:none;width:750px; margin-left:12em;'>
		<label for='geodata'>Geo Data</label>
		<label style='width:25em; font-size:0.8em;height:1.5em;' >This data is in the form: <a id='latlong_mode' href='javascript:;' onclick="mapSwitchCoordMode(this.id);" >Lat, Long</a> | <a id='longlat_mode' href='javascript:;' onclick="mapSwitchCoordMode(this.id);" >Long, Lat</a></label>
		<input type='hidden' name='geocoord_mode' value='latlong_mode' />
		<!--<label><a href="javascript:;" onclick="swapCoordinates();" >Swap Lat/Long data</a></label>-->
		<br />
		<p style='font-size:0.8em;font-style:italic;display:none;'>
			Use a one line for one geodata coordinate. Separate latitude and longitude with commas, use a newline (enter key) to separate coordinates.<br />
			Example: <br />
			31, 32 <br />
			31.05, 32.004
		</p>
		<textarea id='geodata' name='geodata' style='display:none;width:750px;height:100px;overflow:auto;float:left;clear:right;' onchange='mapUpdateFromText();' ></textarea>
	</div>
	</div>
</fieldset>
<fieldset id='time' style='clear:both;'>
	<legend>Time</legend>
	<div id='timerow_0' class="timerow">
		<div class="quarter_column" id="time_type_col" >
			<label for="time_type_0">Time Type</label><br />
			<select id='time_type_0' name='time_type_id[]' >
				<?php $this->displayTimeTypes(); ?>
			</select>
		</div>
		<div class="quarter_column" id="start_time_period_col">
			<label for="start_time_period_id_0">Start</label><br />
			<select id='start_time_period_id_0' name='start_time_period_id[]' >
				<option value=""></option>
				<?php $this->displayTimeTree(); ?>
			</select>
		</div>
		<div class="quarter_column" id="end_time_period_col">
			<label for="End_time_period_id_0">End</label><br />
			<select id='end_time_period_id_0' name='end_time_period_id[]' >
				<option value=""></option>
				<?php $this->displayTimeTree(); ?>
			</select>
		</div>
		<div class="quarter_column">
			<br /><a href="javascript:;"><img src="<?php echo SITE_PATH; ?>/graphics/plus.gif" alt="ADD" /></a>
			<a href="javascript:;">Add</a>
		</div>
	</div>
</fieldset>
<fieldset id='parent_places' style="clear:both;">
	<legend>Parents</legend>
	<div style="clear:both;">	
		<p><em>This place is considered part of: </em></p>
		<p><label for="parents-search">LiveSearch:</label><input type='text' id='parents-search' name='parents-search' /></p>
		<ul id='parents_tabs' class='tabs'>
			<li class='selected'><a href="javascript:;">Regions</a></li>
			<li><a href="javascript:;">Governates</a></li>
			<li><a href="javascript:;">Nomes</a></li>
			<li><a href="javascript:;">AltNomes</a></li>
			<li><a href="javascript:;">Sites</a></li>
			<li><a href="javascript:;">Features</a></li>
		</ul>
		<div id="parent_regions" style='clear:both;'>
			Regions
			<div id="parent_regions-box" class='places-checkbox-box' >
				<?php $this->displayRegions(); ?>
			</div>
		</div>
		<div id="parent_governates" style='clear:both;'>
			Governates
			<div id="parent_governates-box" class='places-checkbox-box' >
				<?php $this->displayGovernates(); ?>
			</div>
		</div>
		<div id="parent_nomes" style='clear:both;'>
			Nome
			<div id="parent_nomes-box">
				<?php $this->displayNomes(); ?>
			</div>
		</div>
		<div id="parent_altnomes" style='clear:both;'>
			<abbrev title="Alternate">Alt.</abbrev> Nomes
			<div id="parent_altnomes-box" class='places-checkbox-box' >
				<?php $this->displayAltNomes(); ?>
			</div>
		</div>
		<div id="parent_sites" style='clear:both;'>
			Sites
			<div id="parent_sites-box" class='places-checkbox-box' >
				<?php $this->displaySites(); ?>
			</div>
		</div>
		<div id="parent_features" style='clear:both;'>
			Features
			<div id="parent_features-box" class='places-checkbox-box' >
				<?php $this->displayFeatures(); ?>
			</div>
		</div>
	</div>
</fieldset>
<fieldset id='other-places' style="clear:both;">
	<legend>Other</legend>
	<div class='other-box' id='other-box_0' style='clear:both;padding-top:0.5em;' >
		<div class='quarter_column' style='' >
			<label for='other_places-select_0'>
			This place also existed as: 
			</label>
			<div id='other-places-box' class='places-select-box' >
				<select id='other_places-select_0' name='other_places[]' class='other_places-select' >
					<option value=''></option>
					<?php $this->displayOtherPlaces(); ?>
				</select>
			</div>
		</div>
		<div class='quarter_column' >
			<p><br /> in the</p>
		</div>
		<div class='quarter_column' >
			<p><br />
			<select id='other-places-pastpresent-toggle_0' name='pastpresent-toggle[]' class='pastpresent-toggle' >
				<option value=''></option>
				<option value='past'>Past</option>
				<option value='future'>Future</option>
			</select></p>
		</div>
		<div class='quarter_column' >
			<p><label for='transition_type_id_0'>What happened?</label><br />
			<select id='transition_type_id_0' name='transition_type_id[]' class='transition_type_id' >
				<option value='1'>It shifted</option>
				<option value='2'>It contracted</option>
				<option value='3'>It expanded</option>
			</select>
			</p>
		</div>
		<div class='quarter_column'><label for='other-descriptions_0'>Is this what you meant? <em>(if not, try to fix it by adjusting the values)</em></label></div>
		<div class='quarter_column'><p id='other-descriptions_0'></p></div>
		<div class='quarter_column'><a id='newotherbox' href='javascript:;'><img src='/graphics/plus.gif' alt=''> Add another relation?</a><div>
	</div>
</fieldset>
<fieldset>
	<legend>Comments</legend>
	<p>
		<label for="comments" style="visibility:hidden;">Comments</label>
		<textarea id='comments' name='comments' ></textarea>
	</p>
</fieldset>

<p id="placeform_submit" class="submit" style="width:60%;text-align:right;" ><input type="submit" name="insert_record" value="Add" /></p>
</form>
