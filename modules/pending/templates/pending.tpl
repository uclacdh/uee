	<h1>User Registration</h1>
	<p>Congratulations on successfully registering a user account. In order to begin using the UEE website, you must <a href="/?module=front&class=profile&event=edit">create a profile</a>. Additional permissions may be granted to you by an administrator once your profile has been created.</p>
