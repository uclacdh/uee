<?php
	// contains the list of objects we'll need to refer to to talk to the Library's soap service
	class Article {
		public $id;
		public $thumbnailURL;
		public $title;
		public $xmlURL;
		
		public function __construct($id = '', $thumbnailURL = '', $title = '', $xmlURL = '') {
			$this->id = $id;
			$this->thumbnailURL = $thumbnailURL;
			$this->title = $title;
			$this->xmlURL = $xmlURL;
		}
	}
	
	class getMetadata {
		public $ark;
		
		public function __construct($ark = '') {
			$this->ark = $ark;
		}	
	}

	class listAllImages {
		public $sortAscending;
		
		public function __construct($sortAscending = false) {
			$this->sortAscending = $sortAscending;
		}
	}
	
	class findPlace {
		public $pid;
		
		public function __construct($pid = '') {
			$this->pid = $pid;
		}
	}

	class findGloss {
		public $gid;
		
		public function __construct($gid = '') {
			$this->gid = $gid;
		}
	}

	class findBibliography {
		public $bid;
		
		public function __construct($bid = '') {
			$this->bid = $bid;
		}
	}

	class findInGloss {
		public $glossId;
		public $keyword;
		
		public function __construct($glossId = '', $keyword = '') {
			$this->glossId = $glossId;
			$this->keyword = $keyword;
		}
	}
	
	class findInBibliography {
		public $bibliographyId;
		public $keyword;
		
		public function __construct($bibliographyId = '', $keyword = '') {
			$this->bibliographyId = $bibliographyId;
			$this->keyword = $keyword;
		}
	}
	
	class findInText {
		public $keywords;
		
		public function __construct($keywords = '') {
			$this->keywords = $keywords;
		}
	}
	
	class searchImage {
		public $keyword;
		
		public function __construct($keyword = '') {
			$this->keyword = $keyword;
		}
	}
	
	class searchArticle {
		public $keyword;
		
		public function __construct($keyword = '') {
			$this->keyword = $keyword;
		}
	}
	
	class setArticle {
		public $article;
		
		public function __construct($article = null) {
			$this->article = $article;
		}
	}
	
	class getContentFileId {
		public $ark;
		
		public function __construct($ark = '') {
			$this->ark = $ark;
		}
	}

	class listAltTitles {
		public $ark;
		
		public function __construct($ark = '') {
			$this->ark = $ark;
		}
	}
	
	class repeat {
		public $text;
		
		public function __construct($text = '') {
			$this->text = $text;
		}
	}
?>