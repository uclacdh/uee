<?php 

	/*
	editgloss.php
	Contains a variant input module for updating gloss records
	
	Oct 28, 2008 
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class editgloss extends MVC_Authentication_Edit {

	public $gloss;
	public $glossnames;
	public $gloss_db;
	public $gloss_record;
	public $glossname_db;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
		if (!$_POST['row_id'] and !$id)
			die("error: no gloss to update");
		$id = $_POST['row_id'];
		$this->gloss_db = new MVC_Model_Gloss(); // assign the model
		$matches = $this->gloss_db->dbSearchTable(array('gloss_id'=>$id));
		$this->gloss_record = $matches[0];
		$this->glossname_db = new MVC_Model_GlossName(null);
		$this->glossnames = $this->glossname_db->getAllRows();
	}

	public function __destruct() {
		parent::__destruct();
	}

	public function listGlossNames($gloss_id='')
	{
		if (!$gloss_id and !($this->gloss_record['gloss_id']))
			die("error: no gloss_id");
		if (!$gloss_id)
			$gloss_id = $this->gloss_record['gloss_id'];
		$names = array();
		$count = 0;
		$glossnames = $this->glossname_db->dbSearchTable(array('gloss_id'=>$gloss_id));
		if (!$glossnames)
		{
			echo "<p id='glossname_{$count}' class='glossname_row'>";
			$names[] = $glossname['gloss_name'];
			echo "<label for='name_field_{$count}'>Name</label>";
			echo "<input type='text' id='name_field_{$count}' name='gloss_name[]' maxlength='100' value='{$glossname['gloss_name']}' />";
			echo "<input type='hidden' name='gloss_name_id[]' value='{$glossname['gloss_name_id']}' />";
			echo "<a href='javascript:;'><img src='".SITE_PATH."/graphics/plus.gif' alt='' /></a> <a href='javascript:;'>More</a>";
			echo "</p>";		
		}
		foreach ($glossnames as $glossname)
		{
			echo "<p id='glossname_{$count}' class='glossname_row'>";
			$names[] = $glossname['gloss_name'];
			echo "<label for='name_field_{$count}'>Name</label>";
			echo "<input type='text' id='name_field_{$count}' name='gloss_name[]' maxlength='100' value='{$glossname['gloss_name']}' />";
			echo "<input type='hidden' name='gloss_name_id[]' value='{$glossname['gloss_name_id']}' />";
			if ($count == 0)
			{
				echo "<a href='javascript:;'><img src='".SITE_PATH."/graphics/plus.gif' alt='' /></a> <a href='javascript:;'>Add</a>";
			}
			echo "</p>";
			$count++;
		}
		return true;
	}
	
}
?>
