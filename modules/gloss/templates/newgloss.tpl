<!--
	newgloss.tpl
	An input template for the gloss module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Add a Gloss</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=insertRow" id="glossform" class="dataentry">

<p id="glossname_0" class="glossname_row">
	<label for="name_field_0">Name</label>
	<input type="text" id="name_field_0" name="gloss_name[]" maxlength="100" />
	<a href="javascript:;"><img src="<?php echo SITE_PATH; ?>/graphics/plus.gif" alt="" /></a> <a href="javascript:;">More</a>
</p>
<p id="definition_p" >
	<label for="definition_field">Definition</label>
	<textarea id="definition_field" name="definition"></textarea>
</p>
<p id="old_id_p" >
	<label for="old_id">Old ID</label>
	<input type="text" id="old_id" name="old_id" maxlength="10" />
</p>

<p id="glossform_submit" class="submit" ><input type="submit" name="insert_record" value="Add" /></p>

</form>

