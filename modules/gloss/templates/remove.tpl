<!--
	remove.tpl
	An input template for the gloss module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Confirm Deletion</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=deleteRow" id="glossform" class="dataentry">

<p>Please confirm you want to delete this record</p>
<?php
	foreach ($this->gloss_record as $fieldname=>$val)
		echo "<p>{$fieldname}: $val</p>";
?>

<input type="hidden" name="gloss_id" value="<?php echo $this->gloss_record['gloss_id']; ?>" />
<p id="glossform_submit" class="submit" ><input type="submit" name="insert_record" value="Delete" /></p>
</form>

