<!--
	newgloss.tpl
	An input template for the gloss module
		
 -->
<!-- link back to home -->
<a class='backlink' href="<?php echo SITE_PATH; ?>/">&lt;&lt; Public Home</a>
&nbsp;&nbsp;&nbsp;&nbsp;<a class='backlink' href="<?php echo SITE_PATH; ?>/admin/">&lt;&lt; Admin Home</a>

<h1 class="tablename">Update a Gloss</h1>

<form method="POST" action="<?php echo SITE_PATH; ?>/?module=<?php echo $this->moduleName; ?>&class=input&event=updateRow" id="glossform" class="dataentry">
<p><em>* blank names = will be removed</em></p>
<?php
	$this->listGlossNames();
?>
<p id="definition_p" >
	<label for="definition_field">Definition</label>
	<textarea id="definition_field" name="definition"><?php echo $this->gloss_record['definition']; ?></textarea>
</p>
<p id="old_id_p" >
	<label for="old_id">Old ID</label>
	<input type="text" id="old_id" name="old_id" maxlength="10" value="<?php echo $this->gloss_record['old_id']; ?>" />
</p>

<input type="hidden" name="gloss_id" value="<?php echo $this->gloss_record['gloss_id']; ?>" />
<p id="glossform_submit" class="submit" ><input type="submit" name="update_record" value="Update" /></p>

</form>

