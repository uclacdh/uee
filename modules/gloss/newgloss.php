<?php 

	/*
	newgloss.php
	Contains a variant input module for creating new gloss records
	
	Oct 28, 2008 
	Andrew Tai
	
	Usage: actual module for this application
	The default authentication for input: LoggedIn
	*/

class newgloss extends MVC_Authentication_Edit {

	public $gloss;
	// You might need to add some other fields here
	// For data input (insert/edit), are there fields we want to ignore? Leave the fieldnames here
	public $ignore_fields = array("create_datetime","modify_datetime");
	
	public function __construct($id=null) {
		parent::__construct();
		$this->gloss = new MVC_Model_Gloss($id); // assign the model
	}

	public function __destruct() {
		parent::__destruct();
	}
	
}
?>
