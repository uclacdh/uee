<?php 

	/*
	logout.php

	Allows users to log in for pages which require authentication
	Some other insightful comment here.
	*/

class logout extends MVC_Authentication_None {
	
	public $ueeUser;		//need funtionality to look up users / compare login info for validation and authentication
	
	public function __construct() {
		parent::__construct();
		$this->ueeUser = new MVC_Model_UeeUser(); // assign the model
		// check if we can acknowledge this is a logout?
	}

	public function __destruct() {
		parent::__destruct();
	}
	
	public function logout() {
		// $sess = new MVC_Session2();
		// $sess->clear();
		foreach ($_SESSION as $key =>$val)
			$_SESSION[$key] = null;
		session_write_close();
		session_unset();
		session_write_close();
		// print_r($sess);
		// echo '<br />';
	}
	
	public function authenticate_login() {
		
		foreach ($_POST as $fieldname=>$data)
		{
			if (in_array($fieldname,$this->ueeUser->fields) and !empty($data))
				$this->ueeUser->data[$fieldname] = $data;
			else if (in_array($fieldname, $this->ueeUser->fields))
				$this->ueeUser->data[$fieldname] = NULL;
		}
		// print_r($_POST);
		// echo '<br />';
		
		if($this->ueeUser->validateData())
		{
			
			$this->ueeUser->search['username'] = $this->ueeUser->data['username'];
			$this->ueeUser->search['password'] = md5($this->ueeUser->data['password']);
			$this->ueeUser->dbSearchTable();
			
			if( !empty($this->ueeUser->search_results))
			{
				//check if more than one result was given somehow?
				// echo 'Login Successful! <br/>';
				// echo $this->ueeUser->search_results[0]['user_id'] . '<br/>';
				//Set some "logged in" variable here.
				$_SESSION['user_id'] = $this->ueeUser->search_results[0]['user_id'];
				session_write_close(); // make sure this gets written! (some weird errors w/o it)
				// can we be smarter about this?
				// $refererUrl = $_POST['ref'];
				// if (substr_count($refererUrl,SITE_PATH) > 0 and (substr_count($refererUrl,'login') < 1) )
					// header('Location: '.$refererUrl);
				if ($_SESSION['target'])
				{
					$target = $_SESSION['target'];
					unset($_SESSION['target']); // clear this;
					session_write_close(); // make sure this gets written! (some weird errors w/o it)	
					header('Location: '.SITE_PATH.$target);
				}
				else if (substr_count($refererUrl,SITE_PATH) > 0 and (substr_count($refererUrl,'login') < 1) )
					header('Location: '.$refererUrl);
				else if ($_SESSION['user_id'] == 1)
					header('Location: '.SITE_PATH.'/admin/');
				else
					header('Location: '.SITE_PATH.'/welcome/');
				exit();
			}
			else
				echo 'Incorrect Login Info';
		}
		else
			echo 'Invalid data entered';
	}
}
?>
