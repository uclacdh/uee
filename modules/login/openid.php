<?php 

	/*
	login.php

	Allows users to log in for pages which require authentication
	Some other insightful comment here.
	*/

class openid extends MVC_Authentication_None {
	
	public $ueeUser;		//need funtionality to look up users / compare login info for validation and authentication
	private $has_profile;
	private $CLIENT_ID = GOOGLE_OPENID_CLIENT_ID;
	private $CLIENT_SECRET = GOOGLE_OPENID_CLIENT_SECRET;

	public function __construct() {
		parent::__construct();
		// $this->ueeUser = new MVC_Model_UeeUser(); // assign the model
	}

	public function __destruct() {
		parent::__destruct();
	}

	public function generate_session_token(){
		// Create a state token to prevent request forgery.
		// Store it in the session for later validation.
		$state = md5(rand());
		$_SESSION['google_openid_state'] = $state;
		session_write_close(); // make sure this gets written! (some weird errors w/o it)		
		return $state;
	}


	public function login(){
		$openid = new LightOpenID;

        if(isset($_POST['openid_identifier'])) {
			if ($_POST['submit'] == "google") {
				$this->google2();
			} else {
				$openid->identity = $_POST['openid_identifier'];
				header('Location: ' . $openid->authUrl());
			}
			exit();
        }

        $state = $_GET['state'];
		$code = $_GET['code'];

		// Get the open id identity
		if (isset($state) and $state == $_SESSION['google_openid_state']){
			// If the open_id provider is Google, 
			$providerUrl = 'https://www.googleapis.com/oauth2/v3/token';
			$params = array(
				'code' => $code,
				'client_id' => $this->CLIENT_ID,
				'client_secret' => $this->CLIENT_SECRET,
				'redirect_uri' => 'https://'.$_SERVER['SERVER_NAME'].'/login/openid/authenticate',
				'grant_type' => 'authorization_code',
			);
			$ch = curl_init();
    		curl_setopt($ch, CURLOPT_URL, $providerUrl);
    		curl_setopt($ch, CURLOPT_POST, TRUE);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    		curl_setopt($ch, CURLOPT_HTTPHEADER, [
    		    "Content-type: application/x-www-form-urlencoded",
    		]);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		    $result = curl_exec($ch);
		    curl_close($ch);
		
			$data = json_decode($result, TRUE);
			// Parse the id_token with JWT
			require_once('JWT.php');
			$id_token = JWT::decode($data['id_token'], null, false);
			$openid->identity = $id_token->sub;
		} else {
			// if the open_id provider is not Google
			echo $_SERVER['REQUEST_URI'];
			$params = explode("?",$_SERVER['REQUEST_URI']);
			$params2 = explode("&",$params[1]);

			foreach ($params2 as $k => $p) {
				if (substr_count($p,'openid.identity') > 0) {
					$i = explode("=",$p);
					$identity = urldecode($i[1]);
				}
			}
			var_dump($params2);
			$openid->identity = urldecode($identity);
		}
		
		// We can now perform login or signup based on $openid->identity
		

		try {
            $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		} catch (mysqli_sql_exception $e) {
            var_dump($e);
            exit;
		} 
		// try creating a record in the openID table
		$stmt = $db->prepare("SELECT uee_user_id, completed_profile, keyphrase FROM open_id WHERE identity=?") or die("Error preparing statement");
		$stmt->bind_param("s",$openid->identity);
		$stmt->execute() or die("Error executing select statement");
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$has_profile = 0;
		if ($num_rows == 0) {
			$stmt->close();
			; // echo "Creating new OpenID record in local DB <br />";
			// create a new record
			// 7/28/2011 - By default, users are auto-approved for read-only
			$stmt2 = $db->prepare("INSERT INTO open_id (identity, uee_user_id, pending, date_added, date_updated) VALUES(?, NULL, 1, NOW(), NOW())");
			$stmt2->bind_param("s", $openid->identity);
			$stmt2->execute() or die("Error executing insert statement");
			$stmt2->close();
			; // echo "Assigning UEE user id <br />";
			$stmt2 = $db->prepare("INSERT INTO uee_user (user_id, password) VALUES(NULL,?)");
			$stmt2->bind_param("s",$openid->identity);
			$stmt2->execute() or die("Error generating uee user id");
			$stmt2->close();
			$stmt2 = $db->prepare("SELECT LAST_INSERT_ID()");
			$stmt2->execute();
			$stmt2->bind_result($uee_id);
			while ($stmt2->fetch()) {
				; // echo "attaching uee user id {$uee_id} <br />";
			}
			$stmt2->close();
			$stmt2 = $db->prepare("UPDATE open_id SET uee_user_id=(SELECT LAST_INSERT_ID()) WHERE identity=?");
			$stmt2->bind_param("s",$openid->identity);
			$stmt2->execute();
			$stmt2->close();
			$has_profile = 0;
		}
		else {		
			; // echo "OpenID record already exists <br />";
			$stmt->bind_result($uee_id, $has_profile, $keyphrase);
			while ($stmt->fetch()) {
				; // echo "found attached id {$uee_id} <br />";
			}
			$stmt->close();
		}
		$this->has_profile = $has_profile;

		$_SESSION['user_id'] = $uee_id;
		// todo - write  a session keyphrase that should be unique to this user
		// hash the keyphrase against the identity?
		$salted_str = UEE_SALT.$keyphrase.$uee_id.UEE_SALT;
		$hash_str = md5($salted_str);
		$_SESSION['uee_session_id'] = $hash_str;
		
		session_write_close(); // make sure this gets written! (some weird errors w/o it)		
		$this->authenticate_login();
	}

	// test a fast connection to google w/o lightopenid
	public function google() {
		// discovery
		// acquire the google OpenID endpoint

/*
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/accounts/o8/id');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(	
				"Accept: application/xrds+xml"
			)
		);
		$output = curl_exec($ch);
		print_r($output);
*/

		// endopint address = https://www.google.com/accounts/o8/ud
		// connect to the endpoint by making a http post request
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/accounts/o8/ud');
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		// curl_setopt($ch, FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array(
			'openid.mode' => 'checkid_setup',
			'openid.ns' => 'http://specs.openid.net/auth/2.0',
			'openid.return_to' => 'http://'.$_SERVER['SERVER_NAME'].'/?module=login&class=openid&event=login',
			'openid.claimed_id' => 'http://specs.openid.net/auth/2.0/identifier_select',
			'openid.identity' => 'http://specs.openid.net/auth/2.0/identifier_select',
			'openid.ns.pape' => 'http://specs.openid.net/extensions/pape/1.0',
			'openid.ns.us' => 'http://specs.openid.net/extensions/pape/1.0',
			'openid.ns.ax' => 'http://openid.net/srv/ax/1.0',
			'openid.ax.mode' => 'fetch_request',
			'openid.ax.required' => ''
			)
		);
		$output = curl_exec($ch);
		echo $output;

		curl_close($ch);
		die();
	}

	public function google2() {
        // direct immediately using a URL pattern.
		// parameter information
		// * providerUrl - the authentication provider's url. This ideally should be found by performing an initial openid discovery query 		
		// * openid.return_to - once the user authenticates, the URL on our web application where the user should be sent back to
		// * openid.realm - the server name of the application making the request. Must match against the openid.return_to parameter's server name
		// * openid.mode - 'checkid_setup' or 'checkid_immediate'; usually provides the user a way to interact with the authentication service; in general choose 'checkid_setup'

		// TODO - this may require updating every now and then... (in particular, the providerUrl)
		$providerUrl = 'https://accounts.google.com/o/oauth2/auth';
		$params = array(
			'client_id' => $this->CLIENT_ID,
			'response_type' => 'code',
			'scope' => 'openid email',
			'redirect_uri' => 'https://'.$_SERVER['SERVER_NAME'].'/login/openid/authenticate',
			'state' => $this->generate_session_token(),
			'openid.realm' => 'https://'.$_SERVER['SERVER_NAME'].'/'
		);
		// construct the request URL to the authentication provider
		$paramStrArr = array();
		foreach ($params as $k => $v) {
			$paramStrArr[] = "{$k}={$v}";
		}
		
		$paramStr = implode('&',$paramStrArr);
		$url = $providerUrl . '?' . $paramStr;
		
		header('Location: '.$url);
		exit();
	}
	
	public function logout() {
		// $sess = new MVC_Session2();
		// $sess->clear();
		foreach ($_SESSION as $key =>$val)
			$_SESSION[$key] = null;
		session_write_close();
		session_unset();
		session_write_close();
		// print_r($sess);
		// echo '<br />';
	}
	
	private function authenticate_login() {
		if( $_SESSION['user_id'])
		{
			// first check if this user has a profile
			if (!$this->has_profile) {
				header('Location: '.SITE_PATH.'/?module=front&class=profile&event=edit');
				exit();
			}
			if ($_SESSION['target'])
			{
				$target = $_SESSION['target'];
				unset($_SESSION['target']); // clear this;
				session_write_close(); // make sure this gets written! (some weird errors w/o it)	
				header('Location: '.SITE_PATH.$target);
			}
			else if (substr_count($refererUrl,SITE_PATH) > 0 and (substr_count($refererUrl,'login') < 1) )
				header('Location: '.$refererUrl);
			else if ($_SESSION['user_id'] == 1)
				header('Location: '.SITE_PATH.'/admin/');
			else
				header('Location: '.SITE_PATH.'/welcome/');
			exit();
		}
		else
			die("Error: You should be logged in but something bad happened");
	}
}
?>
