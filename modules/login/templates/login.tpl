<html>

<head>
	<title>Default Page: Something</title>
</head>


<!--
	login.tpl
	A generic template for the login module

	Check if user is already logged in?
	Redirect to where once logged in?
	-->
 
<body>
<h1>Log in</h1>
<?php
/*
	// leave for debugging
<p>coming from: <?php echo $_SERVER["HTTP_REFERER"]; ?> <br />
going to: <?php echo $_SESSION['target']; ?></p>
*/
?>
 
<form method="POST" action="/?module=<?php echo $this->moduleName; ?>&event=authenticate_login">

	<fieldset id='data-fields' >
		<legend>Log In</legend>
		<p><label for='username_label'>Username</label><input type='text' id='username' name='username' value='' /></p>
		<p><label for='password_label'>Password</label><input type='password' id='password' name='password' value=''/></p>
	</fieldset>
	<input type="submit" name="login" value="Log In" />
	<input type="hidden" name="ref" value="<?php echo $_SERVER['HTTP_REFERER'];?>" />

</body>
</html>
