<h1>Log Out</h1>

<p>You have successfully logged out of the UEE application.</p>

<p><strong>Warning:</strong> For users who logged in using Google, you are still logged into Google. You may log out of Google by visiting the <a href="http://www.google.com/">Google homepage</a> and logging out.</p>

