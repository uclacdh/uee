<!--
	openid.tpl
	A generic template for the login module

	Check if user is already logged in?
	Redirect to where once logged in?
	-->
<form action="?module=login&class=openid&event=login" method="post">
	<fieldset>
		<legend>Sign in</legend>
		<p>You may sign in with any OpenID account. There is a separate button for <a href="#googlebutton">sign in using a Google Account</a>.</p>
		<p>OpenID: <input type="text" name="openid_identifier" /> <button name="submit" value="submit">Submit</button></p>
		<p><strong>OR:</strong>
		<a name="googlebutton"></a>
		<p><button name="submit" value="google">Use a Google Account</button></p>
	</fieldset>
</form>
