<?php
	// pending.php
	// a generic module to handle when a user is registered but not approved for access yet
	// Andrew Tai
	// October 29, 2008
	
	class permission extends MVC_Authentication_NotApproved {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function __destruct() {
			parent::__construct();
		}
		
	}
?>