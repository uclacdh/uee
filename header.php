<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */

/**
 * This is a "Docblock Comment," also known as a "docblock." The class'
 * docblock, below, contains a complete description of how to write these.
 */


/** header.php
 * October 20, 2008
 * a generic header file for UEE application
 * this header is automatically included by the controller, index.php
 * so any logic / behaviors in here should be application wide
 */

/**
 * NOTE: If the header changes, please check that the changes are applied to
 * the wordpress ueetheme, as desired
 */


// TODO 6/27/2011 - This code could be more readable
?><!-- HTML 5 -->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US" >
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <?php if ($className=='error') : ?>
    <meta name="robots" content="noindex" />
    <?php endif; ?>

    <meta name="google-signin-client_id" content="858202788374-5ju6spiumtm8ei73c9h6ldar30rh5thu.apps.googleusercontent.com">
    <title><?php echo ucwords($moduleName); ?>: UCLA Encyclopedia of Egyptology</title>
    <!-- base.css is a base layer of css that should be applied to all webpages -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/modules/front/css/common.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/css/topnav.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/css/footer.css" />
    <?php if (in_array($moduleName, array('category','endnote','gloss','place','refcheck','timeperiod','users')) ): ?>
    <!-- main.css is meant for the UEE admin data entry -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/css/admin.css" />
    <?php endif; ?>
    <link rel="icon" type="image/gif" href="<?php echo SITE_PATH; ?>/icon3.gif" />
    <?php
        /**
         * let's provide a way for pages to provide their own specialized css
         * for their page?
         */
        $paths = array();
        $paths[] = '/css/'.$className.'.css';
        $paths[] = '/modules/'.$moduleName.'/css/'.$className.'.css';
        foreach ($paths as $path) {
            $filepath = $_SERVER['DOCUMENT_ROOT'].$path;
            if (file_exists($filepath)) {
				echo '<link rel="stylesheet" type="text/css" media="screen" href="'.$path.'" />'."\n";
            }
        }
    ?>
    <script type="text/javascript" src="/js/jquery-1.3.1.min.js" ></script>

    <?php if ($moduleName == "login" and $className == "openid") { ?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITE_PATH; ?>/css/openid.css" />
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/openid-jquery.js" ></script>
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>/js/openid-en.js" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            openid.init('openid_identifier');
			openid.setDemoMode(false); //Stops form submission for client javascript-only test purposes
        });
    </script>
<?php
    } // end if
?>
    <!-- basic javascript for all pages -->
    <script type="text/javascript" src="/js/base.js" ></script>
    <?php if ($moduleName == 'front') : ?>
    <!-- javascript for public pages -->
    <script type="text/javascript" src="/js/public.js" ></script>
    <?php endif; ?>
    <script type="text/javascript" src="/js/jquery.dataTables.js" ></script>
    <?php if ($moduleName == 'front' and (substr_count($className,'timemap') > 0) ) : ?>
    <link rel="stylesheet" href="/css/ui.all.css" type="text/css">
    <?php endif; ?>
    <?php
        if ($className != 'newplace2' && $className != 'editplace' && $className != 'viewplace' ):
	?>
    <script type="text/javascript" src="/js/js.js"></script>
    <?php else:  ?>
    <script type="text/javascript" src="/js/places-constants.js"></script>
    <script type="text/javascript" src="/js/js2.js"></script>
    <?php endif; ?>
    <?php
        // are we doing maps work?
        if ($className == 'newplace2' || $className == 'editplace' || $className == 'viewplace' || (substr_count($className,'timemap') > 0) || $className == 'article2' || $className == 'article3' || $className == 'article5'):
	?>
    <!-- google maps v3: We need to add the key later during production -->
    <?php
        if (USE_GMAP_API_KEY):
    ?>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=<?php echo GMAP_API_KEY ?>&sensor=false"></script>
    <?php else:  ?>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
    <?php endif; ?>
    <!-- marker clusterer -->
    <script type="text/javascript" src="/js/markerclusterer.js"></script>
    <script type="text/javascript" src="/js/gmaps2.js"></script>
    <?php endif; ?>
</head>
<!-- if this page has a googleMaps, perform a mapInit() on page load -->
    <?php
        if ($className == 'newplace2' || $className == 'editplace' || $className == 'viewplace' || (substr_count($className,'timemap') > 0) || $className == 'article2' || $className == 'article3' || $className == 'article5'):
    ?>
<!--<body onload="mapInit()">-->
<body>
    <?php else:  ?>
<body>
    <?php endif; ?>

<?php if (GOOGLE_ANALYTICS) include('analytics.php'); ?>

<div id='wrapper'>
<div id='head'>
	<header id="header" rel="banner">
	    <div id='banner-wrapper'>
	    <!-- display the banner -->
	    	<div id="logo">
		        <?php if (in_array($moduleName, array('front', 'login', 'notfound', 'pending', 'permission') ) ): ?>
		            <?php if (SITE_PATH == SHADOW): ?>
		        	    <a style='' href="<?php echo SITE_PATH; ?>/welcome/">
				        <img id='banner' src='<?php echo SITE_PATH; ?>/graphics/uee-tmm-shadow.jpg' alt='UCLA Encyclopedia of Egyptology Time-Map Mashup - Use Firefox' title='UCLA Encyclopedia of Egyptology' />
		            <?php else: ?>
		        	    <a style='' href="/">
				        <img id='banner' src='<?php echo SITE_PATH; ?>/graphics/UEE_BannerRev_Beta_new.jpg' alt='UCLA Encyclopedia of Egyptology' title='UCLA Encyclopedia of Egyptology' />

		            <?php endif; ?>
		        <?php else: ?>
		            <a style='border:none;padding:0;margin:0;background-color:black;text-decoration:none;' href="/">
		            <img id='banner' src='<?php echo SITE_PATH; ?>/graphics/uee-admin-logo.jpg' alt='UCLA Encyclopedia of Egyptology' title='UCLA Encyclopedia of Egyptology' />
		        <?php endif; ?>
		        </a>
		  </div>
	    </div>
	</header>

<?php
    if ($moduleName == 'front' or ($moduleName == 'login' and $className == 'logout' ) )
        require_once('public.nav.php');
?>
</div>
<div id='content'>
<?php if (SITE_PATH == STAGE): ?>
<p><strong>Note: You are working on the STAGE website.</strong></p>
<?php endif; ?>
<!-- do we need to display any development related notes to the public? -->
<?php if(false and WORK_IN_PROGRESS) echo '<p style="color:red;font-size:0.8em;clear:both;"><em>'.WORK_NOTES.'</em></p>'; ?>



