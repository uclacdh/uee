<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * footer.php
 *
 * October 20, 2008
 * adtai@ats.ucla.edu (Andrew Tai) 
 * Generic footer file for the UEE application
 * Conditionally included by index.php as necessary
 *
 * PHP 5.3
 *
 * LICENSE: TODO
 *
 * @category CategoryName
 * @package
 * @author
 * @copyright
 * @license
 * @link
 * @version
 * @link
 * @see
 * @since
 * @deprecated
 */
?>

</div>		
<footer id="footer" class="source-org vcard copyright" role="contentinfo">
      <div id="footer-wrapper">
        <div class="col-1">
          <p>This project is made possible
            through generous support from the</p>
          <p>
                <img src="/graphics/NEFH_logo.png" alt="National Endowment for the Humanities" /></p>
        </div>
       <aside id="sidebar">

    <div id="nav_menu-2" class="widget widget_nav_menu"><h3 class="widget-title">Editorial Information</h3><div class="menu-editorial-information-container"><ul id="menu-editorial-information" class="menu"><li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a href="/about/">About the UEE</a></li>
<li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-49"><a href="/editors/">Editors and Staff</a>
<ul class="sub-menu">
	<li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="/authors/">The Authors</a></li>
</ul>
</li>
<li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50"><a href="/citing/">Citing the UEE</a></li>
</ul></div></div><div id="nav_menu-3" class="widget widget_nav_menu"><h3 class="widget-title">Information for Authors</h3><div class="menu-information-for-authors-container"><ul id="menu-information-for-authors" class="menu"><li id="menu-item-99" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-99"><a href="/style/">Submission and Style Guide</a></li>
<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51"><a href="/spelling/">Preferred Spelling</a></li>
<li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52"><a href="/chronology/">Preffered Chronology</a></li>
</ul></div></div><div id="nav_menu-4" class="widget widget_nav_menu"><h3 class="widget-title">Search the UEE</h3><div class="menu-search-the-uee-container"><ul id="menu-search-the-uee" class="menu"><li id="menu-item-126" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-126"><a href="https://uee.cdh.ucla.edu/subjectbrowse/">Subject Browse</a></li>
<li id="menu-item-127" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-127"><a href="https://uee.cdh.ucla.edu/titleindex">Title Index</a></li>
<li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128"><a href="https://uee.cdh.ucla.edu/timemap">Time Map</a></li>
</ul></div></div><div id="nav_menu-5" class="widget widget_nav_menu"><h3 class="widget-title">Related Links</h3><div class="menu-related-links-container"><ul id="menu-related-links" class="menu"><li id="menu-item-53" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-53"><a href="http://ucla.edu">UCLA</a></li>
<li id="menu-item-54" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-54"><a href="http://escholarship.org/uc/nelc_uee">UEE eScholarship Repository</a></li>
<li id="menu-item-55" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-55"><a href="http://dai.aegaron.ucla.edu/">AEGARON</a></li>
<li id="menu-item-56" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-56"><a href="http://dlib.etc.ucla.edu/projects/Karnak/">Digital Karnak</a></li>
</ul></div></div>
</aside>			 <!--//
       <small>&copy;2017 UCLA Encyclopedia of Egyptology</small>
       //-->
       <div class="clearfix"></div>
      </div>
		</footer>

	</div>

